import gfx.controls.Button;
import gfx.utils.Delegate;

class gfx.controls.SQUAD.ShootingSimulator {
	private var DEBUG_LEVEL : 			Number = 0;
	private var VARIETY_ENABLE : 	Boolean = true;
	
	public function ShootingSimulator (
		mcTarget: 		MovieClip, 		
		btnShoot:			Button,
		bulletIdentifier: 	Array, 
		accurate :	 		Number, 
		maxAccurate :	Number, 
		recoil : 				Number, 
		maxRecoil :		Number, 
		speed:				Number, 
		maxSpeed:		Number, 
		bulletsPerShot:	Number) 
	{ 
		m_mcTarget			= mcTarget;
		m_btnShoot			= btnShoot;
		m_aBulletIdentifiers= bulletIdentifier;
		m_iAccurate			= accurate;
		m_iMaxAccurate 	= maxAccurate;
		m_iRecoil				= recoil;
		m_iMaxRecoil		= maxRecoil;
		m_iSpeed				= speed;
		m_iMaxSpeed		= maxSpeed;
		m_iBulletsPerShot	= bulletsPerShot;
		
		m_sString = "----------------------------------SHOOTER";		
		m_sString += "\nTarget 				= " + m_mcTarget;
		m_sString += "\nShoot Button	= " + m_btnShoot;
		m_sString += "\nBullets				= " + m_aBulletIdentifiers;
		m_sString += "\nAccurate 			= " + m_iAccurate;
		m_sString += "\nMax Accurate	= " + m_iMaxAccurate;
		m_sString += "\nRecoil				= " + m_iRecoil;
		m_sString += "\nMax Recoil		= " + m_iMaxRecoil;
		m_sString += "\nSpeed				= " + m_iSpeed;
		m_sString += "\nMax Speed		= " + m_iMaxSpeed;
		m_sString += "\nBullets / Shot	= " + m_iBulletsPerShot;
		
		btnShoot.addEventListener ("click", this, "simulate");
		
		clearTarget ();
		preCaculateSize ();		
	}
	
	//Editable configuration
	public var NUMBER_OF_SHOTS: Number 					= 10;
	public var MIN_TIME_PER_SHOOT : Number 				= 30;
	public var MAX_TIME_PER_SHOOT : Number 				= 1000;
	public var MAX_OPEN_ANGLE_FROM_90_D : Number	= 90; //Total angle = 90 +/- MAX_OPEN_ANGLE_FROM_90_D
	public var stop : Boolean											= false;
	
	//Contants
	private var FACTOR_DEGREE_TO_RADIAN : Number = Math.PI / 180;
	
	//User-defined variables
	private var m_mcTarget : 			MovieClip;
	private var m_btnShoot : 			Button;
	private var m_aBulletIdentifiers : 	Array;
	private var m_iAccurate:				Number;
	private var m_iMaxAccurate:		Number;
	private var m_iRecoil:					Number;
	private var m_iMaxRecoil:			Number;
	private var m_iSpeed:					Number;
	private var m_iMaxSpeed:			Number;
	private var m_iBulletsPerShot:		Number;
		
	//Private variables
	private var m_iTargetWidth : 			Number;
	private var m_iTargetHeight : 			Number;
	private var m_iTargetBullEyeX : 		Number;
	private var m_iTargetBullEyeY : 		Number;
	private var m_iTurn:						Number;
	private var m_iAngle :						Number;
	private var m_iTimePerShot:			Number;
	private var m_sString:						String;
	
	private function clearTarget () : Void {
		for (var x : String in m_mcTarget) {
			if (m_mcTarget[x] instanceof MovieClip) {
				m_mcTarget[x].removeMovieClip ();
			}
		}
	}
	
	private function preCaculateSize () : Void {
		m_iTargetWidth 		= m_mcTarget._width;
		m_iTargetHeight		= m_mcTarget._height;
		m_iTargetBullEyeX		= m_iTargetWidth / 2;
		m_iTargetBullEyeY		= m_iTargetHeight / 2;
		m_iAngle 					= convertFromAccToAngle ();
		
		if (VARIETY_ENABLE) {
			if (m_iMaxAccurate < 100 && m_iMaxAccurate > 0) {
				var scale : Number = 100 / m_iMaxAccurate;
				m_iMaxAccurate = 100;
				m_iAccurate *= scale;
			}
			if (m_iMaxRecoil < 100 && m_iMaxRecoil > 0) {
				var scale : Number = 100 / m_iMaxRecoil;
				m_iMaxRecoil = 100;
				m_iRecoil *= scale;
			}
		}
		
		//m_iTimePerShot = MAX_TIME_PER_SHOOT - ( (MAX_TIME_PER_SHOOT - MIN_TIME_PER_SHOOT) * (m_iSpeed / m_iMaxSpeed) );
		m_iTimePerShot = Math.floor(1000 / m_iSpeed);//speed = number of bullets shot per second = per 1000 miliseconds
		
		if (DEBUG_LEVEL > 1) {
			trace ("Target:\n"
				+"\tWidth = " + m_iTargetWidth + "\n"
				+"\tHeight = " + m_iTargetHeight + "\n"
				+"\tBull's Eye = (" + m_iTargetBullEyeX + ", "+m_iTargetBullEyeY+")"
			);
		}
	}
	
	private function convertFromAccToAngle () : Number {
		return MAX_OPEN_ANGLE_FROM_90_D * (1 - (m_iAccurate / m_iMaxAccurate));
	}
	
	public function simulate () : Void {
		prepareBeforeSimulating ();
		if (DEBUG_LEVEL > 2) {
			trace ("Begin shooting " 
				+ NUMBER_OF_SHOTS + " shots (x"+m_iBulletsPerShot+") with accuracy = " 
				+ m_iAccurate + " (/" + m_iMaxAccurate
				+"), recoil = " + m_iRecoil + " (/" + m_iMaxRecoil
				+"), speed = " + m_iSpeed + " (/" + m_iMaxSpeed
				+ ") -> Time per shoot = " + m_iTimePerShot
			);
		}
		var i : Number;
		for (i = 0; i < NUMBER_OF_SHOTS; ++i) {
			for (var j = 0; j < m_iBulletsPerShot; ++j ) {
				_global.setTimeout (Delegate.create(this, shoot), m_iTimePerShot * i);
			}
		}
		_global.setTimeout (Delegate.create(this, enableShootBtn), m_iTimePerShot*i);
	}
	
	public function prepareBeforeSimulating () : Void {
		m_iTurn = 0;
		clearTarget ();
		m_btnShoot.disabled = true;
	}
	
	public function enableShootBtn () : Void {
		m_btnShoot.disabled = false;
	}
	
	public function shoot () : Void {
		if (stop) {
			return ;
		}
		var identifier : String = m_aBulletIdentifiers[random(m_aBulletIdentifiers.length)];		
		var bullet : MovieClip = m_mcTarget.attachMovie (
			identifier, 
			m_mcTarget._name + "_" +identifier + "_"+ m_iTurn, 
			m_mcTarget.getNextHighestDepth());			
	
		var direction: Number = (random (Math.round(m_iAngle * 2)) + 90 - m_iAngle) * FACTOR_DEGREE_TO_RADIAN;
		var distance: Number = random(Math.round(m_iRecoil)) / m_iMaxRecoil;
		
		if (DEBUG_LEVEL > 1) {
			trace ("Angle = " + (direction / FACTOR_DEGREE_TO_RADIAN) + ", Distance = " + distance);
		}
		
		//Calculate position in comparing to bull's eye
		var x : Number = Math.cos (direction) * m_iTargetBullEyeX * distance;
		var y : Number = -Math.sin (direction) * m_iTargetBullEyeY * distance;
		
		//Re-calculate position in comparing to top-left corner of target (and move relatively by previous position)
		x += m_iTargetBullEyeX - (bullet._width / 2 );
		y += m_iTargetBullEyeY - (bullet._height / 2 );
		
		x = Math.floor(x);
		y = Math.floor(y);		
		
		//Move
		bullet._x = x;
		bullet._y = y;	
		
		++m_iTurn;
		
		if (DEBUG_LEVEL > 0) {
			trace ("Turn " + m_iTurn + ": x = " + x + ", y = " + y + " <-> ("+(x - m_iTargetBullEyeX)+", "+(y-m_iTargetBullEyeY)+")");
		}
	}
	
	public function toString () : String {
		return "[CCK - ShootingSimulator "+_name+"]";
	}
}