﻿import gfx.utils.Delegate;
import gfx.events.EventDispatcher;

/**
 * Class hỗ trợ play 1 dãy các frame của một MovieClip.
 * 
 * Một số thao tác được cài đặt sẵn:
 * 		Chạy ngược lại thứ tự frame
 * 		Chạy từ frame X tới frame Y
 * 		Chạy lặp lại...
 */
class gfx.controls.SQUAD.MoviePlayer {
	public var m_mcMain :				MovieClip;	
	private var m_iMaxFrame : 			Number;
	
	private var m_iFrom :					Number;
	private var m_iTo :						Number;
	private var m_bRepeat :				Boolean;
	private var m_fStopCondition :	Function;
	private var m_bDispatchEvent :	Boolean;	
	
	/**
	 * Hàm tạo
	 * Khởi tạo MovieClip sẽ được "play".
	 * Không dùng các phương thức static -> có thể play nhiều MovieClip cùng một lúc
	 * 
	 * @param	mc	MovieClip sẽ được "play"
	 */
	public function MoviePlayer (mc : MovieClip) {
		m_mcMain = mc;	
		m_iMaxFrame = m_mcMain._totalframes;
		EventDispatcher.initialize (this);
		m_bDispatchEvent = true;
	}
	
	/**
	 * Hàm "play" MovieClip theo thứ tự tùy ý:
	 * 		Từ frame ban đầu tới frame đích
	 * 		Có lặp lại quá trình hay không
	 * 		Quyết định dừng hẳn vào lúc nào
	 * 
	 * @param	from					frame ban đầu
	 * @param	to						frame đích
	 * @param	repeat				có lặp lại quá trình hay không
	 * @param	stopCondition	hàm xác định có dừng hẳn không, hàm này có khuôn mẫu như sau:
	 * 												Nhận 1 tham số là MovieClip đang được "play"
	 * 												Trả về giá trị kiểu Boolean, trong đó, true = dừng hẳn, false = tiếp tục
	 * 
	 * Sau khi dùng hàm tạo, Lập trình viên sử dụng hàm này để "play" MovieClip, không dùng hàm animate
	 */
	public function play (		
		from :					Number, 
		to :						Number, 
		repeat :					Boolean, 
		stopCondition :		Function
	) : Void {
		m_iFrom 				= from;
		m_iTo 					= to;
		m_bRepeat			= repeat;
		m_fStopCondition	= stopCondition;
				
		if (m_iFrom < 1) {
			m_iFrom = 1;
		}
		if (m_iFrom > m_iMaxFrame) {
			m_iFrom = m_iMaxFrame;
		}
		if (m_iTo < 1) {
			m_iTo = 1;
		}
		if (m_iTo > m_iMaxFrame) {
			m_iTo = m_iMaxFrame;
		}
		
		if (m_iFrom == m_iTo) {
			return;
		}
		m_mcMain.gotoAndStop (from);
		delete m_mcMain.onEnterFrame;
		m_mcMain.onEnterFrame = Delegate.create (this, animate);
	}
	
	/**
	 * Hàm xác định sẽ chuyển sang frame nào tiếp theo
	 * Lập trình viên không được sử dụng hàm này, hàm này để public chỉ để dùng cho Delegate	 
	 */
	public function animate () : Void {
		if (m_iFrom < m_iTo) {			
			playForward ();
		} else if (m_iFrom > m_iTo) {
			playBackward ();
		} else {
			finish ();
		}
	}
	
	/**
	 * Chạy xuôi
	 */
	private function playForward () : Void {
		if (m_fStopCondition != undefined) {						//Nếu có điều kiện dừng
			if (m_fStopCondition(m_mcMain)) {						//thì kiểm tra điều kiện dừng
				finish ();															//kết thúc và thoát khỏi hàm ngay				
				return;
			}
		}
		
		if (m_mcMain._currentframe < m_iTo) {					//nếu còn chưa chạy tới frame đích
			m_mcMain.nextFrame ();										//thì chuyển sang frame tiếp theo
		} else if (m_mcMain._currentframe == m_iTo) {			//nếu đã tới frame đích
			if (m_bRepeat) {													//và yêu cầu lặp lại
				m_mcMain.gotoAndStop (m_iFrom);				//thì quay trở lại frame ban đầu
			} else {																//nếu không yêu cầu lặp lại
				finish();															//kết thúc
			}		
		} else {																	//nếu đã chạy quá frame đích
			finish();																//kết thúc
		}		
	}	
	
	/**
	 * Chạy ngược
	 */
	private function playBackward () : Void {		
		if (m_fStopCondition != undefined) {						//Nếu có điều kiện dừng
			if (m_fStopCondition(m_mcMain)) {						//thì kiểm tra điều kiện dừng
				finish ();															//kết thúc và thoát khỏi hàm ngay				
				return;
			}
		}
		
		if (m_mcMain._currentframe > m_iTo) {					//nếu còn chưa chạy tới frame đích
			m_mcMain.prevFrame ();									//thì chuyển sang frame trước đó
		} else if (m_mcMain._currentframe == m_iTo) {			//nếu đã tới frame đích
			if (m_bRepeat) {													//và yêu cầu lặp lại
				m_mcMain.gotoAndStop (m_iFrom);				//thì quay trở lại frame ban đầu
			} else {																//nếu không yêu cầu lặp lại
				finish();															//kết thúc
			}		
		} else {																	//nếu đã chạy quá frame đích
			finish();																//kết thúc
		}
	}
	
	/**
	 * Hàm chuyển nhãn của frame thành số thứ tự của frame (dùng cho from, to)
	 * 
	 * @param	frameLabel	Nhãn của frame
	 * 
	 * @return							Số thứ tự của frame
	 */
	public function convertFrameLabelToFrameNumber (frameLabel : String) : Number {
		var currentFrame : Number = m_mcMain._currentframe;
		m_mcMain.gotoAndStop (frameLabel);
		var frame = m_mcMain._currentframe;
		m_mcMain.gotoAndStop (currentFrame);
		return frame;
	}
	
	/**
	 * Kết thúc quá trình "play"
	 * Phát sinh sự kiện "onFinish" đi kèm với thông tin lưu trong 1 Object có cấu trúc như sau:
	 * 		type :		"onFinish"
	 * 		target :		MoviePlayer phát sinh sự kiện "onFinish"
	 */
	public function finish () : Void {
		if (m_bDispatchEvent) {
			dispatchEvent ( { type :	"onFinish", target : this } );
		}
		delete m_mcMain.onEnterFrame;			
	}
	
	public function stopReceiveEvent () : Void {
		m_bDispatchEvent = false;
	}
	
	public function startReceiveEvent () : Void {
		m_bDispatchEvent = true;
	}
	
	public static function doOnceAtFrame (mc : MovieClip, frame : Number, func : Function) : Void {
		mc.onEnterFrame = function () : Void {
			if (mc._currentframe == frame) {
				func ();
				delete (mc.onEnterFrame);
			}
		};
	}
	
	public function toString () : String {
		return "[CCK - MoviePlayer "+_name+"]";
	}
	
	//---------------------------------------------------------
	//3 hàm dưới đây phải đưa vào từ class EventDispatcher
	//---------------------------------------------------------
	public function addEventListener() { }
	public function dispatchEvent() { }
	public function removeEventListener() { }
}

/*
//Ví dụ sử dụng class:
//Đặt 1 MovieClip (có ít nhất 20 frame) vào Stage và đặt tên là "mc"
//Đặt đoạn code này vào Frame đầu tiên của Scene và chạy
//Kết quả đúng: MovieClip sẽ chạy từ frame 20 tới frame 9 và lặp đi lặp lại quá trình này khi đếm được 5 lần

import gfx.controls.SQUAD.MoviePlayer;

var kMP : MoviePlayer = new MoviePlayer (mc);
kMP.play (20, 9, true, stopWhen5Times);

var count : Number = 0;
function stopWhen5Times (mc : MovieClip) : Boolean {
	if (mc._currentframe == 9) {
		++count;
		return (count == 5);
	}
	return false;	
}
*/