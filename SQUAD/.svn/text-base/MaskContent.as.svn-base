﻿import gfx.controls.ScrollBar;
import gfx.core.UIComponent;

/**
 * Class di chuyển vị trí của MovieClip theo thanh cuộn.
 * Kết hợp với việc chỉ để lộ ra 1 khung nhìn (viewport) để tạo nên nội dung của MovieClip có thể cuộn được.
 */
class gfx.controls.SQUAD.MaskContent extends UIComponent {
	/**
	 * Linkage Identifier của MovieClip sẽ tạo nên nội dung cuộn được
	 */
	[Inspectable (name="ContentIdentifier", type="String")]
	public var m_sContentIdentifier :		String;
	
	/**
	 * Chiều dài của khung nhìn. Nếu cuộn dọc chính là chiều cao viewport, nếu không là chiều rộng viewport.
	 * Trong trường hợp thanh cuộn có thể cuộn quá hoặc không cuộn hết nội dung thì phải điều chỉnh tham số này.
	 */
	[Inspectable (name="ViewportLength", type="Number"), defaultValue=0]
	public var m_iViewportLength :		Number;
	
	/**
	 * Tên scrollbar hoặc Linkage Identifier của Scrollbar sẽ bắt sự kiện cuộn cho Component này.
	 */
	[Inspectable (name = "ScrollBar", type = "String")]
	private var m_oScrollBar:				Object;
		
	/**
	 * Có cuộn dọc hay không.
	 */
	[Inspectable (name="Vertical", type="Boolean", defaultValue=true)]
	private var m_bVertical :					Boolean;
	
	/**
	 * Có tự động ẩn thanh cuộn khi không cần dùng tới thanh cuộn không
	 */
	[Inspectable (name="AutoHideScrollBar", type="Boolean", defaultValue=true)]
	private var m_bAutoHideScrollBar :	Boolean;
	
	/**
	 * Vị trí hiện tại của nội dung ( theo chiều ngang)
	 */
	private var m_iContentX :				Number;
	
	/**
	 * Vị trí hiện tại của nội dung (theo chiều dọc)
	 */
	private var m_iContentY : 				Number;
	
	/**
	 * Vị trí của nội dung hiện tại / tổng chiều dài nội dung
	 * =
	 * Vị trí nút cuộn / Độ dài thanh cuộn
	 */
	public var m_iPercentage :				Number;
	
	/**
	 * Chiều dài cuộn tối đa ( = Độ dài của nội dung - Độ dài viewport)
	 */
	private var m_iMax:						Number;
	
	/**
	 * Bắt buộc phải có 1 MovieClip tên như sau có mặt trong Component.
	 * Chỉ làm nhiệm vụ chỉ ra tọa độ ban đầu của nội dung.
	 * Chẳng hạn MaskContent còn có viền dày 20px thì nếu đặt nội dung ở vị trí (0, 0) mặc định sẽ bị viền che mất.
	 */
	private var mcContainer:				MovieClip;	
	
	/**
	 * Nội dung
	 */
	private var m_mcContent :				MovieClip;		
	
	/**
	 * Thanh cuộn
	 */
	private var m_Scrollbar :					MovieClip;//ScrollBar
	
	public function MaskContent () {
		super ();			
	}
	
	private function configUI () : Void {
		m_mcContent = mcContainer.attachMovie (m_sContentIdentifier, "content", mcContainer.getNextHighestDepth(), {_x: 0, _y: 0});
		if (m_mcContent instanceof MovieClip) {			
			m_iContentX 			=
			m_iContentY 			= 
			m_iPercentage		= 0;			
			if (m_bVertical) {
				m_iMax				= m_mcContent._height - m_iViewportLength;
			} else {
				m_iMax				= m_mcContent._width - m_iViewportLength;
			}
			
			if (typeof(m_oScrollBar) == "string") {
				m_Scrollbar = MovieClip(_parent[m_oScrollBar.toString()]);
				if (m_Scrollbar == null) {
					m_Scrollbar = mcContainer.attachMovie(m_oScrollBar.toString(), "ScrollBar", mcContainer.getNextHighestDepth());
				}
			} else {
				m_Scrollbar = MovieClip(m_oScrollBar);
			}
			if (m_Scrollbar instanceof ScrollBar) {
				m_Scrollbar.addEventListener ("scroll", this, "scrollContent");
				m_Scrollbar.setScrollProperties (m_iMax/10, 0, m_iMax, m_iMax/10);
				m_Scrollbar.position = 0;
				m_Scrollbar._visible = (m_iMax > 0) || (!m_bAutoHideScrollBar);				
			}
		}
		
		dispatchEvent ( {type:	"ready", target: this});
	}	
	
	/**
	 * Khi cuộn thanh cuộn thì xử lý bằng hàm này	 
	 */
	private function scrollContent (evt : Object) : Void {		
		setScrollPercentage (evt.position * 100.0 / m_iMax);
	}
	
	/**
	 * Cuộn nội dung tới 1 vị trí nhất định
	 * 
	 * @param	percentage	Tỉ lệ cuộn ( = vị trí / tổng độ dài)
	 */
	public function setScrollPercentage (percentage : Number) : Void {		
		var value : Number = -1;
		if (!isNaN(percentage)) {			
			if (percentage < 0) {				
				value = 0;
			} else if (percentage > 100) {
				value = 100;
			} else if (Math.abs(percentage - m_iPercentage) >= 1) {				
				value = percentage;
			}
		}
		if (value >= 0 && value < 100) {
			if (m_bVertical) {
				m_mcContent._y = m_iContentY - (m_iMax * value / 100.0);
			} else {
				m_mcContent._x = m_iContentX - (m_iMax * value / 100.0);
			}
			m_Scrollbar.position = m_iMax * value / 100.0;
			m_iPercentage = value;
		}		
	}
	
	/**
	 * Lấy MovieClip nội dung của component	 
	 */
	public function getContent () : MovieClip {
		return m_mcContent;
	}
	
	/**
	 * Khi chiều dài của viewport hay content (thường là content) thay đổi, thì toàn bộ thuộc tính của thanh scrollbar phải được thiết lập lại
	 * Do đó, khi có thay đổi chiều dài content hoặc viewport, gọi hàm này.
	 */
	public function refresh () : Void {
		if (m_bVertical) {
			m_iMax					= m_mcContent._height - m_iViewportLength;
			m_iPercentage		= m_iContentY / m_iMax;
		} else {
			m_iMax					= m_mcContent._width - m_iViewportLength;		
			m_iPercentage		= m_iContentX / m_iMax;
		}
		if (m_Scrollbar instanceof ScrollBar) {			
			m_Scrollbar.setScrollProperties (m_iMax / 10, 0, m_iMax, m_iMax / 10);
			setScrollPercentage (m_iPercentage);
			m_Scrollbar._visible = (m_iMax > 0) || (!m_bAutoHideScrollBar);
		}		
	}
	
	public function toString () : String {
		return "[CCK - MaskContent "+_name+"]";
	}
}