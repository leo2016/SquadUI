﻿import gfx.controls.Button;
import gfx.core.UIComponent;

import gfx.controls.SQUAD.CPPSimulator;

class gfx.controls.SQUAD.CustomizedButtonBar extends UIComponent {	
	private var m_aData :				Array;	
	public var m_iButtonSpace :	Number;
	public var m_bHorizontal :		Boolean;
	public var m_fLabelFunction :	Function;	
	private var m_oSelectedData :	Object;
	private var m_btnSelected:		Button;	
	
	public var m_bEnableDispatchEvent : Boolean;
	
	public function get buttonSpace () : Number {
		return m_iButtonSpace;
	}
	
	[Inspectable(defaultValue=2)]
	public function set buttonSpace (s : Number) : Void {
		m_iButtonSpace = s;
	}
	
	public function CustomizedButtonBar () {
		super ();
				
		m_aData = [];		
		m_bHorizontal = true;
		m_fLabelFunction = defaultLabelFunction;
		m_oSelectedData = undefined;
		m_btnSelected = undefined;
		m_bEnableDispatchEvent = true;
		
		/*
		var kListener : Object = { };
		kListener.onKeyUp = handleKeyUp;
		Key.addListener (kListener);
		*/
	}
	
	public function setButtonsArray (btnArray : Array) : Boolean {
		clearData();		
		var accPos : Number = 0;
		var index : Number = 1;
		if (m_iButtonSpace == undefined) {
			m_iButtonSpace = 2;
		}
		for (var x : String in btnArray) {
			if (btnArray[x].identifier == undefined || btnArray[x].data == undefined) {
				CPPSimulator.trace ("Invalid data structure for CustomizedButtonBar (" + this._name + "). Each button must have 2 fields: identifier & data", 1);
				m_aData = [];
				invalidate ();
				return false;
			}
			m_aData[x] = { };//1. button, 2.data, 3.identifier
			//Set up button
			m_aData[x].button = this.attachMovie (
				btnArray[x].identifier, 
				this._name + "_" + index, 
				this.getNextHighestDepth());
			if (m_bHorizontal) {
				m_aData[x].button._y = 0;
				m_aData[x].button._x = accPos;
				accPos += m_aData[x].button._width + m_iButtonSpace;
			} else {
				m_aData[x].button._x = 0;
				m_aData[x].button._y = accPos;
				accPos += m_aData[x].button._height + m_iButtonSpace;
			}
			m_aData[x].button.addEventListener ("click", this, "handleButtonClick");			
			m_aData[x].button.groupName = "CustomizedButtonBar_" + this._name;			
			m_fLabelFunction (m_aData[x].button, btnArray[x].data);
			
			//Set up data for button
			m_aData[x].data = btnArray[x].data;
			m_aData[x].identifier = btnArray[x].identifier;
			
			++index;
		}		
		m_oSelectedData = undefined;
		m_btnSelected = undefined;
		invalidate ();		
		return true;
	}
	
	public function clearData() : Void {
		for (var x : String in m_aData) {
			m_aData[x].button.removeMovieClip ();
			delete m_aData[x];
		}
		m_aData = [];
	}
	
	private function handleButtonClick (evt : Object) : Void {		
		var button : Button = evt["target"];
		var isChanged : Boolean = false;
		var isClicked : Boolean = false;
		for (var x : String in m_aData) {			
			if (m_aData[x].button == button) {
				if (m_oSelectedData != m_aData[x].data) {
					m_oSelectedData = m_aData[x].data;					
					isChanged = true;
				} else {					
					isClicked = true;
				}				
				m_btnSelected = button;
				m_btnSelected.selected = true;
				m_btnSelected.displayFocus = true;
			} else {				
				m_aData[x].button.selected = false;
				m_aData[x].button.displayFocus = false;
			}
		} 	
		if (m_bEnableDispatchEvent) {
			if (isChanged) {
				dispatchEvent ( {
					type:		"change", 
					target:	this, 
					button:	button, 						
					data:		m_oSelectedData
				});
			} else if (isClicked) {
				dispatchEvent ( {
					type:		"itemClick", 
					target:	this, 
					button:	button, 						
					data:		m_oSelectedData
				});
			}
		}		
	}
	
	public function invalidateData () : Void {
		var newData : Array = [];
		for (var x : String in m_aData) {
			newData[x] = { };
			newData[x].identifier = m_aData[x].identifier;
			newData[x].data = m_aData[x].data;
		}
		setButtonsArray (newData);
	}
	
	private function defaultLabelFunction (button : Button, data : Object) : Void {
		button.label = String(data);
	}
	
	public function getSelectedButton () : Button {
		return m_btnSelected;
	}
	
	public function getData () : Object {
		return m_oSelectedData;
	}
	
	public function clearSelected () : Void {		
		invalidateData ();
	}
	
	public function setSelectedData (data : Object) : Void {		
		var isChanged : Boolean = false;
		for (var x : String in m_aData) {
			if (m_aData[x].data == data) {
				if (m_oSelectedData != data) {					
					m_oSelectedData = data;
					m_btnSelected = m_aData[x].button;					
					m_btnSelected.selected = true;
					m_btnSelected.displayFocus = true;
					if (m_bEnableDispatchEvent) {						
						isChanged = true;
					}
				}				
			} else {
				m_aData[x].button.selected = false;
				m_aData[x].button.displayFocus = false;
			}
		}
		if (m_bEnableDispatchEvent && isChanged) {
			dispatchEvent ( {
				type:		"change", 
				target:	this, 
				button:	m_btnSelected,
				data:		m_oSelectedData
			});			
		}		
	}
	
	public function disableData (data : Object) : Void {		
		for (var x : String in m_aData) {			
			if (m_aData[x].data == data) {				
				m_aData[x].button.disabled = true;
				break;
			}
		}		
	}
	
	public function enableData (data : Object) : Void {		
		for (var x : String in m_aData) {			
			if (m_aData[x].data == data) {												
				m_aData[x].button.disabled = false;
				break;
			}
		}		
	}	
	
	/*
	private function handleKeyUp () : Void {
		switch (Key.getCode()) {
			case Key.TAB : {
				if (tabEnabled) {
					var l : Number = m_aData.length;
					for (var i : Number  = 0; i < l; ++i ) {
						if (m_aData[i].button == m_btnSelected ) {
							setSelectedData (m_aData[(i + 1) % l].data);
							break;
						}
					}
					break;
				}
			}
		}
	}
	*/
	
	public function toString () : String {
		return "[CCK - CustomizedButtonBar "+_name+"]";
	}
}