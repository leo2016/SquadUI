﻿/**
 * Class chứa một số pattern giả lập phản hồi từ C++ tới ActionScript
 */
class gfx.controls.SQUAD.CPPSimulator {
	/**
	 * Biến FAKE_RESPONSE dùng để quyết định xem C++ có phản hồi không?
	 * Cách sử dụng: Khi hàm ActionScript gọi 1 hàm fscommand để truyền tải thông điệp tới C++ và đợi C++ phản hồi
	 * 		, thay vì phải chạy chương trình C++ để phản hồi (rất lâu, rất nặng và phải mở nhiều chương trình)
	 * 		, tự ActionScript có thể đóng giả vai trò là C++ để phản hồi -> nhanh chóng test
	 * 	Tuy nhiên, khi đã làm việc thật với C++, phải vô hiệu hóa tất cả đoạn mã giả phản hồi đó
	 * -> nếu dùng biến FAKE_RESPONSE để kiểm soát việc có giả lập sự phản hồi của C++ không thì chỉ cần thay đổi biến này
	 * 
	 * Ví dụ: Khi người dùng click vào nút bán vật phẩm, AS phải hỏi C++ xem giá bán là bao nhiêu. Ta có thể sinh ra 1 
	 * 		phản hồi giả để test thử
	 * 
	 * function onSellButtonClick () : Void {
	 * 		....
	 * 		fscommand (...);
	 * 
	 * 		if (CPPSimulator.FAKE_RESPONSE) {
	 * 			...Đóng vai trò là C++, gọi hàm AS sẽ được Invoke nếu C++ nhận phản hồi này
	 * 		}
	 * }	 
	 */
	public static var FAKE_RESPONSE : Boolean = true;

	/**
	 * Biến TRACE_DEBUG_LEVEL giới hạn mức độ "trace" ra màn hình Output
	 * 
	 * Bình thường, khi test thử file AS, phải trace ra rất nhiều thông điệp nhưng khi đưa vào hoạt động
	 * 		thì phải comment hoặc bỏ các lệnh trace này đi -> phải tìm các lệnh trace rất mệt
	 * 		-> Nếu dùng 1 biến để kiểm soát có sử dụng các lệnh trace đó không thì chỉ cần thay đổi giá trị biến đó
	 * 		thì các lệnh trace sẽ được kiểm soát có in ra hay không theo ý muốn của lập trình viên mà không
	* 		cần phải tìm các lệnh trace để comment -> Có thể chọn in ra hay không in ra!
	* 
	* Tuy nhiên, khi đã in ra các lệnh trace, ta cũng không muốn tất cả các lệnh trace hoạt động mà chỉ muốn một số chúng
	* 		được hiển thị thôi -> có thể gán cho mỗi lệnh trace 1 cấp độ và ta giới hạn cấp độ mà lệnh trace sẽ được dùng.
	* 		Chẳng hạn có các lệnh trace cấp độ 1, 2, 3, 4. Và ta chỉ giới hạn in ra các lệnh trace cấp độ 1, 2, thôi, hoặc khi đã
	* 		nắm được các bước cơ bản rồi, chỉ cần theo dõi tổng quan thì ta giới hạn chỉ in ra các lệnh trace cấp độ 1. Và khi
	* 		hoàn toàn không cần tới các lệnh trace, chỉ cần giới hạn mức độ sao cho không có lệnh trace nào đủ điều kiện
	* 		để được in ra.
	* 
	* Tóm lại, dùng hàm CPPSimulator.trace thì hàm này sẽ hiển thị thông điệp nếu cấp độ gắn kèm <= TRACE_DEBUG_LEVEL
	 */
	public static var TRACE_DEBUG_LEVEL : Number = 0;
	
	/**
	 * Biến xác định lệnh trace luôn luôn được in ra!
	 * Điều đó có nghĩa là 
	 * 		CPPSimulator.trace (msg, CPPSimulator.ALWAYS_TRACE);
	 * 		tương đương với
	 * 		trace (msg);
	 */
	public static var ALWAYS_TRACE : Number = -1;
	
	public static var OUTPUT_TIME : Boolean = false;
	
	public static var PREFIX : String = "";
	
	public static var TRACE_FSCOMMAND : Boolean = false;
	
	/**
	 * Hàm in ra màn hình Output các thông điệp
	 * 
	 * @param	message	Thông điệp
	 * @param	level			Chỉ in ra thông điệp này nếu TRACE_DEBUG_LEVEL >= level này (hoặc level = ALWAYS_TRACE)
	 */
	public static function trace (message : String, level : Number) : Void {
		if (TRACE_DEBUG_LEVEL >= level || level == ALWAYS_TRACE) {
			var before : String = "";			
			if (OUTPUT_TIME) {
				var currentDate : Date = new Date ();
				before += 
					currentDate.getHours() + ":" + 
					currentDate.getMinutes() + ":" + 
					currentDate.getSeconds() + ":" + 
					currentDate.getMilliseconds() + ": ";
			}
			before += PREFIX;
			if (level > 0) {
				for (var i : Number = 1; i <= level; ++i ) {
					before += "\t";
				}
			}
			trace (before+message);
		}
	}
	
	public static function getTrace (message : String, level : Number) : String {
		if (TRACE_DEBUG_LEVEL >= level || level == ALWAYS_TRACE) {
			var before : String = "";			
			if (OUTPUT_TIME) {
				var currentDate : Date = new Date ();
				before += 
					currentDate.getHours() + ":" + 
					currentDate.getMinutes() + ":" + 
					currentDate.getSeconds() + ":" + 
					currentDate.getMilliseconds() + ": ";
			}
			before += PREFIX;
			if (level > 0) {
				for (var i : Number = 1; i <= level; ++i ) {
					before += "\t";
				}
			}
			return (before+message);
		}
		return "";
	}
	
	/**
	 * Hàm in ra màn hình thông điệp được sử dụng trong hàm fscommand gọi hàm fscommand ngay sau khi in
	 * Thông điệp sẽ được in ra mặc định với level = 1
	 * 
	 * @param	message	thông điệp truyền đi	 
	 */
	public static function fs (message : String) : Void {
		/*
		if (TRACE_FSCOMMAND) {
			CPPSimulator.trace ("fscommand - " + message, TRACE_DEBUG_LEVEL);
		}
		*/
		fscommand (message);
	}
	
	public static function traceAllComponents (rootMC : MovieClip, level : Number) : Void {	
		var y = rootMC.scale9Grid;
		if (y != undefined) {
			return;
		}		
		CPPSimulator.trace ("" + rootMC, level);		
		var child : MovieClip;
		for (var x : String in rootMC) {
			child = rootMC[x];
			if (child instanceof MovieClip) {
				if (getMovieClipNestedLevel(child) == level + 1) {
					traceAllComponents (child, level + 1);
				}
			}
		}	
	}

	public static function getMovieClipNestedLevel (mc : MovieClip) : Number {
		if (!(mc instanceof MovieClip) || mc == _root || mc == _root._level0) {
			return 0;
		}
		var p : MovieClip = mc._parent;
		var level : Number = 1;
		while (p != _root) {
			++level;
			p = p._parent;
		}
		return level;
	}
	
	public static function traceObject (name : String, o : Object, level : Number) : Void {
		var s : String = name + "\n";
		for (var x : String in o) {
			s += "\t" + x + " = " + o[x] + "\n";
		}
		CPPSimulator.trace (s, level);
	}
}