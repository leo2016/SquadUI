import gfx.controls.ButtonBar;

class gfx.controls.SQUAD.PopUpMenu extends ButtonBar {
	[Inspectable (name="Always Full Visible", type="Boolean", defaultValue=true)]
	public var m_bAlwaysFullVisible : 			Boolean;
	
	public static var RIGHT_MOUSE_BUTTON: Number = 2;
	
	private var isMenuShow : 						Boolean;
	private var disabledButtons : 					Array;
	
	public function PopUpMenu () {
		super ();
		direction = "vertical";
		disabledButtons = [];
		hideMenu ();
	}
	
	public function showHideMenu(button:Number, target:Object):Void {
		if (_disabled) {
			if (isMenuShow == true) {
				hideMenu ();
			}
		} else {
			if (target != undefined && target != null) {
				if (isRightClick(button)) {
					showMenuAt (_root._xmouse + 5, _root._ymouse + 5);
				} else if (isMenuShow == true) {
					hideMenu ();
				}
			} else {
				if (isMenuShow == true) {
					hideMenu ();
				}
			}
		}
	}

	public function showMenuAt (x:Number, y:Number):Void {
		if (x + this._width > Stage.width) {
			x = Stage.width - this._width;
		}
		if (y + this._height > Stage.height) {
			y = Stage.height - this._height;
		}
		this._x = x;
		this._y = y;
		this.selectedIndex = -1;
		this.invalidateData ();
		this._visible = true;
		isMenuShow = true;
	}

	public function hideMenu ():Void {
		this._visible = false;
		isMenuShow = false;
	}
	
	public static function isRightClick (button:Number):Boolean {
		return ( button == RIGHT_MOUSE_BUTTON );
	}
	
	private function populateData():Void {
		for (var i:Number = 0; i < renderers.length; i++) {
			var renderer:MovieClip = renderers[i];
			renderer.label = itemToCommand(_dataProvider.requestItemAt(i));
			renderer.data = _dataProvider.requestItemAt(i);
			renderer.disabled = itemToDisable(_dataProvider.requestItemAt(i));
		}
	}
	
	private function itemToCommand(data:Object):String {
		if (data == null) {
			return "";
		} else if (data.command == null) {
			return "";
		} else {
			return data.command;
		}
	}
	
	private function itemToDisable(data:Object):Boolean {
		return data.disabled;
	}
	
	public function get visible () : Boolean {
		return isMenuShow;
	}
	
	public function toString () : String {
		return "[CCK - PopUpMenu "+_name+"]";
	}
}