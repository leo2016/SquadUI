﻿import gfx.controls.Button;
import gfx.controls.Slider;
import gfx.controls.SQUAD.Setting.Setting;

class gfx.controls.SQUAD.SliderWithValue extends Slider {
	public var m_fLabelFunction : Function;
	
	public var setting : Setting;
	
	[Inspectable (name = "Action", type = "Number", defaultValue = 0)]
	public function set action (a : Number) : Void {
		if (setting instanceof Setting) {
			setting.setAction (a);
		}
	}
	public function get action () : Number {
		if (setting instanceof Setting) {
			return setting.action;
		}
		return 0;
	}
	
	public function SliderWithValue () {
		
		super();
		setting = new Setting();
		m_fLabelFunction = defaultLabelFunction;
	}
	public function resetValue() {
		if (setting instanceof Setting) {
			setting.reset ();
			updateUI();
		}
	}
	public function saveValue() {
		if (setting instanceof Setting) {
			setting.save ();
			updateUI();
		}
	
	}
	public function cancelValue() {
		if (setting instanceof Setting) {
			setting.cancel ();
			updateUI();
		}
	}
	public function setValue (value : Number) : Void {
		if (setting instanceof Setting) {
			setting.setCurrent (value);
			updateUI ();
		}
	}
	
	public function updateUI() {
		if (setting instanceof Setting) {
			value = setting.currentCode;
		}
	}
	public function isChanged () : Boolean {
		if (setting instanceof Setting) {
			return setting.isChanged ();
		}
		return false;
	}
	//--------------------------------------------------
	
	private function updateThumb():Void {
		super.updateThumb ();
		thumb.label = m_fLabelFunction (value);
		setValue(Math.floor(value));
	}
	
	private function defaultLabelFunction (value : Number) : String {
		return "" + value;
	}
	
	public function set disabled (d : Boolean) : Void {
		super.disabled = d;		
	}
	
	public function get disabled () : Boolean {
		return super.disabled;
	}
}