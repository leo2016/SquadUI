﻿class gfx.controls.SQUAD.ImagePathManager {
	/**
	 * Thư mục chứa toàn bộ ảnh dùng cho Flash
	 * Chú ý: Không dùng thư mục này
	 */
	private static var IMAGE_FOLDER : String 			= "Images\\";
	
	//--------------------------------------------------------------------------------------------------------------------
	
	/**
	 * Thư mục chứa ảnh các vật phẩm trong danh sách dùng để hiển thị trong Inventory, Shop
	 */
	public static var INVENTORY_ITEM : String 						= IMAGE_FOLDER + "Inventory\\";
	public static var INVENTORY_ITEM_DEFAULT : String 		= INVENTORY_ITEM + "default.png";
	
	/**
	 * Thư mục chứa ảnh các vật phẩm trong cửa sổ giới thiệu chi tiết dùng để hiển thị trong Inventory, Shop
	 */
	public static var INVENTORY_ITEM_DETAIL : String 				= IMAGE_FOLDER + "Inventory_Detail\\";
	public static var INVENTORY_ITEM_DETAIL_DEFAULT : String = INVENTORY_ITEM_DETAIL + "default.png";
	
	/**
	 * Thư mục chứa ảnh các vật phẩm trong các slot bên phải dùng để hiển thị trong Inventory
	 */
	public static var INVENTORY_ITEM_SIDE_SLOTS : String 				= IMAGE_FOLDER + "Inventory_SideSlots\\";
	public static var INVENTORY_ITEM_SIDE_SLOTS_DEFAULT : String 	= INVENTORY_ITEM_SIDE_SLOTS + "default.png";
	public static var INVENTORY_ITEM_SIDE_SLOTS_INCON_DEFAULT : String 				= IMAGE_FOLDER + "Inventory_SideSlots\\"+ "IconDefault\\";
	
	/**
	 * Thư mục chứa ảnh các vật phẩm sẽ được hiển thị ở panel thông tin phía dưới danh sách
	 */
	public static var INVENTORY_ITEM_BOTTOM : String					= IMAGE_FOLDER + "Inventory Bottom Information\\";
	public static var INVENTORY_ITEM_BOTTOM_DEFAULT : String		= INVENTORY_ITEM_BOTTOM + "default.png";
	
	//--------------------------------------------------------------------------------------------------------------------
	
	/**
	 * Thư mục chứa ảnh các cấp bậc
	 * Chú ý: Dùng RANKS_BIG hoặc RANKS_SMALL, không dùng thư mục này
	 */
	private static var RANKS : String 						= IMAGE_FOLDER + "Ranks\\";
	
	/**
	 * Thư mục chứa ảnh các cấp bậc (cỡ to, 127 x 127)
	 */
	public static var RANKS_BIG : String						= RANKS + "127x127\\";
	
	/**
	 * Thư mục chứa ảnh các cấp bậc (cỡ nhỏ, 17 x 17)
	 */
	public static var RANKS_SMALL : String				= RANKS + "17x17\\";
	
	/**
	 * Thư mục chứa ảnh các huy hiệu
	 * Chú ý: Dùng MEDALS_BIG hoặc MEDALS_SMALL, không dùng thư mục này
	 */
	public static var MEDALS : String							= IMAGE_FOLDER + "Medals\\";
	
	/**
	 * Thư mục chứa ảnh các huy hiệu (cỡ to, 100 x 100)
	 */
	public static var MEDALS_BIG : String					= MEDALS + "100x100\\";
	
	/**
	 * Thư mục chứa ảnh các huy hiệu (cỡ nhỏ, 17 x 17)
	 */
	public static var MEDALS_SMALL : String				= MEDALS + "17x17\\";
	
	
	/**
	 * Thư mục chứa ảnh các icon hien thi ACE, FirstKill, Owner..
	 */
	private static var ICON_SCORES : String						= IMAGE_FOLDER + "IconScoresWindow\\";
	
	/**
	 *  Ten mac dinh cua ACE, FirstKill, Owner..
	 */
	public static var ICON_SCORES_ACE 		: String	= ICON_SCORES + "ACE";
	public static var ICON_SCORES_NUMBER1 : String	= ICON_SCORES + "1st";
	public static var ICON_SCORES_NUMBER2 : String	= ICON_SCORES + "2nd";
	public static var ICON_SCORES_OWNER 	: String	= ICON_SCORES + "Room";
	public static var ICON_SCORES_MVP 		: String	= ICON_SCORES + "MVP";
	
	/**
	 *  Đuôi ảnh mặc định.
	 */
	public static var IMG_TYPE						:String 	= ".png";
	
	/**
	 * Thư mục chứa ảnh các icon hien thi Weapon trong bang InfoWindow..
	 */
	 public static var ICON_INFOWINDOW:String 	= IMAGE_FOLDER + "WeaponInfoWindow\\" ;
	 
	 /*
	  * Thu muc chua anh cua Weapon trong KillDeath
	  * */
	 public static var ICON_KILLDEATH:String = IMAGE_FOLDER + "KillDeath\\";
	 
	 //--------------------------------------------------------------------------------------------------------------------
	 
	 /**
	 * Thư mục chứa ảnh các vật phẩm sẽ được hiển thị ở panel thông tin phía dưới danh sách
	 */
	public static var EVENT_FOLDER : String = IMAGE_FOLDER + "Event\\";
	public static var EVENT_HIGHLIGHTED_FORDER : String					= EVENT_FOLDER + "EventHightLight\\";
	public static var EVENT_NORMAL_FORDER : String							= EVENT_FOLDER + "EventNomal\\";
	//---------------------------------------------------------
	 
	 public static var CLASS_FOLDER : String = IMAGE_FOLDER + "Classes\\";
	 
	 public static var CLASS_INVENTORY_FOLDER : String = CLASS_FOLDER + "Inventory\\";
	 
	 public static var CLASS_ICON_TANKER : String = CLASS_INVENTORY_FOLDER + "tanker";
	 
	 public static var CLASS_ICON_SCOUT : String = CLASS_INVENTORY_FOLDER + "scout";
	 
	 public static var CLASS_ICON_SNIPER : String = CLASS_INVENTORY_FOLDER + "sniper";
	 
	 public static var CLASS_EQUIPPED_ICON_TANKER : String = CLASS_INVENTORY_FOLDER + "tanker-equipped";
	 
	 public static var CLASS_EQUIPPED_ICON_SCOUT : String = CLASS_INVENTORY_FOLDER + "scout-equipped";
	 
	 public static var CLASS_EQUIPPED_ICON_SNIPER : String = CLASS_INVENTORY_FOLDER + "sniper-equipped";
	 
	 public static var CLASS_EQUIPPED_ICON_NONE : String = CLASS_INVENTORY_FOLDER + "class-none-equipped";
	 
	 public static var CLASS_INVENTORY_COMPARE_FOLDER : String = CLASS_FOLDER + "Inventory Comparing List\\";
	 
	 public static var CLASS_COMPARE_TANKER : String = CLASS_INVENTORY_COMPARE_FOLDER + "tanker";
	 
	 public static var CLASS_COMPARE_SCOUT : String = CLASS_INVENTORY_COMPARE_FOLDER + "scout";
	 
	 public static var CLASS_COMPARE_SNIPER : String = CLASS_INVENTORY_COMPARE_FOLDER + "sniper";
	 
	 public static var CLASS_COMPARE_EQUIPPED_TANKER : String = CLASS_INVENTORY_COMPARE_FOLDER + "tanker-equipped";
	 
	 public static var CLASS_COMPARE_EQUIPPED_SCOUT : String = CLASS_INVENTORY_COMPARE_FOLDER + "scout-equipped";
	 
	 public static var CLASS_COMPARE_EQUIPPED_SNIPER : String = CLASS_INVENTORY_COMPARE_FOLDER + "sniper-equipped";
	 
	 public static var HOT_LABEL : String = IMAGE_FOLDER + "hot.png";
	 
	 public static var PROMOTION_LABEL : String = IMAGE_FOLDER + "promotion.png";
	 
	 public static var HOT_2_LABEL : String = IMAGE_FOLDER + "hot2.png";
	 
	 public static var PROMOTION_2_LABEL : String = IMAGE_FOLDER + "promotion2.png";
	 
	 //--------------------------------------------------------------------------------------------------------------------
	 
	 public static var WEAPON_PREVIEW_FOLDER : String = IMAGE_FOLDER + "WeaponPreview\\";
	 
	 public static var WEAPON_PREVIEW_DEFAULT : String = WEAPON_PREVIEW_FOLDER + "default.png";
	 
	 public static var WEAPON_PREVIEW_LARGE_FOLDER : String = IMAGE_FOLDER + "WeaponPreviewLarge\\";
	 
	 public static var WEAPON_PREVIEW_LARGE_DEFAULT : String = WEAPON_PREVIEW_LARGE_FOLDER + "default.png";
	 
	 //--------------------------------------------------------------------------------------------------------------------
	 
	 public static var MAP_FOLDER : String			= IMAGE_FOLDER + "Map\\";
	 
	 public static var MAP_WAITING_ROOM : String	= MAP_FOLDER + "WaitingRoom\\";
	 
	 public static var MAP_CREATE_ROOM : String	= MAP_FOLDER + "CreateRoom\\";
	 
	 public static var MAP_LOADING : String	= MAP_FOLDER + "Loading\\";
	 
	 public static var MAP_LOADING_WIDE : String	= MAP_FOLDER + "LoadingWide\\";
	 
	 public static var MAP_POPUP : String	= MAP_FOLDER + "PopUp\\";
	 
	 public static var MAP_RADAR : String = MAP_FOLDER + "Radar\\";
	 
	 public static var MAP_MINI_MAP : String = MAP_FOLDER + "MiniMap\\";
	 
	 //--------------------------------------------------------------------------------------------------------------------
	 
	 public static var MISSION_FOLDER : String = IMAGE_FOLDER + "Mission\\";
	 
	 public static var MISSION_ACHIEVEMENT_FOLDER : String = MISSION_FOLDER + "Achievement\\";
	 
	 public static var MISSION_HIGHLIGHTED_ACHIEVEMENT_FOLDER : String = MISSION_FOLDER + "HighlightedAchievement\\";	 
	 
	 	// thu muc chua cac trang thai danh sach ban be
	public static var STATUS_FOLDER : String			= IMAGE_FOLDER + "IconStatus\\";
	public static var STATUS_FIREND_OLINE : String		= STATUS_FOLDER + "online\\"+"online.png";
	public static var STATUS_FIREND_OFFLINE : String	= STATUS_FOLDER + "offline\\"+"offline.png";
	public static var STATUS_FIREND_PLAY : String		= STATUS_FOLDER + "play\\" + "games.png";
	
	//thu muc chua trang thai cua Email
	public static var Mail_FOLDER : String				= IMAGE_FOLDER + "Mail_icon\\";
	public static var READ_MAIL : String				= Mail_FOLDER + "read\\"+"read.png";
	public static var UNREAD_MAIL : String				= Mail_FOLDER + "unread\\"+"unread.png";
	
	
	//anh cua character
	public static var AVATAR_CHARACTER : String			= IMAGE_FOLDER + "Characters\\";
	//icon play sound
	public static var SOUND_CHARACTER : String			= IMAGE_FOLDER + "IconPlaySoud\\";
	public static var PLAYSOUND : String				= SOUND_CHARACTER + "play\\" + "plays.jpg";
	public static var PAUSESOUND : String				= SOUND_CHARACTER + "pause\\" + "pause.png";
	// clan Avatar
	public static var CLAN : String						= IMAGE_FOLDER + "Clan\\";
	public static var CLAN_AVATAR : String				= CLAN + "ClanAvatar\\";
		// Skill
	public static var SKILL : String					= IMAGE_FOLDER + "Skill\\";
	// room 
	public static var EW_SNIPER : String				= IMAGE_FOLDER + "WeaponType\\"+"sniper.png";
	public static var EW_PISTOL : String				= IMAGE_FOLDER + "WeaponType\\"+"pistol.png";
	public static var EW_KNIFE : String					= IMAGE_FOLDER + "WeaponType\\" + "kinfe.png";
}