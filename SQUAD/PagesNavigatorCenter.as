﻿import gfx.controls.Button;
import gfx.controls.RadioButton;
import gfx.controls.ButtonGroup;
import gfx.controls.SQUAD.ResolutionHelper;
import gfx.core.UIComponent;

/**
 * Component hiển thị thanh điều hướng các trang khi thực hiên phân trang.
 * Người sử dụng tương tác như sau:
 * 		+ Nhìn chung bộ điều hướng gồm có các nút: Trang đầu (F), Trang trước (P), Trang sau (N), Trang cuối (L) và 1 số nút điều hướng trực tiếp tới các trang (PPS)
 * 		+ Nếu ở trang đầu tiên thì F, P không xuất hiện và ngược lại 
 * 		+ Nếu ở trang cuối thì N, L không xuất hiện và ngược lại
 * 		+ Click vào F: Trang đầu tiên
 * 		+ Click vào P: Trang trước đó
 * 		+ Click vào N: Trang tiếp theo
 * 		+ Click vào L: Trang cuối cùng
 * 		+ Click vào 1 trang cụ thể: Toàn bộ bộ điều hướng thay đổi, trang được chọn ở giữa, trước và sau đều có 1 dãy các trang liền kề
 */
class gfx.controls.SQUAD.PagesNavigatorCenter extends UIComponent {	
	//Model
	private var m_iMaxPreviousPages :	Number;// = 2;
	private var m_iMaxNextPages :		Number;// = 2;	
	private var m_iLastPage:					Number;// = 1;
	private var m_iCurrentPage:			Number;// = 1;
	private var m_sBtnPageIdentifier:		String;// = "btnPage";
	
	[Inspectable (name="alignLeft", type="Boolean", defaultValue=true)]
	private var m_bAlignLeft : 				Boolean = true;
	
	private var m_bHideByMovingOut :	Boolean = true;
	private var m_kPagesGroup : 			ButtonGroup;
	
	//View
	private var m_aNavigators:				Array;// = [];
	private var m_aAllNavigators :		Array;// = [];
	private var btnFirst :						Button;
	private var btnLast :						Button;
	private var btnPrevious :					Button;
	private var btnNext :						Button;
	
	/**
	 * Số trang trước tối đa
	 */	
	public function get maxPreviousPages () : Number {
		return m_iMaxPreviousPages;
	}
	
	[Inspectable(type="Number", defaultValue=2)]
	public function set maxPreviousPages (p : Number) : Void {
		if (p != m_iMaxPreviousPages) {
			m_iMaxPreviousPages = p;
			init ();
		}
	}	
		
	/**
	 * Số trang sau tối đa
	 */	
	public function get maxNextPages () : Number {
		return m_iMaxNextPages;
	}
	
	[Inspectable(type="Number", defaultValue=2)]
	public function set maxNextPages (p : Number) : Void {		
		if (p != m_iMaxNextPages) {
			m_iMaxNextPages = p;
			init ();
		}
	}
			
	/**
	 * Linkage Identifier của RadioButton sẽ dùng để làm các nút tới trang trực tiếp
	 */
	[Inspectable(type="String", defaultValue = "btnPage")]
	public function get renderer () : String {
		return m_sBtnPageIdentifier;
	}
	
	public function set renderer (r : String) : Void {		
		if (r != m_sBtnPageIdentifier) {
			m_sBtnPageIdentifier = r;
			init ();
		}
	}
	
	public function PagesNavigatorCenter () {
		super ();
		
		m_aAllNavigators 			= [];
		m_aNavigators 				= [];
		m_iCurrentPage				= 1;
		m_iLastPage					= 1;
		m_kPagesGroup 					= new ButtonGroup ("m_kPagesGroup", this);
		m_kPagesGroup.addEventListener ("change", this, "goTo");	
	}
	
	private function configUI () : Void {
		super.configUI ();
		init ();		
		goToPage (m_iCurrentPage);
		selectPage (m_iCurrentPage);
		
		dispatchEvent ( {type:	"ready", target: this});
	}

	/**
	 * Về trang đầu tiên
	 */
	public function goToFirstPage ():Void {
		goToPage (1);
	}

	/**
	 * Tới trang cuối cùng
	 */
	public function goToLastPage ():Void {		
		goToPage (m_iLastPage);
	}

	/**
	 * Tới trang tiếp theo
	 */
	public function goToNextPage ():Void {		
		if (m_iCurrentPage < m_iLastPage) {
			goToPage (m_iCurrentPage + 1);
		}
	}

	/**
	 * Về trang trước đó
	 */
	public function goToPreviousPage ():Void {		
		if (m_iCurrentPage > 1) {
			goToPage (m_iCurrentPage - 1);
		}
	}

	/**
	 * Tới 1 trang cụ thể
	 * 
	 * @param	evt	Đối tượng đi kèm sự kiện "change" của ButtonGroup của các nút btnPageX
	 */
	private function goTo (evt:Object):Void {
		var page:Number = Math.floor (Number (evt.data));		
		if (isNaN(page) || page < 1 || page > m_iLastPage || m_aAllNavigators.length <= 4) {
			//invalid page or there're only default navigator buttons (F, P, N, L)
			return;
		}		
		if (evt.item instanceof RadioButton && page == m_iCurrentPage) {
			return;
		}		
		//Reset
		while (m_aNavigators.length > 0) {
			m_aNavigators.pop();
		}
		hideAllPageNavigators ();		
		var index:Number = 0;
		//First & Previous
		if (page > 1) {
			m_aNavigators[index++] = btnFirst;
			m_aNavigators[index++] = btnPrevious;
		}			
		//Pages
		var btnPageX:RadioButton;
		var fromPage: Number;
		var toPage: Number;	
		fromPage = page - m_iMaxPreviousPages;
		if (fromPage < 1) {
			fromPage = 1;
		}
		toPage = page + m_iMaxNextPages;
		if (toPage > m_iLastPage) {
			toPage = m_iLastPage;
		}
		for (var i : Number = fromPage; i <= toPage;  ++i ) {
			btnPageX = this["btnPage" + (i - fromPage + 1)];
			btnPageX.data = i;
			btnPageX.label = "" + i;
			if (btnPageX.selected) {
				btnPageX.selected = false;
			}
			m_aNavigators[index++] = btnPageX;
		}
		//Next & Last 
		if (page < m_iLastPage) {
			m_aNavigators[index++] = btnNext;
			m_aNavigators[index++] = btnLast;
		}		
		render ();
		selectPage (page);		
	}

	/**
	 * Tìm vị trí của 1 trang trong danh sách các nút bấm điều hướng
	 * 
	 * @param	page	trang muốn tìm
	 * 
	 * @return		vị trí của (nút bấm) trang trong danh sách m_aNavigators (>=0)
	 */
	private function pageToIndex (page:Number):Number {
		var n:Number = m_aNavigators.length;
		for (var i:Number = 0; i < n; ++i) {
			if (m_aNavigators[i] instanceof RadioButton) {
				if (m_aNavigators[i].data == page) {
					return i;
				}
			}
		}
		return -1;
	}

	/**
	 * Highlight chọn 1 trang.
	 * Đồng thời phát sinh sự kiện "selectPage" có các field sau:
	 * 		+ type			"selectPage"
	 * 		+ target		this
	 * 		+ page			trang được chọn	 
	 * 
	 * @param	page trang muốn chọn
	 */
	private function selectPage (page:Number):Void {
		var index:Number = pageToIndex (page);
		if (index > -1 && index < m_aNavigators.length) {			
			m_iCurrentPage = page;
			if (m_aNavigators[index].selected) {
				m_aNavigators[index].selected = true;
			} else {
				m_aNavigators[index].selected = true;				
				dispatchEvent ( {
					type: 		"selectPage", 
					target: 		this, 
					page: 		m_iCurrentPage
				});
			}			
		}
	}
	
	/**
	 * Xóa tất cả các nút bấm tới trang trực tiếp (để thêm các nút mới do số lượng nút cần thiết bị thay đổi)
	 */
	private function deleteAllPagesButton () : Void {
		var temp : MovieClip;
		var name : String = "";
		while (m_aAllNavigators.length > 0) {
			temp = MovieClip(m_aAllNavigators.pop ());
			name = temp._name;			
			if (temp != btnFirst && temp != btnPrevious && temp != btnNext && temp != btnLast) {
				if (temp instanceof MovieClip) {
					temp.removeMovieClip ();
				}
				delete temp;
			}
		}
	}

	/**
	 * Ẩn toàn bộ các nút điều hướng
	 */
	public function hideAllPageNavigators ():Void {
		for (var x:String in m_aAllNavigators) {
			ResolutionHelper.moveToXY (m_aAllNavigators[x], 0, 0);
			m_aAllNavigators[x]._visible = false;
			m_aAllNavigators[x].disabled = true;
		}
	}

	/**
	 * Khởi tạo PagesNavigator.
	 * Phải được gọi trước khi sử dụng.	 
	 */
	private function init ():Void {		
		//trace ("maxprev = " + maxPreviousPages + ", maxnext = " + maxNextPages + ", lastpage = " + lastPage);
		if (!(btnLast instanceof Button)) {
			return;
		}		
		
		var index : Number = 0;
		deleteAllPagesButton ();
		m_aAllNavigators [index++] = btnFirst;
		m_aAllNavigators [index++] = btnPrevious;
		m_aAllNavigators [index++] = btnNext;
		m_aAllNavigators [index++] = btnLast;
		
		for (var i : Number = 0; i < m_aAllNavigators.length; ++i) {
			m_aAllNavigators[i].removeAllEventListeners ();
		}
		btnFirst.addEventListener ("click",this,"goToFirstPage");
		btnPrevious.addEventListener ("click",this,"goToPreviousPage");
		btnNext.addEventListener ("click",this,"goToNextPage");
		btnLast.addEventListener ("click", this, "goToLastPage");
				
		var btnPageX : MovieClip;// RadioButton;
		for (var i : Number = 1; i<=(m_iMaxPreviousPages+1+m_iMaxNextPages); ++i) {
			btnPageX = attachMovie(m_sBtnPageIdentifier, "btnPage" + i, getNextHighestDepth());
			m_aAllNavigators [index++] = btnPageX;
			btnPageX.group = m_kPagesGroup;
		}		
	}
	
	/**
	 * Lấy trang hiện tại
	 */
	public function get currentPage () : Number {
		return m_iCurrentPage;
	}
	
	
	/**
	 * Tổng số trang
	 */
	public function get lastPage () : Number {
		return m_iLastPage;
	}
	
	public function set lastPage (p : Number) : Void {		
		if (p != m_iLastPage) {
			m_iLastPage = p;
			if (m_iCurrentPage > m_iLastPage) {
				m_iCurrentPage = m_iLastPage;
			}
			refresh ();
		}
	}
	
	/**
	 * Vẽ lại toàn bộ các nút
	 */
	public function refresh () : Void {
		if (m_iCurrentPage < 1) {
			m_iCurrentPage = 1;
		} else if (m_iCurrentPage > m_iLastPage) {
			m_iCurrentPage = m_iLastPage;
		}
		goToPage (m_iCurrentPage);
	}	
	
	/**
	 * Đi tới 1 trang nhất định
	 * 
	 * @param	p	trang muốn tới
	 */
	public function goToPage (p : Number) : Void {
		goTo ({	data	:	p	});
	}	
	
	private function render () : Void {
		for (var x : String in m_aNavigators) {
			m_aNavigators[x]._visible = true;
			m_aNavigators[x].disabled = false;
		}
		if (m_bAlignLeft) {
			ResolutionHelper.moveToXY (m_aNavigators[0],0,0);
			ResolutionHelper.distributeEvenly (ResolutionHelper.DIR_LEFT_TO_RIGHT,m_aNavigators,5);			
		} else {
			m_aNavigators.reverse ();
			ResolutionHelper.moveToXY (m_aNavigators[0],-(m_aNavigators[0]._width),0);
			ResolutionHelper.distributeEvenly (ResolutionHelper.DIR_RIGHT_TO_LEFT,m_aNavigators,5);			
		}
		ResolutionHelper.align (ResolutionHelper.ALIGN_TOP, m_aNavigators);
	}
	
	public function toString () : String {
		return "[CCK - PagesNavigatorCenter "+_name+"]";
	}
	
	//Override
	public function set disabled(value:Boolean):Void {
		_disabled = value;
		super.enabled = !value;
		useHandCursor = !value;		
		var n : Number = m_aAllNavigators.length;
		for (var i : Number = 0 ;  i < n; ++i ) {
			m_aAllNavigators[i].disabled = value;
		}
		invalidate();
	}
}