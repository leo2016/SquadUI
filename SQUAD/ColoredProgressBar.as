﻿import gfx.controls.ProgressBar;

class gfx.controls.SQUAD.ColoredProgressBar extends ProgressBar{
	private var mcRed : 			MovieClip;
	private var mcGreen : 		MovieClip;
	
	public static var COLOR_RED : 		Number = 0;
	public static var COLOR_GREEN :	Number = 1;
	
	public function ColoredProgressBar () {
		super ();
	}
	
	public function setProgress(loaded:Number, total:Number, color : Number):Void {
		super.setProgress (loaded, total);
		
		switch (color) {
			case COLOR_GREEN :
				mcGreen._visible = true;
				mcRed._visible = false;
				break;
			case COLOR_RED:
				mcRed._visible = true;
				mcGreen._visible = false;
				break;
		}
	}
}