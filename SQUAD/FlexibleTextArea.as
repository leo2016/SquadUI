﻿import gfx.controls.TextArea;
class gfx.controls.SQUAD.FlexibleTextArea extends TextArea {	
	private var m_nLineSpacing : 				Number;
	private var m_nOldTextHeight : 			Number;	
	[Inspectable (name = "Padding", type = "Number", defaultValue = 10)]
	private var m_nPadding : 					Number;
	[Inspectable (name = "FlexibleHeight", type = "Boolean", defaultValue = true)]
	private var m_bFlexibleHeight : 			Boolean;
	[Inspectable (name = "Margin", type = "Number", defaultValue = 10)]
	private var m_nMargin : 					Number;
	
	public function FlexibleTextArea () {
		super ();
	}
	
	private function configUI () : Void {
		super.configUI ();
		m_nLineSpacing = defaultTextFormat.leading;
		onChanged (this);
	}
	
	private function onChanged(target:Object):Void {		
		if (m_bFlexibleHeight) {
			if (m_nOldTextHeight != textField.textHeight) {
				m_nOldTextHeight = textField.textHeight;
				height = m_nOldTextHeight + m_nLineSpacing + m_nPadding;
				dispatchChangeSize ();
			}
		}
		super.onChanged (target);
	}
	
	private function dispatchChangeSize () : Void {			
		dispatchEvent ( {
			type : 		"changeSize", 
			target : 	this, 
			y : 			_y,
			height : 	height, 
			margin : 	m_nMargin
		});
	}
	
	public function updateHeight () : Void {
		if (m_bFlexibleHeight) {			
			m_nOldTextHeight = textField.textHeight;
			height = m_nOldTextHeight + m_nLineSpacing + m_nPadding;
			dispatchChangeSize ();
		}
	}
	
	public function get margin () : Number {
		return m_nMargin;
	}
}