import gfx.controls.CheckBox;

class gfx.controls.SQUAD.CheckboxGenerator extends MovieClip {
	public var bg: MovieClip;
	
	private var _itemWidth : Number;
	private var _itemHeight : Number;
	private var _columns : Number;
	private var _rows : Number;
	private var _isHorizontal : Boolean;
	
	private var _itemRenderer : String;
	private var _data : Array;
	private var _renderers : Array;	
	
	public var labelFunction : Function;	
	
	public function CheckboxGenerator () {
		bg._visible = false;
		
		_columns = _rows = 3;
		_isHorizontal = true;
				
		_itemRenderer = "CheckBox";
		_data = [];
		_renderers = [];
		labelFunction = defaultLabelFunction;		
	}
	
	private function defaultLabelFunction (x : Object) : String {
		return x.label;
	}
	
	public function set width (value : Number) : Void {
		_itemWidth = value;
		update ();
	}
	
	public function get width () : Number {
		return _itemWidth;
	}
	
	public function set height (value : Number) : Void {
		_itemHeight = value;
		update ();
	}
	
	public function get height () : Number {
		return _itemHeight;
	}
	
	public function set columns (value : Number) : Void {
		if (value > 0) {
			_columns = value;
			update ();
		}		
	}
	
	public function get columns () : Number {
		return _columns;
	}
	
	public function set rows (value : Number) : Void {
		if (value > 0) {
			_rows = value;
			update ();
		}		
	}
	
	public function get rows () : Number {
		return _rows;
	}
	
	public function set isHorizontal (value : Boolean) : Void {
		if (value == true) {
			if (_columns > 0) {
				_isHorizontal = value;
				update ();
			} else {
				trace ("Error: If you want to generate checkbox horizontally, set 'columns' > 0 first");
			}
		} else {
			if (_rows > 0) {
				_isHorizontal = value;
				update ();
			} else {
				trace ("Error: If you want to generate checkbox vertically, set 'rows' > 0 first");
			}
		}
	}
	
	public function get isHorizontal () : Boolean {
		return _isHorizontal;
	}
	
	public function set dataProvider (data : Array) : Void {
		_data = data;
		update ();
	}
	
	public function get dataProvider () : Array {
		return _data;
	}
	
	public function set itemRenderer (itemRenderer : String) : Void {
		_itemRenderer = itemRenderer;
		update ();
	}
	
	public function get itemRenderer () : String {
		return _itemRenderer;
	}
	
	private function render () : Void {		
		var length: Number = _data.length;
		if (_isHorizontal) {
			for (var z:Number = 0; z < length; ++z ) {
				var i: Number = Math.floor(z / _columns);
				var j: Number = z % _columns;
				var x: Number = j * _itemWidth;				
				var y: Number = i * _itemHeight;
				//trace ("Checkbox '" + _data[z] + "' will be set at position x = " + x + ", y = " + y + "(row "+i+", column "+j+")");
				_renderers[z] = attachMovie (_itemRenderer, _itemRenderer, getNextHighestDepth(), {_x: x, _y:y});
			}
		} else {
			for (var z:Number = 0; z < length; ++z ) {				
				var i: Number = Math.floor(z / _rows);
				var j: Number = z % _rows;
				var x: Number = i * _itemWidth;
				var y: Number = j * _itemHeight;
				//trace ("Checkbox '" + _data[z] + "' will be set at position x = " + x + ", y = " + y + "(row "+i+", column "+j+")");
				_renderers[z] = attachMovie (_itemRenderer, _itemRenderer, getNextHighestDepth(), {_x: x, _y:y});
			}
		}		
	}
	
	public function update () : Void {
		clear ();
		render ();
		populateData ();
	}
	
	private function clear () : Void {
		for (var x:String in _renderers) {			
			_renderers[x].removeMovieClip ();
		}
	}
	
	private function populateData () : Void {
		for (var x:String in _renderers) {												
			_renderers[x].data = _data[x];
			if (_data[x] instanceof String) {
				_renderers[x].label = _data[x];
			} else {
				_renderers[x].label = labelFunction (_data[x]);
			}			
			_renderers[x].addEventListener ("click", this, "traceCheckboxItself");
		}
	}

	private function getSelectedList () : Array {
		var list : Array = [];
		for (var x:String in _renderers) {
			if (_renderers[x].selected) {				
				list.push(_data[x]);
			}
		}
		return list;
	}
	
	 public function check (checkBoxNumber : Number) : Void {
		if (checkBoxNumber >= 0 && checkBoxNumber < _renderers.length) {
			_renderers[checkBoxNumber].selected = true;
		}
	}	
	
	public function uncheck (checkBoxNumber : Number) : Void {
		if (checkBoxNumber >= 0 && checkBoxNumber < _renderers.length) {
			_renderers[checkBoxNumber].selected = false;
		}
	}	
	
	public function findCheckboxByLabel (label : String) : CheckBox {
		for (var x:String in _renderers) {
			if (labelFunction(_data[x]) == label) {
				return _renderers[x];
			}
		}
		return null;
	}
	
	public function getAllCheckboxes():Array {
		return _renderers;
	}
	
	private function traceCheckboxItself (evt : Object) : Void {
		var aCheckbox : CheckBox = evt.target;
		if (aCheckbox instanceof CheckBox) {
			for (var x : String in _renderers) {
				_renderers[x].selected = false;
			}
			aCheckbox.selected = true;
		}
	}
}
