﻿import gfx.controls.ListItemRenderer;
import gfx.controls.UILoader;
import gfx.controls.SQUAD.ResolutionHelper;
import gfx.controls.SQUAD.ImagePathManager

class gfx.controls.SQUAD.CharacterLIR extends ListItemRenderer
{
	//public var id:Number;
	//public var imagePath:String;
	private var characterImage:UILoader;
	private var gender:Number;
	public function CharacterLIR() {
		super();
		//this.addEventListener("click", this, "changeCharacter");
	} 
	public function setData(data:Object):Void {
		if(data != undefined) {
			this.data = data;
			updateAfterStateChange();
			_visible = true;
			_disabled = false;
		} else {
			//characterImage.source = "";
			_visible = false
			_disabled = true;
		}
	}
	public function updateAfterStateChange():Void {
		if (data != undefined) {
			if (characterImage instanceof UILoader)
			{
				characterImage.source = data.imageName;
				//ResolutionHelper.updateUILoader( characterImage, ImagePathManager.INVENTORY_ITEM + data.imageName);
			}
			gender = data.gender;
		}
	}	
	//public function changeCharacter()
	//{
		//if (data != undefined) {
			//_root.loadSoundList(gender);
		//}
	//}
}