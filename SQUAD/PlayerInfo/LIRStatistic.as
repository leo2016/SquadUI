﻿import gfx.controls.Label;
import gfx.controls.ListItemRenderer;

class gfx.controls.SQUAD.PlayerInfo.LIRStatistic extends ListItemRenderer {
	private var labMode:Label;
	private var labKillDeath:Label;
	private var labWinLose:Label;
	
	public function LIRStatistic () {
		super ();	
	}
	
	public function setData(data:Object):Void {
		if(data != undefined) {
			this.data = data;			
			updateAfterStateChange();
			this.visible = true;
			this._disabled = false;
		} else {
			this.visible = false;
			this._disabled = true;
		}
	}
	
	public function updateAfterStateChange():Void {
		if (data != undefined && (labMode instanceof Label)) {
			labMode.text = data.sMode;
			labKillDeath.text = data.iKill + "/" + data.iDeath;
			labWinLose.text = data.iWin +"/" + data.iLose;
		}
	}
}