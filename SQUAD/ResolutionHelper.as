﻿import gfx.controls.UILoader;
class gfx.controls.SQUAD.ResolutionHelper {
	private static var scale4x3 : Number 	= 4 / 3;
	private static var scale16x9 : Number	= 16 / 9;

	private function ResolutionHelper () {}
	
	public static function is4x3 (width : Number, height : Number) : Boolean {
		return (Math.abs (width/height - scale4x3) < 0.2);
	}

	public static function is16x9 (width : Number, height : Number) : Boolean {
		return (Math.abs (width/height - scale16x9) < 0.2);
	}
	
	/* *
	* Để các component của Scaleform được khởi tạo hay nói cách khác là đặt các movieclip vào các Stage hoặc di chuyển movieclip đó ra ngoài Stage.
	* Lý do tạo file: Với những trường hợp thao tác  muốn làm ẩn/ hiện movieclip
	* - Để MovieClip xuất hiện, load các item(text, image...v..vv..) nhanh
	* - Khi ẩn đi vẫn có thể thay đổi trạng thái của các component trên MovieClip đó (VD: thay đổi nội dung, hình ảnh..vv..v)
	*  Thay vì visible=true/ false thì dùng mẹo bằng cách di chuyển MovieClip đó ra ngoài hay vào trong Stage. Điều này đảm bảo MovieClip luôn luôn có mặt, 
	*  chỉ là nằm trong hay ngoài Stage (khung hình) -> chỉ cần khởi tạo 1 lần duy nhất -> lúc "hiển thị" 
	* Movie này luôn rất nhanh vì đã được khởi tạo và chỉ di chuyển vào khung hình thôi.
	* */

	/*
	 * Để di chuyển MovieClip vào vị trí nào trong Stage không nhất thiết phải nhớ tọa độ cần đặt movie clip đó
	 * mà chỉ cần đặt 1 movie clip trong suốt tại vị trí đó rồi dùng hàm moveTo
	 * */
	
	public static function moveTo(movie:MovieClip, moveHit:MovieClip) {
		//trace("TOA DO XYZ!!!!!!!" +  moveHit._x + " - " + moveHit._y);
		moveToXY (movie,moveHit._x,moveHit._y);
	}
	
	/**
	 * Dịch chuyển MovieClip tới 1 tọa độ cụ thể
	 * 
	 * @param	movie	MovieClip muốn di chuyển
	 * @param	x			tọa độ x (ngang, càng lớn càng về bên phải màn hình)
	 * @param	y			tọa độ y (dọc, càng lớn càng xuống phía dưới màn hình)
	 */
	public static function moveToXY(movie:MovieClip, x:Number,y:Number):Void {
		movie._x = x;
		movie._y = y;
	}
	
	/*
	 * Di chuyển movieclip ra khỏi Stage
	 * */
	public static function moveOut(movie:MovieClip):Void {
		moveToXY(movie, Stage.width + movie._width + 1, Stage.height + movie._height + 1);
	}
	
	/**
	 * Kiểm tra xem MovieClip có thể nhìn thấy (dù chỉ một phần) không
	 * 
	 * @param	movie	MovieClip muốn kiểm tra
	 * 
	 * @return		true nếu movie có thể nhìn thấy, false nếu không thể nhìn thấy bất kỳ phần nào của movie
	 */
	public static function isVisibleInStage (movie : MovieClip) : Boolean {
		var globalPos : Object = { x : 0 , y : 0 };
		movie.localToGlobal (globalPos);
		if (globalPos.x < -movie._width || globalPos.x > Stage.width) {
			return false;
		}
		if (globalPos.y < -movie._height || globalPos.y > Stage.height) {
			return false;
		}
		return true;
	}
	
	/**
	 * Kiểm tra xem có thể nhìn thấy toàn bộ MovieClip không
	 * 
	 * @param	movie	MovieClip muốn kiểm tra
	 * 
	 * @return		true nếu toàn bộ movie có thể được nhìn thấy, false nếu không
	 */
	public static function isAllVisibleInStage (movie : MovieClip) : Boolean {		
		if (isVisibleInStage(movie)) {
			var globalPos : Object = { x : 0 , y : 0 };
			movie.localToGlobal (globalPos);
			if (globalPos.x + movie._width > Stage.width) {
				return false;
			}
			if (globalPos.y + movie._height > Stage.height) {
				return false;
			}
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Cập nhật hình ảnh trong UILoader một cách tối ưu:
	 * 		Chỉ cập nhật nếu đường dẫn khác "" và undefined
	 * 		Chỉ cập nhật nếu UILoader đang được hiển thị
	 * 		Chỉ cập nhật nếu hình ảnh hiện tại khác với hình ảnh sẽ được cập nhật
	 * 
	 * @param	uiLoader		UILoader sẽ chứa hình ảnh
	 * @param	imgPath		Đường dẫn hình ảnh mới
	 */
	public static function updateUILoader (uiLoader: UILoader, imgPath : String) : Void {
		if (imgPath != "" && imgPath != undefined) {//Need to show image
			if (uiLoader.visible) {//UILoader is visible already				
				if (uiLoader.source != imgPath) {//Update if needed
					uiLoader.source = imgPath;
					//trace ("Change source of "+uiLoader._parent + "->" + uiLoader + " to " + imgPath);
				}
			} else {//UILoader is invisible, so update image
				uiLoader.visible = true;
				uiLoader.source = imgPath;
				if (!uiLoader.visible) {
					uiLoader.visible = true;
				}
				//trace (uiLoader._parent + "->" + uiLoader + " visible = " + uiLoader.visible + " to " + imgPath);
			}
		} else {
			uiLoader.unload();
			uiLoader.visible = false;
		}
	}
	
	/**
	 * Kiểm tra xem con chuột có đang ở trên MovieClip không.
	 * Ngay cả khi MovieClip bị đè bởi nhiều MovieClip khác thì vẫn tính là con chuột có nằm trên MovieClip.
	 * MovieClip có hình dạng đặc biệt, khác với hình chữ nhật, thì chỉ có những vùng thuộc MovieClip mới được tính.
	 * 
	 * @param	mc	MovieClip muốn kiểm tra
	 * @return		true nếu con chuột nằm trên MovieClip, false nếu ngược lại
	 */
	public static function isMouseOver (mc : MovieClip) : Boolean {
		return mc.hitTest (_root._xmouse, _root._ymouse, true);
	}
	
	public static var DIR_LEFT_TO_RIGHT : Number 	= 0;
	public static var DIR_RIGHT_TO_LEFT : Number		= 1;
	public static var DIR_TOP_TO_BOTTOM : Number = 2;
	public static var DIR_BOTTOM_TO_TOP : Number	= 3;
	
	/**
	 * Sắp xếp các MovieClip cách đều nhau
	 * 
	 * @param	direction		Hướng sắp xếp (sử dụng ResolutionHelper.DIR_....)
	 * @param	movieClips	Danh sách các MovieClip sẽ được sắp xếp 
	 * 										Các MovieClip nên nằm ở Stage hoặc trong cùng 1 MovieClip, vì các tọa độ sắp xếp là cục bộ, 
	 * 										MovieClip đầu tiên)
	 * @param	padding
	 */
	public static function distributeEvenly (direction : Number, movieClips : Array, padding : Number) : Void {
		var n : Number = movieClips.length;
		var mc : MovieClip;
		var suitablePos : Number = undefined;		
		switch (direction) {
			case DIR_LEFT_TO_RIGHT : {
				for (var i : Number = 0 ; i < n ; ++i ) {
					mc = movieClips[i];
					if (mc instanceof MovieClip) {
						if (suitablePos != undefined) {//not the 1st mc
							mc._x = suitablePos;							
						}
						suitablePos = mc._x + mc._width + padding;
					}
				}
				break;
			}
			case DIR_RIGHT_TO_LEFT : {
				for (var i : Number = 0 ; i < n ; ++i ) {
					mc = movieClips[i];
					if (mc instanceof MovieClip) {
						if (suitablePos != undefined) {//not the 1st mc
							mc._x = suitablePos - padding - mc._width;
						}
						suitablePos = mc._x;
					}
				}
				break;
			}
			case DIR_TOP_TO_BOTTOM : {
				for (var i : Number = 0 ; i < n ; ++i ) {
					mc = movieClips[i];
					if (mc instanceof MovieClip) {
						if (suitablePos != undefined) {//not the 1st mc
							mc._y = suitablePos;							
						}
						suitablePos = mc._y + mc._height + padding;
					}
				}
				break;
			}
			case DIR_BOTTOM_TO_TOP : {
				for (var i : Number = 0 ; i < n ; ++i ) {
					mc = movieClips[i];
					if (mc instanceof MovieClip) {
						if (suitablePos != undefined) {//not the 1st mc
							mc._y = suitablePos - padding - mc._width;
						}
						suitablePos = mc._y;
					}
				}
				break;
			}
		}		
	}
	
	public static var ALIGN_TOP : Number			= 0;
	public static var ALIGN_RIGHT : Number		= 1;
	public static var ALIGN_BOTTOM : Number	= 2;
	public static var ALIGN_LEFT : Number 		= 3;	
	
	/**
	 * Dóng hàng cho các MovieClip
	 * 
	 * @param	alignMode	Dóng theo chiều nào (sử dụng ResolutionHelper.ALIGN_...)
	 * @param	movieClips	Danh sách các MovieClip, trong đó MovieClip đầu tiên sẽ được dùng làm chuẩn.
	 */
	public static function align (alignMode : Number, movieClips : Array) : Void {
		var n : Number = movieClips.length;
		var mc : MovieClip;
		var suitablePos : Number = undefined;		
		switch (alignMode) {
			case ALIGN_TOP : {
				for (var i : Number = 0 ; i < n ; ++i ) {
					mc = movieClips[i];
					if (mc instanceof MovieClip) {
						if (suitablePos == undefined) {//1st mc
							suitablePos = mc._y;							
						} else {
							mc._y = suitablePos;
						}
					}
				}
				break;
			}
			case ALIGN_RIGHT : {
				for (var i : Number = 0 ; i < n ; ++i ) {
					mc = movieClips[i];
					if (mc instanceof MovieClip) {
						if (suitablePos == undefined) {//1st mc
							suitablePos = mc._x + mc._width;
						} else {
							mc._x = suitablePos - mc._width;
						}						
					}
				}
				break;
			}
			case ALIGN_BOTTOM : {
				for (var i : Number = 0 ; i < n ; ++i ) {
					mc = movieClips[i];
					if (mc instanceof MovieClip) {
						if (suitablePos == undefined) {//1st mc
							suitablePos = mc._y + mc._height;
						} else {
							mc._y = suitablePos - mc._height;
						}
					}
				}
				break;
			}
			case ALIGN_LEFT : {
				for (var i : Number = 0 ; i < n ; ++i ) {
					mc = movieClips[i];
					if (mc instanceof MovieClip) {
						if (suitablePos == undefined) {//1st mc
							suitablePos = mc._x;
						} else {
							mc._x = suitablePos;
						}						
					}
				}
				break;
			}
		}
	}
	
	public function toString () : String {
		return "[CCK - ResolutionHelper "+_name+"]";
	}
}