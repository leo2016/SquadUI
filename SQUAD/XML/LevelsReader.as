﻿/**
 * ...
 * @author ...
 */
import gfx.controls.SQUAD.Data.Level;
import gfx.controls.SQUAD.XML.BaseReader;

class gfx.controls.SQUAD.XML.LevelsReader extends BaseReader
{
	private var m_aLevels:Array;
	
	/**
	 * Doc toan bo du lieu trong file XML
	 * @param	sFileName: Ten file XML
	 */
	public function LevelsReader(sFileName:String)
	{
		super();
		m_aLevels = [];
		addEventListener("open", readLevels);
		loadFile(sFileName);
	}
	
	/**
	 * Bắt đầu đọc dữ liệu từ file XML. 
	 * Đọc từng bản ghi một bằng hàm readCommonItem. 
	 * Khi đọc xong thì sinh sự kiện "complete".
	 * 
	 * @param	evt eventObject bắt được từ sự kiện "open"
	 */
	public function readLevels(evt:Object):Void
	{
		var item:XMLNode = m_kRoot.firstChild;
		do {
			readLevel(item);
			item = item.nextSibling;
		}while (item != undefined)		
		// dispatche
		dispatchEvent( { type:"complete", target:this, result:m_aLevels } );
	}

	/**
	 * Đọc bản ghi. 
	 * Một bản ghi đọc từ XML vào là 1 instance của class BaseItem
	 * 
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	public function readLevel(itemRoot:XMLNode):Void {
		var properties:Array = convertIndices(itemRoot.childNodes);
		m_aLevels[properties["ID"].firstChild.nodeValue] = new Level(
		properties["ID"].firstChild.nodeValue, 
		properties["NAME"].firstChild.nodeValue, 
		properties["IMAGE"].firstChild.nodeValue
		);
	}
		/**
	 * Lấy danh sách các bản ghi đọc được từ XML. (Chỉ sử dụng hàm này sau khi có sự kiện "complete"). 	 
	 * 
	 * @return Danh sách các bản ghi
	 */
	public function getLevels ():Array {
		return m_aLevels;
	}
}