﻿import gfx.controls.SQUAD.XML.BaseReader;

/**
 * Class đọc dữ liệu bổ sung cho GIÁ TIỀN từ XML.
 * Đọc ngay khi khởi tạo instance mới nhưng phải bắt sự kiện "complete" rồi chờ tới khi đọc xong mới nhận được dữ liệu để xử lý.
 */
class gfx.controls.SQUAD.XML.PricesReader extends BaseReader {
	private var m_aPrices: Array;
	
	/**
	 * Hàm tạo và mở file ngay lập tức.
	 * Ngay khi file được mở xong, bắt sự kiện "open" và đọc ngay.
	 *
	 * @param	sFileName đường dẫn file XML
	 */
	public function PricesReader (sFileName: String) {
		super();
		
		m_aPrices = [];
		addEventListener ("open", readPrices);
		loadFile (sFileName);
	}
	
	/**
	 * Bắt đầu đọc dữ liệu từ file XML.
	 * Đọc từng bản ghi một bằng hàm readPrice.
	 * Khi đọc xong thì sinh sự kiện "complete".
	 *
	 * @param	evt eventObject bắt được từ sự kiện "open"
	 */
	private function readPrices (evt: Object):Void {
		var item: XMLNode = m_kRoot.firstChild;
		do {
			readPrice (item);
			item = item.nextSibling;
		} while (item != undefined) ;
		dispatchEvent ( { type: "complete", target: this, result: m_aPrices } ) ;
	}
	
	/**
	 * Đọc bản ghi.
	 * Một bản ghi đọc từ XML vào có các trường:
	 * + id
	 * + prices, là 1 ma trận 2 chiều nx2
	 * 		Với chỉ số hàng là số ngày thuê (mua đứt thì chỉ số = -1)
	 * 		Chỉ số cột là "GP", "VCOIN"
	 *
	 * Chẳng hạn để lấy giá GP khi thuê 3 ngày thì dùng prices[3]["GP"]
	 * Chẳng hạn để lấy giá VCOIN khi mua đứt thì dùng prices[-1]["VCOIN"]
	 *
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	private function readPrice (itemRoot: XMLNode):Void {
		var properties:Array = convertIndices (itemRoot.childNodes);
		
		var xmlPrices: Array = properties["PRICES"].childNodes;
		var length: Number = xmlPrices.length;
		var prices:Array = [];
		for (var i:Number = 0; i < length; ++i ) {
			var xmlPrice: Array = convertIndices (xmlPrices[i].childNodes);
			var rentingDays: Number = xmlPrice["RENTING_DAYS"].firstChild.nodeValue;
			if (rentingDays == undefined || rentingDays == null) {
				rentingDays = -1;
			}
			prices[rentingDays] = [];
			prices[rentingDays]["GP"] = xmlPrice["GP"].firstChild.nodeValue;
			prices[rentingDays]["VCOIN"] = xmlPrice["VCOIN"].firstChild.nodeValue;						
		}
		
		m_aPrices [properties["ID"].firstChild.nodeValue] = {
			id: 		properties["ID"].firstChild.nodeValue,
			prices:	prices
		};		
	}
	
	/**
	 * Lấy danh sách các bản ghi đọc được từ XML. (Chỉ sử dụng hàm này sau khi có sự kiện "complete").
	 * Các bản ghi này chỉ là dữ liệu bổ sung nên phải đợi đọc xong "Common.xml" mới ghép được dữ liệu bằng phương thức attachPrices
	 *
	 * @return Danh sách các bản ghi
	 */
	public function getPrices ():Array {
		return m_aPrices;
	}
}