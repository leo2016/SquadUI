﻿import gfx.controls.SQUAD.XML.BaseReader;

/**
 * Class đọc dữ liệu cho nội dung của phần Serious Known Bugs
 * Đọc ngay khi khởi tạo instance mới nhưng phải bắt sự kiện "complete" rồi chờ tới khi đọc xong mới nhận được dữ liệu để xử lý.
 */
class gfx.controls.SQUAD.XML.SKBReader extends BaseReader {
	private var m_aBugs : Array;
	
	/**
	 * Hàm tạo và mở file ngay lập tức.
	 * Ngay khi file được mở xong, bắt sự kiện "open" và đọc ngay.
	 *
	 * @param	sFileName đường dẫn file XML
	 */
	public function SKBReader (sFileName: String) {
		super();
		
		m_aBugs = [];
		addEventListener ("open", readBugs);
		loadFile (sFileName);
	}
	
	/**
	 * Bắt đầu đọc dữ liệu từ file XML.	 
	 * Hoàn tất thì sinh sự kiện "complete".
	 *
	 * @param	evt eventObject bắt được từ sự kiện "open"
	 */
	private function readBugs (evt: Object):Void {
		var item: XMLNode = m_kRoot.firstChild;
		do {
			readBug (item);
			item = item.nextSibling;
		} while (item != undefined) ;
		dispatchEvent ( { type: "complete", target: this, result: m_aBugs } ) ;
	}	
	
	/**
	 * Đọc bản ghi.
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	private function readBug (itemRoot: XMLNode):Void {
		var properties:Array = convertIndices (itemRoot.childNodes);
		
		m_aBugs.push ( {
			description 	:	properties["DESCRIPTION"].firstChild.nodeValue, 
			context			:	properties["CONTEXT"].firstChild.nodeValue, 
			type				:	properties["TYPE"].firstChild.nodeValue
		});		
	}
	
	public function getBugs () : Array {
		return m_aBugs;
	}
}