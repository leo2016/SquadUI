﻿import gfx.controls.SQUAD.StringUtility;
import gfx.controls.SQUAD.XML.BaseReader;

/**
 * Class thực hiện bước "Localizing" từ XML
 */
class gfx.controls.SQUAD.XML.Localizer extends BaseReader {
	public var m_aMap :									Array;	//Map chuyển đổi giữa tag và text hiển thị trên GUI
	public static var LOCALIZATION_FOLDER :	String = "XML/Localization/";
	
	/**
	 * Hàm tạo.
	 * Tự động mở file và đọc, chờ sự kiện "complete" để thực hiện các bước tiếp theo
	 * 
	 * @param	sFileName		Đường dẫn tới file "Locallizing"	 
	 */
	public function Localizer (sFileName : String) {
		super();
		
		m_aMap = [];
		addEventListener ("open", readContent);
		loadFile (sFileName);
	}
	
	/**
	 * Hàm đọc nội dung từ file XML sau khi có thông báo đã tìm và mở được file
	 * 
	 * @param	evt	Biến lưu giữ thông tin về thông báo đã tìm và mở được file
	 */
	private function readContent (evt: Object):Void {
		var temp : Array = m_kRoot.childNodes;
		var length : Number = temp.length;
		for (var i : Number  = 0 ; i < length ; ++i ) {
			//m_aMap[TAG] = NODE_VALUE
			m_aMap [temp[i].nodeName] = temp[i].firstChild.nodeValue;
		}
		
		dispatchEvent ( { type: "complete", target: this, result: this} ) ;
	}
	
	/**
	 * Hàm chuyển đổi từ tag sang text (và đồng thời thay thế các biến bằng giá trị của chúng ngay)
	 * 
	 * @param	tag					Tag
	 * @param	applyingArray	Mảng chứa danh sách các biến và giá trị của biến để thay thế.
	 * 											Mảng này có cấu trúc như sau:
	 * 												Mỗi phần tử là 1 Object bao gồm 2 trường : 
	 * 													varName :		Tên biến sẽ tìm để thay thế
	 * 													varValue :		Giá trị sẽ thay thế vào vị trí của tên biến
	 * 												Nếu được, tốt nhất cả 2 trường này đều nên là kiểu String
	 * 
	 * @return		text đã được chuyển đổi và thay thế giá trị biến
	 */
	public function localize (tag : String, applyingArray : Array) : String {
		if (m_aMap[tag] == undefined) {			
			return "NOT FOUND TAG";
		}
		if (applyingArray == undefined || applyingArray == null || applyingArray.length < 1) {			
			return m_aMap[tag];
		} else {
			return applyVars (m_aMap[tag], applyingArray);
		}
	}

	/**
	 * Hàm thay thế giá trị của biến vào vị trí của tên biến 
	 * 
	 * @param	text			Chuỗi mà hàm sẽ thực hiện tìm kiếm và thay thế
	 * @param	varName	Tên biến sẽ tìm để thay thế
	 * @param	varValue	Giá trị sẽ thay thế vào vị trí của tên biến
	 * 
	 * @return		text đã được chuyển đổi và thay thế giá trị biến
	 */
	public function applyVar (text : String, varName : String, varValue : String) : String {
		return StringUtility.replaceAll (text, varName, varValue);
	}
	
	/**
	 * Hàm thay thế giá trị của biến vào vị trí của tên biến 
	 * 
	 * @param	text					Chuỗi mà hàm sẽ thực hiện tìm kiếm và thay thế
	 * @param	applyingArray	Mảng chứa danh sách các biến và giá trị của biến để thay thế.
	 * 											Mảng này có cấu trúc như sau:
	 * 												Mỗi phần tử là 1 Object bao gồm 2 trường : 
	 * 													varName :		Tên biến sẽ tìm để thay thế
	 * 													varValue :		Giá trị sẽ thay thế vào vị trí của tên biến
	 * 												Nếu được, tốt nhất cả 2 trường này đều nên là kiểu String
	 * 
	 * @return		text đã được chuyển đổi và thay thế giá trị biến
	 */
	public function applyVars (text : String, applyingArray : Array) : String {
		for (var x : String in applyingArray) {
			text = StringUtility.replaceAll (text, applyingArray[x].varName, applyingArray[x].varValue);
		}
		return text;
	}
	
	/**
	 * Nối thêm dữ liệu LOCALIZATION đọc được từ file XML khác
	 * 
	 * @param	addedMap	dữ liệu thêm vào
	 */
	public function append (addedMap : Array) : Void {
		for (var x : String in addedMap) {				
			m_aMap[x] = addedMap[x];
		}		
	}
}

//-------------------------------------------------------------------------
//Ví dụ sử dụng
//-------------------------------------------------------------------------
/*
import gfx.controls.SQUAD.XML.Localizer;

var l : Localizer = new Localizer (Localizer.LOCALIZATION_FOLDER + "PlayerInfo.xml");
l.addEventListener ("complete", this, "test");

function test () : Void {
	var cach : Number = 3;
	
	switch (cach) {
		case 1: {
			var s2 : String = l.localize ("TEST");
			s2 = l.applyVar (s2, "**1**", "Thaogau");
			s2 = l.applyVar (s2, "**ITEM_PRICE**", "33000 GP");
			break;
		}
		case 2: {
			var s2 : String = l.localize ("TEST");
			var aa : Array = [
				{varName:"**1**", varValue:"thaogau"}, 
				{varName:"**ITEM_PRICE**", varValue:"33000 GP"}
			];
			s2 = l.applyVars (s2, aa);
			break;
		}
		case 3: {
			var aa : Array = [
				{varName:"**1**", varValue:"thaogau"}, 
				{varName:"**ITEM_PRICE**", varValue:"33000 GP"}
			];
			var s2 : String = l.localize ("TEST", aa);
			break;
		}		
	}
	
	trace (s2);	
}
*/