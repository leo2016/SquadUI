﻿import gfx.controls.SQUAD.XML.BaseReader;
import gfx.controls.SQUAD.Data.Catalogue;

/**
 * Class đọc các Catalogue từ XML.
 * Đọc ngay khi khởi tạo instance mới nhưng phải bắt sự kiện "complete" rồi chờ tới khi đọc xong mới nhận được dữ liệu để xử lý.
 */
class gfx.controls.SQUAD.XML.CataloguesReader extends BaseReader {
	private var m_aCatalogues: Array;
	
	/**
	 * Hàm tạo và mở file ngay lập tức.
	 * Ngay khi file được mở xong, bắt sự kiện "open" và đọc ngay.
	 *
	 * @param	sFileName đường dẫn file XML
	 */
	public function CataloguesReader (sFileName: String) {
		super();
		
		m_aCatalogues = [];
		addEventListener ("open", readCatalogues);
		loadFile (sFileName);
	}
	
	/**
	 * Bắt đầu đọc dữ liệu từ file XML.
	 * Đọc từng bản ghi một bằng hàm readCatalogue.
	 * Khi đọc xong thì gọi hàm buildCatalogueTree để xây dựng cây phân cấp Catalogue.
	 * Hoàn tất thì sinh sự kiện "complete".
	 *
	 * @param	evt eventObject bắt được từ sự kiện "open"
	 */
	private function readCatalogues (evt: Object):Void {
		var item: XMLNode = m_kRoot.firstChild;
		do {
			readCatalogue (item);
			item = item.nextSibling;
		} while (item != undefined) ;
		buildCatalogueTree ();
		dispatchEvent ( { type: "complete", target: this, result: m_aCatalogues } ) ;
	}
	
	/**
	 * Đọc bản ghi.
	 * Một bản ghi đọc từ XML vào là một instance của class Catalogue
	 *
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	private function readCatalogue (itemRoot: XMLNode):Void {
		var properties:Array = convertIndices (itemRoot.childNodes);
		m_aCatalogues [properties["ID"].firstChild] = new Catalogue (
			properties["ID"].firstChild.nodeValue,
			properties["PARENT"].firstChild.nodeValue,
			properties["NAME"].firstChild.nodeValue,
			properties["TAGS"].firstChild.nodeValue,
			properties["DISPLAY"].firstChild.nodeValue == true
		);
	}
	
	/**
	 * Xây dựng cây phân cấp Catalogue.
	 * Mỗi Catalogue đã có ID của Catalogue cha nhưng chưa có danh sách các Catalogue con.
	 * Dựa vào dữ liệu đọc được, xây dựng danh sách các Catalogue con của tất cả các Catalogue.
	 */
	private function buildCatalogueTree ():Void {
		for (var id:String in m_aCatalogues) {
			m_aCatalogues[m_aCatalogues[id].parent()].addChild (m_aCatalogues[id]);
		}
	}
	
	/**
	 * Lấy danh sách các bản ghi đọc được từ XML. (Chỉ sử dụng hàm này sau khi có sự kiện "complete").
	 * Phải đợi đọc xong cả Catalogue và Common để gọi phân loại các item vào các catalogue nhằm tránh việc lọc nhiều lần các item trong các catalogue khác nhau.
	 * Phân loại các item bằng Catalogue.classify ( .. )
	 *
	 * @return Danh sách các catalogue đã được phân cấp
	 */
	public function getAllCatalogues ():Array {
		return m_aCatalogues;
	}
}