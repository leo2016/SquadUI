﻿/**
 * ...
 * @author ...
 */
import gfx.controls.SQUAD.XML.BaseReader;
import gfx.controls.SQUAD.Data.WeaponInGame;
import gfx.events.EventDispatcher;
class gfx.controls.SQUAD.XML.IconWeaponInGameReader extends BaseReader
{
	private var m_pWeaponInGame:Array;
	
	/**
	 * Doc toan bo du lieu trong file XML
	 * @param	sFileName: Ten file XML
	 */
	public function IconWeaponInGameReader(sFileName:String)
	{
		super();
		m_pWeaponInGame = [];
		addEventListener("open", readIcons);
		loadFile(sFileName);
	}
	
	/**
	 * Bắt đầu đọc dữ liệu từ file XML. 
	 * Đọc từng bản ghi một bằng hàm readCommonItem. 
	 * Khi đọc xong thì sinh sự kiện "complete".
	 * 
	 * @param	evt eventObject bắt được từ sự kiện "open"
	 */
	public function readIcons(evt:Object):Void
	{
		var item:XMLNode = m_kRoot.firstChild;
		do {
			readIcon(item);
			item = item.nextSibling;
		}while (item != undefined)
		if (m_pWeaponInGame[0] == undefined)
		{
			m_pWeaponInGame.slice(1,0);
		}		
		// dispatche
		dispatchEvent( { type:"complete", target:this, result:m_pWeaponInGame } );
	}

	/**
	 * Đọc bản ghi. 
	 * Một bản ghi đọc từ XML vào là 1 instance của class BaseItem
	 * 
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	public function readIcon(itemRoot:XMLNode):Void {
		var properties:Array = convertIndices(itemRoot.childNodes);
		m_pWeaponInGame[properties["ID"].firstChild.nodeValue] = new WeaponInGame(
		properties["ID"].firstChild.nodeValue, 
		properties["LABEL"].firstChild.nodeValue, 
		properties["IMG_KILLDEATH"].firstChild.nodeValue, 
		properties["IMG_SHOWWEAPON"].firstChild.nodeValue, 
		properties["IMG_INFOWINDOW"].firstChild.nodeValue
		);
	}
		/**
	 * Lấy danh sách các bản ghi đọc được từ XML. (Chỉ sử dụng hàm này sau khi có sự kiện "complete"). 	 
	 * 
	 * @return Danh sách các bản ghi
	 */
	public function getWeaponInGame ():Array {
		return m_pWeaponInGame;
	}
}