﻿import gfx.controls.SQUAD.XML.BaseReader;

/**
 * Class đọc dữ liệu bổ sung cho SÚNG từ XML.
 * Đọc ngay khi khởi tạo instance mới nhưng phải bắt sự kiện "complete" rồi chờ tới khi đọc xong mới nhận được dữ liệu để xử lý.
 */
class gfx.controls.SQUAD.XML.GunsReader extends BaseReader {
	private var m_aGuns: Array;
	
	/**
	 * Hàm tạo và mở file ngay lập tức.
	 * Ngay khi file được mở xong, bắt sự kiện "open" và đọc ngay.
	 *
	 * @param	sFileName đường dẫn file XML
	 */
	public function GunsReader (sFileName: String) {
		super();
		
		m_aGuns = [];
		addEventListener ("open", readGuns);
		loadFile (sFileName);
	}
	
	/**
	 * Bắt đầu đọc dữ liệu từ file XML.
	 * Đọc từng bản ghi một bằng hàm readGun.
	 * Khi đọc xong thì sinh sự kiện "complete".
	 *
	 * @param	evt eventObject bắt được từ sự kiện "open"
	 */
	private function readGuns (evt: Object):Void {
		var item: XMLNode = m_kRoot.firstChild;
		do {
			readGun (item);
			item = item.nextSibling;
		} while (item != undefined) ;
		dispatchEvent ( { type: "complete", target: this, result: m_aGuns } ) ;
	}
	
	/**
	 * Đọc bản ghi.
	 * Một bản ghi đọc từ XML vào có các trường:
	 * + id
	 * + ammos
	 * + speed
	 * + recoil
	 * + accuracy
	 * + minRank
	 * + slots, mỗi phần tử gồm các trường
	 * 		+ type
	 * 		+ defaultAddOn	 
	 *
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	private function readGun (itemRoot: XMLNode):Void {
		var properties:Array = convertIndices (itemRoot.childNodes);
		
		var xmlSlots: Array = properties["SLOTS"].childNodes;
		var length: Number = xmlSlots.length;
		var slots:Array = [];
		for (var i:Number = 0; i < length; ++i ) {
			slots.push ( {
				type: 				xmlSlots[i].attributes.TYPE,
				defaultAddOn: 	xmlSlots[i].attributes.DEFAULT_ADD_ON
			} );
		}
		
		m_aGuns [properties["ID"].firstChild.nodeValue] = {
			id: 							properties["ID"].firstChild.nodeValue,
			ammos:					properties["AMMOS"].firstChild.nodeValue,
			speed: 						properties["SPEED"].firstChild.nodeValue,
			recoil: 						properties["RECOIL"].firstChild.nodeValue,
			accuracy: 					properties["ACCURACY"].firstChild.nodeValue,
			minRank: 					properties["MIN_RANK_REQUIRED"].firstChild.nodeValue,
			damageAmplifying: 	properties["DAMAGE_AMPLIFYING"].firstChild.nodeValue,
			reloadTime:				properties["RELOAD_TIME"].firstChild.nodeValue,
			slots: 						slots
		};
	}
	
	/**
	 * Lấy danh sách các bản ghi đọc được từ XML. (Chỉ sử dụng hàm này sau khi có sự kiện "complete").
	 * Các bản ghi này chỉ là dữ liệu bổ sung nên phải đợi đọc xong "Common.xml" mới ghép được dữ liệu.
	 *
	 * @return Danh sách các bản ghi
	 */
	public function getAllGuns ():Array {
		return m_aGuns;
	}
}