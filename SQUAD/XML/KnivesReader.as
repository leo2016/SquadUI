﻿import gfx.controls.SQUAD.XML.BaseReader;

/**
 * Class đọc dữ liệu bổ sung cho DAO từ XML.
 * Đọc ngay khi khởi tạo instance mới nhưng phải bắt sự kiện "complete" rồi chờ tới khi đọc xong mới nhận được dữ liệu để xử lý.
 */
class gfx.controls.SQUAD.XML.KnivesReader extends BaseReader {
	private var m_aKnives: Array;
	
	/**
	 * Hàm tạo và mở file ngay lập tức.
	 * Ngay khi file được mở xong, bắt sự kiện "open" và đọc ngay.
	 *
	 * @param	sFileName đường dẫn file XML
	 */
	public function KnivesReader (sFileName: String) {
		super();
		
		m_aKnives = [];
		addEventListener ("open", readKnives);
		loadFile (sFileName);
	}
	
	/**
	 * Bắt đầu đọc dữ liệu từ file XML.
	 * Đọc từng bản ghi một bằng hàm readKnife.
	 * Khi đọc xong thì sinh sự kiện "complete".
	 *
	 * @param	evt eventObject bắt được từ sự kiện "open"
	 */
	private function readKnives (evt: Object):Void {
		var item: XMLNode = m_kRoot.firstChild;
		do {
			readKnife (item);
			item = item.nextSibling;
		} while (item != undefined) ;
		dispatchEvent ( { type: "complete", target: this, result: m_aKnives } ) ;
	}
	
	/**
	 * Đọc bản ghi.
	 * Một bản ghi đọc từ XML vào có các trường:
	 * + id
	 * + baseDamage
	 * + speed
	 * + length
	 * + specialModeDamage
	 * + specialModeSpeed
	 *
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	private function readKnife (itemRoot: XMLNode):Void {
		var properties:Array = convertIndices (itemRoot.childNodes);
		m_aKnives [properties["ID"].firstChild.nodeValue] = {
			id:								properties["ID"].firstChild.nodeValue,
			baseDamage:				properties["BASE_DAMAGE"].firstChild.nodeValue,
			speed:							properties["SPEED"].firstChild.nodeValue,
			length:							properties["LENGTH"].firstChild.nodeValue,
			specialModeDamage:	properties["SPECIAL_MODE_DAMAGE"].firstChild.nodeValue,
			specialModeSpeed:		properties["SPECIAL_MODE_SPEED"].firstChild.nodeValue
		};
	}
	
	/**
	 * Lấy danh sách các bản ghi đọc được từ XML. (Chỉ sử dụng hàm này sau khi có sự kiện "complete").
	 * Các bản ghi này chỉ là dữ liệu bổ sung nên phải đợi đọc xong "Common.xml" mới ghép được dữ liệu.
	 *
	 * @return Danh sách các bản ghi
	 */
	public function getAllKnives ():Array {
		return m_aKnives;
	}
}