﻿import gfx.controls.SQUAD.XML.BaseReader;

/**
 * Class đọc dữ liệu bổ sung cho ADD-ON từ XML.
 * Đọc ngay khi khởi tạo instance mới nhưng phải bắt sự kiện "complete" rồi chờ tới khi đọc xong mới nhận được dữ liệu để xử lý.
 */
class gfx.controls.SQUAD.XML.AddOnsReader extends BaseReader {
	private var m_aAddOns: Array;
	
	/**
	 * Hàm tạo và mở file ngay lập tức.
	 * Ngay khi file được mở xong, bắt sự kiện "open" và đọc ngay.
	 *
	 * @param	sFileName đường dẫn file XML
	 */
	public function AddOnsReader (sFileName: String) {
		super();
		
		m_aAddOns = [];
		addEventListener ("open", readAddOns);
		loadFile (sFileName);
	}
	
	/**
	 * Bắt đầu đọc dữ liệu từ file XML.
	 * Đọc từng bản ghi một bằng hàm readAddOn.
	 * Khi đọc xong thì sinh sự kiện "complete".
	 *
	 * @param	evt eventObject bắt được từ sự kiện "open"
	 */
	private function readAddOns (evt: Object):Void {
		var item: XMLNode = m_kRoot.firstChild;
		do {
			readAddOn (item);
			item = item.nextSibling;
		} while (item != undefined) ;
		dispatchEvent ( { type: "complete", target: this, result: m_aAddOns } ) ;
	}
	
	/**
	 * Đọc bản ghi.
	 * Một bản ghi đọc từ XML vào có các trường:
	 * + id
	 * + properties, mỗi phần tử có các trường
	 * 		+ property
	 * 		+ value
	 * + holders, mỗi phần tử có các trường
	 * 		+ id
	 * 		+ isCatalogue
	 * 		+ isIncluded
	 *
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	private function readAddOn (itemRoot: XMLNode):Void {
		var properties:Array = convertIndices (itemRoot.childNodes);
		
		var addOnHolders: Array = [];
		var xmlHolders: Array = properties["HOLDERS"].childNodes;
		var n:Number = xmlHolders.length;
		for (var i:Number = 0; i < n; ++i ) {
			var xmlHolder: Array = convertIndices (xmlHolders[i].childNodes);
			addOnHolders.push ({
				id:		 		xmlHolder["ID"].firstChild.nodeValue,
				isCatalogue: 	xmlHolder["IS_CATALOGUE"].firstChild.nodeValue == true,
				isIncluded: 	xmlHolder["IS_INCLUDED"].firstChild.nodeValue == true
			});
		}
		
		m_aAddOns[properties["ADDON_ID"].firstChild.nodeValue] = {
			id: 				properties["ADDON_ID"].firstChild.nodeValue,
			properties: 	convertAttributeIndices(properties["PROPERTIES"].childNodes),
			holders: 		addOnHolders};
	}
	
	/**
	 * Lấy danh sách các bản ghi đọc được từ XML. (Chỉ sử dụng hàm này sau khi có sự kiện "complete").
	 * Các bản ghi này chỉ là dữ liệu bổ sung nên phải đợi đọc xong "Common.xml" mới ghép được dữ liệu.
	 *
	 * @return Danh sách các bản ghi
	 */
	public function getAllAddOns ():Array {
		return m_aAddOns;
	}
}