﻿import gfx.controls.SQUAD.XML.BaseReader;
import gfx.controls.SQUAD.Data.Map;
import gfx.controls.SQUAD.Data.Mode;
import gfx.controls.SQUAD.Data.Type;
import gfx.events.EventDispatcher;

class gfx.controls.SQUAD.XML.ModesReader extends BaseReader {
	private var m_aModes: Array;	
	
	/**
	 * Hàm tạo và mở file ngay lập tức. 
	 * Ngay khi file được mở xong, bắt sự kiện "open" và đọc ngay. 
	 * 
	 * @param	sFileName đường dẫn file XML
	 */
	public function ModesReader (sFileName: String) {
		super();
						
		m_aModes = [];				
		addEventListener ("open", readModes);
		loadFile (sFileName);
	}
	
	/**
	 * Bắt đầu đọc dữ liệu từ file XML. 
	 * Đọc từng bản ghi một bằng hàm readCommonItem. 
	 * Khi đọc xong thì sinh sự kiện "complete".
	 * 
	 * @param	evt eventObject bắt được từ sự kiện "open"
	 */
	private function readModes (evt:Object):Void {
		//read types
		var item: XMLNode = m_kRoot.firstChild;		
		if (item) {
			do {
				readMode (item);			
				item = item.nextSibling;			
			} while (item != undefined) ;
		}
		/*
		if (m_aModes[0] == undefined) {
			m_aModes.splice (2, 1);		
		}
		*/
		//dispatch event
		dispatchEvent ( { type: "complete", target: this, result: m_aModes } ) ;
	}		
	
	/**
	 * Đọc bản ghi. 
	 * Một bản ghi đọc từ XML vào là 1 instance của class BaseItem
	 * 
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	private function readMode (itemRoot: XMLNode):Void {		
		var properties:Array = convertIndices (itemRoot.childNodes);
		m_aModes[properties["ID"].firstChild.nodeValue] = new Mode (
			properties["ID"].firstChild.nodeValue, 
			properties["MODE_NAME"].firstChild.nodeValue, 
			readMaps (properties["MAPS_LIST"].firstChild)
		);		
	}
	
	private function readMaps (mapsRoot : XMLNode) : Array {
		var item: XMLNode = mapsRoot;		
		var temp : Array = [];
		var tempMap: Map;
		do {
			tempMap = readMap(item);
			temp[tempMap.id] = tempMap;
			item = item.nextSibling;
		} while (item != undefined) ;
		return temp;
	}
	private function readMap (mapRoot: XMLNode):Map {
		var properties:Array = convertIndices (mapRoot.childNodes);
		//read map
		return new Map(
			properties["MAP_ID"].firstChild.nodeValue, 
			properties["MAP_NAME"].firstChild.nodeValue, 
			properties["IMAGE"].firstChild.nodeValue,
			properties["MIN_PLAYER"].firstChild.nodeValue, 
			properties["MAX_PLAYER"].firstChild.nodeValue, 
			properties["HOP_PLAYER"].firstChild.nodeValue,
			properties["MIN_TARGET"].firstChild.nodeValue, 
			properties["MAX_TARGET"].firstChild.nodeValue, 
			properties["HOP_TARGET"].firstChild.nodeValue,
			properties["TARGET_TYPE"].firstChild.nodeValue,
			properties["MIN_TIME"].firstChild.nodeValue, 
			properties["MAX_TIME"].firstChild.nodeValue, 
			properties["HOP_TIME"].firstChild.nodeValue,
			properties["WEAPON"].firstChild.nodeValue
		);
	}
	
	/**
	 * Lấy danh sách các bản ghi đọc được từ XML. (Chỉ sử dụng hàm này sau khi có sự kiện "complete"). 	 
	 * 
	 * @return Danh sách các bản ghi
	 */
	public function getModes ():Array {
		return m_aModes;
	}
	
}