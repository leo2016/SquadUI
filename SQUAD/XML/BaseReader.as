﻿import gfx.events.EventDispatcher;
import gfx.utils.Delegate;

/**
 * Class đọc dữ liệu từ XML.
 * Không thể tạo instance cho class này.
 * Class chỉ chứa các phương thức chung nhất cho việc mở file, phát sinh sự kiện... của các thao tác đọc XML.
 * Các class khác kế thừa class này để đọc dữ liệu từ XML.
 */
class gfx.controls.SQUAD.XML.BaseReader {
	private var m_kParser: XML;
	private var m_kRoot: XMLNode;
	private var m_sFileName: String;
	
	function dispatchEvent() {};
 	function addEventListener() {};
 	function removeEventListener() {};
	
	/**
	 * Hàm tạo. Chỉ thêm tính năng sinh sự kiện cho class.
	 */
	private function BaseReader() {
		EventDispatcher.initialize(this);
	}
	
	/**
	 * Tìm và mở file XML
	 *
	 * @param	sFileName đường dẫn tới file XML
	 */
	public function loadFile(sFileName:String):Void {
		m_sFileName = sFileName;
		m_kParser = new XML();
		m_kParser.ignoreWhite = true;
		m_kParser.onLoad = Delegate.create(this, openFile);
		m_kParser.load(sFileName);
	}
	
	/**
	 * Kiểm tra file XML tồn tại và mở file XML.
	 * Sinh sự kiện "open" thông báo đã mở file XML thành công
	 *
	 * @param	bSuccess biến đánh dấu file tồn tại sinh từ sự kiện onLoad
	 */
	private function openFile(bSuccess: Boolean):Void {
		if (bSuccess) {
			//trace("XML file is found!");
			if (m_kParser.status == 0) {
				m_kRoot = m_kParser.firstChild;
				dispatchEvent ( { type: "open", target: this, root: m_kRoot } );
			} else {
			trace("There's problem on parsing XML!");
			}
		} else {
			trace("XML file '" + m_sFileName + "' is not found!");
		}
	}
	
	/**
	 * Chuyển danh sách các node thành dạng map để đọc dữ liệu dễ dàng hơn và không phụ thuộc vào thứ tự các node.
	 * Ví dụ:
	 * <ITEM>
	 * 		<ID>12</ID>
	 * 		<NAME>Máy iPod 18GB trắng</NAME>
	 * 		<PRICE>199 USD</PRICE>
	 * <ITEM>
	 *
	 * Để đọc dữ liệu bằng cách thông thường, có 2 cách:
	 * Cách 1:
	 * 		Nhớ thứ tự xuất hiện của từng thuộc tính và đọc lần lượt
	 * 		(.. id = nodes[0].firstChild, name = nodes[1].firstChild, ...)
	 * Cách 2:
	 * 		Đọc từng thuộc tính vào và sử dụng tên của thuộc tính để quyết định giá trị đọc vào là của thuộc tính nào
	 * 		(.. att = nodes[i].firstChild, switch (nodes[i].nodeName) {case "ID": ..., case "NAME":....} )
	 *
	 * Hàm này sẽ chuyển tự động 0 thành "ID", 1 thành "NAME", ... để có thể thực hiện thao tác đọc dễ dàng như sau:
	 * 		id = nodes["ID"].firstChild, name = nodes["NAME"].firstChild, ...
	 *
	 * Tuy có ưu điểm là:
	 * 		+ Không phụ thuộc thứ tự các thuộc tính (kể cả để PRICE lên trước NAME vẫn đúng vì các thao tác chuyển đổi tự động)
	 * 		+ Không cần nhớ thứ tự xuất hiện của các thuộc tính (vì sử dụng trực tiếp tên thuộc tính)
	 * 		+ Code dễ hiểu và đơn giản hơn (rõ ràng property = nodes["PROPERTY"] dễ hiểu hơn property = nodes[7])
	 * Nhưng cũng có nhược điểm là:
	 * 		+ Do map các index dạng String vào dạng Number nên nếu có nhiều thuộc tính cùng tên thì chỉ có thuộc tính sau cùng được nhận
	 * 		Chẳng hạn: 1 item có nhiều giá như sau:
	 * 		<ITEM>
	 * 			<ID>45</ID>
	 * 			<PRICE MEMBER="GOLD">99 USD</PRICE>
	 * 			<PRICE MEMBER="SILVER">129 USD</PRICE>
	 * 			<PRICE MEMBER="BRONZE">159 USD</PRICE>
	 * 		</ITEM>
	 * 		thì chỉ có giá 159 USD được nhận vì nodes["PRICE"] được gán đè liên tiếp tới khi MEMBER="BRONZE".
	 * 		Cách xử lý:
	 * 		Cách 1:
	 * 			Xử lý riêng
	 * 		Cách 2:
	 * 			Dữ liệu trong XML nên được tách ra sao cho không có trường hợp các node trùng tên. Chẳng hạn: Tách riêng các node PRICE là con của PRICES và mỗi bản ghi PRICE tách thành MEMBER và PRICE
	 * 		Cách 3:
	 * 			Nếu tất cả thuộc tính đều trùng tên và phân biệt bởi attribute bên trong (như MEMBER) thì có thể dùng phương thức convertAttributeIndices
	 *
	 * @param	children danh sách các node cần chuyển đổi chỉ số
	 *
	 * @return danh sách các node đã chuyển đổi chỉ số
	 */
	private function convertIndices (children: Array): Array {
		var length: Number = children.length;
		var i:Number = 0;
		var output: Array = [];
		for (i = 0; i < length; ++i ) {
			output[children[i].nodeName] = children[i];
		}
		return output;
	}
	
	/**
	 * Phương thức đặc biệt chỉ dùng attribute "TYPE" để phân biệt các bản ghi khác nhau, đọc ngay giá trị vào danh sách.
	 * Chẳng hạn ta có dữ liệu XML như sau:
	 * 	<PRICE TYPE="GOLD">99 USD</PRICE>
	 * 	<PRICE TYPE="SILVER">129 USD</PRICE>
	 * 	<PRICE TYPE="BRONZE">159 USD</PRICE>
	 *
	 * Khi sử dụng phương thức này để đọc dữ liệu với tham số là mảng các node PRICE trên, ta có được 1 mảng mà mỗi phần tử gồm 2 trường:
	 * + property (giá trị của attribute TYPE)
	 * + value (giá trị của node)
	 * như sau:
	 * [
	 * 	{ property: GOLD, 		value: 99 USD },
	 * 	{ property: SILVER,		value: 129 USD },
	 * 	{ property: BRONZE,	value: 159 USD },
	 * ]
	 *
	 * @param	children danh sách các node cần chuyển đổi chỉ số
	 *
	 * @return danh sách các node đã chuyển đổi chỉ số
	 */
	private function convertAttributeIndices (children: Array): Array {
		var length: Number = children.length;
		var i:Number = 0;
		var output: Array = [];
		for (i = 0; i < length; ++i ) {
			output.push ( {
				property: 	children[i].attributes.TYPE,
				value: 		children[i].firstChild} );
		}
		return output;
	}
}