﻿import gfx.controls.SQUAD.XML.BaseReader;

/**
 * Class đọc dữ liệu bổ sung cho LỰU ĐẠN từ XML.
 * Đọc ngay khi khởi tạo instance mới nhưng phải bắt sự kiện "complete" rồi chờ tới khi đọc xong mới nhận được dữ liệu để xử lý.
 */
class gfx.controls.SQUAD.XML.GrenadesReader extends BaseReader {
	private var m_aGrenades: Array;
	
	/**
	 * Hàm tạo và mở file ngay lập tức.
	 * Ngay khi file được mở xong, bắt sự kiện "open" và đọc ngay.
	 *
	 * @param	sFileName đường dẫn file XML
	 */
	public function GrenadesReader (sFileName: String) {
		super();
		
		m_aGrenades = [];
		addEventListener ("open", readGrenades);
		loadFile (sFileName);
	}
	
	/**
	 * Bắt đầu đọc dữ liệu từ file XML.
	 * Đọc từng bản ghi một bằng hàm readGrenade.
	 * Khi đọc xong thì sinh sự kiện "complete".
	 *
	 * @param	evt eventObject bắt được từ sự kiện "open"
	 */
	private function readGrenades (evt: Object):Void {
		var item: XMLNode = m_kRoot.firstChild;
		do {
			trace("readGrenade");
			readGrenade (item);
			item = item.nextSibling;
		} while (item != undefined) ;
		dispatchEvent ( { type: "complete", target: this, result: m_aGrenades } ) ;
	}
	
	/**
	 * Đọc bản ghi.
	 * Một bản ghi đọc từ XML vào có các trường:
	 * + id
	 * + effectRadius
	 * + baseDamage
	 * + 	
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	private function readGrenade (itemRoot: XMLNode):Void {
		var properties:Array = convertIndices (itemRoot.childNodes);
		m_aGrenades [properties["ID"].firstChild.nodeValue] = {
			id:					properties["ID"].firstChild.nodeValue,
			effectRadius:		properties["EFFECT_RADIUS"].firstChild.nodeValue,
			baseDamage: 	    properties["BASE_DAMAGE"].firstChild.nodeValue,
			lifeTime   :        properties["LIFE_TIME"].firstChild.nodeValue,
			distance   :        properties["DISTANCE"].firstChild.nodeValue,
			effect     :        properties["EFFECT"].firstChild.nodeValue
		};
	}
	
	/**
	 * Lấy danh sách các bản ghi đọc được từ XML. (Chỉ sử dụng hàm này sau khi có sự kiện "complete").
	 * Các bản ghi này chỉ là dữ liệu bổ sung nên phải đợi đọc xong "Common.xml" mới ghép được dữ liệu.
	 *
	 * @return Danh sách các bản ghi
	 */
	public function getAllGrenades ():Array {
		return m_aGrenades;
	}
}