﻿import gfx.controls.SQUAD.XML.BaseReader;

/**
 * Class đọc các enum từ XML.
 * Đọc ngay khi khởi tạo instance mới nhưng phải bắt sự kiện "complete" rồi chờ tới khi đọc xong mới nhận được dữ liệu để xử lý.
 */
class gfx.controls.SQUAD.XML.EnumReader extends BaseReader {
	private var m_aEnums: Array;
	
	/**
	 * Hàm tạo và mở file ngay lập tức.
	 * Ngay khi file được mở xong, bắt sự kiện "open" và đọc ngay.
	 *
	 * @param	sFileName đường dẫn file XML
	 */
	public function EnumReader (sFileName: String) {
		super();
						
		m_aEnums = [];
		addEventListener ("open", readEnums);
		loadFile (sFileName);
	}
	
	/**
	 * Bắt đầu đọc dữ liệu từ file XML.
	 * Đọc từng enum một bằng hàm readEnum.
	 * Khi đọc xong thì sinh sự kiện "complete".
	 *
	 * @param	evt eventObject bắt được từ sự kiện "open"
	 */
	private function readEnums (evt:Object):Void {
		var item: XMLNode = m_kRoot.firstChild;
		do {
			readEnum (item);
			item = item.nextSibling;
		} while (item != undefined) ;
		dispatchEvent ( { type: "complete", target: this, result: m_aEnums } ) ;
	}
	
	/**
	 * Đọc enum và phân loại ngay thành từng nhóm enum cho từng bảng, trong 1 bảng lại chia nhỏ thành nhóm enum cho từng cột.
	 * Một enum đọc từ XML vào là 1 phần tử trong ma trận 3 chiều
	 * ( [ tên bảng ] [ tên cột ] [ enum ] = giá trị enum )
	 *
	 * [ tên bảng 1 ]
	 * ...
	 * [ tên bảng i ]
	 * 		[ tên cột 1 ]
	 * 		...
	 * 		[ tên cột i ]
	 * 			[ enum 1 ]
	 * 			...
	 * 			[ enum i ]
	 * 			...
	 * 			[ enum n ]
	 * 		...
	 * 		[tên cột n]
	 * ...
	 * [ tên bảng n ]
	 *
	 * @param	itemRoot Node chứa dữ liệu của 1 Enum
	 */
	private function readEnum (itemRoot: XMLNode):Void {
		var properties:Array = convertIndices (itemRoot.childNodes);
		var table: String = properties["TABLE"].firstChild.nodeValue;
		var column: String = properties["COLUMN"].firstChild.nodeValue;
		if (m_aEnums[table] == undefined) {
			m_aEnums[table] = [];
		}
		if (m_aEnums[table][column] == undefined) {
			m_aEnums[table][column] = [];
		}
		m_aEnums[ table ][ column ][ properties["VALUE"].firstChild ] = properties["ID"].firstChild.nodeValue;
	}
	
	/**
	 * Lấy danh sách các enum đọc được từ XML. (Chỉ sử dụng hàm này sau khi có sự kiện "complete")
	 *
	 * @return Danh sách các enum (đã phân loại theo từng bảng, từng cột).
	 */
	public function getAllEnums ():Array {
		return m_aEnums;
	}
}