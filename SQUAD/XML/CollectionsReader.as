﻿import gfx.controls.SQUAD.XML.BaseReader;

/**
 * Class đọc dữ liệu bổ sung cho BỘ SƯU TẬP hoặc GÓI từ XML.
 * Vì cấu trúc COLLECTIONS và SETS giống nhau nên có thể dùng chung class này để đọc.
 * Đọc ngay khi khởi tạo instance mới nhưng phải bắt sự kiện "complete" rồi chờ tới khi đọc xong mới nhận được dữ liệu để xử lý.
 */
class gfx.controls.SQUAD.XML.CollectionsReader extends BaseReader {
	private var m_aCollections: Array;
	
	/**
	 * Hàm tạo và mở file ngay lập tức.
	 * Ngay khi file được mở xong, bắt sự kiện "open" và đọc ngay.
	 *
	 * @param	sFileName đường dẫn file XML
	 */
	public function CollectionsReader (sFileName: String) {
		super();
		
		m_aCollections = [];
		addEventListener ("open", readCollections);
		loadFile (sFileName);
	}
	
	/**
	 * Bắt đầu đọc dữ liệu từ file XML.
	 * Đọc từng bản ghi một bằng hàm readCollection.
	 * Khi đọc xong thì sinh sự kiện "complete".
	 *
	 * @param	evt eventObject bắt được từ sự kiện "open"
	 */
	private function readCollections (evt: Object):Void {
		var item: XMLNode = m_kRoot.firstChild;
		do {
			readCollection (item);
			item = item.nextSibling;
		} while (item != undefined) ;
		dispatchEvent ( { type: "complete", target: this, result: m_aCollections } ) ;
	}
	
	/**
	 * Đọc bản ghi.
	 * Một bản ghi đọc từ XML vào có các trường:
	 * + id
	 * + items
	 *
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	private function readCollection (itemRoot: XMLNode):Void {
		var properties:Array = convertIndices (itemRoot.childNodes);
		
		var xmlItems: Array = properties["ITEMS"].childNodes;
		var items: Array = [];
		for (var x:String in xmlItems) {
			items.push (xmlItems[x].firstChild.nodeValue);
		}
		
		m_aCollections [properties["ID"].firstChild.nodeValue] = {
			id:		properties["ID"].firstChild.nodeValue,
			items:	items
		};
	}
	
	/**
	 * Lấy danh sách các bản ghi đọc được từ XML. (Chỉ sử dụng hàm này sau khi có sự kiện "complete").
	 * Các bản ghi này chỉ là dữ liệu bổ sung nên phải đợi đọc xong "Common.xml" mới ghép được dữ liệu.
	 *
	 * @return Danh sách các bản ghi
	 */
	public function getAllCollections ():Array {
		return m_aCollections;
	}
}