﻿import gfx.controls.SQUAD.XML.BaseReader;

/**
 * Class đọc dữ liệu bổ sung cho GIÁP từ XML.
 * Đọc ngay khi khởi tạo instance mới nhưng phải bắt sự kiện "complete" rồi chờ tới khi đọc xong mới nhận được dữ liệu để xử lý.
 */
class gfx.controls.SQUAD.XML.ArmorsReader extends BaseReader {
	private var m_aArmors: Array;
	
	/**
	 * Hàm tạo và mở file ngay lập tức.
	 * Ngay khi file được mở xong, bắt sự kiện "open" và đọc ngay.
	 *
	 * @param	sFileName đường dẫn file XML
	 */
	public function ArmorsReader (sFileName: String) {
		super();
		
		m_aArmors = [];
		addEventListener ("open", readArmors);
		loadFile (sFileName);
	}
	
	/**
	 * Bắt đầu đọc dữ liệu từ file XML.
	 * Đọc từng bản ghi một bằng hàm readArmor.
	 * Khi đọc xong thì sinh sự kiện "complete".
	 *
	 * @param	evt eventObject bắt được từ sự kiện "open"
	 */
	private function readArmors (evt: Object):Void {
		var item: XMLNode = m_kRoot.firstChild;
		do {
			readArmor (item);
			item = item.nextSibling;
		} while (item != undefined) ;
		dispatchEvent ( { type: "complete", target: this, result: m_aArmors } ) ;
	}
	
	/**
	 * Đọc bản ghi.
	 * Một bản ghi đọc từ XML vào có các trường:
	 * + id
	 * + damageReduction
	 *
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	private function readArmor (itemRoot: XMLNode):Void {
		var properties:Array = convertIndices (itemRoot.childNodes);
		m_aArmors [properties["ID"].firstChild] = {
			id:						properties["ID"].firstChild.nodeValue,
			damageReduction:	properties["DAMAGE_REDUCTION"].firstChild.nodeValue
		};
	}
	
	/**
	 * Lấy danh sách các bản ghi đọc được từ XML. (Chỉ sử dụng hàm này sau khi có sự kiện "complete").
	 * Các bản ghi này chỉ là dữ liệu bổ sung nên phải đợi đọc xong "Common.xml" mới ghép được dữ liệu.
	 *
	 * @return Danh sách các bản ghi
	 */
	public function getAllArmors ():Array {
		return m_aArmors;
	}
}