﻿/**
 * ...
 * @author ...
 */
import gfx.controls.SQUAD.XML.BaseReader;
import gfx.controls.SQUAD.Data.RoomName;
import gfx.events.EventDispatcher;
class gfx.controls.SQUAD.XML.RoomNameReader extends BaseReader
{
	private var m_pRoomName:Array;
	
	/**
	 * Doc toan bo du lieu trong file XML
	 * @param	sFileName: Ten file XML
	 */
	public function RoomNameReader(sRoomName:String)
	{
		super();
		m_pRoomName = [];
		addEventListener("open", readRooms);
		loadFile(sRoomName);
	}
	
	/**
	 * Bắt đầu đọc dữ liệu từ file XML. 
	 * Đọc từng bản ghi một bằng hàm readCommonItem. 
	 * Khi đọc xong thì sinh sự kiện "complete".
	 * 
	 * @param	evt eventObject bắt được từ sự kiện "open"
	 */
	public function readRooms(evt:Object):Void
	{
		var item:XMLNode = m_kRoot.firstChild;
		do {
			readRoom(item);
			item = item.nextSibling;
		}while (item != undefined)
		if (m_pRoomName[0] == undefined)
		{
			m_pRoomName.slice(1,0);
		}		
		// dispatche
		dispatchEvent( { type:"complete", target:this, result:m_pRoomName } );
	}

	/**
	 * Đọc bản ghi. 
	 * Một bản ghi đọc từ XML vào là 1 instance của class BaseItem
	 * 
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	public function readRoom(itemRoot:XMLNode):Void {
		var properties:Array = convertIndices(itemRoot.childNodes);
		m_pRoomName[properties["ID"].firstChild.nodeValue] = new RoomName(
		properties["ID"].firstChild.nodeValue, 
		properties["LABEL"].firstChild.nodeValue
		);
	}
		/**
	 * Lấy danh sách các bản ghi đọc được từ XML. (Chỉ sử dụng hàm này sau khi có sự kiện "complete"). 	 
	 * 
	 * @return Danh sách các bản ghi
	 */
	public function getRoomName ():Array {
		return m_pRoomName;
	}
}