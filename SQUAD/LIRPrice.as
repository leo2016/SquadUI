﻿import gfx.controls.CF.OverlapItemRenderer;
import gfx.controls.Label;

class gfx.controls.SQUAD.LIRPrice extends OverlapItemRenderer {
	private var labRentingDays :	Label;	
	private var mcGP : 				MovieClip;
	private var labGP : 				Label;
	private var mcVCoin : 			MovieClip;
	private var labVCoin : 			Label;
	
	public function LIRPrice () {
		super ();
		
		var index:Number = registerSubComponent(mcGP);
		setEventHandler(index, EVENT_UP, chooseGP);
		
		index = registerSubComponent(labGP);
		setEventHandler(index, EVENT_UP, chooseGP);
		
		index = registerSubComponent(mcVCoin);
		setEventHandler(index, EVENT_UP, chooseVCoin);	
		
		index = registerSubComponent(labVCoin);
		setEventHandler(index, EVENT_UP, chooseVCoin);
		
		addEventListener ("chooseGP", _parent._parent);
		addEventListener ("chooseVCoin", _parent._parent);
	}
	
	public function setData(data:Object):Void {
		if(data != undefined) {
			this.data = data;			
			updateAfterStateChange();
			this.visible = true;
			this._disabled = false;
		} else {
			this.visible = false;
			this._disabled = true;
		}
	}
	
	public function updateAfterStateChange():Void {
		if (data != undefined && (labRentingDays instanceof Label)) {
			//Renting days
			if (data.rentingDays == "-1") {
				labRentingDays.text = "Mua";
			} else {
				labRentingDays.text = "Thuê "+data.rentingDays+" ngày";
			}			
			//GP
			if (data.GP == undefined || data.GP == null || data.GP == "-1" || data.GP == -1) {
				mcGP._visible = false;
				labGP._visible = false;
			} else {
				mcGP._visible = true;
				labGP._visible = true;
				labGP.text = data.GP + " GP";
				if (data.selected && data.selectGP) {
					mcGP.gotoAndStop ("on");
				} else {
					mcGP.gotoAndStop ("off");
				}
			}	
			//VCoin
			if (data.VCoin == undefined || data.VCoin == null || data.VCoin == "-1" || data.VCoin == -1) {
				mcVCoin._visible = false;
				labVCoin._visible = false;
			} else {				
				mcVCoin._visible = true;
				labVCoin._visible = true;
				labVCoin.text = data.VCoin + " VCoin";
				if (data.selected && !data.selectGP) {
					mcVCoin.gotoAndStop ("on");
				} else {
					mcVCoin.gotoAndStop ("off");
				}
			}
		}
	}
	
	private function chooseGP () : Void {
		if (mcGP._visible) {
			dispatchEvent ( { type: "chooseGP", target: this, data: data, id: data.id } );
		}
	}
	
	private function chooseVCoin () : Void {		
		if (mcVCoin._visible) {
			dispatchEvent ( { type: "chooseVCoin", target: this, data: data, id: data.id } );
		}
	}
}