﻿class gfx.controls.SQUAD.UTF8Comparator {
	public function UTF8Comparator () {}
	
	public static function compareStrings (string1:String, string2:String) : Boolean {
		string1 = new String(string1);
		string2 = new String(string2);
		return (string1.valueOf() == string2.valueOf());
	}
}