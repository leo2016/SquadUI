﻿class gfx.controls.SQUAD.MathUtility {
	public static function round (originalNumber : Number, digitsAfterDot : Number) : Number {
		if (digitsAfterDot > 0) {
			var factor : Number = Math.pow (10, Math.floor(digitsAfterDot));
			originalNumber *= factor;
			originalNumber = Math.round (originalNumber) / factor;
		}
		return originalNumber;
	}
	
	public function toString () : String {
		return "[CCK - MathUtility "+_name+"]";
	}
	
	//Return angle (degree) to rotate a (ax, ay) CLOCKWISE to be the same direction with b (bx, by)
	public static function angleBetweenVectors (ax : Number, ay : Number, bx : Number, by : Number) : Number {
		return -(180.0 / Math.PI) * Math.atan2(ax * by - ay * bx, ax * bx + ay * by);		
	}
	
	public static function toDegree (radian : Number) : Number {
		return (radian * 180.0 / Math.PI);
	}
	
	public static function toRadian (degree : Number) : Number {
		return (degree / 180.0 * Math.PI);
	}
	
	public static function distance (x1 : Number, y1 : Number, x2 : Number, y2 : Number) : Number {		
		return Math.sqrt ((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
	}
	
	public static function distance3D (x1 : Number, y1 : Number, z1 : Number, x2 : Number, y2 : Number, z2 : Number) : Number {
		return Math.sqrt ((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) + (z1-z2)*(z1-z2));
	}
	
	public static function randomBetween (min : Number, max : Number) : Number {
		return random (max - min + 1) + min;
	}
}