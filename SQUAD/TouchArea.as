﻿import gfx.core.UIComponent;
import gfx.utils.Delegate;

/**
 * Component bắt các sự kiện nhấn chuột, giữ và di chuyển chuột, thả chuột, cuộn con lăn chuột
 */
class gfx.controls.SQUAD.TouchArea extends UIComponent {
	/**
	 * Đánh dấu nút chuột đang được giữ
	 */
	private var m_bPressedMouse :	Boolean;
	
	/**
	 * Khoảng cách tối thiểu cho phép thông báo sự kiện di chuyển chuột
	 */	 
	public var m_iMinDistance :		Number;
	
	/**
	 * Tọa độ X con trỏ chuột
	 */
	private var m_iMouseX :				Number;
	
	/**
	 * Tọa độ Y con trỏ chuột
	 */
	private var m_iMouseY :				Number;
	
	/**
	 * Hàm xử lý khi component thông báo giữ nút chuột.
	 * Hàm này không trả về và nhận 1 tham số là 1 object gồm các field như sau:	 
	 *			type:		"onPressing" 	- loại sự kiện
	 *			target:	this					- component phát sinh sự kiện
	 *			x :			m_iMouseX, 		- tọa độ X giữ nút chuột
	 *			y :			m_iMouseY		- tọa độ Y giữ nút chuột	 	
	 */
	public var m_fOnMouseDown :	Function;
	
	/**
	 * Hàm xử lý khi component thông báo thả nút chuột.
	 * Hàm này không trả về và nhận 1 tham số là 1 object gồm các field như sau:	 
	 *			type:		"onReleasing" 	- loại sự kiện
	 *			target:	this					- component phát sinh sự kiện	 
	 */
	public var m_fOnMouseUp :		Function;
	
	/**
	 * Hàm xử lý khi component thông báo di chuyển chuột bên trong component khi đang giữ nút chuột
	 * Hàm này không trả về và nhận 1 tham số là 1 object gồm các field như sau:	 
	 *			type:			"onDraggingInside" 	- loại sự kiện
	 *			target:		this							- component phát sinh sự kiện
	 *			x :				m_iMouseX, 				- tọa độ X cũ của chuột
	 *			y :				m_iMouseY				- tọa độ Y cũ của chuột
	 * 			newX : 		_root._xmouse, 					- tọa độ X mới của chuột
	 *			newY : 		_root._ymouse					- tọa độ Y mới của chuột	 
	 * 
	 * Các field x, y, newX, newY có thể dùng làm tham số cho hàm toMoveVector để chuyển đổi thành dạng vectơ	 
	 */
	public var m_fOnDraggingInside :			Function;
	
	/**
	 * Hàm xử lý khi component thông báo di chuyển chuột từ ngoài component vào trong component khi đang giữ nút chuột
	 * Hàm này không trả về và nhận 1 tham số là 1 object gồm các field như sau:	 
	 *			type:			"onDraggingIn" 	- loại sự kiện
	 *			target:		this						- component phát sinh sự kiện
	 *			x :				m_iMouseX, 			- tọa độ X cũ của chuột
	 *			y :				m_iMouseY			- tọa độ Y cũ của chuột
	 * 			newX : 		_root._xmouse, 				- tọa độ X mới của chuột
	 *			newY : 		_root._ymouse				- tọa độ Y mới của chuột	 
	 * 
	 * Các field x, y, newX, newY có thể dùng làm tham số cho hàm toMoveVector để chuyển đổi thành dạng vectơ	 
	 */
	public var m_fOnDraggingIn :					Function;//không bao giờ có trường hợp này, vì khi drag out, đã ko còn theo dõi nữa rồi
	
	/**
	 * Hàm xử lý khi component thông báo di chuyển chuột từ trong component ra ngoài component khi đang giữ nút chuột
	 * Hàm này không trả về và nhận 1 tham số là 1 object gồm các field như sau:	 
	 *			type:			"onDraggingOut" 	- loại sự kiện
	 *			target:		this						- component phát sinh sự kiện
	 *			x :				m_iMouseX, 			- tọa độ X cũ của chuột
	 *			y :				m_iMouseY			- tọa độ Y cũ của chuột
	 * 			newX : 		_root._xmouse, 				- tọa độ X mới của chuột
	 *			newY : 		_root._ymouse				- tọa độ Y mới của chuột	 
	 * 
	 * Các field x, y, newX, newY có thể dùng làm tham số cho hàm toMoveVector để chuyển đổi thành dạng vectơ	 
	 */
	public var m_fOnDraggingOut :			Function;
	
	/**
	 * Hàm xử lý khi component thông báo di chuyển chuột bên trong component mà không giữ nút chuột
	 * Hàm này không trả về và nhận 1 tham số là 1 object gồm các field như sau:	 
	 *			type:			"onMovingInside" 	- loại sự kiện
	 *			target:		this						- component phát sinh sự kiện
	 *			x :				m_iMouseX, 			- tọa độ X cũ của chuột
	 *			y :				m_iMouseY			- tọa độ Y cũ của chuột
	 * 			newX : 		_root._xmouse, 				- tọa độ X mới của chuột
	 *			newY : 		_root._ymouse				- tọa độ Y mới của chuột	 
	 * 
	 * Các field x, y, newX, newY có thể dùng làm tham số cho hàm toMoveVector để chuyển đổi thành dạng vectơ	 
	 */
	public var m_fOnMovingInside :			Function;
	
	/**
	 * Hàm xử lý khi component thông báo di chuyển chuột từ ngoài component vào trong component mà không giữ nút chuột
	 * Hàm này không trả về và nhận 1 tham số là 1 object gồm các field như sau:	 
	 *			type:			"onMovingIn" 		- loại sự kiện
	 *			target:		this						- component phát sinh sự kiện
	 *			x :				m_iMouseX, 			- tọa độ X cũ của chuột
	 *			y :				m_iMouseY			- tọa độ Y cũ của chuột
	 * 			newX : 		_root._xmouse, 				- tọa độ X mới của chuột
	 *			newY : 		_root._ymouse				- tọa độ Y mới của chuột	 
	 * 
	 * Các field x, y, newX, newY có thể dùng làm tham số cho hàm toMoveVector để chuyển đổi thành dạng vectơ	 
	 */
	public var m_fOnMovingIn:					Function;
	
	/**
	 * Hàm xử lý khi component thông báo di chuyển chuột từ trong component ra ngoài component mà không giữ nút chuột
	 * Hàm này không trả về và nhận 1 tham số là 1 object gồm các field như sau:	 
	 *			type:			"onMovingOut"		- loại sự kiện
	 *			target:		this						- component phát sinh sự kiện
	 *			x :				m_iMouseX, 			- tọa độ X cũ của chuột
	 *			y :				m_iMouseY			- tọa độ Y cũ của chuột
	 * 			newX : 		_root._xmouse, 				- tọa độ X mới của chuột
	 *			newY : 		_root._ymouse				- tọa độ Y mới của chuột	 
	 * 
	 * Các field x, y, newX, newY có thể dùng làm tham số cho hàm toMoveVector để chuyển đổi thành dạng vectơ	 
	 */
	public var m_fOnMovingOut:				Function;
	
	/**
	 * Hàm xử lý khi component thông báo cuộn con lăn chuột
	 * Hàm này không trả về và nhận 1 tham số là 1 object gồm các field như sau:	 
	 *			type:		"onScrolling" 	- loại sự kiện
	 *			target:	this					- component phát sinh sự kiện	 
	 * 			delta:	delta					- Hướng và khoảng cách cuộn con lăn (thường là +/- 3)
	 */
	public var m_fOnScrolling :			Function;
	
	/**
	 * Cho phép phát sinh sự kiện
	 */
	public var m_bEnableDispatchEvent : Boolean;
	
	public function TouchArea () {
		super ();

		m_iMinDistance = 0;
		m_bPressedMouse = false;		
		m_iMouseX = 0;
		m_iMouseY = 0;		
		m_bEnableDispatchEvent = true;
		this.onMouseDown = handleMouseDown;
		this.onMouseMove = handleMouseMove;
		this.onMouseUp = handleMouseUp;
		
		var mListener:Object = new Object ();
		mListener.onMouseWheel = Delegate.create (this, handleMouseWheel);
		Mouse.addListener(mListener);
		
		m_fOnDraggingIn 			= doNothing;
		m_fOnDraggingInside	= doNothing;
		m_fOnDraggingOut		= doNothing;
		m_fOnMouseDown		= doNothing;
		m_fOnMouseUp			= doNothing;
		m_fOnMovingIn				= doNothing;
		m_fOnMovingInside		= doNothing;
		m_fOnMovingOut			= doNothing;
		m_fOnScrolling				= doNothing;
	}
	
	/**
	 * Xử lý khi bắt được sự kiện giữ nút chuột
	 */
	private function handleMouseDown () : Void {
		//trace ("---------------------------------------------------");
		if (isInArea()) {
			m_bPressedMouse = true;		
			m_iMouseX = _root._xmouse;
			m_iMouseY = _root._ymouse;
			if (m_bEnableDispatchEvent) {
				m_fOnMouseDown ( {
					type:		"onPressing", 
					target:	this, 
					x :			m_iMouseX, 
					y :			m_iMouseY
				} );
			}
		}		
	}
	
	/**
	 * Xử lý khi bắt được sự kiện di chuyển chuột
	 */
	private function handleMouseMove () : Void {
		if (isInArea()) {//chuột nằm trong component
			if (m_bPressedMouse) {//chuột vẫn đang giữ
				if (isPointInArea(m_iMouseX, m_iMouseY)) {//nhưng vị trí ngay trước đó là ở bên trong component
					var d : Number = Math.sqrt (
						(_root._xmouse - m_iMouseX) * (_root._xmouse - m_iMouseX) + 
						(_root._ymouse - m_iMouseY) * (_root._ymouse - m_iMouseY));
					if (d >= m_iMinDistance) {	
						if (m_bEnableDispatchEvent) {
							m_fOnDraggingInside ( { 
								type: 		"onDraggingInside", 
								target : 	this, 
								x : 			m_iMouseX, 
								y : 			m_iMouseY, 
								newX : 		_root._xmouse, 
								newY : 		_root._ymouse
							} );					
						}
					}
				} else {//vị trí ngay trước đó là ở ngoài component
					if (m_bEnableDispatchEvent) {
						m_fOnDraggingIn ( { 
							type: 		"onDraggingIn", 
							target : 	this, 
							x : 			m_iMouseX, 
							y : 			m_iMouseY, 
							newX : 		_root._xmouse, 
							newY : 		_root._ymouse
						} );					
					}
				}
			} else {//chuột đang không được giữ
				if (isPointInArea(m_iMouseX, m_iMouseY)) {//nhưng vị trí ngay trước đó là ở bên trong component
					if (m_bEnableDispatchEvent) {
						m_fOnMovingInside ( { 
							type: 		"onMovingInside", 
							target : 	this, 
							x : 			m_iMouseX, 
							y : 			m_iMouseY, 
							newX : 		_root._xmouse, 
							newY : 		_root._ymouse
						} );					
					}
				} else {//vị trí ngay trước đó là ở ngoài component
					if (m_bEnableDispatchEvent) {
						m_fOnMovingIn ( { 
							type: 		"onMovingIn", 
							target : 	this, 
							x : 			m_iMouseX, 
							y : 			m_iMouseY, 
							newX : 		_root._xmouse, 
							newY : 		_root._ymouse
						} );					
					}
				}
			}
		} else {//chuột nằm ngoài component
			if (m_bPressedMouse) {//vẫn đang giữ nút chuột
				if (isPointInArea(m_iMouseX, m_iMouseY)) {//nhưng vị trí ngay trước đó là ở bên trong component
					//-> drag ra ngoài -> phát sinh sự kiện và thôi không theo dõi hoạt động chuột nữa, coi như MouseUp
					if (m_bEnableDispatchEvent) {
						m_fOnDraggingOut ( {
							type: 		"onDraggingOut", 
							target : 	this, 
							x : 			m_iMouseX, 
							y : 			m_iMouseY, 
							newX : 		_root._xmouse, 
							newY : 		_root._ymouse
						} );
					}
					m_bPressedMouse = false;
				}
			} else {//chuột đang không được giữ
				if (isPointInArea(m_iMouseX, m_iMouseY)) {//nhưng vị trí ngay trước đó là ở bên trong component
					if (m_bEnableDispatchEvent) {
						m_fOnMovingOut ( {
							type: 		"onMovingOut", 
							target : 	this, 
							x : 			m_iMouseX, 
							y : 			m_iMouseY, 
							newX : 		_root._xmouse, 
							newY : 		_root._ymouse
						} );
					}
				}
			}
		}
		m_iMouseX = _root._xmouse;
		m_iMouseY = _root._ymouse;
	}
	
	/**
	 * Xử lý khi bắt được sự kiện thả nút chuột
	 */
	private function handleMouseUp () : Void {	
		if (isInArea()) {
			m_bPressedMouse = false;	
			if (m_bEnableDispatchEvent) {
				m_fOnMouseUp ( {
					type:		"onReleasing", 
					target:	this
				} );
			}
		}
	}
	
	/**
	 * Xử lý khi bắt được sự kiện cuộn con lăn chuột
	 * 
	 * @param	delta		Hướng và khoảng cách lăn
	 */
	private function handleMouseWheel (delta : Number) : Void {	
		if (isInArea()) {
			if (m_bEnableDispatchEvent) {
				m_fOnScrolling ( { 
					type: 		"onScrolling", 
					target : 	this, 
					delta :		delta
				} );
			}
		}
	}
	
	/**
	 * Dựa vào tọa độ mới và cũ, xây dựng vectơ di chuyển
	 * 
	 * @param	x			tọa độ X cũ
	 * @param	y			tọa độ Y cũ
	 * @param	newX	tọa độ X mới
	 * @param	newY	tọa độ Y mới
	 * 
	 * @return		1 object gồm có các field:
	 * 			x				thành phần x của vectơ di chuyển
	 * 			y				thành phần y của vectơ di chuyển -> vectơ di chuyển = (x, y)
	 * 			xPercent	tỉ lệ giữa khoảng cách các tọa độ X và chiều rộng của cả component
	 * 			yPercent	tỉ lệ giữa khoảng cách các tọa độ Y và chiều cao của cả component
	 * Các field xPercent và yPercent dùng trong trường hợp chỉ muốn biết đã di chuyển chuột 2% diện tích của component chứ
	 * không quan tâm đã di chuyển bao nhiều pixel. Ví dụ trường hợp code 2 file giao diện cho 2 kích thước màn hình 16:9 và4:3
	 * nhưng không muốn làm riêng 2 file .as, mà đều muốn khi di chuyển chuột hết chiều dài của component thì vectơ di chuyển
	 * giống nhau (trong khi chiều dài khác nhau) -> phải dùng tỉ lệ chứ không dùng được các số thực
	 */
	public function toMoveVector (x : Number, y : Number, newX : Number, newY : Number) : Object {
		return { 
			x :				newX - x,
			y :				newY - y,
			xPercent :	(newX - x) / _width * 100, 
			yPercent :	(newY - y) / _height * 100
		};
	}
	
	/**
	 * Kiểm tra xem con trỏ chuột có nằm trong component không
	 * 
	 * @return true nếu con trỏ chuột nằm trong component, false nếu ngược lại
	 */
	private function isInArea () : Boolean {
		return isPointInArea (_root._xmouse, _root._ymouse);		
	}
	
	/**
	 * Kiểm tra xem 1 tọa độ nào đó có nằm trong component không
	 * 
	 * @param	x	tọa độ X
	 * @param	y	tọa độ Y
	 * 
	 * @return		true nếu tọa độ nằm trong component, false nếu ngược lại
	 */
	private function isPointInArea (x : Number, y : Number) : Boolean {		
		return hitTest (x, y, true);
	}
	
	public function toString () : String {
		return "[CCK - TouchArea "+_name+"]";
	}
	
	private function doNothing (evt : Object) : Void {}
}