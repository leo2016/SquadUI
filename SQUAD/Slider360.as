﻿import gfx.utils.Delegate;

class gfx.controls.SQUAD.Slider360 {
	public var m_iMminStep : 		Number;
	public var m_iMaxStep : 		Number;
	public var m_sIdentifier : 		String;
	
	private var m_mcContainer :MovieClip;
	private var m_iIndex : 			Number;
	private var m_aMCArray :		Array;	
	private var m_iViewportWidth:Number;	
	public var calculateStepFunc :	Function;
	
	public function Slider360 (
		identifier : 			String, 		
		minStep : 				Number, 
		maxStep : 				Number, 
		area :					MovieClip, 
		x :							Number, 
		y :							Number) {
			
		m_sIdentifier = identifier;
		m_iMminStep = minStep;
		m_iMaxStep = maxStep;
		m_mcContainer = area;
		m_iViewportWidth = m_mcContainer._width;

		m_iIndex = 0;
		var tempMC : MovieClip = m_mcContainer.attachMovie (m_sIdentifier, m_sIdentifier + m_iIndex, m_mcContainer.getNextHighestDepth());
		if (x == undefined) {
			x = 0;
		}
		if (y == undefined) {
			y = 0;
		}
		tempMC._x = x;
		tempMC._y = y;
		m_aMCArray = [tempMC];				
		
		calculateStepFunc = calculateStepLinear;
	}
	
	private function decideToSlide () : Void {
		var step : Number = calculateStepFunc ();		
		if (Math.abs(step) < m_iMminStep) {
			return ;
		}
		
		for (var x : String in m_aMCArray) {
			m_aMCArray[x]._x += step;
		}
		
		if (m_aMCArray.length == 1) {
			if (step < 0) {
				var firstMC : MovieClip = m_aMCArray[0];
				if (firstMC._x + firstMC._width <= m_iViewportWidth) {
					var tempMC : MovieClip = m_mcContainer.attachMovie (m_sIdentifier, m_sIdentifier + m_iIndex, m_mcContainer.getNextHighestDepth());
					tempMC._y = firstMC._y;
					tempMC._x = firstMC._x + firstMC._width;
					m_aMCArray.push (tempMC);
					++m_iIndex;
				}
			} else {
				var firstMC : MovieClip = m_aMCArray[0];
				if (firstMC._x >= 0) {
					var tempMC : MovieClip = m_mcContainer.attachMovie (m_sIdentifier, m_sIdentifier + m_iIndex, m_mcContainer.getNextHighestDepth());
					tempMC._y = firstMC._y;
					tempMC._x = firstMC._x - firstMC._width;
					m_aMCArray.push (tempMC);
					++m_iIndex;
				}
			}
		} else {
			var length : Number = m_aMCArray.length;
			for (var i : Number = 0 ; i < length ; ++i ) {
				if ((m_aMCArray[i]._x > m_iViewportWidth) || (m_aMCArray[i]._x < -m_aMCArray[i]._width)) {					
					m_aMCArray[i].removeMovieClip ();
					m_aMCArray.splice(i, 1);
					--i;
					--length;
				}
			}
		}
	}
	
	private function calculateStepLinear () : Number {
		var centerX : Number = m_iViewportWidth / 2;		
		return (m_mcContainer._xmouse - centerX) / centerX * m_iMaxStep;
	}
	
	public function start () : Void {
		m_mcContainer.onEnterFrame = Delegate.create (this, decideToSlide);
	}
	
	public function stop () : Void {
		delete m_mcContainer.onEnterFrame;
	}
	
	public function toString () : String {
		return "[CCK - Slider360 "+_name+"]";
	}	
}