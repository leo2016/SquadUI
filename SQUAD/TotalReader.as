﻿import gfx.controls.SQUAD.CPPSimulator;
import gfx.controls.SQUAD.XML.*;
import gfx.controls.SQUAD.Data.*;
import gfx.events.EventDispatcher;

/**
 * Class đọc và ghép dữ liệu cho tất cả các phần dữ liệu từ XML
 */
class gfx.controls.SQUAD.TotalReader {
	//----------------------------------------------------------------------------------------
	private var FOLDER:				String = "XML/";
	private var EXTENSION: 		String = ".xml";
	private var F_COMMON: 		String = "Common";
	private var F_CATALOGUES: 	String = "Catalogues";
	private var F_ENUMS: 			String = "Enums";
	private var F_PRICES: 				String = "ItemPrices";
	private var F_ADDON:			String = "AddOns";
	private var F_ARMOR:			String = "Armors";
	private var F_COLLECTION:		String = "Collections";
	private var F_CHARACTER:		String = "Characters";
	private var F_FASHION:			String = "FashionItems";
	private var F_FUNCTIONAL:		String = "FunctionalItems";
	private var F_GRENADE:			String = "Grenades";
	private var F_GUN:					String = "Guns";
	private var F_KNIFE:				String = "Knives";
	private var F_SET:					String = "Sets";
	private var F_MODE: 				String = "GameEnums";
	private var F_WEAPON:			String = "WeaponInGame";
	private var F_ROOM: 				String = "RoomName";
	private var F_LEVELS:				String = "Ranks";
	//----------------------------------------------------------------------------------------
	private var STEP_READING: 	Number = 0;
	private var STEP_READ: 			Number = 1;
	private var STEP_MERGED:		Number = 2;
	//----------------------------------------------------------------------------------------
	private var readers: 	Array;
	private var data: 		Array;
	private var steps:		Array;
	private var files:			Array;
	private var handlers:	Array;
	//----------------------------------------------------------------------------------------
	private var finishCount: Number;

	function dispatchEvent() {};
 	function addEventListener() {};
 	function removeEventListener() {};
	
	/**
	 * Hàm tạo.
	 * Ngay lập tức mở và đọc tất cả các file XML cần thiết.
	 * Mỗi khi hoàn tất việc đọc 1 file XML, chuyển sang hàm prepareToMergeData để ghép thông tin (nếu được)
	 */
	public function TotalReader () {
		CPPSimulator.trace ("Reading data from XML files.......", 10);
		readers 	= [];
		data 		= [];
		steps 		= [];
		files 			= [];
		handlers 	= [];
		finishCount = 0;
		
		readers [F_COMMON] 		= new CommonReader (FOLDER + F_COMMON + EXTENSION);
		readers [F_CATALOGUES] 	= new CataloguesReader (FOLDER + F_CATALOGUES + EXTENSION);
		readers [F_ENUMS] 			= new EnumReader (FOLDER + F_ENUMS + EXTENSION);
		readers [F_PRICES] 			= new PricesReader (FOLDER + F_PRICES + EXTENSION);
		readers [F_ADDON] 			= new AddOnsReader (FOLDER + F_ADDON + EXTENSION);
		readers [F_ARMOR] 			= new ArmorsReader (FOLDER + F_ARMOR + EXTENSION);
		readers [F_COLLECTION] 	= new CollectionsReader (FOLDER + F_COLLECTION + EXTENSION);
		readers [F_CHARACTER] 	= new CharactersReader (FOLDER + F_CHARACTER + EXTENSION);
		readers [F_FASHION] 			= new FashionItemsReader (FOLDER + F_FASHION + EXTENSION);
		readers [F_FUNCTIONAL] 	= new FunctionalItemsReader (FOLDER + F_FUNCTIONAL + EXTENSION);
		readers [F_GRENADE] 		= new GrenadesReader (FOLDER + F_GRENADE + EXTENSION);
		readers [F_GUN] 				= new GunsReader (FOLDER + F_GUN + EXTENSION);
		readers [F_KNIFE] 				= new KnivesReader (FOLDER + F_KNIFE + EXTENSION);
		readers [F_SET] 					= new CollectionsReader (FOLDER + F_SET + EXTENSION);
		readers [F_MODE]				= new ModesReader (FOLDER + F_MODE + EXTENSION);
		readers[F_WEAPON]			= new IconWeaponInGameReader(FOLDER + F_WEAPON + EXTENSION);
		readers[F_ROOM]				= new RoomNameReader(FOLDER + F_ROOM + EXTENSION);
		readers[F_LEVELS]				= new LevelsReader(FOLDER + F_LEVELS + EXTENSION);

		handlers [F_ADDON] 			= attachAsAddon;
		handlers [F_ARMOR] 			= attachAsArmor;
		handlers [F_COLLECTION] 	= attachAsCollection;
		handlers [F_CHARACTER] 	= attachAsCharacter;
		handlers [F_FASHION] 		= attachAsFashion;
		handlers [F_FUNCTIONAL] 	= attachAsFunctional;
		handlers [F_GRENADE] 		= attachAsGrenade;
		handlers [F_GUN] 				= attachAsGun;
		handlers [F_KNIFE] 			= attachAsKnife;
		handlers [F_SET] 				= attachAsSet;
		
		files = [
			F_COMMON,
			F_CATALOGUES,
			F_ENUMS,
			F_PRICES,
			F_ADDON,
			F_ARMOR,
			F_COLLECTION,
			F_CHARACTER,
			F_FASHION,
			F_FUNCTIONAL,
			F_GRENADE,
			F_GUN,
			F_KNIFE,
			F_SET, 
			F_MODE,
			F_WEAPON,
			F_ROOM,
			F_LEVELS
		];
		
		for (var x: String in files) {
			readers[files[x]].addEventListener ("complete", this, "prepareToMergeData");
			data [files[x]] = null;
			steps [files[x]] = STEP_READING;
		}
		
		EventDispatcher.initialize (this);
	}

	/**
	 * Ghép nối thông tin
	 *
	 * @param	evt	Đối tượng sinh bởi sự kiện "complete"
	 */
	private function prepareToMergeData (evt: Object) : Void {
		++finishCount;
		
		for (var x: String in files) {
			var fileX:String = files[x];
			if (evt.target == readers[fileX]) {
				CPPSimulator.trace (finishCount + ". " + fileX, 11);
				steps [fileX] = STEP_READ;
				data [fileX] = evt.result;
				break;
			}
		}
		
		if (steps [F_COMMON] == STEP_READ) {
			for (var x:String in handlers) {
				if (steps[x] == STEP_READ) {
					handlers[x] (data[F_COMMON], data[x]);					
					if (x != F_CATALOGUES) {
						finish (x);
					}					
				}
			}
		}
		
		if (finishCount == files.length) {
			attachPrices (data[F_COMMON], data[F_PRICES]);
			CPPSimulator.trace ("Finish reading data from XML completely!", 10);
			dispatchEvent ( { type: "complete", target: this } );
		}
	}

	/**
	 * Ghép giá tiền
	 *
	 * @param	baseItems	danh sách vật phẩm
	 * @param	prices	giá tiền
	 */
	private function attachPrices (baseItems: Array, prices: Array) : Void {		
		for (var x:String in prices) {						
			baseItems[x].attachPrices ( prices[x].prices );			
		}
	}

	/**
	 * Ghép thông tin về phụ kiện
	 *
	 * @param	baseItems	danh sách vật phẩm
	 * @param	extraInfo	thông tin bổ sung
	 */
	private function attachAsAddon (baseItems: Array, extraInfo: Array) : Void {
		for (var x:String in extraInfo) {
			var extra: Object = extraInfo[x];
			baseItems[x] = new AddOn (baseItems[x], extra.properties, extra.holders);
		}
	}

	/**
	 * Ghép thông tin về giáp
	 *
	 * @param	baseItems	danh sách vật phẩm
	 * @param	extraInfo	thông tin bổ sung
	 */
	private function attachAsArmor (baseItems: Array, extraInfo: Array) : Void {
		for (var x:String in extraInfo) {
			baseItems[x] = new Armor (baseItems[x], extraInfo[x].damageReduction);
		}
	}

	/**
	 * Ghép thông tin về nhân vật
	 *
	 * @param	baseItems	danh sách vật phẩm
	 * @param	extraInfo	thông tin bổ sung
	 */
	private function attachAsCharacter (baseItems: Array, extraInfo: Array) : Void {
		for (var x:String in extraInfo) {
			baseItems[x] = new Character (baseItems[x], extraInfo[x].baseForce, extraInfo[x].isMale);
		}
	}

	/**
	 * Ghép thông tin về bộ sưu tập
	 *
	 * @param	baseItems	danh sách vật phẩm
	 * @param	extraInfo	thông tin bổ sung
	 */
	private function attachAsCollection (baseItems: Array, extraInfo: Array) : Void {
		for (var x:String in extraInfo) {
			baseItems[x] = new Collection (baseItems[x], extraInfo[x].items);
		}
	}

	/**
	 * Ghép thông tin về vật phẩm thời trang
	 *
	 * @param	baseItems	danh sách vật phẩm
	 * @param	extraInfo	thông tin bổ sung
	 */
	private function attachAsFashion (baseItems: Array, extraInfo: Array) : Void {
		for (var x:String in extraInfo) {
			var extra: Object = extraInfo[x];
			baseItems[x] = new FashionItem (
				baseItems[x],
				extra.isSexLimited,
				extra.isMale,
				extra.extraType,
				extra.extraValue,
				extra.isCharacterLimited,
				extra.characterIds);
		}
	}

	/**
	 * Ghép thông tin về vật phẩm chức năng
	 *
	 * @param	baseItems	danh sách vật phẩm
	 * @param	extraInfo	thông tin bổ sung
	 */
	private function attachAsFunctional (baseItems: Array, extraInfo: Array) : Void {
		for (var x:String in extraInfo) {
			baseItems[x] = new FunctionalItem (baseItems[x], extraInfo[x].properties);
		}
	}

	/**
	 * Ghép thông tin về lựu đạn
	 *
	 * @param	baseItems	danh sách vật phẩm
	 * @param	extraInfo	thông tin bổ sung
	 */
	private function attachAsGrenade (baseItems: Array, extraInfo: Array) : Void {
		for (var x:String in extraInfo) {
			var extra: Object = extraInfo[x];
			trace("extra.distance"+extra.distance);
			baseItems[x] = new Grenade (baseItems[x], extra.effectRadius, extra.baseDamage, extra.lifeTime, extra.distance, extra.effect );
		}
	}

	/**
	 * Ghép thông tin về súng
	 *
	 * @param	baseItems	danh sách vật phẩm
	 * @param	extraInfo	thông tin bổ sung
	 */
	private function attachAsGun (baseItems: Array, extraInfo: Array) : Void {
		for (var x:String in extraInfo) {
			var extra: Object = extraInfo[x];
			baseItems[x] = new Gun (
				baseItems[x],
				extra.ammos,
				extra.speed,
				extra.recoil,
				extra.accuracy,
				extra.minRank,
				extra.damageAmplifying,
				extra.reloadTime, 
				extra.slots);
		}
	}

	/**
	 * Ghép thông tin về dao
	 *
	 * @param	baseItems	danh sách vật phẩm
	 * @param	extraInfo	thông tin bổ sung
	 */
	private function attachAsKnife (baseItems: Array, extraInfo: Array) : Void {
		for (var x:String in extraInfo) {
			var extra: Object = extraInfo[x];
			baseItems[x] = new Knife (
				baseItems[x],
				extra.baseDamage,
				extra.speed,
				extra.length,
				extra.specialModeDamage,
				extra.specialModeSpeed);
		}
	}

	/**
	 * Ghép thông tin về gói
	 *
	 * @param	baseItems	danh sách vật phẩm
	 * @param	extraInfo	thông tin bổ sung
	 */
	private function attachAsSet (baseItems: Array, extraInfo: Array) : Void {
		for (var x:String in extraInfo) {
			baseItems[x] = new Set (baseItems[x], extraInfo[x].items);
		}
	}

	/**
	 * Đánh dấu đã ghép xong thông tin cho 1 loại vật phẩm, đồng thời xóa dữ liệu thừa để giải phóng bộ nhớ
	 *
	 * @param	file	loại vật phẩm đã ghép xong
	 */
	private function finish (file: String) : Void {
		delete readers[file];		
		data[file] = null;		
		steps[file] = STEP_MERGED;
		CPPSimulator.trace ("Finish merging " + file, 11);
	}
	
	/**
	 * Lấy danh sách các enum đọc được. Chỉ sử dụng hàm này sau khi có sự kiện "complete"
	 *
	 * @return danh sách các enum đọc được
	 */
	public function getEnums(): Array {
		return data[F_ENUMS];
	}
	
	/**
	 * Lấy danh sách các catalogue đọc được. Chỉ sử dụng hàm này sau khi có sự kiện "complete"
	 *
	 * @return danh sách các catalogue đọc được (đã phân cấp)
	 */
	public function getCatalogues(): Array {
		return data[F_CATALOGUES];
	}
	
	/**
	 * Lấy danh sách các vật phẩm đọc được. Chỉ sử dụng hàm này sau khi có sự kiện "complete"
	 *
	 * @return danh sách các vật phẩm đọc được (đã ghép đủ thông tin)
	 */
	public function getItems(): Array {
		return data[F_COMMON];
	}
	/**
	 * 
	 * @return danh sach Mode cua Game trong Lobby
	 */
	public function getModes () : Array {
		return data[F_MODE];
	}
	/**
	 * 
	 * @return danh sach vu khi trong InGame
	 */
	public function getWeaponInGame():Array {
		return data[F_WEAPON];
	}
	
	/**
	 * 
	 * @return danh sach phong trong Lobby
	 */
	public function getRoomName():Array {
		return data[F_ROOM];
	}
	
	/**
	 * 
	 * @return Level cua Player
	 */
	public function getLevel():Array {
		return data[F_LEVELS];
	}
	
	public function toString () : String {
		return "[CCK - TotalReader "+_name+"]";
	}
}
