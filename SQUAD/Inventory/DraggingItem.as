﻿import gfx.controls.SQUAD.Data.InventoryItem;
import gfx.controls.SQUAD.ImagePathManager;

import gfx.controls.Label;
import gfx.controls.ProgressBar;
import gfx.controls.UILoader;

class gfx.controls.SQUAD.Inventory.DraggingItem extends MovieClip{
	public var mcContainerName : 		MovieClip;
	public var mcContainerExpiration : 	MovieClip;
	public var mcContainerDurability :	MovieClip;
	public var mcContainerInSack :		MovieClip;	
	public var mcContainerImage :		MovieClip;	
	
	private var uiItemImage : 			MovieClip;
	private var labItemName : 			MovieClip;
	private var labItemExpiration :	MovieClip;
	private var labItemQuantity :		MovieClip;
	private var labInSack :				MovieClip;
	private var pgbItemDurability : 	MovieClip;
		
	public function DraggingItem () {
		super ();
		
		labItemName 		= mcContainerName.attachMovie ("labItemName", "labItemName", getNextHighestDepth ());
		labItemExpiration 	= mcContainerExpiration.attachMovie ("labItemExpiration", "labItemExpiration", getNextHighestDepth ());
		labInSack 				= mcContainerInSack.attachMovie ("labInSack", "labInSack", getNextHighestDepth ());
		pgbItemDurability 	= mcContainerDurability.attachMovie ("pgbDefault", "pgbDefault", getNextHighestDepth ());
		var w: Number 		= mcContainerImage._width;
		var h : Number 		= mcContainerImage._height;
		uiItemImage			= mcContainerImage.attachMovie ("uiItemImage", "uiItemImage", getNextHighestDepth ());			
		uiItemImage._width = 166;
		uiItemImage._height = 53;
	}
	
	public function setData (data : InventoryItem) : Void {		
		if (data != undefined && data != null) {			
			labItemName.text = data.baseItem.name ();
			if (data.deadline != 0 && data.deadline != undefined && data.deadline != null) {
				labItemExpiration.text = "Còn " + data.deadline + " ngày";
			} else {
				labItemExpiration.text = "";
			}
			pgbItemDurability.setProgress (data.durability, 100);
			if (data.inSack != InventoryItem.NOT_USE && data.inSack != InventoryItem.IN_USE) {
				labInSack.text = "" + data.inSack;
			} else {
				labInSack.text = "";
			}
			if (data.image == undefined) {
				if (uiItemImage.source != ImagePathManager.DEFAULT_ITEM_IMAGE) {
					uiItemImage.source = ImagePathManager.DEFAULT_ITEM_IMAGE;
				}
			} else if (uiItemImage.source != ImagePathManager.INVENTORY_ITEM + data.image) {
				uiItemImage.source = ImagePathManager.INVENTORY_ITEM + data.image;
			}			
		}
	}
}