import gfx.controls.SQUAD.Data.InventoryItem;
import gfx.controls.SQUAD.ImagePathManager;

class gfx.controls.SQUAD.Inventory.DraggingAddOn extends MovieClip{
	public var mcContainerName : 		MovieClip;	
	public var mcContainerImage :		MovieClip;
	
	private var uiAddOnImage : 			MovieClip;
	private var labAddOnName : 			MovieClip;
		
	public function DraggingAddOn () {
		super ();
		
		labAddOnName 		= mcContainerName.attachMovie ("labAddOnName", "labAddOnName", getNextHighestDepth ());		
		uiAddOnImage			= mcContainerImage.attachMovie ("uiAddOnImage", "uiAddOnImage", getNextHighestDepth ());
		this._xscale = 100;
		this._yscale = 100;
	}
	
	public function setData (data : InventoryItem) : Void {
		if (data != undefined && data != null) {			
			labAddOnName.text = data.baseItem.name ();
			if (data.image == undefined || data.image == "") {
				uiAddOnImage.source = ImagePathManager.DEFAULT_ITEM_IMAGE;
			} else {
				uiAddOnImage.source = ImagePathManager.INVENTORY_ITEM + data.image;
			}
		}
	}
}