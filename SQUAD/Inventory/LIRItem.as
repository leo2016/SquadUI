﻿import gfx.controls.SQUAD.Data.AddOn;
import gfx.controls.SQUAD.Data.BaseItem;
import gfx.controls.SQUAD.Data.InventoryItem;
import gfx.controls.SQUAD.DragDropComponent;
import gfx.controls.SQUAD.ImagePathManager;

import gfx.controls.Label;
import gfx.controls.ListItemRenderer;
import gfx.controls.ProgressBar;
import gfx.controls.UILoader;
import gfx.events.EventDispatcher;

class gfx.controls.SQUAD.Inventory.LIRItem extends ListItemRenderer {
	private var uiItemImage : 			UILoader;
	private var labItemName : 			Label;
	private var labItemExpiration :	Label;
	private var labItemQuantity :		Label;
	private var labInSack :				Label;
	private var labInChar :				Label;
	private var pgbItemDurability : 	ProgressBar;
	private var uiUnlocked:				UILoader;
	private var ddc : 						DragDropComponent;
		
	public static var POP_UP_SELL : 		String = "BÁN";
	public static var POP_UP_REPAIR :	String = "SỬA CHỮA";
	public static var POP_UP_EXTEND :	String = "GIA HẠN";
	public static var POP_UP_EQUIP : 	String = "SỬ DỤNG";
	public static var POP_UP_UNEQUIP :String = "NGỪNG SỬ DỤNG";
	
	public function LIRItem () {
		super ();
		
		EventDispatcher.initialize (this);
		addEventListener ("handleItemDrag", _parent._parent);
		//ddc._visible = false;
	}
	
	private function configUI () : Void {
		super.configUI ();
		
		ddc.addEventListener("beginDrag", this, "beginDrag");
		ddc.addEventListener("drag", this, "drag");				
	}
	
	private function beginDrag(evt:Object):Void {		
		if (data == undefined || data == null || !allowDrag()) {
			ddc.cancelDrag();
		}		
	}		
	
	private function drag(evt:Object):Void {
		if (!disabled) {
			dispatchEvent( { type: "handleItemDrag", target: this, data: data, from: _parent._parent } );
		}
	}
	
	public function setData(data:Object):Void {
		if(data != undefined) {
			this.data = data;
			updateAfterStateChange();
						
			this.visible = true;
			this._disabled = false;
		} else {
			this.visible = false;
			this._disabled = true;
		}
	}

	public function updateAfterStateChange():Void {
		if (data instanceof InventoryItem && (labItemName instanceof Label)) {
			labItemName.text = data.baseItem.name ();			
			labInChar.text = "";			
			if (data.deadline != 0 && data.deadline != undefined && data.deadline != null) {
				labItemExpiration.text = "Còn " + data.deadline + " ngày";
			} else {
				labItemExpiration.text = "";
			}
			pgbItemDurability.setProgress (data.durability, 100);
			if (data.inSack != InventoryItem.NOT_USE && data.inSack != InventoryItem.IN_USE) {
				labInSack.text = data.inSack;
			} else {
				labInSack.text = "";
			}
			if (data.image == undefined) {
				if (uiItemImage.source != ImagePathManager.DEFAULT_ITEM_IMAGE) {
					uiItemImage.source = ImagePathManager.DEFAULT_ITEM_IMAGE;
				}
			} else if (uiItemImage.source != ImagePathManager.INVENTORY_ITEM+data.image) {
				uiItemImage.source = ImagePathManager.INVENTORY_ITEM+data.image;
			}
			uiUnlocked._visible = !data.unlocked;
		}
	}	
	
	private function allowDrag () : Boolean {		
		var x : BaseItem = data.baseItem;
		if (x instanceof AddOn || !data.unlocked) {
			return false;
		}
		return true;
	}	
}