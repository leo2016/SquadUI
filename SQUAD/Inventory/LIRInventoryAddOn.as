﻿import gfx.controls.SQUAD.Data.InventoryItem;
import gfx.controls.SQUAD.DragDropComponent;
import gfx.controls.SQUAD.ImagePathManager;

import gfx.controls.Label;
import gfx.controls.ListItemRenderer;
import gfx.controls.UILoader;
import gfx.events.EventDispatcher;

class gfx.controls.SQUAD.Inventory.LIRInventoryAddOn extends ListItemRenderer {
	private var uiItemImage : 			UILoader;
	private var labItemName : 			Label;
	private var labItemExpiration :	Label;
	private var labUseFor :				Label;	
	private var mcBorder : 				MovieClip;
	private var mcMask : 					MovieClip;
	private var ddc : 						DragDropComponent;
	
	public function LIRInventoryAddOn () {
		super ();
		
		EventDispatcher.initialize (this);
		addEventListener ("handleItemDrag", _parent._parent);
		//ddc._visible = false;
	}
	
	private function configUI () : Void {
		super.configUI ();
		
		ddc.addEventListener("beginDrag", this, "beginDrag");
		ddc.addEventListener("drag", this, "drag1");		
	}
	
	private function beginDrag(evt:Object):Void {		
		if (data == undefined || data == null || !allowDrag()) {
			ddc.cancelDrag();
		}		
	}		
	
	private function drag1(evt:Object):Void {
		if (!disabled) {
			dispatchEvent( { type: "handleItemDrag", target: this, data: data.item, from: _parent._parent } );
		}
	}
	
	public function setData(data:Object):Void {
		/**
		 * data
		 * 		item : InventoryItem
		 * 		useFor : String
		 * 		suitable : Boolean
		 */
		if(data != undefined) {
			this.data = data;
			updateAfterStateChange();							
			
			this.visible = true;			
		} else {
			this.visible = false;
			this._disabled = true;
		}		
	}
		
	public function updateAfterStateChange():Void {
		if (data != null && data != undefined && (labItemName instanceof Label)) {
			labItemName.text = data.item.baseItem.name ();
			if (data.item.deadline != 0 && data.item.deadline != undefined && data.item.deadline != null) {
				labItemExpiration.text = "Còn " + data.item.deadline + " ngày";
			} else {
				labItemExpiration.text = "";
			}
			labUseFor.text = data.useFor;
			if (data.suitable) {
				mcMask._visible = false;
				this._disabled = false;
			} else {
				mcMask._visible = true;
				this._disabled = true;
			}			
			if (data.image == undefined) {
				if (uiItemImage.source != ImagePathManager.DEFAULT_ITEM_IMAGE) {
					uiItemImage.source = ImagePathManager.DEFAULT_ITEM_IMAGE;
				}
			} else if (uiItemImage.source != ImagePathManager.INVENTORY_ITEM+data.item.image) {
				uiItemImage.source = ImagePathManager.INVENTORY_ITEM+data.item.image;
			}
		}
	}
	
	private function allowDrag() : Boolean {
		if (data.item.inSack == InventoryItem.NOT_USE && data.suitable) {
			return true;
		}
		return false;
	}
}