﻿import gfx.controls.SQUAD.Data.BaseItem;
import gfx.controls.SQUAD.ImagePathManager;

import gfx.controls.Label;
import gfx.controls.ListItemRenderer;
import gfx.controls.UILoader;
import gfx.events.EventDispatcher;

class gfx.controls.SQUAD.Inventory.LIRSuggestion extends ListItemRenderer {
	private var uiItemImage : 			UILoader;
	private var labItemName : 			Label;
		
	public function LIRSuggestion () {
		super ();
		
		EventDispatcher.initialize (this);
	}
	
	private function configUI () : Void {
		super.configUI ();
	}
	
	public function setData(data:Object):Void {
		if(data != undefined) {
			this.data = data;
			updateAfterStateChange();
			this.visible = true;
			this._disabled = false;
		} else {
			this.visible = false;
			this._disabled = true;
		}
	}
		
	public function updateAfterStateChange():Void {
		if (data instanceof BaseItem && labItemName instanceof Label) {
			labItemName.text = data.name ();
			if (data.image() == undefined) {
				if (uiItemImage.source != ImagePathManager.DEFAULT_ITEM_IMAGE) {
					uiItemImage.source = ImagePathManager.DEFAULT_ITEM_IMAGE;
				}
			} else if (uiItemImage.source != ImagePathManager.INVENTORY_ITEM + data.image()) {				
				uiItemImage.source = ImagePathManager.INVENTORY_ITEM+data.image();
			}
		}
	}
}