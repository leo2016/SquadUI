﻿import gfx.controls.SQUAD.NewStyle.XML.Localizer;
import gfx.controls.SQUAD.StringUtility;

/**
 * Class phụ trách việc định dạng dữ liệu ngày tháng.
 * Các ký hiệu sử dụng trong class bao gồm (Xét 15 giờ 27 phút 34 giây, ngày 25 tháng 4 năm 2011):
 * 		+ YYYY			Năm với 4 số (2011)
 * 		+ YYY			Năm đầy đủ (Năm 2011, Year 2011)
 * 		+ YY				năm với 2 số cuối (11)
 * 		+----------------------------------------------------
 * 		+ MMMM		Tháng đầy đủ (Tháng Tư, April) 
 * 		+ MMM		Tháng ngắn gọn (Tháng 4, Apr)
 * 		+ MM			Tháng chỉ có số (4)
 * 		+----------------------------------------------------
 * 		+ DAY			Ngày trong tuần (Thứ Hai, Monday)
 * 		+ DDD			Ngày trong tháng (Ngày 25, Date 25)
 * 		+ DD			Ngày trong tháng chỉ có số (25)
 * 		+----------------------------------------------------
 * 		+ hhh			Giờ đầy đủ (15 giờ, 15h)
 * 		+ hh12			Giờ chu kỳ 12 tiếng (3 giờ chiều, 3PM)
 * 		+ hh				Giờ chỉ có số (15)
 * 		+----------------------------------------------------
 * 		+ mmm		Phút đầy đủ (27 phút, 27min, 27')
 * 		+ mm			Phút chỉ có số (27)
 * 		+----------------------------------------------------
 * 		+ sss			Giây đầy đủ (34 giây, 34s, 34'')
 * 		+ ss				Giây chỉ có số (34)
 * 
 * Giả sử xâu định dạng là "hh12 (hh:mm:ss, DDD MMMM YYY)" thì kết quả là:
 * 3 giờ chiều (15:27:34, Ngày 25 Tháng 4 Năm 2011)
 * hoặc sau khi localization:
 * 3PM (15:27:34, 25 April 2011)
 */
class gfx.controls.SQUAD.DateFormatter {
	private static var m_bInitialized : 			Boolean = false;
	private static var m_kLocalizer : 			Localizer;
	private static var m_aDays : 				Array;
	private static var m_aFullMonths : 		Array;
	private static var m_aShortMonths : 	Array;
	
	public static function init (localizer : Localizer, force : Boolean) : Void {
		if (!force && m_bInitialized) {
			return;
		}
		m_kLocalizer = localizer;
		
		m_aDays = [];
		m_aDays [0] = m_kLocalizer.localize ("SUNDAY");
		m_aDays [1] = m_kLocalizer.localize ("MONDAY");
		m_aDays [2] = m_kLocalizer.localize ("TUESDAY");
		m_aDays [3] = m_kLocalizer.localize ("WEDNESDAY");
		m_aDays [4] = m_kLocalizer.localize ("THURSDAY");
		m_aDays [5] = m_kLocalizer.localize ("FRIDAY");
		m_aDays [6] = m_kLocalizer.localize ("SATURDAY");
		
		m_aFullMonths = [];
		for (var i : Number = 1; i <= 12; ++i ) {
			m_aFullMonths[i] = m_kLocalizer.localize ("MONTH_FULL_" + i);
		}
		
		m_aShortMonths = [];
		for (var i : Number = 1; i <= 12; ++i ) {
			m_aShortMonths[i] = m_kLocalizer.localize ("MONTH_SHORT_" + i);
		}
		
		m_bInitialized = true;
	}
	
	public static function format (YYYY : Number, MM : Number, DD : Number, hh : Number, min : Number, ss : Number, formatString : String) : String {
		if (!m_bInitialized) {			
			formatString = "DD/MM/YYYY hh:mm:ss";
		}
		var output : String = formatString;
		var tempDate : Date = new Date (YYYY, MM, DD, hh, min, ss, 0);		
		
		//Year
		output = StringUtility.replaceAll (output, "YYYY", "" + YYYY);
		output = StringUtility.replaceAll (output, "YYY", m_kLocalizer.localize("YEAR_FULL", [ { varName:"**YYYY**", varValue: "" + YYYY } ]));
		output = StringUtility.replaceAll (output, "YY", "" + (Math.floor(YYYY) % 100));
		
		//Month
		output = StringUtility.replaceAll (output, "MMMM", m_aFullMonths[MM]);
		output = StringUtility.replaceAll (output, "MMM", m_aShortMonths[MM]);
		output = StringUtility.replaceAll (output, "MM", "" + MM);
		
		//Date
		output = StringUtility.replaceAll (output, "DAY", m_aDays[tempDate.getDay()]);
		output = StringUtility.replaceAll (output, "DDD", m_kLocalizer.localize("DATE_FULL", [ { varName:"**DD**", varValue: "" + DD } ]));
		output = StringUtility.replaceAll (output, "DD", "" + DD);
		
		//Hour		
		output = StringUtility.replaceAll (output, "hhh", m_kLocalizer.localize("HOUR_FULL", [ { varName:"**hh**", varValue: "" + hh } ]));
		output = StringUtility.replaceAll (output, "hh12", 
			hh > 12 ? 
			(m_kLocalizer.localize("HOUR_12_PM", [ { varName:"**hh**", varValue: "" + (hh-12) } ])) : 
			(m_kLocalizer.localize("HOUR_12_AM", [ { varName:"**hh**", varValue: "" + hh } ]))
		);		
		output = StringUtility.replaceAll (output, "hh", "" + hh);
		
		//Minute
		output = StringUtility.replaceAll (output, "mmm", m_kLocalizer.localize("MINUTE_FULL", [{varName:"**mm**", varValue: "" + min}]));
		output = StringUtility.replaceAll (output, "mm", "" + min);
		
		//Second
		output = StringUtility.replaceAll (output, "sss", m_kLocalizer.localize("SECOND_FULL", [{varName:"**ss**", varValue: "" + ss}]));
		output = StringUtility.replaceAll (output, "ss", "" + ss);
		
		//Result
		return output;
	}	
}