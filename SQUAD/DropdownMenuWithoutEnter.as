﻿import gfx.controls.DropdownMenu;
import gfx.ui.InputDetails;
import gfx.ui.NavigationCode;

class gfx.controls.SQUAD.DropdownMenuWithoutEnter extends DropdownMenu {
	public function handleInput(details:InputDetails, pathToFocus:Array):Boolean {
		var handled:Boolean;
			
		if (details.type == "key") {
			return true;
		}
		
		if (_dropdown != null && isOpen) {
			handled = _dropdown.handleInput(details);
			if (handled) { return true; } 
		}							
		// Return the value the button handleInput returned.
		return handled;
	}
}