﻿import gfx.controls.SQUAD.Data.Grenade;
import gfx.controls.SQUAD.Data.Gun;
import gfx.controls.SQUAD.Data.Knife;
class gfx.controls.SQUAD.Utilities.WeaponComparator {
	public static var MAX_GUN_POWER : 				Number = -1;
	public static var MAX_GUN_ACCURACY : 			Number = -1;
	public static var MAX_GUN_SPEED : 				Number = -1;
	public static var MAX_GUN_RECOIL : 				Number = -1;
	public static var MAX_GUN_WEIGHT : 				Number = -1;
	public static var MAX_KNIFE_DAMAGE : 			Number = -1;
	public static var MAX_KNIFE_SPEED : 				Number = -1;
	public static var MAX_KNIFE_LENGTH : 			Number = -1;
	public static var MAX_GRENADE_DAMAGE :		Number = -1;
	public static var MAX_GRENADE_RADIUS : 		Number = -1;
	
	public static function init (itemList : Array) : Void {
		for (var x : String in itemList) {
			var y = itemList[x];
			if (y instanceof Gun) {			
				if (MAX_GUN_ACCURACY < y.accuracy ()) {
					MAX_GUN_ACCURACY = y.accuracy ();
				}			
				if (MAX_GUN_SPEED < y.speed ()) {
					MAX_GUN_SPEED = y.speed ();				
				}
				if (MAX_GUN_RECOIL < y.recoil ()) {
					MAX_GUN_RECOIL = y.recoil ();
				}			
				if (MAX_GUN_WEIGHT < y.weight ()) {
					MAX_GUN_WEIGHT = y.weight ();				
				}
			} else if (y instanceof Knife) {			
				if (MAX_KNIFE_SPEED < y.speed()) {
					MAX_KNIFE_SPEED = y.speed();
				}
				if (MAX_KNIFE_LENGTH < y.length()) {
					MAX_KNIFE_LENGTH = y.length();
				}
			} else if (y instanceof Grenade) {
				if (MAX_GRENADE_RADIUS < y.effectRadius()) {
					MAX_GRENADE_RADIUS = y.effectRadius();
				}
			}
		}
		
		++MAX_GUN_ACCURACY;
		++MAX_GUN_SPEED;
		++MAX_GUN_RECOIL;
		++MAX_GUN_WEIGHT;
		++MAX_KNIFE_LENGTH;
		++MAX_KNIFE_SPEED;
		++MAX_GRENADE_RADIUS;
		
		MAX_GUN_POWER = 150;
		MAX_KNIFE_DAMAGE = 150;
		MAX_GRENADE_DAMAGE = 150;
	}
}