﻿import gfx.controls.Button;
import gfx.controls.SQUAD.DragDropComponent;

class gfx.controls.SQUAD.Test.ButtonWithDDC extends Button {
	public var ddc: DragDropComponent;
	private var item: Object;
	
	public function ButtonWithDDC() {
		super();
		item = undefined;
	}
	
	private function configUI():Void {
		super.configUI();
		
		ddc.addEventListener("beginDrag", this, "beginDrag");
		ddc.addEventListener("drag", this, "drag");
		ddc.addEventListener("drop", this, "drop");
	}
	
	private function beginDrag(evt:Object):Void {
		if (item == undefined || item == null) {
			ddc.cancelDrag();
		}
	}
	
	private function drag(evt:Object):Void {
		dispatchEvent( { type: "drag", target: this, data: item} );
		//You can handle it right here instead of dispatching it out
	}
	
	private function drop(evt:Object):Void {
		dispatchEvent( { type: "drop", target: this, data: evt.data} );
		//You can handle it right here instead of dispatching it out
	}
	
	private function updateAfterStateChange():Void {
		if (item != undefined) {
			_label = item.toString();
		} else {
			_label = "";
		}
		super.updateAfterStateChange();
	}
}