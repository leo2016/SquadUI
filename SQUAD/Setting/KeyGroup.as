﻿import gfx.controls.SQUAD.Setting.ObjectKey;

class gfx.controls.SQUAD.Setting.KeyGroup{
	private var keys : Array;
	
	public function KeyGroup() {
		keys = [];
	}
	
	public function addObjectKey (objectKey : ObjectKey) : Void {
		keys.push (objectKey);
	}
	
	public function isDuplicatedKey (keyCode : Number) : ObjectKey {
		for (var x : String in keys) {
			if (keys[x].setting.currentCode == keyCode) {
				return keys[x];
			}
		}
		return undefined;
	}
	
	public function getChangedKeys() : Array {
		var output : Array = [];
		for (var x : String in keys) {
			if (keys[x].isChanged()) {
				output.push (keys[x]);
			}
		}
		return output;
	}
	
	public function getAllKeys() : Array {
		return keys;
	}
	
	public function resetAllKeys() : Void {
		for (var x : String in keys) {
			keys[x].resetKey();
		}
	}
	public function SaveAllKeys() : Void {
		for (var x : String in keys) {
			keys[x].saveKey ();
		}
	}
}