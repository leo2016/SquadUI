﻿import gfx.controls.Button;
import gfx.controls.SQUAD.Setting.Setting;
import gfx.controls.TextInput;
import gfx.ui.InputDetails;
import gfx.ui.NavigationCode;
import gfx.controls.SQUAD.NewStyle.XML.Localizer;

class gfx.controls.SQUAD.Setting.ObjectKey extends TextInput {	
	public var setting : Setting;
	private var savedAction : Number;
	public static var localizer : 		Localizer = undefined;
	public  var English:String = "en"
	public  var Sphanish:String="tbn"
	private function configUI () : Void {
		super.configUI ();
		onMouseDown = handleMouseDown;
	}		
	
	//Action Code (Specific Project) - Begin ---------------------------------------------------
	public static var AC_FORWAD : Number = 0;
	public static var AC_DOWN : Number = 1;
	public static var AC_LEFT : Number = 2;
	public static var AC_RIGHT : Number = 3;
	public static var AC_SITDOWN : Number =4;
	public static var AC_WALK : Number = 5;
	public static var AC_JUMP : Number = 6;
	public static var AC_WEAPON_OLD : Number = 7;
	public static var AC_CHANGECLASS : Number = 8;
	public static var AC_WEAPON_PRIMARY : Number = 9;
	public static var AC_WRAPON_SUB : Number = 10;
	public static var AC_KNIFE : Number = 11;
	public static var AC_BOOMD : Number = 12;
	public static var AC_BOOMDC : Number = 13;
	public static var AC_FIRE : Number = 14;
	public static var AC_ADDON : Number = 15;
	public static var AC_RELOAD : Number = 16;
	public static var AC_DROP : Number = 17;
	public static var AC_REMOVE_BOMDS : Number = 18;
	public static var AC_FLASHLIGHT : Number = 19;
	public static var AC_SKILL_1 : Number = 20;
	public static var AC_SKILL_2 : Number = 21;
	//Action Code (Specific Project) - End ---------------------------------------------------
				
	public static var	specialKeys :Array = [];
	public static var	MOUSE_LBUTTON : Number = 256;
	public static var	MOUSE_RBUTTON : Number = 257;
	public static var	EMPTY : Number = 258;
	private var m_kLocalizer : 						Localizer;
	
	public function ObjectKey() {
		super ();
		setting = new Setting ();
		setting.setAction (savedAction);
		specialKeys[Key.UP] = "Up";
		specialKeys[Key.DOWN] = "Down";
		specialKeys[Key.TAB] = "Tab";
		specialKeys[Key.ENTER] = "Enter";
		specialKeys[Key.END] = "End";
		specialKeys[Key.ALT] = "Alt";
		specialKeys[Key.ESCAPE] = "Escape";
		specialKeys[Key.SPACE] = "Space";
		specialKeys[Key.RIGHT] = "Right";
		specialKeys[Key.SHIFT] = "Shift";
		specialKeys[Key.LEFT] = "Left";
		specialKeys[Key.CONTROL] = "Ctrl";
		specialKeys[Key.CAPSLOCK] = "CapsLock";
		specialKeys[Key.DELETEKEY ] = "Delete";
		specialKeys[112] = "F1";
		specialKeys[113] = "F2";
		specialKeys[114] = "F3";
		specialKeys[115] = "F4";
		specialKeys[116] = "F5";
		specialKeys[117] = "F6";
		specialKeys[118] = "F7";
		specialKeys[119] = "F8";
		specialKeys[120] = "F9";
		specialKeys[121] = "F10";
		specialKeys[122] = "F11";
		specialKeys[123] = "F12";
		specialKeys[187] = "+";
		specialKeys[189] = "-";
		specialKeys[222] = "'";
		specialKeys[191] = "/";
		specialKeys[186] = ";";
		specialKeys[190] = ".";	
		specialKeys[188] = ",";	
		specialKeys[219] = "[";
		specialKeys[221] = "]";
		specialKeys[192] = "`";
		specialKeys[144] = "Num Lock";
		specialKeys[97] = "Num1";
		specialKeys[98] = "Num2";
		specialKeys[99] = "Num3";
		specialKeys[100] = "Num4";
		specialKeys[101] = "Num5";
		specialKeys[102] = "Num6";
		specialKeys[103] = "Num7";
		specialKeys[104] = "Num8";
		specialKeys[105] = "Num9";
		specialKeys[96] = "Num0";
		specialKeys[106] = "*";
		specialKeys[111] = "/";
		specialKeys[109] = "-";
		specialKeys[107] = "+";
		specialKeys[110] = "NumDel";
		specialKeys[36] = "Home";
		specialKeys[45] = "Insert";
		specialKeys[MOUSE_RBUTTON] = "Right Click";
		specialKeys[MOUSE_LBUTTON] = "Left Click";
		specialKeys[EMPTY] = " ";
	}		
	public function resetKey() {
		if (setting instanceof Setting) {
			setting.reset ();
			updateUI();
		}
	}
	public function saveKey() {
		if (setting instanceof Setting) {
			setting.save ();
			updateUI();
		}
	
	}
	public function cancelKey() {
		if (setting instanceof Setting) {
			setting.cancel ();
			updateUI();
		}
	}
	public function setKey (code : Number) : Void {
		if (setting instanceof Setting) {
			setting.setCurrent (code);
			updateUI ();
		}
	}
	
	public function updateUI() {
		var textLocal:String = localizer.localize("LANGAUGE");
		if (setting instanceof Setting) {
			if (textLocal == Sphanish) {
				if (setting.currentCode == MOUSE_LBUTTON) {
					text = "Clic izquierdo";
				}else if(setting.currentCode ==MOUSE_RBUTTON ) {
					text = "Clic Derecho";
				}else if (setting.currentCode == Key.SPACE) {
					text = "Espacio";
				}else {
					text = getKeyText (setting.currentCode);
				}
			}else {
				text = getKeyText (setting.currentCode);
			}
		}
	}
	
	public function getKeyText (code : Number) : String {
		var text = getSpecialKeyText (code);
		//var textLocal:String = localizer.localize("LANGAUGE");
		if (text == "") {
			return chr(code);
		} else {
			return text;
		}
	}
	
	public function getSpecialKeyText (code: Number) : String {
		for (var x : String in specialKeys) {
			if (Number(x) == code) {
				return specialKeys[x];
			}
		}
		return "";
	}
	public function clearKey() {
		if (setting instanceof Setting) {
			setting.setCurrent (EMPTY);
			updateUI();
		}
	}
	public function isChanged () : Boolean {
		if (setting instanceof Setting) {
			return setting.isChanged ();
		}
		return false;
	}
	
	public function handleInput(details:InputDetails, pathToFocus:Array):Boolean {		
		switch(details.navEquivalent) {
			case NavigationCode.TAB:
				if (details.type != "key") { 
					break;
				}
			case NavigationCode.DOWN:
				if (details.type != "key") { 
					break;
				}
			case NavigationCode.UP:
				if (details.type != "key") { 
					break;
				}		
				return true;
			case NavigationCode.LEFT:
				if (details.type != "key") { 
					break;
				}
			case NavigationCode.RIGHT:
				if (details.type != "key") { 
					break;
				}		
				return true;
		}
		return false;
	}	
	
	private function handleMouseDown () : Void {
		//if (_focused) {
			//trace(this);
		//}
	}
	
	[Inspectable (name = "Action", type = "Number", defaultValue = 0)]
	public function set action (a : Number) : Void {
		if (setting instanceof Setting) {
			setting.setAction (a);
		} else {
			savedAction = a;
		}
	}

	public function get action () : Number {
		if (setting instanceof Setting) {
			return setting.action;
		}
		return 0;
	}
}