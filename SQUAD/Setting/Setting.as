﻿class gfx.controls.SQUAD.Setting.Setting {	
	
	//Action Code (Specific Project) - Begin ---------------------------------------------------
	public static var AC_FORWAD : Number = 0;
	public static var AC_DOWN : Number = 1;
	public static var AC_LEFT : Number = 2;
	public static var AC_RIGHT : Number = 3;
	public static var AC_SITDOWN : Number =4;
	public static var AC_WALK : Number = 5;
	public static var AC_JUMP : Number = 6;
	public static var AC_WEAPON_OLD : Number = 7;
	public static var AC_CHANGECLASS : Number = 8;
	public static var AC_WEAPON_PRIMARY : Number = 9;
	public static var AC_WRAPON_SUB : Number = 10;
	public static var AC_KNIFE : Number = 11;
	public static var AC_BOOMD : Number = 12;
	public static var AC_BOOMDC : Number = 13;
	public static var AC_FIRE : Number = 14;
	public static var AC_ADDON : Number = 15;
	public static var AC_RELOAD : Number = 16;
	public static var AC_DROP : Number = 17;
	public static var AC_REMOVE_BOMDS : Number = 18;
	public static var AC_FLASHLIGHT : Number = 19;
	public static var AC_SKILL_1 : Number = 20;
	public static var AC_SKILL_2 : Number = 21;
	public static var AC_MOUSE:Number = 22;
	public static var AC_SCOPE:Number = 23
	// keyboard
	public static var AC_SOUND_TRACK:Number = 24;
	public static var AC_SOUND:Number = 25;
	public static var AC_SOUND_RADIO:Number = 26;
	//sound
	public static var AC_EFFECT_SPECIAL:Number = 27;
	public static var AC_EFFECT_BLOOD:Number = 28;
	public static var AC_EFFECT_BALLISTIC:Number = 29;
	public static var AC_GRAPHIC_WIDTH:Number = 30;
	public static var AC_GRAPHIC_HEIGHT:Number = 31;
	public static var AC_GRAPHIC_FULLSCREEN:Number = 32;
	public static var AC_GRAPHIC_BIT:Number = 33;
	public static var AC_GRAPHIC_MULTSAMPLING:Number = 34;
	//video
	

	
	//Action Code (Specific Project) - End ---------------------------------------------------
		
	private var _action:Number;
	private var _defaultCode:Number;
	private var _originalCode:Number;
	private var _currentCode:Number;
	
	public function reset() {
		_currentCode = _defaultCode;
	}
	public function save() {
		_originalCode = _currentCode;
	}
	public function cancel() {
		_currentCode = _originalCode;
	}
	public function setAction (a : Number) : Void {
		_action = a;
	}
	public function setCurrent (code : Number) : Void {
		_currentCode = code;
	}
	public function setDefault (code : Number) : Void {
		_defaultCode = code;
	}
	public function setOriginal (code : Number) : Void {
		_originalCode = code;
	}	
	public function isChanged () : Boolean {
		return (_currentCode != _originalCode);
	}
	public function get action () : Number {
		return _action;
	}
	public function get currentCode () : Number {
		return _currentCode;
	}
	public function get defaultCode () : Number {
		return _defaultCode;
	}
	public function get originalCode () : Number {
		return _originalCode;
	}
}