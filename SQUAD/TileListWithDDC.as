import gfx.controls.SQUAD.NewStyle.Data.BaseItem;
import gfx.controls.SQUAD.NewStyle.Data.InventoryItem;
import gfx.controls.SQUAD.DragDropComponent;
import gfx.ui.InputDetails;
import gfx.ui.NavigationCode;

import gfx.controls.TileList;
import gfx.managers.DragManager;
import gfx.events.EventDispatcher;


class gfx.controls.SQUAD.TileListWithDDC extends TileList {
	private var ddc : DragDropComponent;	
	
	public function TileListWithDDC() {
		super();
		
		EventDispatcher.initialize(this);
		
	}
	
	private function configUI():Void {
		super.configUI();
		
		ddc.addEventListener("drop", this, "dropInTls");
	}
	
	private function dropInTls (evt : Object) : Void {
		dispatchEvent ( { type: "handleItemDrop", target: this, data: evt.data } );
	}
	
	private function handleItemDrag ( evt : Object ) : Void {
		if (_disabled) {
			return ;
		}
		if (evt.data instanceof BaseItem) {			
			dispatchEvent ( { type: "notifyDrag", target: this, data: evt.data, item: evt.target } );			
		} else if (evt.data instanceof InventoryItem) {
			if (evt.data.inClass != InventoryItem.NOT_USE || evt.data.unlocked == false) {
				return;
			}							
			dispatchEvent ( { type: "notifyDrag", target: this, data: evt.data, item: evt.target} );
		}
	}
	// 
	public function handleInput(details:InputDetails, pathToFocus:Array):Boolean {		
		switch(details.navEquivalent) {
			case NavigationCode.ENTER:
				if (details.type != "key") { 
					return
				}
			case NavigationCode.TAB:
				if (details.type != "key") { 
					return
				}
			case NavigationCode.UP:
				if (details.type != "key") { 
					return
				}
			case NavigationCode.DOWN:
				if (details.type != "key") {
					return
				}
			case NavigationCode.LEFT:
				if (details.type != "key") { 
					return
				}
			case NavigationCode.RIGHT:
				if (details.type != "key") { 
					return
				}
				return true;
		}
		return false;
	}
	
	public function get selectedRenderer () : MovieClip {
		return getRendererAt (selectedIndex);
	}
	
	public function toString () : String {
		return "[CCK - TileListWithDDC "+_name+"]";
	}
}