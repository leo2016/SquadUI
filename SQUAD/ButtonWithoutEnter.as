﻿import gfx.controls.SQUAD.NewStyle.Core.Button2;
import gfx.ui.InputDetails;
import gfx.ui.NavigationCode;

class gfx.controls.SQUAD.ButtonWithoutEnter extends Button2 {
	public function handleInput(details:InputDetails, pathToFocus:Array):Boolean {		
		switch(details.navEquivalent) {
			case NavigationCode.ENTER:
				if (details.type != "key") { 
					handleRelease();
				}
			case NavigationCode.TAB:
				if (details.type != "key") { 
					handleRelease();
				}
			case NavigationCode.UP:
				if (details.type != "key") { 
					handleRelease();
				}
			case NavigationCode.DOWN:
				if (details.type != "key") { 
					handleRelease();
				}
			case NavigationCode.LEFT:
				if (details.type != "key") { 
					handleRelease();
				}
			case NavigationCode.RIGHT:
				if (details.type != "key") { 
					handleRelease();
				}
				return true;
		}
		return false;
	}
}