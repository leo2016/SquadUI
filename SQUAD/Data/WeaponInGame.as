﻿class gfx.controls.SQUAD.Data.WeaponInGame
{
	public var id:	Number;
	public var label: String;
	public var imgKillDeath:Number;
	public var imgShowWeapon:Number;
	public var imgInfoWindow:Number;
	public function WeaponInGame(
	id:Number,
	name:String,
	imgKillDeath:Number,
	imgShowWeapon:Number,
	imgInfoWindow:Number)
	{
		this.id = id;
		this.label = name;
		this.imgKillDeath = imgKillDeath;
		this.imgShowWeapon = imgShowWeapon;
		this.imgInfoWindow = imgInfoWindow;
	}
	public function toString():String {
		trace("Weapon InGame :" + label);
		return label;
	}
	public static function findWeaponById (weapons : Array, id : Number) : WeaponInGame {
		for (var x : String in weapons) {
			if (weapons[x].id == id) {
				return weapons[x];
			}
		}
		return null;
	}
}