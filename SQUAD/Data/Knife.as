﻿import gfx.controls.SQUAD.Data.BaseItem;

/**
 * Class lưu trữ dữ liệu của DAO
 */
class gfx.controls.SQUAD.Data.Knife extends BaseItem {
	private var _baseDamage: Number;
	private var _speed: Number;
	private var _length: Number;
	private var _specialModeDamage: Number;
	private var _specialModeSpeed: Number;
	
	/**
	 * Hàm tạo
	 *
	 * @param	baseItem BaseItem với dữ liệu đọc từ Common.xml
	 * @param	baseDamage Sát thương cơ bản
	 * @param	speed Tốc độ chém
	 * @param	length Chiều dài
	 * @param	specialModeDamage	Sát thương ảnh hưởng khi ở chế độ đặc biệt
	 * @param	specialModeSpeed Tốc độ ảnh hưởng khi ở chế độ đặc biệt
	 */
	public function Knife(
		baseItem: BaseItem,
		baseDamage: Number,
		speed: Number,
		length: Number,
		specialModeDamage: Number,
		specialModeSpeed: Number) {
		copyFrom(baseItem);
		_baseDamage = baseDamage;
		_speed = speed;
		_length = length;
		_specialModeDamage = specialModeDamage;
		_specialModeSpeed = specialModeSpeed;
	}
	
	/**
	 * @return Sát thương cơ bản
	 */
	public function baseDamage():Number {
		return Number(_baseDamage);
	}
	
	/**
	 * @return Tốc độ chém
	 */
	public function speed():Number {
		return Number(_speed);
	}
	
	/**
	 * @return	Chiều dài
	 */
	public function length():Number {
		return Number(_length);
	}
	
	/**
	 * @return Sát thương ảnh hưởng khi ở chế độ đặc biệt
	 */
	public function specialModeDamage():Number {
		return Number(_specialModeDamage);
	}
	
	/**
	 * @return Tốc độ ảnh hưởng khi ở chế độ đặc biệt
	 */
	public function specialModeSpeed():Number {
		return Number(_specialModeSpeed);
	}
	
	/**
	 * @return Mô tả dao
	 */
	public function toString():String {
		var s:String = super.toString();
		s += "\tBase Damage = " + _baseDamage + "\n";
		s += "\tSpeed = " + _speed + "\n";
		s += "\tLength = " + _length + "\n";
		s += "\tSpecial Mode Damage = " + _specialModeDamage + "\n";
		s += "\tSpecial Mode Speed = " + _specialModeSpeed + "\n";
		return s;
	}
}