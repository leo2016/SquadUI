﻿
/**
 * Class lưu trữ dữ liệu về SÚNG
 */
class gfx.controls.SQUAD.Data.Level  {
	public var id:					Number;
	public var image:			String;
	public var name:			String;

	public function Level(
		id: Number,
		name:String,
		image: String
		) {
		this.id = id;
		this.name = name;
		this.image = image;
	}
	
}