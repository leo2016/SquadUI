﻿import gfx.controls.SQUAD.Data.BaseItem;

/**
 * Class lưu trữ dữ liệu của NHÂN VẬT
 */
class gfx.controls.SQUAD.Data.Character extends BaseItem {
	private var _baseForce: 			Number;
	private var _isMale: 				Boolean;	
	
	/**
	 * Hàm tạo
	 *
	 * @param	baseItem	BaseItem với dữ liệu đọc từ Common.xml
	 * @param	baseForce Lực di chuyển cơ bản (càng lớn càng nhanh)
	 * @param	isMale Giới tính có phải NAM không
	 */
	public function Character(
		baseItem: BaseItem,
		baseForce: Number,
		isMale: Boolean) {
		copyFrom(baseItem);
		_baseForce = baseForce;
		_isMale = isMale == true;		
	}
	
	/**
	 * @return Lực di chuyển cơ bản (càng lớn càng nhanh)
	 */
	public function baseForce(): Number {
		return _baseForce;
	}
	
	/**
	 * @return Giới tính có phải NAM không
	 */
	public function isMale(): Boolean {
		return _isMale;
	}
	
	/**
	 * @return Mô tả nhân vật
	 */
	public function toString():String {
		var s:String = super.toString();
		s += "\tBase Force = " + _baseForce + "\n";
		s += "\tIs Male = " + _isMale + "\n";
		return s;
	}
}