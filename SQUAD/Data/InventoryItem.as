﻿import gfx.controls.SQUAD.Data.BaseItem;

/**
 * Class lưu trữ dữ liệu về một vật phẩm mà người chơi sở hữu
 */
class gfx.controls.SQUAD.Data.InventoryItem {
	public static var NOT_USE: Number = -1;
	public static var IN_USE: Number = 0;
	
	/**
	 * Base Item
	 */
	public var baseItem : BaseItem;
	
	/**
	 * Đường dẫn ảnh 2D
	 */
	public var image : String;
	
	/**
	 * Mã vật phẩm
	 */
	public var itemId : 		Number;
	
	/**
	 * Nếu đang được sử dụng, thì ở ba lô số mấy
	 */
	public var inSack : Number;
	
	/**
	 * Độ bền hiện tại của vật phẩm
	 */
	public var durability : Number;
	
	/**
	 * Vật phẩm hiện đang được thuê (true) hay đã được mua hẳn (false)
	 */
	public var isRent : 		Boolean;
	
	/**
	 * Thời điểm hết hạn thuê vật phẩm
	 */
	public var deadline : 	Number;
	
	/**
	 * Vật phẩm hiện đang được sử dụng kèm với vật phẩm nào.
	 * 		Nếu là phụ kiện thì useFor = mã súng đang gắn phụ kiện này
	 */
	public var useFor : 	Number;
	
	/**
	 * Ô chứa lựu đạn (chỉ dùng khi vật phẩm là lựu đạn và đang được dùng trong 1 ba lô nào đó)	 
	 */
	public var number : Number;
	
	/**
	 * Vật phẩm đã được unlock hay chưa?
	 */
	public var unlocked : Boolean;
	
	public function InventoryItem (
		baseItem:		BaseItem,
		itemId : 		Number,
		inSack :		Number,
		durability : 	Number,
		isRent : 		Boolean,
		deadline : 		Number,
		useFor : 		Number, 
		unlocked:		Boolean) {
				
		this.baseItem 	= baseItem;
		this.itemId 		= itemId;
		this.inSack 		= inSack;
		this.durability		= durability;
		this.isRent 		= isRent;
		this.deadline		= deadline;
		this.useFor 		= useFor;
		this.unlocked 	= unlocked;
		this.image 		= baseItem.image();
	}
	
	public function catalogue () : Number {
		return baseItem.catalogue ();
	}
	
	public static function addOnsAdapted (gun : InventoryItem, allAddOns : Array) : Array {
		var output : Array = [];
		for (var x : String in allAddOns) {
			if (allAddOns[x].useFor == gun.itemId) {
				output.push (allAddOns[x]);
			}
		}
		return output;
	}
	
	public function toString () : String {
		var s : String = baseItem.toString ();
		s += "\tItem Id = " + itemId + "\n";
		s += "\tIn Sack = " + inSack + "\n";
		s += "\tDurability = " + durability + "\n";
		s += "\tIs Rent = " + isRent + "\n";
		s += "\tDeadline = " + deadline + "\n";
		s += "\tUse for = " + useFor + "\n";
		return s;
	}
}