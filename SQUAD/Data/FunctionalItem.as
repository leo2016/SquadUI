﻿import gfx.controls.SQUAD.Data.BaseItem;

/**
 * Class lưu trữ dữ liệu về VẬT PHẨM CHỨC NĂNG
 */
class gfx.controls.SQUAD.Data.FunctionalItem extends BaseItem {
	private var _properties: Array;
	
	/**
	 * Hàm tạo
	 *
	 * @param	baseItem BaseItem với dữ liệu đọc từ Common.xml
	 * @param	properties Danh sách các thuộc tính của vật phẩm
	 */
	public function FunctionalItem(
		baseItem: BaseItem,
		properties: Array) {
		copyFrom(baseItem);
		_properties = properties;
	}
	
	/**
	 * @return Danh sách các thuộc tính của vật phẩm
	 */
	public function properties():Array {
		return _properties;
	}
		
	/**
	 * @return Mô tả vật phẩm
	 */
	public function toString():String {
		var s:String = super.toString();
		for (var x:String in _properties) {
			s += "\t" + _properties[x].property + " = " + _properties[x].value + "\n";
		}
		return s;
	}
}