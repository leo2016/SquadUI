﻿import gfx.controls.SQUAD.Data.Collection;
import gfx.controls.SQUAD.Data.BaseItem;

/**
 * Class lưu trữ dữ liệu của GÓI (Giống hệt với Collection)
 */
class gfx.controls.SQUAD.Data.Set extends Collection {
	
	public function Set(
		baseItem: BaseItem,
		itemIds: Array) {
		super (baseItem, itemIds);
	}
}