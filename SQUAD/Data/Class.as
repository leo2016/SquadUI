﻿import gfx.controls.SQUAD.Data.InventoryItem;

class gfx.controls.SQUAD.Data.Class {
	public var m_iClassNumber :					Number;
	public var m_iClassType :						Number;
	public var m_sClassName :						String;
	public var m_kMainGun :						InventoryItem;
	public var m_kSubGun :							InventoryItem;
	public var m_kKnife :								InventoryItem;	
	public var m_aGrenades :						Array;
	//public var m_kUniform :							InventoryItem;
		
	public static var UNDEFINED_TIME :		InventoryItem = undefined;
	public static var MAX_GRENADES :			Number = 3;
	public static var FREE_NONE :					Number = -1;
	
	public static var TYPE_SNIPER :				Number = 1;
	public static var TYPE_SCOUT :				Number = 2;
	public static var TYPE_TANKER :				Number = 3;
	
	public function Class (
		classNumber :									Number, 
		classType :										Number
		//,uniform :										InventoryItem
	) {
		m_iClassNumber 								= classNumber;
		m_iClassType										= classType;		
		//m_kUniform									= uniform;
		
		//m_sClassName
		m_kMainGun										= UNDEFINED_TIME;
		m_kSubGun										= UNDEFINED_TIME;
		m_kKnife											= UNDEFINED_TIME;
		m_aGrenades 									= [];		
		for (var i : Number = 1; i <= MAX_GRENADES; ++i) {
			m_aGrenades[i] = UNDEFINED_TIME;
		}
	}
	
	public function replaceMainGun (newGun : InventoryItem) : Void {
		if (m_kMainGun != UNDEFINED_TIME) {
			//m_kMainGun.classNumber = ??
		}
		m_kMainGun = newGun;
		//m_kMainGun.classNumber = ??
	}
	
	public function replaceSubGun (newGun : InventoryItem) : Void {
		if (m_kMainGun != UNDEFINED_TIME) {
			//m_kSubGun.classNumber = ??
		}		
		m_kSubGun = newGun;
		//m_kSubGun.classNumber = ??
	}
		
	public function replaceKnife (newKnife : InventoryItem) : Void {
		if (m_kMainGun != UNDEFINED_TIME) {
			//m_kKnife.classNumber = ??
		}		
		m_kKnife = newKnife;
		//m_kKnife.classNumber = ??
	}
	
	public function getFreeGrenadeSlot () : Number {
		for (var i : Number = 1; i <= MAX_GRENADES; ++i) {
			if (m_aGrenades[i] == UNDEFINED_TIME) {
				return i;
			}
		}
		return FREE_NONE;
	}
	
	public function addGrenade (grenade : InventoryItem, slot : Number) : Void {
		if (slot < 1 || slot > MAX_GRENADES) {
			return;
		}
		var suitableSlot : InventoryItem = m_aGrenades[slot];
		if (suitableSlot != UNDEFINED_TIME) {
			//suitableSlot.classNumber = ??;
			suitableSlot.number = InventoryItem.NOT_USE;
		}
		suitableSlot = grenade;
		//suitableSlot.classNumber = ??;
		suitableSlot.number = slot;
	}
	
	public function addGrenadeToFreeSlot (grenade : InventoryItem) : Void {
		var freeSlot : Number = getFreeGrenadeSlot();
		if (freeSlot != FREE_NONE) {
			addGrenade(grenade, freeSlot);
		}
	}
	
	public function removeGrenade (slot : Number) : Void {
		if (slot < 1 || slot > MAX_GRENADES) {
			return;
		}
		var suitableSlot : InventoryItem = m_aGrenades[slot];
		if (suitableSlot != UNDEFINED_TIME) {
			//suitableSlot.classNumber = ??;
			suitableSlot.number = InventoryItem.NOT_USE;
			suitableSlot = UNDEFINED_TIME;
			
			for (var i : Number = slot; i < MAX_GRENADES; ++i ) {
				m_aGrenades[i] = m_aGrenades[i + 1];
				m_aGrenades[i].number = i;
			}
		}
	}
}