﻿import gfx.controls.SQUAD.Data.BaseItem;
import gfx.controls.SQUAD.Data.InventoryItem;
class gfx.controls.SQUAD.Data.Sorter {
	private function Sorter () {}
	
	public static var SORT_BY_NAME : 					String 	= "Xếp tăng dần theo tên"; 
	public static var SORT_BY_NAME_DESC : 			String 	= "Xếp giảm dần theo tên"; 
	public static var SORT_BY_DURABILITY_ASC : 	String 	= "Xếp tăng dần theo độ bền"; 
	public static var SORT_BY_DURABILITY_DESC : 	String 	= "Xếp giảm dần theo độ bền"; 
	public static var SORT_BY_EXPIRATION_ASC : 	String 	= "Xếp tăng dần theo hạn sử dụng"; 
	public static var SORT_BY_EXPIRATION_DESC : 	String	= "Xếp giảm dần theo hạn sử dụng"; 
	public static var SORT_BY_GP_ASC :					String 	= "Xếp tăng dần theo giá GP";
	public static var SORT_BY_GP_DESC :				String 	= "Xếp giảm dần theo giá GP";
	public static var SORT_BY_VCOIN_ASC :			String 	= "Xếp tăng dần theo giá VCoin";
	public static var SORT_BY_VCOIN_DESC :			String 	= "Xếp giảm dần theo giá VCoin";
	public static var SORT_BY_NAME_ADDON :		String 	= "Xếp tăng dần theo tên phụ kiện";
	public static var SORT_BY_GP_ADDON :			String 	= "Xếp tăng dần theo giá GP của phụ kiện";
	
	public static var UNLOCKED_FIRST : 				Boolean = false;
	
	private static var ORDER_12: 							Number = -1;
	private static var ORDER_MESS:						Number = 0;
	private static var ORDER_21: 							Number = 1;
	
	public static function sort (data : Array, criteria : String) : Array {
		var cmpFunc : Function = undefined;
		switch (criteria) {
			case SORT_BY_NAME : 
				if (data[0] instanceof InventoryItem) {
					cmpFunc = compareByName;
				} else {//if (data[0] instanceof BaseItem) {
					cmpFunc = compareByNameOfBaseItem;
				}
				break;
			case SORT_BY_NAME_DESC : 
				if (data[0] instanceof InventoryItem) {
					cmpFunc = compareByNameDESC;
				} else {//if (data[0] instanceof BaseItem) {
					cmpFunc = compareByNameOfBaseItemDESC;
				}
				break;
			case SORT_BY_DURABILITY_ASC : 
				cmpFunc = compareByDurabilityASC;
				break;
			case SORT_BY_DURABILITY_DESC : 
				cmpFunc = compareByDurabilityDESC;
				break;
			case SORT_BY_EXPIRATION_ASC : 
				cmpFunc = compareByExpirationASC;
				break;
			case SORT_BY_EXPIRATION_DESC : 
				cmpFunc = compareByExpirationDESC;
				break;
			case SORT_BY_GP_ASC : 
				cmpFunc = compareByGP_ASC;
				break;
			case SORT_BY_GP_DESC:
				cmpFunc = compareByGP_DESC;
				break;
			case SORT_BY_VCOIN_ASC:
				cmpFunc = compareByVCoin_ASC;
				break;
			case SORT_BY_VCOIN_DESC:
				cmpFunc = compareByVCoin_DESC;
				break;
			case SORT_BY_NAME_ADDON :
				cmpFunc = compareByNameOfAddOn;
				break;
			case SORT_BY_GP_ADDON :
				cmpFunc = compareByGPOfAddOn;
				break;
		}
		if (cmpFunc == undefined) {
			return data;
		} else {			
			return data.sort (cmpFunc, Array.RETURNINDEXEDARRAY);
		}
	}
	
	private static function compareByName (item1 : InventoryItem, item2 : InventoryItem) : Number {
		if (UNLOCKED_FIRST) {
			if (item1.unlocked && !item2.unlocked) {
				return ORDER_12;
			} else if (!item1.unlocked && item2.unlocked) {
				return ORDER_21;
			}
		}	
		return compareByNameOfBaseItem (item1.baseItem, item2.baseItem);
	}
	
	private static function compareByNameOfBaseItem (item1 : BaseItem, item2 : BaseItem) : Number {		
		if (UNLOCKED_FIRST) {
			if (item1.allowBuy() && !item2.allowBuy()) {
				return ORDER_12;
			} else if (!item1.allowBuy() && item2.allowBuy()) {
				return ORDER_21;
			}
		}
			
		var name1 : String = item1.name ();
		var name2 : String = item2.name ();
		var length1 : Number = name1.length;
		var length2 : Number = name2.length;
		var length : Number;
		if (length1 > length2 ) {
			length = length2;
		} else {
			length = length1;
		}
		for (var i = 0; i < length; ++i ) {
			if (name1.charCodeAt(i) < name2.charCodeAt(i)) {
				return ORDER_12;
			} else if (name1.charCodeAt(i) > name2.charCodeAt(i)) {
				return ORDER_21;
			}
		}
		if (length1 == length2) {
			return ORDER_MESS;;
		} else {
			if (length1 < length2) {
				return ORDER_12;
			} else {
				return ORDER_21;
			}
		}
	}
	
	private static function compareByNameDESC (item1 : InventoryItem, item2 : InventoryItem) : Number {
		if (UNLOCKED_FIRST) {
			if (item1.unlocked && !item2.unlocked) {
				return ORDER_12;
			} else if (!item1.unlocked && item2.unlocked) {
				return ORDER_21;
			}
		}
		return compareByNameOfBaseItemDESC (item1.baseItem, item2.baseItem);		
	}
	
	private static function compareByNameOfBaseItemDESC (item1 : BaseItem, item2 : BaseItem) : Number {
		if (UNLOCKED_FIRST) {
			if (item1.allowBuy() && !item2.allowBuy()) {
				return ORDER_12;
			} else if (!item1.allowBuy() && item2.allowBuy()) {
				return ORDER_21;
			}
		}
		
		var name1 : String = item1.name ();
		var name2 : String = item2.name ();
		var length1 : Number = name1.length;
		var length2 : Number = name2.length;
		var length : Number;
		if (length1 > length2 ) {
			length = length2;
		} else {
			length = length1;
		}
		for (var i = 0; i < length; ++i ) {
			if (name1.charCodeAt(i) < name2.charCodeAt(i)) {
				return ORDER_21;
			} else if (name1.charCodeAt(i) > name2.charCodeAt(i)) {
				return ORDER_12;
			}
		}
		if (length1 == length2) {
			return ORDER_MESS;;
		} else {
			if (length1 < length2) {
				return ORDER_21;
			} else {
				return ORDER_12;
			}
		}
	}
	
	private static function compareByDurabilityASC (item1 : InventoryItem, item2 : InventoryItem) : Number {
		if (UNLOCKED_FIRST) {
			if (item1.unlocked && !item2.unlocked) {
				return ORDER_12;
			} else if (!item1.unlocked && item2.unlocked) {
				return ORDER_21;
			}
		}
		
		if (item1.durability < item2.durability) {
			return ORDER_12;
		} else if (item1.durability > item2.durability) {
			return ORDER_21;
		}
		return ORDER_MESS;;
	}

	private static function compareByDurabilityDESC (item1 : InventoryItem, item2 : InventoryItem) : Number {
		if (UNLOCKED_FIRST) {
			if (item1.unlocked && !item2.unlocked) {
				return ORDER_12;
			} else if (!item1.unlocked && item2.unlocked) {
				return ORDER_21;
			}
		}
		
		if (item1.durability < item2.durability) {
			return ORDER_21;
		} else if (item1.durability > item2.durability) {
			return ORDER_12;
		}
		return ORDER_MESS;
	}
	
	private static function compareByExpirationASC (item1 : InventoryItem, item2 : InventoryItem) : Number {
		if (UNLOCKED_FIRST) {
			if (item1.unlocked && !item2.unlocked) {
				return ORDER_12;
			} else if (!item1.unlocked && item2.unlocked) {
				return ORDER_21;
			}
		}
		
		if (item1.deadline < item2.deadline) {
			return ORDER_12;
		} else if (item1.deadline > item2.deadline) {
			return ORDER_21;
		}
		return ORDER_MESS;
	}
	
	private static function compareByExpirationDESC (item1 : InventoryItem, item2 : InventoryItem) : Number {
		if (UNLOCKED_FIRST) {
			if (item1.unlocked && !item2.unlocked) {
				return ORDER_12;
			} else if (!item1.unlocked && item2.unlocked) {
				return ORDER_21;
			}
		}
		
		if (item1.deadline < item2.deadline) {
			return ORDER_21;
		} else if (item1.deadline > item2.deadline) {
			return ORDER_12;
		}
		return ORDER_MESS;;
	}
	
	private static function compareByGP_ASC (item1 : BaseItem, item2 : BaseItem) : Number {
		if (UNLOCKED_FIRST) {
			if (item1.allowBuy() && !item2.allowBuy()) {
				return ORDER_12;
			} else if (!item1.allowBuy() && item2.allowBuy()) {
				return ORDER_21;
			}
		}
		
		var p1 : Number = Number(item1.getGPPriceForRentingDays(item1.getMinRentingDays()));
		var p2 : Number = Number(item2.getGPPriceForRentingDays(item2.getMinRentingDays()));		
		if (p1 == -1 || p1 == NaN) {			
			return ORDER_21;
		} else if (p2 == -1 || p2 == NaN) {			
			return ORDER_12;
		} else if (p1 < p2) {			
			return ORDER_12;
		} else if (p1 > p2) {
			return ORDER_21;
		} else {			
			return ORDER_MESS;// compareByNameOfBaseItem (item1, item2);
		}
	}
	
	private static function compareByGP_DESC (item1 : BaseItem, item2 : BaseItem) : Number {
		if (UNLOCKED_FIRST) {
			if (item1.allowBuy() && !item2.allowBuy()) {
				return ORDER_12;
			} else if (!item1.allowBuy() && item2.allowBuy()) {
				return ORDER_21;
			}
		}
		
		var p1 : Number = Number(item1.getGPPriceForRentingDays(item1.getMinRentingDays()));
		var p2 : Number = Number(item2.getGPPriceForRentingDays(item2.getMinRentingDays()));
		if (p1 == -1 || p1 == NaN) {			
			return ORDER_21;
		} else if (p2 == -1 || p2 == NaN) {			
			return ORDER_12;
		} else if (p1 > p2) {			
			return ORDER_12;
		} else if (p1 < p2) {
			return ORDER_21;
		} else {			
			return ORDER_MESS;// compareByNameOfBaseItem (item1, item2);
		}
	}
	
	private static function compareByVCoin_ASC (item1 : BaseItem, item2 : BaseItem) : Number {
		if (UNLOCKED_FIRST) {
			if (item1.allowBuy() && !item2.allowBuy()) {
				return ORDER_12;
			} else if (!item1.allowBuy() && item2.allowBuy()) {
				return ORDER_21;
			}
		}
		
		var p1 : Number = Number(item1.getVCoinPriceForRentingDays(item1.getMinRentingDays()));
		var p2 : Number = Number(item2.getVCoinPriceForRentingDays(item2.getMinRentingDays()));
		if (p1 == -1 || p1 == NaN) {
			return ORDER_21;
		} else if (p2 == -1 || p2 == NaN) {			
			return ORDER_12;
		} else if (p1 < p2) {			
			return ORDER_12;
		} else if (p1 > p2) {
			return ORDER_21;
		} else {
			return ORDER_MESS;// compareByNameOfBaseItem (item1, item2);
		}
	}
	
	private static function compareByVCoin_DESC (item1 : BaseItem, item2 : BaseItem) : Number {
		if (UNLOCKED_FIRST) {
			if (item1.allowBuy() && !item2.allowBuy()) {
				return ORDER_12;
			} else if (!item1.allowBuy() && item2.allowBuy()) {
				return ORDER_21;
			}
		}
		
		var p1 : Number = item1.getVCoinPriceForRentingDays(item1.getMinRentingDays());
		var p2 : Number = item2.getVCoinPriceForRentingDays(item2.getMinRentingDays());
		if (p1 == -1 || p1 == NaN) {			
			return ORDER_21;
		} else if (p2 == -1 || p2 == NaN) {			
			return ORDER_12;
		} else if (p1 > p2) {			
			return ORDER_12;
		} else if (p1 < p2) {
			return ORDER_21;
		} else {			
			return ORDER_MESS;// compareByNameOfBaseItem (item1, item2);
		}
	}	
	
	private static function compareByNameOfAddOn (item1 : Object, item2 : Object) : Number {
		if (item1.suitable && !item2.suitable) {			
			return ORDER_12;			
		} else if (!item1.suitable && item2.suitable) {
			return ORDER_21;
		} else {
			return compareByNameOfBaseItem (item1.item, item2.item);
		}
	}
	
	private static function compareByGPOfAddOn (item1 : Object, item2 : Object) : Number {
		if (item1.suitable && !item2.suitable) {			
			return ORDER_12;			
		} else if (!item1.suitable && item2.suitable) {
			return ORDER_21;
		} else {
			return compareByGP_ASC (item1.item, item2.item);
		}
	}
}