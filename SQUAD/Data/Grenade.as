﻿import gfx.controls.SQUAD.Data.BaseItem;

/**
 * Class lưu trữ dữ liệu về LỰU ĐẠN
 */
class gfx.controls.SQUAD.Data.Grenade extends BaseItem {
	private var _effectRadius: 	Number;
	private var _baseDamage	 : 	Number;
	
	/**
	 * Hàm tạo
	 *
	 * @param	baseItem BaseItem với dữ liệu đọc từ Common.xml
	 * @param	effectRadius Phạm vi ảnh hưởng
	 * @param	baseDamage	Sát thương cơ bản
	 */
	public function Grenade(
		baseItem: BaseItem,
		effectRadius: Number,
		baseDamage: Number) {
		copyFrom(baseItem);
		_effectRadius = effectRadius;
		_baseDamage = baseDamage
	}
	
	/**
	 * @return Phạm vi ảnh hưởng
	 */
	public function effectRadius():Number {
		return Number(_effectRadius);
	}
	
	/**
	 * @return Sát thương cơ bản
	 */
	public function baseDamage():Number {
		return Number(_baseDamage);
	}
	 
	/**
	 * @return Mô tả lựu đạn
	 */
	public function toString():String {
		var s:String = super.toString();
		s += "\tEffect Radius = " + _effectRadius + "\n";
		s += "\tBase Damage = " + _baseDamage + "\n";
		return s;
	}
}