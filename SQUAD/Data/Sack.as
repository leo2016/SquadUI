﻿import gfx.controls.SQUAD.Data.InventoryItem;

class gfx.controls.SQUAD.Data.Sack {
	public var itemId :	 					Number;
	public var number:						Number;
	public var maxFrags:					Number;
	public var maxOtherGrenades:	Number;
	public var primaryClips:				Number;
	public var secondaryClips:			Number;
	public var primaryWeapon : 		InventoryItem;
	public var secondaryWeapon : 	InventoryItem;
	public var knife : 						InventoryItem;
	public var grenades : 				Array;
		
	public static var TYPE_PRIMARY_WEAPON : 		Number = 0;
	public static var TYPE_SECONDARY_WEAPON :	Number = 1;
	public static var TYPE_KNIFE:							Number = 2;
	public static var TYPE_GRENADE:						Number = 3;
	
	public function Sack (
		itemId : 					Number,
		number: 					Number,
		maxFrags: 				Number,
		maxOtherGrenades: 	Number,
		primaryClips: 			Number,
		secondaryClips: 		Number) {
		this.itemId = itemId;
		this.number = number;
		this.maxFrags = maxFrags;
		this.maxOtherGrenades = maxOtherGrenades;
		this.primaryClips = primaryClips;
		this.secondaryClips = secondaryClips;
		grenades = [];
	}
	
	public function setItem (item : InventoryItem, type: Number) : Void {
		unsetItem (type);
		switch (type) {
			case TYPE_PRIMARY_WEAPON:									
				primaryWeapon = item;
				break;
			case TYPE_SECONDARY_WEAPON:								
				secondaryWeapon = item;
				break;
			case TYPE_KNIFE:											
				knife = item;
				break;
		}
		item.inSack = number;
	}
	
	public function unsetItem (type: Number) : Void {
		switch (type) {
			case TYPE_PRIMARY_WEAPON:
				if (primaryWeapon != null) {
					primaryWeapon.inSack = InventoryItem.NOT_USE;					
					primaryWeapon = null;
				}				
				break;				
			case TYPE_SECONDARY_WEAPON:
				if (secondaryWeapon != null) {
					secondaryWeapon.inSack = InventoryItem.NOT_USE;
					secondaryWeapon = null;
				}				
				break;
			case TYPE_KNIFE:
				if (knife != null) {
					knife.inSack = InventoryItem.NOT_USE;
					knife = null;
				}				
				break;
		}
	}
	
	public function setGrenade (grenade : InventoryItem, grenadeSlot : Number) : Void {		
		unsetGrenade (grenadeSlot);
		grenades [grenadeSlot] = grenade;
		grenade.inSack = number;
		grenade.number = grenadeSlot;
	}
	
	public function unsetGrenade (grenadeSlot : Number) : Void {
		if (grenades[grenadeSlot] != null) {
			grenades [grenadeSlot].inSack = InventoryItem.NOT_USE;		
			grenades [grenadeSlot].number = 0;		
			grenades [grenadeSlot] = null;
		}
	}
	
	public function getFreeGrenadeSlot () : Number {
		var max : Number = maxFrags + maxOtherGrenades;
		for (var i : Number = 1; i <= max; ++i ) {
			if (grenades[i] == null) {
				return i;
			}
		}
		return null;
	}
	
	public function toString () : String {
		var s : String = "";
		s += "SACK\n";
		s += "\tItem Id = " + itemId + "\n";
		s += "\tSack number = " + number + "\n";
		if (primaryWeapon != undefined && primaryWeapon != null) {
			s += "\tPrimary weapon = " + primaryWeapon.baseItem.name () + "\n";
		}
		if (secondaryWeapon != undefined && secondaryWeapon != null) {
			s += "\tSecondary weapon = " + secondaryWeapon.baseItem.name () + "\n";
		}
		if (knife != undefined && knife != null) {
			s += "\tKnife = " + knife.baseItem.name () + "\n";
		}
		s += "\tClips = " + primaryClips + " for primary weapon, " + secondaryClips + " for secondary weapon\n";
		s += "\tGrenades = ";
		for (var x : String in grenades) {
			s += grenades[x].baseItem.name () + ", ";
		}
		s += "\n";
		return s;
	}
}