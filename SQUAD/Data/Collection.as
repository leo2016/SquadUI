﻿import gfx.controls.SQUAD.Data.BaseItem;

/**
 * Class lưu trữ dữ liệu của BỘ SƯU TẬP
 */
class gfx.controls.SQUAD.Data.Collection extends BaseItem {
	private var _itemIds:Array;
	
	/**
	 * Hàm tạo
	 *
	 * @param	baseItem	BaseItem với thông tin đọc từ Common.xml
	 * @param	itemIds	danh sách các id của item nằm trong bộ sưu tập
	 */
	public function Collection(
		baseItem: BaseItem,
		itemIds: Array) {
		copyFrom(baseItem);
		_itemIds = itemIds;
	}
	
	/**
	 * @return Danh sách các item trong bộ sưu tập
	 */
	public function itemIds(): Array {
		return _itemIds;
	}
	
	/**
	 * Kiểm tra một vật phẩm có nằm trong bộ sưu tập này không
	 *
	 * @param	itemId mã vật phẩm
	 * @return		true = có nằm trong bộ sưu tập, false = không nằm trong bộ sưu tập
	 */
	public function containItem (itemId: Number): Boolean {
		for (var x:String in _itemIds) {
			if (_itemIds[x] == itemId) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * @return Mô tả bộ sưu tập
	 */
	public function toString():String {
		var s:String = super.toString();
		s += "\tItems: ";
		for (var x:String in _itemIds) {
			s += _itemIds[x] + ", ";
		}
		s += "\n";
		return s;
	}
}