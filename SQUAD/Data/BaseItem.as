﻿/**
 * Class lưu trữ dữ liệu chung của tất cả các loại vật phẩm
 */
class gfx.controls.SQUAD.Data.BaseItem {
	private var _id: 						Number;
	private var _catalogue: 			Number;
	private var _detailTable:			Number;
	private var _fitting: 				Number;
	private var _name: 				String;
	private var _description: 		String;
	private var _weight: 				Number;
	private var _isExtendAllowed:	Boolean;
	private var _isBuyAllowed: 		Boolean;
	private var _isSellAllowed:		Boolean;
	private var _isGiveAllowed:		Boolean;
	private var _image:				String;
	private var _unlockCondition:	Number;
	private var _unlockRequirement: Number;
	private var _prices:					Array;
	
	/**
	 * Hàm tạo.
	 * BaseItem chỉ lưu trữ các thông tin chung nhất của vật phẩm (đọc từ Common.xml), do đó với mỗi loại vật phẩm,
	 * cần bổ sung các thông tin khác nhau thì mới tạo nên thông tin hoàn chỉnh của vật phẩm đó.
	 *
	 * @param	id							Mã vật phẩm
	 * @param	catalogue 				Mã Catalogue của vật phẩm
	 * @param	detailTable			Enum bảng thông tin chi tiết (dùng để xác định loại vật phẩm)
	 * @param	fitting					Vị trí gắn vật phẩm trên người nhân vật
	 * @param	name					Tên vật phẩm
	 * @param	description			Miêu tả vật phẩm
	 * @param	weight					Cân nặng
	 * @param	isExtendAllowed	Có được gia hạn không
	 * @param	isBuyAllowed			Có được mua không
	 * @param	isSellAllowed			Có được bán không
	 * @param	isGiveAllowed		Có được tặng không
	 * @param	image					Ảnh vật phẩm
	 * @param	unlockCondition		Loại điều kiện để mở khóa vật phẩm
	 * @param	unlockRequirement	Yêu cầu cần đạt được để mở khóa vật phẩm
	 */
	public function BaseItem(
		id: 						Number,
		catalogue: 			Number,
		detailTable: 			Number,
		fitting: 					Number,
		name: 					String,
		description: 			String,
		weight: 					Number,
		isExtendAllowed:	Boolean,
		isBuyAllowed: 		Boolean,
		isSellAllowed: 		Boolean,
		isGiveAllowed: 		Boolean, 
		image:					String, 
		unlockCondition:	Number, 
		unlockRequirement:	Number) {
			
		_id = id;
		_catalogue = catalogue;
		_detailTable = detailTable;
		_fitting = fitting;
		_name = name;
		_description = description;
		_weight = weight;
		_isExtendAllowed = isExtendAllowed == true;
		_isBuyAllowed = isBuyAllowed == true;
		_isSellAllowed = isSellAllowed == true;
		_isGiveAllowed = isGiveAllowed == true;
		_image = image;
		_unlockCondition = unlockCondition;		
		_unlockRequirement = unlockRequirement;
		_prices = undefined;
	}
	
	/**
	 * Tạo bản sao các thuộc tính chung của vật phẩm (sử dụng cho việc mở rộng thêm thông tin của từng loại vật phẩm)
	 *
	 * @param	otherItem Vật phẩm muốn sao chép thông tin
	 */
	public function copyFrom(otherItem: BaseItem):Void {
		_id = otherItem.id();
		_catalogue = otherItem.catalogue();
		_detailTable = otherItem.detailTable ();
		_fitting = otherItem.fitting();
		_name = otherItem.name();
		_description = otherItem.description();
		_weight = otherItem.weight();
		_isExtendAllowed = otherItem.allowExtend();
		_isBuyAllowed = otherItem.allowBuy();
		_isSellAllowed = otherItem.allowSell();
		_isGiveAllowed = otherItem.allowGive();
		_image = otherItem.image();
		_unlockCondition = otherItem.unlockCondition ();
		_unlockRequirement = otherItem.unlockRequirement ();
		_prices = otherItem.prices ();
	}
	
	/**
	 * @return Mã vật phẩm
	 */
	public function id(): Number {
		return _id;
	}
	
	/**
	 * @return Mã Catalogue của vật phẩm
	 */
	public function catalogue():Number {
		return _catalogue;
	}
	
	/**
	 * @return	Enum bảng thông tin chi tiết (dùng để xác định loại vật phẩm)
	 */
	public function detailTable () : Number {
		return _detailTable;
	}
	
	/**
	 * @return Vị trí gắn vật phẩm trên người nhân vật
	 */
	public function fitting():Number {
		return _fitting;
	}
	
	/**
	 * @return Tên vật phẩm
	 */
	public function name():String {
		return _name;
	}
	
	/**
	 * @return Miêu tả vật phẩm
	 */
	public function description():String {
		return _description;
	}
	
	/**
	 * @return Cân nặng
	 */
	public function weight():Number {
		return Number(_weight);
	}
	
	/**
	 * @return Có được gia hạn không
	 */
	public function allowExtend():Boolean {
		return _isExtendAllowed;
	}
	
	/**
	 * @return Có được mua không
	 */
	public function allowBuy():Boolean {
		return _isBuyAllowed;
	}
	
	/**
	 * @return Có được bán không
	 */
	public function allowSell():Boolean {
		return _isSellAllowed;
	}
	
	/**
	 * @return Có được tặng không
	 */
	public function allowGive():Boolean {
		return _isGiveAllowed;
	}
	
	/**
	 * @return Ảnh vật phẩm
	 */
	public function image():String {
		return _image;
	}
	
	/**
	 * @return Loại điều kiện để mở khóa vật phẩm
	 */
	public function unlockCondition():Number {		
		return _unlockCondition;
	}
	
	/**
	 * @return yêu cầu cần đạt được để mở khóa vật phẩm
	 */
	public function unlockRequirement():Number {
		return _unlockRequirement;
	}
	
	/**
	 * Gắn kèm giá vật phẩm
	 *
	 * @param	prices 1 ma trận 2 chiều nx2 chứa giá vật phẩm
	 * 		Với chỉ số hàng là số ngày thuê (mua đứt thì chỉ số = -1)
	 * 		Chỉ số cột là "GP", "VCOIN"
	 *
	 * Chẳng hạn để lấy giá GP khi thuê 3 ngày thì dùng prices[3]["GP"]
	 * Chẳng hạn để lấy giá VCOIN khi mua đứt thì dùng prices[-1]["VCOIN"]
	 */
	public function attachPrices (prices: Array): Void {
		_prices = prices;
	}
	
	/**
	 * @return Mảng chứa giá bán vật phẩm
	 */
	public function get prices (): Array {
		return _prices;
	}
	
	/**
	 * Truy vấn giá tiền của vật phẩm
	 *
	 * @param	rentingDays	Số ngày thuê (nếu mua đứt, có thể truyền vào tham số undefined hoặc null hoặc không truyền)
	 * @param	isGP				Giá truy vấn có phải tính bằng GP không
	 * @return		Giá tiền theo ngày thuê và đơn vị tính truyền vào
	 */
	public function howMuch (rentingDays: Number, isGP: Boolean): Number {
		var row: String ;
		if (rentingDays == undefined || rentingDays == null) {
			row = "";
		} else {
			row = "" + rentingDays;
		}
		if (_prices[row] == undefined || _prices[row] == null) {
			return undefined;
		}
		
		var col: String;
		if (isGP) {
			col = "GP";
		} else {
			col = "VCOIN";
		}
		
		var price: Number = _prices[row][col];
		if (price == undefined || price == null) {
			return undefined;
		} else if (price == -1){
			return undefined;
		} else {
			return price;
		}
	}
	
	/**
	 * @return Mô tả vật phẩm
	 */
	public function toString():String {
		var s:String = "";
		s += "BASE ITEM:\n";
		s += "\tid = " + _id + "\n";
		s += "\tcatalogue = " + _catalogue + "\n";
		s += "\tdetailTable = " + _detailTable + "\n";
		s += "\tfitting = " + _fitting + "\n";
		s += "\tname = " + _name + "\n";
		s += "\tweight = " + _weight + "\n";
		s += "\tdescription = " + _description + "\n";
		s += "\textend = " + _isExtendAllowed + "\n";
		s += "\tbuy = " + _isBuyAllowed + "\n";
		s += "\tsell = " + _isSellAllowed + "\n";
		s += "\tgive = " + _isGiveAllowed + "\n";
		s += "\tprices = \n";
		if (_prices != undefined) {
			for (var x:String in _prices) {
				s += "\t\tRenting days = " + x + ", GP = " + _prices[x]["GP"] + ", Vcoin = " + _prices[x]["VCOIN"] +"\n";
			}
		}
		return s;
	}
	
	public function getMinRentingDays () : Number {
		if (_prices != undefined) {
			var min : Number = Number.MAX_VALUE;
			var canBuy : Boolean = false;
			for (var x:String in _prices) {
				var y : Number = Number(x);
				if (y == -1) {
					canBuy = true;
				} else if (y != NaN) {
					if (min > y) {
						min = y;
					}
				}
			}
			if (min == Number.MAX_VALUE) {//item doesn't have price or just can be bought, not rented
				if (canBuy) {
					return -1;
				} else {
					return 0;
				}
			}
			return min;
		} else {
			return 0;
		}
	}	
	
	public function getGPPriceForRentingDays (days : Number) : Number {
		return _prices[days]["GP"];
	}
	
	public function getVCoinPriceForRentingDays (days : Number) : Number {
		return _prices[days]["VCOIN"];
	}
}