﻿class gfx.controls.SQUAD.Data.Mode {
	public var label : 			String;
	public var id : 				Number;
	public var types : 			Array;
	public var maps : 			Array;
	
	
	public function Mode (
		id : Number, 
		name : String, 
		types : Array, 
		maps : Array
		
	) {
		this.id = id;
		this.label = name;
		this.types = types;
		this.maps = maps
		
	}		
	
	public function toString () : String {
		var s : String = "";
		s = label + "\n";
		s += "\tTypes:\n";
		for (var x : String in types) {
			s += "\t\t" + types[x] + "\n";
		}
		s += "\tMaps:\n";
		for (var x : String in maps) {
			s += "\t\t" + maps[x] + "\n";
		}
		return s;
	}
}