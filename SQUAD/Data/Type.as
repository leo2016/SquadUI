﻿class gfx.controls.SQUAD.Data.Type {
	public var id : 				Number;
	public var label : 			String;	
	public var unit :				String;
	public var destinations :	Array;
	
	public function Type (
		id : Number, 
		name : String, 
		unit : String,
		destinations : Array
	) {
		this.id = id;
		this.label = name;
		this.unit = unit;
		this.destinations = destinations;
	}
	
	public function toString () : String {
		var s : String = "";
		s  += "" + label + "\n";
		s += "\t\tDestinations:\n";
		for (var x : String in destinations) {
			s += "\t\t\t" + destinations[x] + "\n";
		}
		return s;
	}
}