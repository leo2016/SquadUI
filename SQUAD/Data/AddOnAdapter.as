﻿import gfx.controls.SQUAD.Data.AddOn;
import gfx.controls.SQUAD.Data.Gun;
import gfx.controls.SQUAD.Data.InventoryItem;

/**
 * Class quản lý tháo/lắp phụ kiện vào súng
 */
class gfx.controls.SQUAD.Data.AddOnAdapter {	
	//Parameter Type - Dùng các enum này để lấy thông số của súng (có bị ảnh hưởng bởi phụ kiện)	
	public static var PT_ACCURACY : 					Number = 0;
	public static var PT_AMMOS : 					Number = 1;
	public static var PT_DAMAGE_AMPLIFY : 		Number = 2;
	public static var PT_POWER : 						Number = 3;
	public static var PT_RECOIL : 						Number = 4;
	public static var PT_RELOAD_TIME : 			Number = 5;
	public static var PT_SPEED : 						Number = 6;
	
	//AddOn Type - Dùng các enum này để đại diện cho từng loại phụ kiện
	public static var AT_CLIP :							String = "Băng đạn";
	public static var AT_AMMO :						String = "Đạn";
	public static var AT_SCOPE :						String = "Ống ngắm";
	public static var AT_SILENCER :					String = "Ống giảm thanh";
	public static var AT_OTHER:						String = "Phụ kiện khác";
	
	private var m_kGun : 									Gun;
	private var m_aCatalogues :						Array;
	private var m_aAddOnSlots :						Array;
	
	/**
	 * Hàm tạo.
	 * Khi thực hiện xong hàm tạo, mới chỉ biết súng này có các slot nào mà chưa có cụ thể phụ kiện nào đang nằm ở slot nào.
	 * Do đó, nếu đã có phụ kiện nằm trong súng thì phải gọi hàm adapt để lắp phụ kiện vào.
	 * 
	 * @param	gun				Súng muốn quản lý các phụ kiện gắn kèm
	 * @param	catalogues	Danh sách các catalogue (dùng cho các hàm của Gun)
	 */
	public function AddOnAdapter (gun : Gun, catalogues : Array) {
		m_kGun = gun;
		m_aCatalogues = catalogues;
		m_aAddOnSlots = [];
		
		var slots : Array = m_kGun.slots ();
		for (var x : String in slots) {
			m_aAddOnSlots[slots[x].type] = undefined;
		}
	}
	
	/**
	 * Lắp phụ kiện vào súng
	 * 
	 * @param	addOn	phụ kiện
	 * 
	 * @return		true - lắp được, false - không lắp được
	 */
	public function adapt (addOn : AddOn) : Boolean {
		if (m_kGun.canAdaptAddon(addOn, m_aCatalogues)) {
			m_aAddOnSlots [m_aCatalogues[addOn.catalogue()].name()] = addOn;
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Gỡ phụ kiện ra khỏi súng
	 * 
	 * @param	at_xxx	Loại phụ kiện (dùng các enum AT_*** của class này)
	 */
	public function remove (at_xxx : String) : Void {
		for (var x : String in m_aAddOnSlots) {
			if (x == at_xxx) {
				m_aAddOnSlots[x] = undefined;
				break;
			}
		}
	}
	
	/**
	 * Kiểm tra xem súng có slot nào đó không
	 * 
	 * @param	at_xxx	Loại phụ kiện (dùng các enum AT_*** của class này)
	 * 
	 * @return		true - có, false - không
	 */
	public function hasSlot (at_xxx : String) : Boolean {
		for (var x : String in m_aAddOnSlots) {
			if (x == at_xxx) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Lấy ID của phụ kiện mặc định lắp trong súng
	 * 
	 * @param	at_xxx	Loại phụ kiện (dùng các enum AT_*** của class này)
	 * 
	 * @return		BaseItemID của phụ kiện
	 */
	public function getDefaultAddOnId (at_xxx : String) : Number {
		return m_kGun.getDefaultAddOnInSlot (at_xxx);
	}
	
	/**
	 * Lấy 1 phụ kiện đang lắp vào súng
	 * 
	 * @param	at_xxx	Loại phụ kiện (dùng các enum AT_*** của class này)
	 * 
	 * @return		Phụ kiện loại at_xxx đang lắp trong súng
	 */
	public function getAddOn (at_xxx : String) : AddOn {
		for (var x : String in m_aAddOnSlots) {
			if (x == at_xxx) {
				return m_aAddOnSlots[x];
			}
		}
		return undefined;
	}
	
	/**
	 * Kiểm tra xem súng này có thể lắp phụ kiện 'addOn' không
	 * 
	 * @param	addOn	Phụ kiện muốn kiểm tra
	 * 
	 * @return		true - lắp được, false - không lắp được
	 */
	public function canAdapt (addOn : AddOn) : Boolean {
		return m_kGun.canAdaptAddon (addOn, m_aCatalogues);
	}
	
	/**
	 * Tính toán 1 loại thông số của súng sau khi đã bị ảnh hưởng bởi các phụ kiện.
	 * Nếu muốn lấy các thông số nguyên bản của súng (chưa bị ảnh hưởng bởi các phụ kiện), có thể dùng các hàm của Gun (chú ý tham số)
	 * 		accuracy ()				Độ chính xác
	 * 		ammos ()					Số lượng đạn
	 * 		damageAmplifying ()	Độ khuếch đại sát thương
	 * 		power (0)					Độ sát thương (khi không có đạn thì độ sát thương = 0)
	 * 		recoil ()						Độ giật
	 * 		reloadTime (0)			Thời gian thay đạn
	 * 
	 * @param	pt		Loại thông số muốn tính toán
	 * 
	 * @return		Thông số tính toán được
	 */
	public function param (pt : Number) : Number {
		switch (pt) {
			case PT_ACCURACY : {
				return m_kGun.accuracy ();
			}
			case PT_AMMOS : {
				var clipExtraAmmos : Number = 0;
				if (getAddOn(AT_CLIP) != undefined) {
					clipExtraAmmos = Number(getAddOn(AT_CLIP).getProperty("ClipExtraAmmos"));
				}
				return m_kGun.ammos () + clipExtraAmmos;				
			}
			case PT_DAMAGE_AMPLIFY : {
				return m_kGun.damageAmplifying ();				
			}
			case PT_POWER : {
				return m_kGun.power (getAddOn(AT_AMMO).getProperty("AmmoBaseDamage"));
			}
			case PT_RECOIL : {
				return m_kGun.recoil ();
			}
			case PT_RELOAD_TIME : {
				var clipReloadAffect : Number = 0;
				if (getAddOn(AT_CLIP) != undefined) {
					clipReloadAffect = Number(getAddOn(AT_CLIP).getProperty("ClipReloadingSpeedAffect"));
				}
				return m_kGun.reloadTime (clipReloadAffect);
			}
			case PT_SPEED : {
				return m_kGun.speed ();
			}				
		}
		return 0;
	}
}