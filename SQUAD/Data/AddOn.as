﻿import gfx.controls.SQUAD.Data.BaseItem;
import gfx.controls.SQUAD.Data.Catalogue;
import gfx.controls.SQUAD.Data.Gun;

/**
 * Class lưu trữ dữ liệu về PHỤ KIỆN
 */
class gfx.controls.SQUAD.Data.AddOn extends BaseItem {
	private var _properties: Array;
	private var _holders: Array;		
	
	/**
	 * Hàm tạo
	 *
	 * @param	baseItem BaseItem với dữ liệu đọc từ Common.xml
	 * @param	properties	Danh sách các thuộc tính của phụ kiện (mỗi phần tử có { property, value } )
	 * @param	holders	Danh sách các Catalogue hoặc vật phẩm mà phụ kiện hỗ trợ (mỗi phần tử có { id, isCatalogue, isIncluded } )
	 */
	public function AddOn(
		baseItem: BaseItem,
		properties: Array,
		holders: Array) {
		super.copyFrom(baseItem);
		_properties = properties;
		_holders = holders;
	}
	
	/**
	 * @return Danh sách các thuộc tính của phụ kiện
	 */
	public function properties():Array {
		return _properties;
	}
	
	/**
	 * @return Danh sách các Catalogue hoặc vật phẩm mà phụ kiện hỗ trợ
	 */
	public function holders():Array {
		return _holders;
	}
	
	/**
	 * Kiểm tra xem phụ kiện này có thể lắp vào loại súng nào đó không.
	 * Chú ý: Phải kiểm tra 2 chiều:
	 * 	+	Phụ kiện có hỗ trợ súng không:
	 * 		+ Đầu tiên phải kiểm tra súng có nằm trong danh sách không hỗ trợ không?
	 * 			+ Nếu có thì return false
	 * 			+ Nếu không thì tiếp tục kiểm tra súng có nằm trong danh sách được hỗ trợ không
	 * 				+ Nếu không thì return false
	 * 				+ Nếu có thì tiếp tục
	 * 					+	Súng có vị trí để lắp loại phụ kiện đó không
	 * 						+ Nếu có thì return true
	 * 						+ Nếu không thì return false
	 *
	 * @param	gun				Súng
	 * @param	catalogues	Danh sách các catalogue
	 * @return		true = Có thể lắp, false = Không thể lắp
	 */
	public function canAdaptIntoGun (gun: Gun, catalogues: Array):Boolean {
		var gunId: Number = gun.id ();
		var gunCatalogue: Catalogue = catalogues [gun.catalogue ()];
		var thisCatalogueName : String = catalogues[_catalogue].name();
		
		//Kiểm tra xem súng có nằm trong danh sách không được hỗ trợ không
		for (var x:String in _holders) {
			var holder: Object = _holders[x];
			if (!holder.isIncluded) {
				if (holder.isCatalogue) {
					if ( gunCatalogue.isDescendantOf (holder.id) ) {
						return false;
					}
				} else {
					if ( gunId == holder.id ) {
						return false;
					}
				}
			}
		}
		//trace (gun.name() + " isn't in the excluding list of " + _name + "!");
		
		//Kiểm tra xem súng có nằm trong danh sách được hỗ trợ không
		for (var x:String in _holders) {
			var holder: Object = _holders[x];
			if (holder.isIncluded) {
				//Nếu có trong danh sách được hỗ trợ, kiểm tra súng có slot để lắp add-on này không
				if (holder.isCatalogue) {
					//trace ("\tGun catalogue = " + gunCatalogue.name() + " -> is descendant of " + holder.id + " = " + gunCatalogue.isDescendantOf (holder.id));
					if ( gunCatalogue.isDescendantOf (holder.id) ) {
						return gun.hasSlot (thisCatalogueName);
					}
				} else {
					//trace ("\tGun id = " + gunId + " == " + holder.id + " -> " + (gunId == holder.id));
					if ( gunId == holder.id ) {
						return gun.hasSlot (thisCatalogueName);
					}
				}
			}
		}
		//trace (gun.name() + " isn't in the including list of " + _name + "!");
		return false;
	}
	
	/**
	 * Lấy 1 thuộc tính của phụ kiện
	 *
	 * @param	propertyName	Tên thuộc tính (giống như trong bảng Enums,
	 * có thể xem tên các thuộc tính bằng enum["AddOns"]["Property"] với enum là danh sách các enum đọc được bằng EnumsReader)
	 *
	 * @return	Giá trị của thuộc tính (0 nếu không có thuộc tính nào tên như vậy, chú ý: phụ kiện Băng đạn không thể có thuộc tính của phụ kiện Đạn...)
	 */
	public function getProperty(propertyName: String): Number {
		for (var x: String in _properties) {
			if (_properties[x].property == propertyName) {
				return parseFloat(_properties[x].value);
			}
		}
		return 0;
	}
	
	public function getMagnificationLevels () : Array {
		var d : Array = [];
		for (var x: String in _properties) {
			if (_properties[x].property == "Magnification") {
				d.push(parseFloat(_properties[x].value));
			}
		}		
		return d;
	}
	
	public function describeAmmo () : String {
		var s : String = "";
		s += "Sát thương cơ bản: " + this.getProperty ("AmmoBaseDamage") + "\n";
		s += "Bán kính: " + this.getProperty ("AmmoRadius") + "\n";
		s += "Độ dài: " + this.getProperty ("AmmoLength") + "\n";
		s += "Xuyên giáp: " + this.getProperty ("AmmoArmorAffect") + "\n";
		s += "Lực: " + this.getProperty ("AmmoForce") + "\n";
		s += "Tốc độ: " + this.getProperty ("AmmoSpeed") + "\n";
		return s;
	}
	
	public function describeClipItself () : String {
		var s : String = "";
		s += "Số lượng đạn cộng thêm: " + this.getProperty("ClipExtraAmmos") + " viên / băng đạn\n";
		s += "Tốc độ thay đạn bị ảnh hưởng:" + this.getProperty("ClipReloadingSpeedAffect") + "% \n";
		return s;
	}
	
	public function describeClip (ammos : Number, reloadTime : Number) : String {		
		return describeClipAffected (
			ammos, 
			this.getProperty("ClipExtraAmmos"), 
			reloadTime, 
			this.getProperty("ClipReloadingSpeedAffect"));
	}
	
	public static function describeClipAffected (ammos : Number, extraAmmos : Number, reloadTime : Number, reloadTimeAffected : Number) : String {
		var s : String = "";
		s += "Số lượng đạn: " + ammos + "+" + extraAmmos + " viên / băng đạn\n";
		s += "Tốc độ thay đạn:" + (reloadTime * (1 + (reloadTimeAffected/100))) + "\n";
		return s;
	}
	
	public function describeScope () : String {
		return describeScopeAffected (this);
	}
	
	public static function describeScopeAffected (addOn : AddOn) : String {
		if (addOn == null) {
			return "";
		} else {
			var s : String = "";
			s += "Số mức phóng đại: " + addOn.getProperty("ZoomLevel") + " mức\n";
			s += "Các nấc phóng đại: ";
			var ms : Array = addOn.getMagnificationLevels();
			for (var x: String in ms) {
				s += ms[x] + "m, ";
			}
			s += "\nĐộ trễ:" + addOn.getProperty("Delay") + " giây\n";
			if (addOn.getProperty("Water-proof") != 0) {
				s += "Ống ngắm chống nước\n";
			}
			if (addOn.getProperty("Fog-proof") != 0) {
				s += "Ống ngắm chống sương mù\n";
			}
			if (addOn.getProperty("BrighterView") != 0) {
				s += "Ống ngắm nhìn đêm\n";
			}
			return s;
		}
	}
	
	public function describeSilencer () : String {
		return describeSilencerAffected (this);
	}
	
	public static function describeSilencerAffected (addOn : AddOn) : String {
		if (addOn == null) {
			return "";
		} else {
			var s : String = "";
			s += "Mức độ giảm âm: " + addOn.getProperty("SoundReduce") + "%\n";
			s += "Ảnh hưởng sát thương: " + addOn.getProperty("DamageAffect") + "%\n";
			return s;
		}
	}
	
	public function describeOther () : String {
		return describeOtherAffected (this);
	}
	
	public static function describeOtherAffected (addOn : AddOn) : String {
		if (addOn == null) {
			return "";
		} else {
			var s : String = "";			
			var otherFunctions : Array = [
				{ type: "FlashLight", label: "Đèn chiếu sáng" },
				{ type: "RedLaser", label: "Tia la-de đỏ" },
				{ type: "BlueLaser", label: "Tia la-de xanh" },
				{ type: "LaunchGrenade", label: "Ống phóng lựu đạn" }				
			];			
			for (var x : String in otherFunctions) {
				if (addOn.getProperty(otherFunctions[x].type) != 0) {
					s += otherFunctions[x].label + "\n";
					break;
				}
			}
			s += "Khoảng cách áp dụng: " + addOn.getProperty("Range") + "m\n";
			return s;
		}
	}
	
	/**
	 * @return Mô tả phụ kiện
	 */
	public function toString():String {
		var s:String = super.toString();
		for (var x:String in _properties) {
			s += "\t" + _properties[x].property + " = " + _properties[x].value + "\n";
		}
		for (var x:String in _holders) {
			s += "\tHolder = " + _holders[x].id + " (Catalogue: " + _holders[x].isCatalogue + ", Included: " + _holders[x].isIncluded + ")\n";
		}
		return s;
	}
}