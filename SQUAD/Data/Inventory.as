﻿import gfx.controls.SQUAD.Data.InventoryItem;

class gfx.controls.SQUAD.Data.Inventory {
	private var _items : Array;
	
	public function Inventory () {
		_items = [];
	}
	
	public function addItem (item: InventoryItem) : Void {
		_items[item.itemId] = item;
	}
	
	public function removeItem (itemId : Number) : Void {
		var length : Number = _items.length;
		for (var i:Number = 0; i < length; ++i ) {
			if (_items[i].itemId == itemId) {
				_items.splice(i, 1);
				break;
			}
		}
	}
	
	public function getItems () : Array {
		return _items;
	}
	
	public function item (itemId : Number) : InventoryItem {
		return _items[itemId];
	}
	
	public function clear () : Void {
		_items = [];
	}
}