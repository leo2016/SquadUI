﻿import gfx.controls.SQUAD.Data.BaseItem;

/**
 * Class lưu trữ dữ liệu của VẬT PHẨM THỜI TRANG
 */
class gfx.controls.SQUAD.Data.FashionItem extends BaseItem {
	private var _isSexLimited: Boolean;
	private var _isMale: Boolean;
	private var _extraType: String;
	private var _extraValue: Number;
	private var _isCharacterLimited: Boolean;
	private var _characterIds: Array;

	/**
	 * Hàm tạo
	 *
	 * @param	baseItem	BaseItem với dữ liệu đọc từ Common.xml
	 * @param	isSexLimited	Có giới hạn giới tính sử dụng không
	 * @param	isMale Có phải giới hạn giới tính nam không
	 * @param	extraType Loại thuộc tính bổ sung
	 * @param	extraValue Giá trị thuộc tính bổ sung
	 * @param	isCharacterLimited Có giới hạn nhân vật sử dụng không
	 * @param	characterIds Danh sách ID các nhân vật có thể sử dụng vật phẩm
	 */
	public function FashionItem(
		baseItem: BaseItem,
		isSexLimited: Boolean,
		isMale: Boolean,
		extraType: String,
		extraValue: Number,
		isCharacterLimited: Boolean,
		characterIds: Array) {
		copyFrom(baseItem);
		_isSexLimited = isSexLimited == true;
		_isMale = isMale == true;
		_extraType = extraType;
		_extraValue = extraValue;
		_isCharacterLimited = isCharacterLimited == true;
		_characterIds = characterIds;
	}
	
	/**
	 * @return Có giới hạn giới tính sử dụng không
	 */
	public function limitSex(): Boolean {
		return _isSexLimited;
	}
	
	/**
	 * @return Có phải giới hạn giới tính nam không
	 */
	public function limitMale(): Boolean {
		return _isMale;
	}
	
	/**
	 * @return Có giới hạn nhân vật sử dụng không
	 */
	public function limitCharacter(): Boolean {
		return _isCharacterLimited;
	}
	
	/**
	 * @return Loại thuộc tính bổ sung
	 */
	public function extraType(): String {
		return _extraType;
	}
	
	/**
	 * @return Giá trị thuộc tính bổ sung
	 */
	public function extraValue(): Number {
		return _extraValue;
	}
	
	/**
	 * @return Danh sách ID các nhân vật có thể sử dụng vật phẩm
	 */
	public function characterIds():Array {
		return _characterIds;
	}
	
	/**
	 * Kiểm tra xem vật phẩm này có thích hợp với 1 nhân vật nào đó không
	 *
	 * @param	isMale giới tính của nhân vật có phải nam không
	 * @param	characterId	mã nhân vật
	 * @return	true = nhân vật có thể sử dụng vật phẩm này, false = nhân vật không thể sử dụng vật phẩm này
	 */
	public function isSuitableForCharacter (isMale: Boolean, characterId: Number): Boolean {
		//Nếu giới hạn giới tính thì kiểm tra
		if (_isSexLimited) {
			//Nếu giới tính không khớp thì nhân vật không thể mặc được
			if (_isMale != isMale) {
				return false;
			}
		}
		//Nếu giới hạn nhân vật sử dụng thì kiểm tra
		if (_isCharacterLimited) {
			for (var x:String in _characterIds) {
				//Nếu nhân vật có trong danh sách giới hạn thì có thể mặc được
				if (_characterIds[x] == characterId) {
					return true;
				}
			}
			//Nếu nhân vật không có trong danh sách giới hạn thì không thể mặc được
			return false;
		} else {
			//Nếu không giới hạn nhân vật thì có thể mặc được
			return true;
		}
	}
	
	/**
	 * @return Mô tả vật phẩm
	 */
	public function toString():String {
		var s:String = super.toString();
		s += "\tIs Sex Limited = " + _isSexLimited + "\n";
		s += "\tIs Male = " + _isMale + "\n";
		s += "\tExtra type = " + _extraType + "\n";
		s += "\tExtra value = " + _extraValue + "\n";
		s += "\tIs Character Limited = " + _isCharacterLimited + "\n";
		s += "\tCharacter IDs: ";
		for (var x:String in _characterIds) {
			s += _characterIds[x] + ", ";
		}
		s += "\n";
		return s;
	}
}