﻿import gfx.controls.Button;
import gfx.controls.ListItemRenderer;
import gfx.utils.Delegate;

class gfx.controls.SQUAD.MixedListItemRenderer extends ListItemRenderer {
	private var hit : MovieClip;
	/*
	private var btn : Button;
	private var background : Button;
	*/
	
	private function draw () : Void {
		if (sizeIsInvalid) {
			_width = __width;
			_height = __height;
		}
	}
	
	private function configUI () : Void {
		//super.configUI ();
		
		hit.onRollOver 			= Delegate.create (this, handleHitRollOver);
		hit.onRollOut 			= Delegate.create (this, handleHitRollOut);
		hit.onPress				= Delegate.create (this, handleHitPress);
		hit.onRelease				= Delegate.create (this, handleHitRelease);
		hit.onDragOver 			= Delegate.create (this, handleHitDragOver);
		hit.onDragOut			= Delegate.create (this, handleHitDragOut);
		hit.onReleaseOutside	= Delegate.create (this, handleHitReleaseOutside);
		
		if (!_autoSize){
			sizeIsInvalid = true;
		} 
		
		if (focusIndicator != null && !_focused && focusIndicator._totalframes == 1) {
			focusIndicator._visible = false;
		}
		focusTarget = owner;
		
		//btn.addEventListener ("click", this, "handleButtonPress");
	}
	
	private function handleHitRollOver () : Void {
		//trace ("handleHitRollOver");
	}
	
	private function handleHitRollOut () : Void {
		//trace ("handleHitRollOut");
	}
	
	private function handleHitPress () : Void {
		//trace ("handleHitPress");
	}
	
	private function handleHitRelease () : Void {
		//trace ("handleHitRelease");
	}
	
	private function handleHitDragOver () : Void {
		//trace ("handleHitDragOver");
	}
	
	private function handleHitDragOut () : Void {
		//trace ("handleHitDragOut");
	}
	
	private function handleHitReleaseOutside () : Void {
		//trace ("handleHitReleaseOutside");
	}	
	
	public function toString () : String {
		return "[CCK - MixedListItemRenderer "+_name+"]";
	}
	
	/*
	public function handleButtonPress () : Void {
		trace ("handleButtonPress");
	}
	*/
}