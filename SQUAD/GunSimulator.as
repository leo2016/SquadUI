﻿import gfx.core.UIComponent;
import gfx.utils.Delegate;
import gfx.controls.SQUAD.ResolutionHelper;

class gfx.controls.SQUAD.GunSimulator extends UIComponent {
	private function GunSimulator () { 
		super ();		
	}
	
	//Editable configuration	
	public var MIN_TIME_PER_SHOOT : Number 				= 30;
	public var MAX_TIME_PER_SHOOT : Number 				= 1000;
	public var MAX_OPEN_ANGLE_FROM_90_D : Number	= 90; //Total angle = 90 +/- MAX_OPEN_ANGLE_FROM_90_D	
	
	//Contants
	private var FACTOR_DEGREE_TO_RADIAN : Number = Math.PI / 180;
	
	//User-defined variables
	public var m_mcTarget : 			MovieClip;
	public var m_mcCrosshair :			MovieClip;	
	public var m_aBulletIdentifiers : 	Array;
	public var m_iAccurate:				Number;
	public var m_iMaxAccurate:		Number;
	public var m_iRecoil:					Number;
	public var m_iMaxRecoil:			Number;
	public var m_iSpeed:					Number;
	public var m_iMaxSpeed:			Number;
	public var m_iBulletsPerShot:		Number;
	public var m_bAuto : 					Boolean;
	public var m_iMaxImprints : 		Number;	
	public var m_iMovingDistance : 	Number = 20;
	public var m_iReturnPercentage : Number = 5;		
		
	//Private variables	
	private var m_iTurn:						Number;
	private var m_iAngle :						Number;
	private var m_iTimePerShot:			Number;	
	private var m_bIsPullingTrigger : 	Boolean;
	private var m_bIsCharging : 			Boolean;	
	private var m_iXMouseOffset : 		Number;
	private var m_iYMouseOffset : 		Number;
	private var m_iReturnStep : 			Number;
	private var m_aImprints :				Array;
	
	private function preCaculateSize () : Void {		
		
	}
	
	private function convertFromAccToAngle () : Number {
		return MAX_OPEN_ANGLE_FROM_90_D * (1 - (m_iAccurate / m_iMaxAccurate));
	}	
	
	private function shootAt (x : Number, y : Number) : Void {
	//	trace ("Shoot at (" + x + ", " + y + ")");
		
		//attach bullet imprint
		ResolutionHelper.moveToXY (m_aImprints[m_iTurn], x, y);
		m_iTurn = (m_iTurn + 1) % m_iMaxImprints;
		
		//move crosshair
		var direction: Number = (random (m_iAngle * 2) + 90 - m_iAngle) * FACTOR_DEGREE_TO_RADIAN;
		var distance: Number = (random(m_iRecoil) / m_iMaxRecoil) * m_iMovingDistance * m_mcTarget._height / 100;
		var x : Number = -Math.cos (direction) * distance;
		var y : Number = -Math.sin (direction) * distance;
		x = Math.floor(x);
		y = Math.floor(y);
		m_mcCrosshair._x += x;
		m_mcCrosshair._y += y;
		m_iXMouseOffset = m_mcCrosshair._x - this._xmouse;
		m_iYMouseOffset = m_mcCrosshair._y - this._ymouse;		
	}
	
	public function init () : Void {
		m_iReturnStep = m_iReturnPercentage * m_mcTarget._height / 100;
		m_iXMouseOffset = 0;
		m_iYMouseOffset = 0;
		m_aImprints = [];
		for (var i : Number = 0; i < m_iMaxImprints; ++i) {
			m_aImprints[i] = this.attachMovie (
				m_aBulletIdentifiers[random(m_aBulletIdentifiers.length)], 
				"bulletImprint"+i, 
				this.getNextHighestDepth()
			);
	//		trace (m_aImprints[i]);
			ResolutionHelper.moveOut (m_aImprints[i]);
		}
		m_iTurn = 0;
		
		m_mcCrosshair.swapDepths (m_aImprints[m_iMaxImprints-1]);
		
		m_iAngle = convertFromAccToAngle ();
		Mouse.hide();
		m_bIsCharging = false;
		
		m_iAngle 					= convertFromAccToAngle ();		
		if (m_iMaxAccurate < 100 && m_iMaxAccurate > 0) {
			var scale : Number = 100 / m_iMaxAccurate;
			m_iMaxAccurate = 100;
			m_iAccurate *= scale;
		}
		if (m_iMaxRecoil < 100 && m_iMaxRecoil > 0) {
			var scale : Number = 100 / m_iMaxRecoil;
			m_iMaxRecoil = 100;
			m_iRecoil *= scale;
		}
		
		//m_iTimePerShot = MAX_TIME_PER_SHOOT - ( (MAX_TIME_PER_SHOOT - MIN_TIME_PER_SHOOT) * (m_iSpeed / m_iMaxSpeed) );
		m_iTimePerShot = Math.floor(1000 / m_iSpeed);//speed = number of bullets shot per second = per 1000 miliseconds		
		
		this.onMouseMove 	= moveCrosshair;
		this.onEnterFrame 		= returnCrosshair;
		this.onMouseDown 	= pullTrigger;
		this.onMouseUp 		= releaseTrigger;
	}
	
	public function examineTrigger () : Void {
		m_bIsCharging = false;
		if (m_bIsPullingTrigger) {		
			shootAt (m_mcCrosshair._x - this._x, m_mcCrosshair._y - this._y);
			if (m_bAuto) {				
				_global.setTimeout (Delegate.create(this, examineTrigger), m_iTimePerShot);
			} else {
				m_bIsPullingTrigger = false;
			}
			m_bIsCharging = true;
		}
	}
	
	private function moveCrosshair () : Void {
		m_mcCrosshair._x = this._xmouse + m_iXMouseOffset;
		m_mcCrosshair._y = this._ymouse + m_iYMouseOffset;
	}
	
	private function pullTrigger () : Void {
		if (!m_bIsCharging) {
			m_bIsPullingTrigger = true;			
			examineTrigger ();
		}
	}
	
	private function releaseTrigger () : Void {
		m_bIsPullingTrigger = false;	
	}
	
	private function returnCrosshair () : Void {
		if (!m_bIsPullingTrigger && (m_iXMouseOffset != 0 || m_iYMouseOffset != 0)) {
			var distance : Number = Math.sqrt (
				(m_iXMouseOffset * m_iXMouseOffset) +
				(m_iYMouseOffset * m_iYMouseOffset)
			);
			if (m_iReturnStep >= distance) {
				m_iXMouseOffset = 0;
				m_iYMouseOffset = 0;
			} else {
				m_iXMouseOffset *= (1 - (m_iReturnStep / distance));
				m_iYMouseOffset *= (1 - (m_iReturnStep / distance));
			}
			m_mcCrosshair._x = this._xmouse + m_iXMouseOffset;
			m_mcCrosshair._y = this._ymouse + m_iYMouseOffset;				
		}
		
		/*
		if (m_bIsCharging) {
			txtStatus.text = "Đang nạp đạn...";
		} else {
			txtStatus.text = "";		
		}
		*/
	}
	
	public function toString () : String {
		return "[CCK - GunSimulator "+_name+"]";
	}
}