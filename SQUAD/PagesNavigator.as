﻿import gfx.controls.Button;
import gfx.controls.RadioButton;
import gfx.controls.ButtonGroup;
import gfx.controls.SQUAD.ResolutionHelper;
import gfx.core.UIComponent;

/**
 * Component hiển thị thanh điều hướng các trang khi thực hiên phân trang.
 * Người sử dụng tương tác như sau:
 * 		+ Nhìn chung bộ điều hướng gồm có các nút: Trang đầu (F), Các trang trước (P), Các trang sau (N), Trang cuối (L) và 1 số nút điều hướng trực tiếp tới các trang (PPS)
 * 		+ Nếu ở trang đầu tiên thì F không xuất hiện và ngược lại
 * 		+ Nếu ở section đầu tiên (gồm các trang 1, 2, ....PPS) thì P không xuất hiện và ngược lại
 * 		+ Nếu ở section cuối (gồm các trang cuối cùng) thì N không xuất hiện và ngược lại
 * 		+ Nếu ở trang cuối thì L không xuất hiện và ngược lại
 * 		+ Click vào F: Về section 1, chọn trang 1
 * 		+ Click vào P: Về section trước đó, chọn trang đầu tiên trong section
 * 		+ Click vào N: Tới section tiếp theo, chọn trang đầu tiên trong section
 * 		+ Click vào L: Tới section cuối cùng, chọn trang cuối cùng
 */
class gfx.controls.SQUAD.PagesNavigator extends UIComponent {	
	//Model
	private var m_iPagesPerSection:		Number = 5;
	private var m_iLastPage:					Number;
	private var m_iCurrentPage:			Number;
	private var m_iLastSection:				Number;
	private var m_sBtnPageIdentifier:		String;
	
	//View
	private var m_aNavigators:				Array = [];
	private var m_aAllNavigators :		Array = [];
	private var btnFirst :						Button;
	private var btnLast :						Button;
	private var btnPrevious :					Button;
	private var btnNext :						Button;
	
	public function PagesNavigator () {
		super ();
	}

	/**
	 * Về trang đầu tiên
	 */
	public function goToFirstPage ():Void {		
		changeSection (1, 1);		
	}

	/**
	 * Tới trang cuối cùng
	 */
	public function goToLastPage ():Void {		
		changeSection (m_iLastSection, m_iLastPage);		
	}

	/**
	 * Tới section tiếp theo
	 */
	public function goToNextSection ():Void {		
		changeSection (currentSection + 1, firstPageInSection (currentSection + 1));		
	}

	/**
	 * Về section trước đó
	 */
	public function goToPreviousSection ():Void {		
		changeSection (currentSection - 1, firstPageInSection (currentSection - 1));		
	}

	/**
	 * Tới 1 trang cụ thể
	 * 
	 * @param	evt	Đối tượng đi kèm sự kiện "change" của ButtonGroup của các nút btnPageX
	 */
	public function goTo (evt:Object):Void {
		var page:Number = Math.floor (Number (evt.data));
		//trace ("to page " + page);
		if (page != m_iCurrentPage) {
			changeSection ( pageToSection (page), page);	
		}		
	}

	/**
	 * Tìm section tương ứng của 1 trang
	 * 
	 * @param	page	trang muốn tìm section
	 * 
	 * @return		section
	 */
	public function pageToSection (page:Number):Number {
		return Math.ceil (page / m_iPagesPerSection);
	}

	/**
	 * Tìm trang đầu tiên của 1 section
	 * 
	 * @param	section section (>=1, <= m_iLastSection)
	 * 
	 * @return		trang đầu tiên của section
	 */
	public function firstPageInSection (section:Number):Number {
		return ((section - 1) * m_iPagesPerSection) + 1;
	}

	/**
	 * Tìm trang cuối cùng của 1 section
	 * 
	 * @param	section	 section (>=1, <= m_iLastSection)
	 * 
	 * @return		trang cuối cùng của section (chú ý với section cuối, có thể section không có đầy đủ số trang trong 1 section)
	 */
	public function lastPageInSection (section:Number):Number {
		var lastPageInThisSection:Number = (section * m_iPagesPerSection);
		if (lastPageInThisSection < m_iLastPage) {
			return lastPageInThisSection;
		} else {
			return m_iLastPage;
		}
	}

	/**
	 * Tìm vị trí của 1 trang trong danh sách các nút bấm điều hướng
	 * 
	 * @param	page	trang muốn tìm
	 * 
	 * @return		vị trí của (nút bấm) trang trong danh sách m_aNavigators (>=0)
	 */
	private function pageToIndex (page:Number):Number {
		var n:Number = m_aNavigators.length;
		for (var i:Number = 0; i < n; ++i) {
			if (m_aNavigators[i] instanceof RadioButton) {
				if (m_aNavigators[i].data == page) {
					return i;
				}
			}
		}
		return -1;
	}

	/**
	 * Highlight chọn 1 trang.
	 * Đồng thời phát sinh sự kiện "selectPage" có các field sau:
	 * 		+ type			"selectPage"
	 * 		+ target		this
	 * 		+ page			trang được chọn
	 * 		+ section		section của trang được chọn
	 * 
	 * @param	page trang muốn chọn
	 */
	private function selectPage (page:Number):Void {
		var index:Number = pageToIndex (page);
		if (index > -1 && index < m_aNavigators.length) {			
			m_aNavigators[index].selected = true;			
			m_iCurrentPage = page;
			dispatchEvent ( {
				type: 		"selectPage", 
				target: 		this, 
				page: 		m_iCurrentPage, 
				section: 	pageToSection(m_iCurrentPage)
			});
		}
	}

	/**
	 * Thay đổi toàn bộ các nút điều hướng
	 * 
	 * @param	section		số thứ tự section
	 * @param	newPage	số thứ tự trang được chọn
	 */
	private function changeSection (section:Number, newPage:Number):Void {
		//Reset
		delete m_aNavigators;
		m_aNavigators = [];
		hideAllPageNavigators ();		
		var firstPageInThisSection:Number = firstPageInSection (section);
		var lastPageInThisSection:Number = lastPageInSection (section);
		var index:Number = 0;
		//First
		if (newPage > 1) {
			m_aNavigators[index++] = btnFirst;
		}
		//Previous 
		if (section > 1) {
			m_aNavigators[index++] = btnPrevious;
		}
		//Pages 
		var btnPageX:RadioButton;
		for (var p:Number = firstPageInThisSection; p <= lastPageInThisSection; ++p) {
			btnPageX = this["btnPage" + (p - firstPageInThisSection + 1)];
			btnPageX.data = p;
			btnPageX.label = "" + p;
			m_aNavigators[index++] = btnPageX;
		}
		//Next
		if (section < m_iLastSection) {
			m_aNavigators[index++] = btnNext;
		}
		//Last 
		if (newPage < m_iLastPage) {
			m_aNavigators[index++] = btnLast;
		}
		//Render 
		ResolutionHelper.moveToXY (m_aNavigators[0],0,0);
		ResolutionHelper.distributeEvenly (ResolutionHelper.DIR_LEFT_TO_RIGHT,m_aNavigators,5);
		ResolutionHelper.align (ResolutionHelper.ALIGN_TOP, m_aNavigators);		
		//Select
		selectPage (newPage);
	}

	/**
	 * Ẩn toàn bộ các nút điều hướng
	 */
	public function hideAllPageNavigators ():Void {
		for (var x:String in m_aAllNavigators) {
			ResolutionHelper.moveOut (m_aAllNavigators[x]);
		}
	}

	/**
	 * Khởi tạo PagesNavigator.
	 * Phải được gọi trước khi sử dụng.
	 * 
	 * @param	btnPageIdentifier	Linkage của Symbol trong Library sẽ dùng làm các nút tới trang trực tiếp
	 * @param	pagesPerSection	Số trang trong 1 section
	 * @param	lastPage				Tổng số trang
	 */
	public function init (btnPageIdentifier : String, pagesPerSection : Number, lastPage : Number):Void {
		m_sBtnPageIdentifier = btnPageIdentifier;
		m_iPagesPerSection = pagesPerSection;
		m_iLastPage = lastPage;
		m_iLastSection = pageToSection (m_iLastPage);
		
		var index : Number = 0;
		delete m_aAllNavigators;
		m_aAllNavigators = [];
		m_aAllNavigators [index++] = btnFirst;
		m_aAllNavigators [index++] = btnPrevious;
		m_aAllNavigators [index++] = btnNext;
		m_aAllNavigators [index++] = btnLast;
		
		for (var i : Number = 0; i < m_aAllNavigators.length; ++i) {
			m_aAllNavigators[i].removeAllEventListeners ();
		}
		btnFirst.addEventListener ("click",this,"goToFirstPage");
		btnPrevious.addEventListener ("click",this,"goToPreviousSection");
		btnNext.addEventListener ("click",this,"goToNextSection");
		btnLast.addEventListener ("click", this, "goToLastPage");		
		
		var pagesGroup:ButtonGroup = new ButtonGroup ("pagesGroup", this);
		var btnPageX : MovieClip;// RadioButton;
		for (var i : Number = 1; i<=m_iPagesPerSection; ++i) {
			btnPageX = attachMovie(btnPageIdentifier, "btnPage" + i, getNextHighestDepth());
			m_aAllNavigators [index++] = btnPageX;
			btnPageX.group = pagesGroup;
		}	
		pagesGroup.addEventListener ("change", this, "goTo");		
		
		changeSection (1, 1);
	}
	
	/**
	 * Lấy trang hiện tại
	 */
	public function get currentPage () : Number {
		return m_iCurrentPage;
	}
	
	/**
	 * Lấy section hiện tại
	 */
	public function get currentSection () : Number {
		return pageToSection (m_iCurrentPage);
	}
	
	/**
	 * Lấy số trang trong 1 section
	 */
	public function get pagePerSection () : Number {
		return m_iPagesPerSection;
	}
	
	/**
	 * Đổi số trang trong 1 section (phải thay đổi toàn bộ các nút điều hướng)
	 */
	public function set pagePerSection (page : Number) : Void {
		init (m_sBtnPageIdentifier, page, m_iLastPage);
	}
	
	/**
	 * Đổi tổng số trang.
	 * Chẳng hạn: Xóa dữ liệu -> số trang ít đi
	 */
	public function set lastPage (page : Number) : Void {
		m_iLastPage = page;
		m_iLastSection = pageToSection (m_iLastPage);
		if (currentSection == 1 || currentSection == m_iLastSection) {
			changeSection (currentSection, m_iCurrentPage);
		}
	}
	
	public function toString () : String {
		return "[CCK - PagesNavigator "+_name+"]";
	}
}