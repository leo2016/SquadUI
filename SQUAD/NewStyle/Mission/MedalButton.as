﻿import gfx.controls.RadioButton;
import gfx.controls.SQUAD.ImagePathManager;
import gfx.controls.SQUAD.NewStyle.Data.Medal;
import gfx.controls.SQUAD.ResolutionHelper;
import gfx.controls.UILoader;

class gfx.controls.SQUAD.NewStyle.Mission.MedalButton extends RadioButton {
	//Data
	private var m_nMissionID : 		Number;
	private var m_kMedal : 			Medal;
	private var m_bIsInUse : 		Boolean;
	private var m_bIsUnlocked : 	Boolean;	
	
	//GUI
	//public var uiImage : 				UILoader;
	public var mcInUse : 				MovieClip;
	public var mcLocked : 			MovieClip;
	
	public function MedalButton () {
		super ();
	}
	
	private function configUI () : Void {
		super.configUI ();
		
		isInUse = false;
		unlocked = true;
	}
	
	public function get unlocked () : Boolean {
		return m_bIsUnlocked;
	}
	
	public function get isInUse () : Boolean {
		return m_bIsInUse;
	}
	
	public function get medal () : Medal {
		return m_kMedal;
	}
	
	public function get missionID () : Number {
		return m_nMissionID;
	}
	
	public function set unlocked (v : Boolean) : Void {
		m_bIsUnlocked = v;
		mcLocked._visible = !m_bIsUnlocked;
	}
	
	public function set isInUse (v : Boolean) : Void {
		m_bIsInUse = v;
		mcInUse._visible = m_bIsInUse;		
	}
	
	public function set medal (v : Medal) : Void {
		if (v instanceof Medal) {			
			m_kMedal = v;
			//ResolutionHelper.updateUILoader (uiImage, ImagePathManager.MEDALS_BIG + m_kMedal.image);
		}
	}
	
	public function set missionID (v : Number) : Void {
		m_nMissionID = v;
	}	
}