﻿import gfx.controls.SQUAD.NewStyle.Mission.Mission;
import gfx.controls.SQUAD.NewStyle.Mission.MissionStat;

class gfx.controls.SQUAD.NewStyle.Mission.MissionOfPlayer {
	private var m_nMOPID : 				Number;
	private var m_kMission : 			Mission;	
	private var m_bIsCompleted : 	Boolean;
	public var m_bHighlighted : 		Boolean;
	public var m_nProgress : 			Number;
	public var m_nStatus : 			Number;
	
	public function MissionOfPlayer (MOPID : Number, mission : Mission, highlighted : Boolean,status:Number) {
		m_nMOPID 							= MOPID;
		m_kMission 							= mission;
		m_bHighlighted 					= (highlighted == true);
		m_bIsCompleted 					= false;
		m_nStatus 					= status;
	}
	
	public function initStat (requirementID : Number, statID : Number, quantity : Number) : Void {		
		m_kMission.setMyStat (requirementID, statID, quantity);
		checkIfCompleted ();
		m_nProgress = progress;
	}
	
	public function getStat (statID : Number) : MissionStat {
		return m_kMission.getStat (statID);
	}
	
	public function setStat (statID : Number, quantity : Number) : Boolean {
		m_kMission.changeQuantity (statID, quantity);
		return checkIfCompleted ();
	}
	
	public function get MOPID () : Number {
		return m_nMOPID;
	}
	public function get m_Status () : Number {
		return m_nStatus;
	}
	
	public function get missionID () : Number {
		return m_kMission.m_nMissionID;
	}
	
	public function get type () : Number {
		return m_kMission.m_nType;
	}
	
	public function get level () : Number {
		return m_kMission.m_nLevel;
	}
	
	public function get name () : String {
		return m_kMission.m_sName;
	}
	
	public function get EXP () : Number {
		return m_kMission.m_nEXP;
	}
	
	public function get cashInGame () : Number {
		return m_kMission.m_nCashInGame;
	}
	
	public function get baseItemID () : Number {
		return m_kMission.m_nBaseItemID;
	}
	
	public function get rentingDays () : Number {
		return m_kMission.m_nRentingDays;
	}
	
	public function get icon () : String {
		return m_kMission.icon;
	}
	
	public function get stats () : Array/*MissionStat*/ {
		return m_kMission.m_aStats;
	}
	
	public function toString () : String {
		var s : String = m_kMission.toString ();
		s += "\tMOPID = " + m_nMOPID;
		return s;
	}
	
	public function getRequirementsSummary () : String {
		return m_kMission.getRequirementsSummary ();
	}
	
	public function get prizeName () : String {
		return m_kMission.m_sPrizeName;
	}
	
	public function get missionGroupID () : Number {
		return m_kMission.m_nMissionGroupID;
	}
	
	public function checkIfCompleted () : Boolean {
		if (m_bIsCompleted) {
			return;
		}		
		var n : Number = stats.length;		
		for (var i : Number = 0; i < n; ++i ) {
			if (stats[i].quantity < stats[i].maxQuantity) {
				m_bIsCompleted = false;
				return false;
			}
		}
		m_bIsCompleted = true;
		return true;
	}
	
	public function get completed () : Boolean {
		if (!m_bIsCompleted) {
			checkIfCompleted ();
		}
		return m_bIsCompleted;
	}
	
	public function getStatsSummary () : String {
		return m_kMission.getStatsSummary ();
	}
	
	public function get needTracked () : Boolean {
		m_nProgress = progress;
		return (m_nProgress >= 80 && m_nProgress <= 100);
	}
	
	public function get progress () : Number {
		var s : Array = stats;
		var n : Number = s.length;
		var p = s[0].calculateProgress ();
		var count = 1;
		if (n <= 0) {
			return 0;
		} else if (n > 1) {
			for (var i : Number = 1; i < n; ++i ) {
				if (s[i].maxQuantity > 0) {
					p += s[i].calculateProgress ();
					++count;
				}
			}
		}
		return p / count;
	}
}