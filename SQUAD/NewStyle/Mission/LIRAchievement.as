﻿import gfx.controls.ListItemRenderer;
import gfx.controls.SQUAD.ImagePathManager;
import gfx.controls.SQUAD.NewStyle.Mission.MissionOfPlayer;
import gfx.controls.SQUAD.ResolutionHelper;
import gfx.controls.UILoader;
class gfx.controls.SQUAD.NewStyle.Mission.LIRAchievement extends ListItemRenderer {	
	public var uiImage: UILoader;	
		
	public function LIRAchievement () {
		super ();
		addEventListener ("itemDragOver", _parent._parent);
		addEventListener ("itemDragOut", _parent._parent);
	}	
	
	public function setData(data:Object):Void {
		if(data != undefined) {
			this.data = data;
			updateAfterStateChange();			
			this.visible = true;
			this._disabled = false;
		} else {
			this.visible = false;
			this._disabled = true;
		}
	}

	public function updateAfterStateChange():Void {
		if (data instanceof MissionOfPlayer) {
			if (data.m_bHighlighted || data.progress >= 100) {
				ResolutionHelper.updateUILoader (uiImage, ImagePathManager.MISSION_HIGHLIGHTED_ACHIEVEMENT_FOLDER + data.icon);
			} else {
				ResolutionHelper.updateUILoader(uiImage, ImagePathManager.MISSION_ACHIEVEMENT_FOLDER + data.icon);
			}
		}
	}
	private function handleDragOut () : Void {
		super.handleDragOut();
		dispatchEvent ({type: "itemDragOut"});
	}
	
	private function handleDragOver () : Void {
		super.handleDragOver ();
		dispatchEvent ({type: "itemDragOver", item: data});
	}
}