﻿import gfx.controls.ProgressBar;
import gfx.controls.SQUAD.ImagePathManager;
import gfx.controls.SQUAD.NewStyle.Mission.Mission;
import gfx.controls.SQUAD.ResolutionHelper;
class gfx.controls.SQUAD.NewStyle.Mission.MonthlyStatProgressBar extends ProgressBar {	
	private var m_aCheckPoints : 	Array;//Mission
	private var m_nMaxQuantity : 	Number;
	
	public function MonthlyStatProgressBar () {
		super ();
		clearCheckPoints ();
	}
	
	public function clearCheckPoints () : Void {
		m_aCheckPoints = [];
		m_nMaxQuantity = 0;
	}
	
	public function addCheckPoint (m : Mission) : Void {
		if (!(m instanceof Mission)) {
			return;
		}
		var quantity : Number = m.stats[0].m_nMaxQuantity;
		var inserted : Boolean = false;
		var n : Number = m_aCheckPoints.length;
		for (var i : Number = n - 1; i >= 0; --i ) {
			if (m_aCheckPoints[i].stats[0].m_nMaxQuantity < quantity) {
				m_aCheckPoints.splice (i+1, 0, m);
				inserted = true;
				break;
			}
		}
		if (!inserted) {
			m_aCheckPoints.splice (0, 0, m);			
		}
		m_nMaxQuantity = m_aCheckPoints[n].stats[0].m_nMaxQuantity;		
	}
	
	public function setProgress(loaded:Number, total:Number):Void {//don't need total parameter		
		if (m_nMaxQuantity > 0) {
			super.setProgress (loaded, m_nMaxQuantity);
			updateData ();
		}
	}
	
	private function updateData () : Void {
		var n : Number = m_aCheckPoints.length;
		var mc : MovieClip;
		var m : Mission;
		for (var i : Number = 0 ; i < n; ++i ) {			
			mc = this["mcPrize" + (i + 1)];
			if (mc instanceof MovieClip) {
				m = m_aCheckPoints[i];
				if (m instanceof Mission && m.m_sImage != undefined && m.m_sImage != "") {
					ResolutionHelper.updateUILoader (mc.uiImage, ImagePathManager.INVENTORY_ITEM + m.m_sImage);					
					mc.labName.text = m.m_sPrizeName;
				} else {
					mc.uiImage.visible = false;
					mc.labName.visible = false;
				}
			}
		}		
	}
}