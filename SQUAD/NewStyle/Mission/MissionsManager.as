﻿import gfx.controls.SQUAD.NewStyle.Data.BaseItem;
import gfx.controls.SQUAD.NewStyle.Data.Map;
import gfx.controls.SQUAD.NewStyle.Data.Mode;
import gfx.controls.SQUAD.NewStyle.Mission.Mission;
import gfx.controls.SQUAD.NewStyle.Mission.MissionOfPlayer;
import gfx.controls.SQUAD.NewStyle.Mission.MissionsGroup;
import gfx.controls.SQUAD.NewStyle.Mission.MissionStat;
import gfx.controls.SQUAD.NewStyle.XML.Localizer;

class gfx.controls.SQUAD.NewStyle.Mission.MissionsManager {
	private var m_aMissionGroups : 				Array;//MissionsGroup	
	private var m_aMissions : 						Array;//MissionOfPlayer	
	private var m_aModeNames : 				Array;//ModeID -> String
	private var m_aMapNames : 					Array;//MapBlockID -> String
	private var m_aItems : 							Array;//BaseItem
	private var m_kLocalizer : 						Localizer;
	
	//List of cases for mission type
	public static var MT_DAILY : 					Number = 1 << 1;
	public static var MT_MONTHLY : 			Number = 1 << 2;
	public static var MT_MEDAL : 					Number = 1 << 3;
	public static var MT_ACHIEVEMENT : 		Number = 1 << 4;
	public static var MT_SPECIAL : 		Number = 1 << 5;
	
	public function MissionsManager (
		missionGroups : 	Array/*MissionsGroup*/, 		
		localizer : 				Localizer, 
		modes : 				Array/*Mode*/, 
		items : 					Array/*BaseItem*/
	) {
		m_aMissionGroups = missionGroups;		
		MissionStat.setLocalizer (localizer);
		m_kLocalizer = localizer;
		m_aItems = items;
		m_aMissions = [];
		m_aModeNames = [];
		m_aMapNames = [];
		var mode : Mode;
		var map : Map;
		var maps : Array;//Map
		for (var x:String in modes) {
			mode = modes[x];
			if (mode instanceof Mode) {
				m_aModeNames[mode.id] = mode.label;
			}
			maps = mode.maps;
			for (var y:String in maps) {
				map = maps[y];
				if (map instanceof Map) {
					m_aMapNames[map.mapBlockID] = map.label;
				}
			}
		}		
		var mg : MissionsGroup;
		for (var x:String in m_aMissionGroups) {
			mg = m_aMissionGroups[x];
			if (mg instanceof MissionsGroup) {
				mg.initModeMapForMissions (m_aModeNames, m_aMapNames);
				mg.initPrizes (m_aItems, m_kLocalizer);
			}
		}
	}
	
	public function addMission (MOPID : Number, missionID : Number, highlighted : Boolean,status:Number) : Boolean {
		var mission : Mission = findPrototypeMission (missionID);
		//trace ("\t\t\t\tRequest to add mission " + mission.m_sName + " with # of stats = " + mission.m_aStats.length);
		if (mission instanceof Mission) {
			m_aMissions.push (new MissionOfPlayer(MOPID, mission, highlighted,status));
			setCurrentLevel (mission.m_nMissionGroupID, mission.m_nLevel);
			return true;
		} else {
			return false;
		}
	}
	
	private function setCurrentLevel (missionGroupID : Number, level : Number) : Void {
		var n : Number = m_aMissionGroups.length;
		var missionGroup : MissionsGroup;		
		for (var i : Number = 0 ; i < n; ++i ) {
			missionGroup = m_aMissionGroups[i];
			if (missionGroup instanceof MissionsGroup) {
				if (missionGroup.missionGroupID == missionGroupID) {
					missionGroup.m_nCurrentLevel = level;
					return;
				}
			}			
		}		
	}
	
	private function findPrototypeMission (missionID : Number) : Mission {
		var n : Number = m_aMissionGroups.length;
		var missionGroup : MissionsGroup;
		var mission : Mission;
		for (var i : Number = 0 ; i < n; ++i ) {
			missionGroup = m_aMissionGroups[i];
			if (missionGroup instanceof MissionsGroup) {
				mission = missionGroup.getMission (missionID);
				if (mission instanceof Mission) {
					return mission;
				}
			}			
		}
		return undefined;
	}
	
	public function getMission (MOPID : Number) : MissionOfPlayer {
		var mop : MissionOfPlayer;
		for (var x:String in m_aMissions) {
			mop = m_aMissions[x];
			if (mop instanceof MissionOfPlayer) {
				if (mop.MOPID == MOPID) {
					return mop;
				}
			}
		}
		return undefined;
	}
	
	public function getMissionsByType (type : Number/*MissionsManager.MT_...*/) : Array/*MissionOfPlayer*/ {
		var output : Array/*MissionOfPlayer*/ = [];
		var mop : MissionOfPlayer;
		for (var x:String in m_aMissions) {
			mop = m_aMissions[x];
			if (mop instanceof MissionOfPlayer) {
				if (mop.type == type) {
					output.push (mop);
				}
			}
		}
		return output;
	}
	
	public function getMissionByMissionGroupID (missionGroupID : Number) : MissionOfPlayer {
		var mop : MissionOfPlayer;
		for (var x:String in m_aMissions) {
			mop = m_aMissions[x];
			if (mop instanceof MissionOfPlayer) {
				if (mop.missionGroupID == missionGroupID) {
					return mop;//return first mission having that missionGroupID
				}
			}
		}
		return undefined;
	}
	
	public function getMissionByMissionID (missionID : Number) : MissionOfPlayer {
		var mop : MissionOfPlayer;
		for (var x:String in m_aMissions) {
			mop = m_aMissions[x];
			if (mop instanceof MissionOfPlayer) {
				if (mop.missionID == missionID) {
					return mop;//return first mission having that misisonID
				}
			}
		}
		return undefined;
	}
	
	public function removeMission (MOPID : Number) : Boolean {
		var n : Number = m_aMissions.length;
		var mop : MissionOfPlayer;
		for (var i : Number = 0; i < n; ++i ) {
			mop = m_aMissions[i];
			if (mop instanceof MissionOfPlayer) {
				if (mop.MOPID == MOPID) {
					m_aMissions.splice (i, 1);
					return true;
				}
			}
		}
		return false;
	}
	
	public function initStat (MOPID : Number, statID : Number, requirement : Number, quantity : Number) : Void {
		var mission : MissionOfPlayer = getMission (MOPID);
		if (mission instanceof MissionOfPlayer) {
			mission.initStat (requirement, statID, quantity);
		}
	}	
	
	public function setStat (MOPID : Number, statID : Number, newQuantity : Number) : Void {
		var mission : MissionOfPlayer = getMission (MOPID);
		if (mission instanceof MissionOfPlayer) {
			mission.setStat (statID, newQuantity);
		}
	}
	
	public function getStat (MOPID : Number, statID : Number) : MissionStat {
		var mission : MissionOfPlayer = getMission (MOPID);
		if (mission instanceof MissionOfPlayer) {
			return mission.getStat (statID);
		}
		return undefined;
	}
	
	public function clearMissions () : Void {
		var m : Object;// MissionOfPlayer;
		var as : Array;//MissionStat
		var s : Object;// MissionStat;
		while (m_aMissions.length > 0) {
			m = m_aMissions.pop ();			
			delete m;
		}
		//trace ("# OF PROTOTYPE MISSIONS = " + numOfPrototypeMissions);
		//trace ("# OF PROTOTYPE MISSION STATS = " + numOfPrototypeMissionStats);
	}	
	
	private function initModeMapForMissions () : Void {
		var mg : MissionsGroup;
		for (var x:String in m_aMissionGroups) {
			mg = m_aMissionGroups[x];
			
		}
	}
	
	public function toString () : String {
		var s : String = "";
		var n : Number = m_aMissions.length;
		var mop : MissionOfPlayer;
		for (var i : Number = 0; i < n; ++i ) {
			mop = m_aMissions[i];
			if (mop instanceof MissionOfPlayer) {
				s += "Mission: MOPID = " + mop.MOPID + ", MissionID = " + mop.missionID + ", Type = " + mop.type + "\n";
			}
		}
		return s+"-------------------------------------------------------------------------";
	}	
	
	public function isMedalMissionCompleted (missionID : Number) : Boolean {
		var mop : MissionOfPlayer = getMissionByMissionID (missionID);
		if (mop instanceof MissionOfPlayer) {			
			return mop.completed;
		} else {			
			var n : Number = m_aMissionGroups.length;
			var missionGroup : MissionsGroup;
			for (var i : Number = 0 ; i < n; ++i ) {
				missionGroup = m_aMissionGroups[i];
				if (missionGroup instanceof MissionsGroup) {
					if (missionGroup.contains (missionID)) {
						if (missionGroup.m_nCurrentLevel == 0) {
							return false;
						} else {
							var m : Mission = missionGroup.getMission (missionID);
							if (m instanceof Mission) {
								return (m.m_nLevel < missionGroup.m_nCurrentLevel);
							} else {
								return false;
							}
						}
					}
				}			
			}
		}
	}
	
	public function getMissionGroup (missionGroupID : Number) : MissionsGroup {
		var n : Number = m_aMissionGroups.length;
		var missionGroup : MissionsGroup;		
		for (var i : Number = 0 ; i < n; ++i ) {
			missionGroup = m_aMissionGroups[i];
			if (missionGroup instanceof MissionsGroup) {
				if (missionGroup.missionGroupID == missionGroupID) {
					return missionGroup;
				}
			}			
		}
		return undefined;
	}
	
	public function get numOfPrototypeMissions () : Number {
		var sum : Number = 0;
		var n : Number = m_aMissionGroups.length;
		var missionGroup : MissionsGroup;		
		for (var i : Number = 0 ; i < n; ++i ) {
			sum += m_aMissionGroups[i].numOfMissions;
		}
		return sum;
	}
	
	public function get numOfPrototypeMissionStats () : Number {
		var sum : Number = 0;
		var n : Number = m_aMissionGroups.length;
		var missionGroup : MissionsGroup;		
		for (var i : Number = 0 ; i < n; ++i ) {
			sum += m_aMissionGroups[i].numOfStats;
		}
		return sum;
	}
	
	public function setProgress (MOPID : Number, progress : Number) : Void {
		var mission : MissionOfPlayer = getMission (MOPID);
		if (mission instanceof MissionOfPlayer) {
			mission.m_nProgress = progress;			
		}		
	}
	
	public function getProgress (MOPID : Number) : Number {
		var mission : MissionOfPlayer = getMission (MOPID);
		if (mission instanceof MissionOfPlayer) {
			return mission.m_nProgress;			
		}
		return 0;
	}
	
	public function removeMissionsHighlight () : Void {
		var mop : MissionOfPlayer;
		for (var x:String in m_aMissions) {
			mop = m_aMissions[x];
			if (mop instanceof MissionOfPlayer) {				
				mop.m_bHighlighted = false;
			}
		}
	}
}