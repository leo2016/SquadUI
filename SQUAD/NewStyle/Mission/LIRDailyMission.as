﻿import gfx.controls.Button;
import gfx.controls.Label;
import gfx.controls.SQUAD.MixedListItemRenderer;
import gfx.controls.SQUAD.NewStyle.Mission.MissionOfPlayer;
import gfx.controls.SQUAD.NewStyle.Mission.MissionStat;
import gfx.controls.SQUAD.NewStyle.XML.Localizer;
import gfx.controls.SQUAD.StringUtility;
import gfx.controls.TextArea;
import gfx.events.EventDispatcher;

class gfx.controls.SQUAD.NewStyle.Mission.LIRDailyMission extends MixedListItemRenderer {	
	public var txaRequirement : 	TextArea;
	public var labStat : 				Label;
	public var txaPrizes : 				TextArea;
	public var btnReceive : 			Button;
	public var btnComplete : 		Button;
	public static var localizer : 		Localizer = undefined;
	public var MS_IN_PROGRESSL:Number = 1;
	public var MS_COMPLETED:Number = 2; 
	public var MS_EXPIRED:Number = 3;
	public var MS_GIFT:Number = 4;		
		
	public function LIRDailyMission () {
		super ();
		
		EventDispatcher.initialize (this);
		addEventListener ("onBtnReceiveClick", _parent._parent);
		addEventListener ("onBtnCompleteClick", _parent._parent);
	}
	
	private function configUI () : Void {
		super.configUI();
		btnReceive.addEventListener ("click", this, "handleBtnReceiveClick");
		btnComplete.addEventListener ("click", this, "handleBtnCompleteClick");
	}
	
	public function setData(data:Object):Void {
		if (data != undefined) {
			if (localizer instanceof Localizer) {
				btnReceive.label = StringUtility.convert (localizer.localize ("MISSION_RECEIVE")
					, StringUtility.MODE_IN_UTF8, StringUtility.MODE_OUT_NORMAL_VNI);
				btnComplete.label = StringUtility.convert (localizer.localize ("MISSION_COMPLETE")
					, StringUtility.MODE_IN_UTF8, StringUtility.MODE_OUT_NORMAL_VNI);
			}
			this.data = data;
			updateAfterStateChange();			
			this.visible = true;
			this._disabled = false;
		} else {
			this.visible = false;
			this._disabled = true;
		}
	}

	public function updateAfterStateChange():Void {
		if (data instanceof MissionOfPlayer && localizer instanceof Localizer) {
			txaRequirement.text = data.getStatsSummary ();
			var stat : MissionStat = data.stats[0];
			var quantity : Number = stat.quantity;
			var maxQuantity : Number = stat.maxQuantity;
			if (quantity > maxQuantity) {
				quantity = maxQuantity;
			}
			labStat.text = stat.quantityToString ();
			//txaPrizes.text = summaryPrize ();
			btnReceive.disabled = true;
			btnComplete.disabled = true;
			 if (data.m_Status == MS_COMPLETED) {
				//trace("MS_COMPLETED");
				btnReceive.disabled = false;
			}else if (data.m_Status ==MS_IN_PROGRESSL ){
				btnComplete.disabled = false;
				//trace("MS_IN_PROGRESSL")
			}
		}
	}
	
	private function summaryPrize () : String {
		var s : String = "";		
		if (data.EXP > 0) {
			s += localizer.localize ("EXP_PRIZE", [{varName: "**QUANTITY**", varValue: data.EXP}]);
		}
		if (data.cashInGame > 0) {
			s += "\n"+localizer.localize ("CASH_IN_GAME_PRIZE", [{varName: "**QUANTITY**", varValue: data.cashInGame}]);
		}		
		if (data.prizeName != undefined && data.prizeName != null && data.prizeName != "") {
			s += "\n"+data.prizeName;
		}		
		return s;
	}
	
	private function handleBtnReceiveClick () : Void {
		dispatchEvent ( {
			type :		"onBtnReceiveClick", 
			target : 	this, 
			data :		data
		});
	}
	
	private function handleBtnCompleteClick () : Void {
		dispatchEvent ( {
			type :		"onBtnCompleteClick", 
			target : 	this, 
			data :		data
		});
	}
}