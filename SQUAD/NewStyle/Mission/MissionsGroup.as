﻿import gfx.controls.SQUAD.NewStyle.Mission.Mission;
import gfx.controls.SQUAD.NewStyle.Mission.MissionStat;
import gfx.controls.SQUAD.NewStyle.XML.Localizer;
class gfx.controls.SQUAD.NewStyle.Mission.MissionsGroup {
	private var m_nMissionGroupID : 	Number;
	private var m_nType : 					Number;//cases of type are in MissionsManager
	private var m_aMissions : 				Array;//Mission
	private var m_bSorted : 					Boolean;
	public var m_nCurrentLevel : 			Number;
	
	private static var ORDER_12: 									Number = -1;	//Mission 1 appears before Mission 2
	private static var ORDER_MESS:								Number = 0;		//Random order
	private static var ORDER_21: 									Number = 1;		//Mission 1 appears after Mission 2
	
	public function MissionsGroup (
		missionGroupID : 	Number, 
		type : 					Number		
	) {
		m_nMissionGroupID 	= missionGroupID;
		m_nType 					= type;
		m_aMissions 			= [];
		m_bSorted 				= false;
		m_nCurrentLevel 		= 0;
	}
	
	public function get missionGroupID () : Number {
		return m_nMissionGroupID;
	}
	
	public function get type () : Number {
		return m_nType;
	}
	
	public function get missions () : Array/*Mission*/ {
		if (!m_bSorted) {
			m_aMissions.sort (compareMissionLevel);
			m_bSorted = true;
		}
		return m_aMissions;
	}
	
	public function addMission (mission : Mission) : Boolean {
		if (mission instanceof Mission) {			
			mission.m_nType = m_nType;
			mission.m_nMissionGroupID = m_nMissionGroupID;
			m_aMissions.push (mission);	
			m_bSorted = false;
			return true;
		} else {
			return false;
		}
	}
	
	public function contains (missionID : Number) : Boolean {
		var n : Number = m_aMissions.length;
		for (var i : Number = 0; i < n; ++i ) {
			if (m_aMissions[i].m_nMissionID == missionID) {
				return true;
			}
		}
		return false;
	}
	
	public function havePrize (baseItemID : Number) : Boolean {
		var n : Number = m_aMissions.length;		
		for (var i : Number = 0; i < n; ++i ) {
			if (m_aMissions[i].m_nBaseItemID == baseItemID) {
				return true;
			}
		}
		return false;
	}
	
	public function getMission (missionID : Number) : Mission {
		var n : Number = m_aMissions.length;
		for (var i : Number = 0; i < n; ++i ) {
			if (m_aMissions[i].m_nMissionID == missionID) {
				return m_aMissions[i];
			}
		}
		return undefined;
	}
	
	public function toString () : String {
		var s : String = "";
		s += "------\n";
		s += "Mission Group ID = " + m_nMissionGroupID + "\n";
		s += "Type = " + m_nType + "\n";		
		s += "Missions:\n";
		var n : Number = m_aMissions.length;
		for (var i : Number = 0; i < n; ++i ) {
			s += m_aMissions[i].toString ();
		}
		return s;
	}
	
	public static function compareMissionLevel (m1 : Mission, m2 : Mission) : Number {
		if (m1.m_nLevel < m2.m_nLevel) {
			return ORDER_12;
		} else if (m1.m_nLevel > m2.m_nLevel) {
			return ORDER_21;
		} else {
			return ORDER_MESS;
		}
	}	
	
	public function initModeMapForMissions (modeNames : Array, mapNames : Array) : Void {		
		var m : Mission;		
		for (var x:String in m_aMissions) {
			m = m_aMissions[x];
			if (m instanceof Mission) {
				m.initModeMapForStats (modeNames, mapNames);
			}
		}
	}
	
	public function getMissionByLevel (level : Number) : Mission {
		var m : Mission;		
		for (var x:String in m_aMissions) {
			m = m_aMissions[x];
			if (m instanceof Mission) {
				if (m.m_nLevel == level) {
					return m;
				}
			}
		}
		return undefined;
	}
	
	public function initPrizes (items : Array /*BaseItem*/, localizer : Localizer) : Void {
		var m : Mission;
		for (var x:String in m_aMissions) {
			m = m_aMissions[x];
			if (m instanceof Mission) {
				m.initPrize (items, localizer);
			}
		}
	}
	
	public function get numOfMissions () : Number {
		return m_aMissions.length;
	}
	
	public function get numOfStats () : Number {
		var sum : Number = 0;
		var m : Mission;
		for (var x:String in m_aMissions) {
			m = m_aMissions[x];
			if (m instanceof Mission) {
				sum += m.numOfStats;
			}
		}
		return sum;
	}
}