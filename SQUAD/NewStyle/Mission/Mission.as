﻿import gfx.controls.SQUAD.NewStyle.Data.BaseItem;
import gfx.controls.SQUAD.NewStyle.Mission.MissionStat;
import gfx.controls.SQUAD.NewStyle.XML.Localizer;
class gfx.controls.SQUAD.NewStyle.Mission.Mission {
	public var m_nMissionID : 			Number;	
	public var m_nLevel : 					Number;
	public var m_sName : 				String;
	public var m_nEXP : 					Number;
	public var m_nCashInGame : 		Number;
	public var m_nBaseItemID : 		Number;
	public var m_nRentingDays : 		Number;	
	public var m_nType : 					Number;//will be set when this mission is inserted into a group (cases of type are in MissionsManager)
	public var m_nMissionGroupID : 	Number;
	public var m_aStats : 					Array;//MissionStat
	public var m_sPrizeName : 			String;//Item name + renting days (need localized) -> will be updated by MissionsOfPlayer
	public var m_sImage : 				String;//is the same as m_sPrizeName	
	
	public function Mission (
		missionID : 			Number, 		
		level : 					Number, 
		name : 					String, 
		EXP : 					Number, 
		cashInGame :			Number, 
		baseItemID : 			Number, 
		rentingDays : 		Number
	) {
		m_nMissionID 			= missionID;		
		m_nLevel 					= level;
		m_sName 					= name;
		m_nEXP 					= EXP;
		m_nCashInGame 		= cashInGame;
		m_nBaseItemID 			= baseItemID;
		m_nRentingDays 		= rentingDays;
		
		m_aStats 					= [];
	}
	
	public function toString () : String {
		var s : String = "";
		s += "---\n\t";
		s += "MissionID = " + m_nMissionID + "\n\t";
		s += "Level = " + m_nLevel + "\n\t";
		s += "Name = " + m_sName + "\n\t";
		s += "EXP = " + m_nEXP + "\n\t";
		s += "CashInGame = " + m_nCashInGame + "\n\t";
		s += "PrizeID = " + m_nBaseItemID + "\n\t";
		s += "RentingDays = " + m_nRentingDays + "\n\t";	
		s += "Requirements:\n\t\t";
		for (var x:String in m_aStats) {
			s += m_aStats[x] + "\n\t\t";
		}
		return s;
	}
	
	public function addStat (ms : MissionStat) : Void {
		m_aStats.push (ms);
	}
	
	public function setMyStat (requirementID : Number, statID : Number, quantity : Number) : Void {
		//trace ("\t\t\t\t\tRequest to set stat " + statID + " = " + quantity + ", need to find among " + m_aStats.length + " stats!");
		var ms : MissionStat;
		for (var x:String in m_aStats) {
			ms = m_aStats[x];
			if (ms instanceof MissionStat) {
				//trace ("\t\t\t\t\t\tChecking " + ms.requirement);
				if (ms.requirementID == requirementID) {
					//trace ("\t\t\t\t\t\tFound stat!");
					ms.setPersonalStat (statID, quantity);
					return;
				}
			}
		}
	}
	
	public function changeQuantity (statID : Number, quantity : Number) : Void {
		var ms : MissionStat;
		for (var x:String in m_aStats) {
			ms = m_aStats[x];
			if (ms instanceof MissionStat) {
				if (ms.statID == statID) {
					ms.quantity = quantity;
					return;
				}
			}
		}
	}
	
	public function getStat (statID : Number) : MissionStat {
		var numOfStats : Number = m_aStats.length;
		var stat : MissionStat;
		for (var i : Number = 0; i < numOfStats; ++i ) {
			stat = m_aStats[i];
			if (stat instanceof MissionStat) {
				if (stat.statID == statID) {
					return stat;
				}
			}
		}
		return undefined;
	}
	
	public function getRequirementsSummary () : String {
		var s : String = "";		
		var n : Number = m_aStats.length;
		var stat : MissionStat;
		if (n > 1) {
			var count : Number = 0;
			for (var i : Number = 0; i < n; ++i ) {
				stat = m_aStats[i];
				if (stat instanceof MissionStat) {
					if (stat.maxQuantity > 0) {
						++count;
						s += ""+count+". "+stat.description + "\n";
					}
				}			
			}
		} else if (n == 1) {
			stat = m_aStats[0];
			if (stat instanceof MissionStat) {
				if (stat.maxQuantity > 0) {
					s = stat.description;
				}
			}
		}
		return s;
	}
	
	public function getStatsSummary () : String {
		var s : String = "";		
		var n : Number = m_aStats.length;
		var stat : MissionStat;
		if (n > 1) {
			var count : Number = 0;
			for (var i : Number = 0; i < n; ++i ) {
				stat = m_aStats[i];
				if (stat instanceof MissionStat) {
					if (stat.maxQuantity > 0) {
						++count;					
						s += ""+count+". "+stat.descriptionStat + "\n";
					}
				}
			}
		} else if (n == 1) {
			stat = m_aStats[0];
			if (stat instanceof MissionStat) {
				if (stat.maxQuantity > 0) {
					s = stat.descriptionStat;
				}
			}
		}
		return s;
	}
	
	public function initModeMapForStats (modeNames : Array, mapNames : Array) : Void {		
		var ms : MissionStat;		
		for (var x:String in m_aStats) {
			ms = m_aStats[x];
			if (ms instanceof MissionStat) {
				ms.m_sMode = modeNames[ms.modeID];
				ms.m_sMap = mapNames[ms.mapID];
			}
		}
	}
	
	public function get stats () : Array /*MissionStat*/ {
		return m_aStats;
	}
	
	public function initPrize (items : Array /*BaseItem*/, localizer : Localizer) : Void {
		m_sPrizeName = "";
		var item : BaseItem = items[m_nBaseItemID];
		if (item instanceof BaseItem) {
			m_sPrizeName = item.name;
			if (m_nRentingDays != BaseItem.RENT_FOREVER) {
				m_sPrizeName += " - ";
				m_sPrizeName += localizer.localize ("LABEL_DDM_RENTING_DAYS", [ { varName: "**DAYS**", varValue: m_nRentingDays } ]);
			}
			m_sImage = item.image;
		}
	}
	
	public function get numOfStats () : Number {
		return m_aStats.length;
	}
	
	public function get icon () : String {
		return "" + m_nMissionID + ".png";
	}
	
	public function getStatByRequirement (requirementID : Number) : MissionStat {
		var numOfStats : Number = m_aStats.length;
		var stat : MissionStat;
		for (var i : Number = 0; i < numOfStats; ++i ) {
			stat = m_aStats[i];
			if (stat instanceof MissionStat) {
				if (stat.requirementID == requirementID) {
					return stat;
				}
			}
		}
		return undefined;
	}
}