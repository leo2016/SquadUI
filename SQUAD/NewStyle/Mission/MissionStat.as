﻿import gfx.controls.SQUAD.NewStyle.XML.Localizer;
/**
 * Class lưu trữ tiến độ thực hiện 1 yêu cầu trong nhiệm vụ.
  * Các câu lệnh gán trong init được copy từ file MissionData.xlsx (Sheet 'Requirement Type', cột 'Enum in AS')
 */
class gfx.controls.SQUAD.NewStyle.Mission.MissionStat {
	private static var LOCALIZATION_QUANTITY : 				String = "**QUANTITY**";
	private static var LOCALIZATION_MAX_QUANTIY : 		String = "**MAX_QUANTITY**";
	private static var LOCALIZATION_MODE_TAG : 				String = "MISSION_STAT_MODE";
	private static var LOCALIZATION_MODE : 					String = "**MISSION_STAT_MODE**";
	private static var LOCALIZATION_MAP_TAG : 				String = "MISSION_STAT_MAP";
	private static var LOCALIZATION_MAP : 						String = "**MISSION_STAT_MAP**";
	private static var LOCALIZATION_COMPLETED_STAT : 	String = "ST_COMPLETED";
	public static var ALL_MODES : 									String = undefined;
	public static var ALL_MAPS : 										String = undefined;
	private static var m_kLocalizer : 									Localizer = undefined;
	
	private var m_nRequirementID :  	Number;
	private var m_nRequirement : 		Number;	
	private var m_nMaxQuantity : 			Number;	
	private var m_nMode : 					Number;
	private var m_nMap : 					Number;
	private var m_sRequirementTag : 	String;
	private var m_sStatTag : 				String;
	private var m_sDescription : 			String;	
	
	private var m_nStatID : 					Number;
	private var m_nQuantity : 				Number;
	public var m_sMode : 					String;
	public var m_sMap : 						String;
	
	public function MissionStat (
		id : 							Number,
		requirement : 					Number, 
		maxQuantity : 					Number, 
		mode : 								Number, 
		map : 								Number		
	) {
		m_nRequirementID 			= id;
		m_nRequirement 				= requirement;
		m_nMaxQuantity 				= maxQuantity;
		m_nMode 						= mode;
		m_nMap 							= map;
		m_sRequirementTag 			= getRequirementTypeByID (m_nRequirement);
		m_sStatTag 						= getStatTypeByID (m_nRequirement);
		m_sDescription 					= "";			
		
		m_nStatID 							= 0;
		m_nQuantity 						= 0;
		m_sMode 							= ALL_MODES;
		m_sMap 							= ALL_MAPS;
	}
	
	public function calculateProgress () : Number {
		if (m_nMaxQuantity > 0) {
			return (m_nQuantity * 100.0 / m_nMaxQuantity);
		} else {
			return 100;
		}
	}
	
	public function get statID () : Number {
		return m_nStatID;
	}
	
	public function get requirementID () : Number {
		return m_nRequirementID;
	}
	
	public function get quantity () : Number {
		return m_nQuantity;
	}
	
	public static function getRequirementTypeByID (id : Number) : String {
		return "RT_" + id;
	}
	
	public static function getRequirementIDByType (type : String) : Number {
		var elements : Array /*String*/ = type.split ("RT_");
		return Number(elements[1]);
	}
	
	public static function getStatTypeByID (id : Number) : String {
		return "ST_" + id;
	}
	
	public static function getStatIDByType (type : String) : Number {
		var elements : Array /*String*/ = type.split ("ST_");
		return Number(elements[1]);
	}
	
	public static function setLocalizer (localizer : Localizer) : Void {
		m_kLocalizer = localizer;
		if (m_kLocalizer instanceof Localizer) {
			LOCALIZATION_COMPLETED_STAT = m_kLocalizer.localize (LOCALIZATION_COMPLETED_STAT);
		}
	}	
	
	public function get requirement () : Number {
		return m_nRequirement;
	}	
	
	public function get maxQuantity () : Number {
		return m_nMaxQuantity;
	}	
	
	public function get description () : String {
		if (m_sDescription != "") {
			return m_sDescription;
		}
		m_sDescription = "";
		if (m_kLocalizer instanceof Localizer) {
			m_sDescription = m_kLocalizer.localize (
				m_sRequirementTag, 
				[{varName : LOCALIZATION_MAX_QUANTIY, varValue : m_nMaxQuantity}]
			);
			if (m_sMode != ALL_MODES) {				
				m_sDescription += " - " + m_kLocalizer.localize (
					LOCALIZATION_MODE_TAG, 
					[ { varName:LOCALIZATION_MODE, varValue:m_sMode } ]
				);
			}
			if (m_sMap != ALL_MAPS) {
				m_sDescription += " - " + m_kLocalizer.localize (
					LOCALIZATION_MAP_TAG, 
					[ { varName: LOCALIZATION_MAP, varValue: m_sMap } ]
				);
			}
		}		
		return m_sDescription;
	}
	
	public function get descriptionStat () : String {
		if (m_kLocalizer instanceof Localizer) {
			var sModeMap : String = "";
			if (m_sMode != ALL_MODES) {				
				sModeMap += " - " + m_kLocalizer.localize (
					LOCALIZATION_MODE_TAG, 
					[ { varName:LOCALIZATION_MODE, varValue:m_sMode } ]
				);
			}
			if (m_sMap != ALL_MAPS) {
				sModeMap += " - " + m_kLocalizer.localize (
					LOCALIZATION_MAP_TAG, 
					[ { varName: LOCALIZATION_MAP, varValue: m_sMap } ]
				);
			}
			if (m_nQuantity < m_nMaxQuantity) {
				return (m_kLocalizer.localize (
					m_sStatTag, 
					[
						{varName : LOCALIZATION_QUANTITY, varValue : displayQuantity(m_nQuantity) }, 
						{varName : LOCALIZATION_MAX_QUANTIY, varValue : displayQuantity(m_nMaxQuantity) }
					]
				) + sModeMap);
			} else {//completed
				return (m_kLocalizer.localize (
					m_sRequirementTag, 
					[{varName : LOCALIZATION_MAX_QUANTIY, varValue : this.displayQuantity(m_nMaxQuantity) }]
				) + sModeMap + " " + LOCALIZATION_COMPLETED_STAT);
			}
		}		
		return "";
	}
	
	private function displayQuantity (quantity : Number) {
		if (requirement != 47) {//Other requirement
			return quantity;
		} else {//Running requirement (save in cm, display in km)
			return Math.floor(quantity / 10000.0) / 10.0;
		}
	}
	
	public function toString () : String {
		var s : String = "";
		s += "\t\tType = " + m_nRequirement;
		s += "\n\t\tQuantity = " + m_nQuantity+"/"+m_nMaxQuantity;
		s += "\n\t\tMap = " + m_nMap;
		s += "\n\t\tMode = " + m_nMode;
		return s;
	}
	
	public function set quantity (newQuantity : Number) : Void {
		if (newQuantity <= m_nMaxQuantity) {
			m_nQuantity = newQuantity;
		}
	}
	
	public function setPersonalStat (statID : Number, quantity : Number) : Void {
		if (quantity > m_nMaxQuantity) {
			quantity = m_nMaxQuantity;
		}
		m_nStatID 		= statID;
		m_nQuantity 	= quantity;
	}
	
	public function get modeID () : Number {
		return m_nMode;
	}
	
	public function get mapID () : Number {
		return m_nMap;
	}
	
	public function quantityToString () : String {
		if (m_nQuantity > m_nMaxQuantity) {
			m_nQuantity = m_nMaxQuantity;
		}
		return displayQuantity(m_nQuantity) + " / " + displayQuantity(m_nMaxQuantity);
	}
}