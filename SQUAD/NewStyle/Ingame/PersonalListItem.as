﻿import gfx.controls.Label;
import gfx.controls.ListItemRenderer;
import gfx.controls.UILoader;
import gfx.controls.SQUAD.ResolutionHelper;
import gfx.controls.SQUAD.ImagePathManager;


class gfx.controls.SQUAD.NewStyle.Ingame.PersonalListItem extends ListItemRenderer {
	private var labAchieveName:Label;
	private var uiAchievement:UILoader;
	
	public function PersonalListItem () {
		super ();	
	}
	
	public function setData(data:Object):Void {
		if (data != undefined) {
			this.data = data;			
			updateAfterStateChange();
			this.visible = true;
			this._disabled = false;
		} else {
			this.visible = false;
			this._disabled = true;
		}
	}
	
	public function updateAfterStateChange():Void {
		if (data != undefined && (labAchieveName instanceof Label)) {
			labAchieveName.text = data.achievementName;			
			ResolutionHelper.updateUILoader(uiAchievement, data.image);
		}
	}
}