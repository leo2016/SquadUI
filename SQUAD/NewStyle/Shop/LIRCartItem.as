﻿import gfx.controls.SQUAD.NewStyle.Data.BaseItem;
import gfx.controls.SQUAD.MixedListItemRenderer;
import gfx.controls.SQUAD.ImagePathManager;
import gfx.controls.Label;
import gfx.controls.SQUAD.NewStyle.XML.Localizer;
import gfx.controls.SQUAD.ResolutionHelper;
import gfx.controls.SQUAD.StringUtility;
import gfx.controls.UILoader;
import gfx.controls.Button;
import gfx.controls.DropdownMenu;
import gfx.events.EventDispatcher;

class gfx.controls.SQUAD.NewStyle.Shop.LIRCartItem extends MixedListItemRenderer {
	private var labName :			Label;
	private var uiImage :				UILoader;
	private var ddmDays : 			DropdownMenu;	
	private var ddmUnits : 			DropdownMenu;	
	private var btnRemove:			Button;	
	
	public static var localizer :		Localizer;
	
	public function LIRCartItem () {
		super ();
		
		EventDispatcher.initialize (this);
		addEventListener ("onBtnRemoveClick", _parent._parent);
		addEventListener ("onChangeUnit", _parent._parent);			
	}
	
	private function configUI () : Void {
		super.configUI();
				
		ddmDays.labelFunction = ddmDaysLabelFunction;
		ddmUnits.labelFunction = ddmUnitsLabelFunction;
		
		ddmDays.addEventListener ("change", this, "onChangeRentingDay");
		ddmDays.addEventListener ("select", this, "onSelectRentingDay");
		
		ddmUnits.addEventListener ("change", this, "onChangeUnit");
		ddmUnits.addEventListener ("select", this, "onSelectUnit");
		
		btnRemove.addEventListener ("click", this, "onBtnRemoveClick");
	}
	
	public function setData(data:Object):Void {
		if(data != undefined) {
			this.data = data;
			updateAfterStateChange();
			
			this.visible = true;
			this._disabled = false;
		} else {
			this.visible = false;
			this._disabled = true;
		}
	}

	public function updateAfterStateChange():Void {		
		if (data.item instanceof BaseItem && ddmDays instanceof DropdownMenu) {			
			//Image			
			if (data.item.image == undefined || data.item.image == "") {
				ResolutionHelper.updateUILoader (uiImage, ImagePathManager.INVENTORY_ITEM_DEFAULT);
			} else {
				ResolutionHelper.updateUILoader (uiImage, ImagePathManager.INVENTORY_ITEM + data.item.image);
			}
			
			//Item name
			labName.text = data.item.name;	
			
			//Renting days
			var ps : Array = data.item.prices;
			var d : Array = [];
			for (var x : String in ps) {
				if (Number(x) == BaseItem.RENT_FOREVER) {//If this price is buying price, so put it in the bottom of the list
					d[d.length] = { 
						day :		Number(x), 
						gp :		ps[x]["GP"],
						vcoin:	ps[x]["VCOIN"]
					};
				} else {//If this price is renting price, so put it in the middle of the list so that, the minimum is on the top
					var i : Number;
					for (i = 0; i < d.length; ++i ) {					
						if (d[i].day == -1 || d[i].day > Number(x)) {												
							break;
						}
					}					
					d.splice ( i, 0, {
						day :		Number(x), 
						gp :		ps[x]["GP"],
						vcoin:	ps[x]["VCOIN"]
					});									
				}
			}			
			ddmDays.dataProvider = d;		
			ddmDays.selectedIndex = data.dayIndex;
			
			//Unit (automatic by ddmDays.addEventListener ("change", this, "onChangeRentingDay");)
		}
	}
	
	private function ddmDaysLabelFunction (dayObj : Object) : String {
		if (localizer == undefined) {
			return "Uninitialized Localizer";
		}
		var n : Number = Number(dayObj.day);
		if (n == BaseItem.RENT_FOREVER) {
			return localizer.localize ("LABEL_DDM_RENT_FOREVER");
		} else {
			return localizer.localize ("LABEL_DDM_RENTING_DAYS", [{varName: "**DAYS**", varValue: n}]);
		}
	}
	
	private function ddmUnitsLabelFunction (unitObj : Object) : String {
		if (localizer == undefined) {
			return "Uninitialized Localizer";
		}
		if (unitObj.gp) {
			return localizer.localize ("PRICE_IN_GAME", [ {
				varName: "**PRICE**", 
				varValue: 	StringUtility.formatMoney("" + unitObj.price)
			}]);
		} else {
			return localizer.localize ("PRICE_IN_LIFE", [ {
				varName: "**PRICE**", 
				varValue: 	StringUtility.formatMoney("" + unitObj.price)
			}]);
		}
	}
	
	private function onChangeRentingDay (evt : Object) : Void {		
		var dayObj : Object = evt.data;
		var d : Array = [];
		if (dayObj.gp != undefined && dayObj.gp > 0) {
			d.push ( {
				price :	Number(dayObj.gp),
				gp: 		true
			});
		}		
		if (dayObj.vcoin != undefined && dayObj.vcoin > 0) {
			d.push ( {
				price :	Number(dayObj.vcoin),
				gp: 		false
			});
		}
		ddmUnits.dataProvider = d;
		ddmUnits.selectedIndex = data.priceIndex;
	}
	
	private function onChangeUnit (evt : Object) : Void {		
		data.gp = evt.data.gp;		
		data.day = ddmDays.selectedItem.day;
		if (data.gp) {
			data.price = Number(ddmDays.selectedItem.gp);
		} else {
			data.price = Number(ddmDays.selectedItem.vcoin);
		}		
		dispatchEvent ({type :		"onChangeUnit"});
	}
	
	private function onBtnRemoveClick () : Void {
		dispatchEvent ( {
			type :		"onBtnRemoveClick", 
			target : 	this, 
			data :		data, 
			index :		data.index
		});
	}
	
	private function restoreSelection () : Void {
		ddmUnits.selectedIndex = data.priceIndex;
	}
	
	private function onSelectRentingDay () : Void {
		if (!ddmDays.isOpen) {//If user has just closed the dropdown menu
			//then we store the day index, and store new price index
			data.dayIndex = ddmDays.selectedIndex;
			data.priceIndex = 0;			
		}
	}
	
	private function onSelectUnit () : Void {
		if (!ddmUnits.isOpen) {//If user has just closed the dropdown menu
			data.priceIndex = ddmUnits.selectedIndex;			
		}
	}	
}