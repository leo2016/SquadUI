﻿import gfx.controls.ScrollingList;
import gfx.controls.SQUAD.NewStyle.Data.BaseItem;
import gfx.utils.Delegate;

class gfx.controls.SQUAD.NewStyle.Shop.ShoppingCart {	
	public var m_slList ;// : 						ScrollingList;
	public var m_aData : 						Array;
	public var m_iTotalGP :					Number;
	public var m_iTotalVCoin :				Number;
	
	private var m_iIndex :						Number;
	
	public var checkBeforeAdding : 		Function;
	public var checkBeforeRemoving : 	Function;	
	public var doAfterAdding :				Function;
	public var doAfterRemoving :			Function;
	public var doWhenChangePrice :		Function;
	
	public function ShoppingCart (list) {				
		m_aData 							= [];
		m_iTotalGP 						= 0;
		m_iTotalVCoin 					= 0;
		m_iIndex 							= 0;
		
		checkBeforeAdding 			= undefined;
		checkBeforeRemoving 		= undefined;
		doAfterAdding 					= undefined;
		doAfterRemoving 				= undefined;		
		doWhenChangePrice 			= undefined;
		
		m_slList 							= list;
		m_slList.dataProvider 		= m_aData;		
		m_slList.onBtnRemoveClick 	= Delegate.create(this, handleBtnRemoveClick);
		m_slList.onChangeUnit 		= Delegate.create(this, autoCalculate);
	}
	
	public function addItem (item : BaseItem) : Void {
		if (checkBeforeAdding != undefined) {
			if (!checkBeforeAdding(item)) {
				return ;
			}
		}
		
		var order : Number = 0;
		for (var x : String in m_aData) {
			if (m_aData[x].order > order) {
				order = m_aData[x].order;
			}
		}
		var minDay : Number = item.getMinRentingDays ();
		var gp : Boolean = true;
		var price : Number = item.howMuch (minDay, true);
		if (price == undefined) {
			gp = false;
			price = item.howMuch (minDay, false);
		}
		m_aData.push ( {
			item : 			item, 
			day :			minDay, 
			price :			price, 
			gp :			gp,
			order :			order+1,
			index :			m_iIndex,
			dayIndex :		0, 
			priceIndex : 	0
		});
		++m_iIndex;
		m_slList.invalidateData ();
		autoCalculate ();
		
		if (doAfterAdding != undefined) {
			doAfterAdding (item);
		}
		
		//trace (this);
	}
	
	public function removeItemAt (index : Number) : Void {		
		var l : Number = m_aData.length;
		for (var x : Number = 0; x < l; ++x ) {
			if (m_aData[x].index == index) {
				if (checkBeforeRemoving != undefined) {
					if (!checkBeforeRemoving(m_aData[x])) {
						return;
					}
				}
				
				m_aData.splice (x, 1);
				//m_slList.invalidateData ();//after updating new order, we call this again -> we don't need to call twice
				autoCalculate ();
				
				if (doAfterRemoving != undefined) {
					doAfterRemoving ();
				}
				//trace (this);
				return ;
			}
		}
	}
	
	public function autoCalculate () : Void {
		var gp : Number = 0;
		var vc : Number = 0;	
		var change : Boolean = false;
		for (var x : String in m_aData) {
			if (m_aData[x].gp) {
				gp += m_aData[x].price;
			} else {
				vc += m_aData[x].price;
			}
		}
		
		if (m_iTotalGP != gp) {
			m_iTotalGP = gp;
			change = true;
		}
		if (m_iTotalVCoin != vc) {
			m_iTotalVCoin = vc;
			change = true;
		}
		
		if (doWhenChangePrice != undefined && change) {
			doWhenChangePrice ();
		}
	}		
	
	public function handleBtnRemoveClick (evt : Object) : Void {
		removeItemAt (evt.index);
		
		var order : Number = 1;
		var l : Number = m_aData.length;
		for (var i : Number = 0; i < l; ++i ) {
			if (m_aData[i].item instanceof BaseItem) {				
				m_aData[i].order = order;
				++order;				
			}
		}
		m_slList.invalidateData ();
	}
	
	public function clear () : Void {
		while (m_aData.length > 0) {
			m_aData.pop ();
		}
		m_iTotalGP = 0;
		m_iTotalVCoin = 0;		
		m_slList.invalidateData ();
	}
	
	public function getNumberOfItems () : Number {
		return m_aData.length;
	}
	
	public function toString () : String {
		var s : String = "------------------------------------\n";
		for (var x : String in m_aData) {
			s += x + " => " + m_aData[x].item.id + " (" + m_aData[x].item.name + "), ";
			s += m_aData[x].price + (m_aData[x].gp ? " GP" : " VCoin");
			s += "\n";
		}
		return s;
	}
}