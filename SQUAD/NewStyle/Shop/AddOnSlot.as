﻿import gfx.controls.Label;
import gfx.controls.SQUAD.NewStyle.Core.Button2;
import gfx.controls.SQUAD.ResolutionHelper;
import gfx.controls.UILoader;
import gfx.controls.SQUAD.StringUtility;
import gfx.managers.DragManager;

import gfx.controls.SQUAD.NewStyle.Data.BaseItem;
import gfx.controls.SQUAD.DragDropComponent;
import gfx.controls.SQUAD.ImagePathManager;

/**
 * Add-on Slot trong cửa hàng
 */
class gfx.controls.SQUAD.NewStyle.Shop.AddOnSlot extends Button2 {
	public var mcHint :		MovieClip;
	public var uiImage: 	UILoader;
	public var ddc: 			DragDropComponent;
	public var allowDrag:	Boolean;
	public var labTip:		Label;
	
	private var item: BaseItem;	
	public var suitableCatalogueId : Number;
	public var suitableCatalogueName : String;
	public var number : Number;
		
	public function AddOnSlot() {
		super();
		item = undefined;	
		hideHint();		
		allowDrag = true;
	}
	
	private function configUI():Void {
		super.configUI();
		
		ddc.addEventListener("beginDrag", this, "beginDrag");
		ddc.addEventListener("drag", this, "drag");		
		DragManager.instance.addEventListener ("dragEnd", this, "hideHint");		
		
		label = "";		
	}
	
	private function beginDrag(evt:Object):Void {
		if (item == undefined || item == null || !allowDrag) {
			ddc.cancelDrag();
		}
	}
	
	private function drag(evt:Object):Void {
		if (!_disabled && item != null && item != undefined) {
			dispatchEvent ({type: "notifyDrag", target: this, data: item, from: "addOnSlot"});
		}
	}
	
	public function setData (item : BaseItem) : Void {		
		this.item = item;		
		updateAfterStateChange ();
	}
	
	public function unsetData () : Void {
		this.item = undefined;		
		updateAfterStateChange ();
	}
	
	private function updateAfterStateChange():Void {		
		// Redraw should only happen AFTER the initialization.
		if (!initialized) { return; }
		validateNow();// Ensure that the width/height is up to date.

		if (item == undefined) {
			textField.text = StringUtility.convert(suitableCatalogueName, StringUtility.MODE_IN_UTF8, StringUtility.MODE_OUT_UPPER_VNI);
			uiImage.visible = false;			
			labTip.text = "";
		} else {
			textField.text = "";			
			if (item.image == undefined || item.image == "") {
				ResolutionHelper.updateUILoader (uiImage, ImagePathManager.INVENTORY_ITEM_SIDE_SLOTS_DEFAULT);
			} else {
				ResolutionHelper.updateUILoader (uiImage, ImagePathManager.INVENTORY_ITEM_SIDE_SLOTS+ item.image);
			}			
			labTip.text = item.name;			
		}		
		if (constraints != null) { 
			constraints.update(width, height);
		}
		dispatchEvent({type:"stateChange", state:state});
	}
	
	public function showHint () : Void {
		mcHint._visible = true;
	}
	
	public function hideHint () : Void {
		mcHint._visible = false;
		hideTip ();
	}
	
	public function show () : Void {		
		disabled = false;
	}
	
	public function hide () : Void {		
		disabled = true;
	}
	
	public function get data () : BaseItem {
		return item;		
	}
	
	private function handleMouseRollOut(mouseIndex:Number):Void {
		hideTip ();
		super.handleMouseRollOut (mouseIndex);		
	}
	
	private function handleMouseRollOver(mouseIndex:Number):Void {
		showTip ();
		super.handleMouseRollOver (mouseIndex);		
	}
	
	private function handleDragOut(mouseIndex:Number):Void {
		hideTip ();
		super.handleDragOut (mouseIndex);		
	}
	
	private function handleDragOver(mouseIndex:Number):Void {
		showTip ();
		super.handleDragOver (mouseIndex);		
	}
	
	public function showTip () : Void {
		labTip._visible = true;
	}
	
	public function hideTip () : Void {
		labTip._visible = false;		
	}
}