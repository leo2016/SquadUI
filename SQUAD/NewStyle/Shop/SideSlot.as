﻿import gfx.controls.Button;
import gfx.controls.SQUAD.ResolutionHelper;
import gfx.controls.UILoader;
import gfx.controls.SQUAD.StringUtility;
import gfx.managers.DragManager;

import gfx.controls.SQUAD.NewStyle.Data.BaseItem;
import gfx.controls.SQUAD.DragDropComponent;
import gfx.controls.SQUAD.ImagePathManager;

class gfx.controls.SQUAD.NewStyle.Shop.SideSlot extends Button {	
	public var mcHint :		MovieClip;
	public var uiImage: 	UILoader;
	public var ddc: 			DragDropComponent;
	public var allowDrag:	Boolean;	
	//public var txtTip:		TextField;
	
	private var item: BaseItem;	
	public var suitableCatalogueId : Number;
	public var suitableCatalogueName : String;
	public var number : Number;
		
	public function SideSlot() {
		super();
		item = undefined;	
		hideHint();		
		allowDrag = true;
	}
	
	private function configUI():Void {
		super.configUI();
		
		ddc.addEventListener("beginDrag", this, "beginDrag");
		ddc.addEventListener("drag", this, "drag");		
		DragManager.instance.addEventListener ("dragEnd", this, "hideHint");		
		
		label = "";
	}
	
	private function beginDrag(evt:Object):Void {
		if (item == undefined || item == null || !allowDrag) {
			ddc.cancelDrag();
		}
	}
	
	private function drag(evt:Object):Void {
		if (!_disabled && item != null && item != undefined) {			
			dispatchEvent ({type: "notifyDrag", target: this, data: item, from: "sideSlot"});
		}
	}
	
	public function setData (item : BaseItem) : Void {		
		this.item = item;		
		updateAfterStateChange ();
	}
	
	public function unsetData () : Void {
		this.item = undefined;		
		updateAfterStateChange ();
	}
	
	private function updateAfterStateChange():Void {		
		// Redraw should only happen AFTER the initialization.
		if (!initialized) { return; }
		validateNow();// Ensure that the width/height is up to date.

		if (item == undefined) {			
			textField.text = StringUtility.convert(suitableCatalogueName, StringUtility.MODE_IN_UTF8, StringUtility.MODE_OUT_UPPER_VNI);
			uiImage.visible = false;			
			//txtTip.text = "";
		} else {		
			textField.text = "";			
			if (item.image == undefined || item.image == "") {
				ResolutionHelper.updateUILoader (uiImage, ImagePathManager.INVENTORY_ITEM_SIDE_SLOTS_DEFAULT);
			} else {
				ResolutionHelper.updateUILoader (uiImage, ImagePathManager.INVENTORY_ITEM_SIDE_SLOTS + item.image);
			}			
			//txtTip.text = StringUtility.convert(item.name, StringUtility.MODE_IN_UTF8, StringUtility.MODE_OUT_UPPER_VNI);
		}		
		if (constraints != null) { 
			constraints.update(width, height);
		}		
		dispatchEvent({type:"stateChange", state:state});
	}
	
	public function showHint () : Void {
		mcHint._visible = true;
	}
	
	public function hideHint () : Void {
		mcHint._visible = false;		
	}	
	
	public function get data () : BaseItem {
		return item;
	}
	
	/*
	private function handleMouseRollOut(mouseIndex:Number):Void {
		super.handleMouseRollOut (mouseIndex);
		txtTip._visible = false;
	}
	
	private function handleMouseRollOver(mouseIndex:Number):Void {
		super.handleMouseRollOver (mouseIndex);
		txtTip._visible = true;		
	}
	
	private function handleDragOut(mouseIndex:Number):Void {
		super.handleDragOut (mouseIndex);
		txtTip._visible = false;		
	}
	
	private function handleDragOver(mouseIndex:Number):Void {
		super.handleDragOver (mouseIndex);
		txtTip._visible = true;		
	}
	*/
}