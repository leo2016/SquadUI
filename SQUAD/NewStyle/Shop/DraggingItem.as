﻿import gfx.managers.DragManager;
import gfx.controls.SQUAD.NewStyle.Data.BaseItem;
import gfx.controls.SQUAD.ImagePathManager;
import gfx.controls.SQUAD.ResolutionHelper;
import gfx.controls.UILoader;
/*
import gfx.controls.SQUAD.NewStyle.Shop.LIRMainItem;
import gfx.controls.SQUAD.StringUtility;

import gfx.controls.Label;
*/

class gfx.controls.SQUAD.NewStyle.Shop.DraggingItem extends MovieClip {		
	//GUI
	private var uiMainImage										: UILoader;
	/*
	private var labMainName 									: Label;	
	private var uiMainSupportedClass						: UILoader;	
	private var uiExtra												: UILoader;
	private var uiUnlocked										: UILoader;	
	*/
	
	private var _data 												: BaseItem;
	public var bIsDragging										: Boolean;
	
	public function DraggingItem () {
		super ();		
		
		DragManager.instance.addEventListener("dragEnd", this, "hide");
	}
	
	public function setData (data : BaseItem) : Void {
		if (data instanceof BaseItem) {
			//Image
			if (uiMainImage instanceof UILoader) {
				if (data.image == undefined || data.image == "") {
					ResolutionHelper.updateUILoader (uiMainImage, ImagePathManager.INVENTORY_ITEM_DEFAULT);
				} else {
					ResolutionHelper.updateUILoader (uiMainImage, ImagePathManager.INVENTORY_ITEM + data.image);
				}
			}	
			
			/*
			//Name
			if (labMainName instanceof Label) {
				labMainName.text = data.name;
			}		
			
			//Supported Class			
			switch (data.classId) {
				case BaseItem.CLASS_TANKER: {		
					uiMainSupportedClass.visible = true;
					uiMainSupportedClass.source = ImagePathManager.CLASS_ICON_TANKER + LIRMainItem.SELECTED_EXTENSION;
					break;
				}
				case BaseItem.CLASS_SCOUT: {		
					uiMainSupportedClass.visible = true;
					uiMainSupportedClass.source = ImagePathManager.CLASS_ICON_SCOUT + LIRMainItem.SELECTED_EXTENSION;
					break;
				}
				case BaseItem.CLASS_SNIPER: {			
					uiMainSupportedClass.visible = true;
					uiMainSupportedClass.source = ImagePathManager.CLASS_ICON_SNIPER + LIRMainItem.SELECTED_EXTENSION;
					break;
				}
				default : {
					uiMainSupportedClass.visible = false;
					break;
				}
			}			
			
			//Unlock
			uiUnlocked._visible = !data.allowBuy;
			
			//Extra
			if (data.hot) {
				if (uiExtra.source != ImagePathManager.HOT_LABEL) {
					uiExtra.visible = true;				
					uiExtra.source = ImagePathManager.HOT_LABEL;
				}				
			} else if (data.promotion) {
				if (uiExtra.source != ImagePathManager.PROMOTION_LABEL) {
					uiExtra.visible = true;
					uiExtra.source = ImagePathManager.PROMOTION_LABEL;
				}				
			} else {
				uiExtra.visible = false;
			}		
			*/
			
			_data = data;
			bIsDragging = true;
		}
	}
	
	public function hide () : Void {
		ResolutionHelper.moveOut (this);		
		bIsDragging = false;
	}
	
	public function get data () : BaseItem {
		return _data;
	}
}