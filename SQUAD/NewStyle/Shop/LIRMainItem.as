﻿//Default Scaleform UI
import gfx.controls.Label;
import gfx.controls.SQUAD.ResolutionHelper;
import gfx.controls.UILoader;
import gfx.controls.ListItemRenderer;
import gfx.events.EventDispatcher;
//SQUAD UI & Class
import gfx.controls.SQUAD.DragDropComponent;
import gfx.controls.SQUAD.ImagePathManager;
import gfx.controls.SQUAD.StringUtility;
//SQUAD Data
import gfx.controls.SQUAD.NewStyle.Data.BaseItem;
import gfx.controls.SQUAD.NewStyle.XML.Localizer;

class gfx.controls.SQUAD.NewStyle.Shop.LIRMainItem extends ListItemRenderer {
	//GUI
	private var labMainName 									: Label;	
	private var labMainPrice 									: Label;	
	private var uiMainImage										: UILoader;
	private var uiMainSupportedClass							: UILoader;	
	private var uiExtra											: UILoader;	
	private var ddc												: DragDropComponent;
	private var mcMask											: MovieClip;	
	private var labUnlockLevel									: Label;
	
	private var mcLimitLevel									: MovieClip;
	public static var localizer 								: Localizer;
	public static var currentClassType							: Number;
	public static var cashInGameFirst							: Boolean = true;
	public static var isUnlockLockLevels							: Boolean = false;
	public static var isUnlockLockLevel							: Boolean = false;
	
	//Will be read from localization file
	public static var PUC_TRY									: String = "";
	public static var PUC_REMOVE							: String = "";
	public static var PUC_BUY									: String = "";
	public static var PUC_ADD_TO_CART					: String = "";
	public static var PUC_GIVE									: String = "";
	public static var PUC_COMPARE							: String = "";
	public static var PUC_UPGRADE 							: String = "";
	public static var EXTENSION 								: String = ".png";
	public static var SELECTED_EXTENSION 				: String = "-selected.png";
	private var currentExtension								: String;
	public static var rankName 								:String = "";
	
	public function LIRMainItem () {
		super ();
		
		EventDispatcher.initialize (this);
		addEventListener ("handleItemDrag", _parent._parent);
		addEventListener ("handleScrollItem", _parent._parent);
	}
	
	private function configUI () : Void {
		super.configUI ();
		
		ddc.addEventListener("beginDrag", this, "beginDrag");
		ddc.addEventListener("drag", this, "drag");		
	}
	
	private function beginDrag(evt:Object):Void {				
		if (data == undefined || data == null || !allowDrag()) {			
			ddc.cancelDrag();
		}		
	}		
	
	private function drag(evt:Object):Void {
		if (!disabled) {
			dispatchEvent( { type: "handleItemDrag", target: this, data: data, from: _parent._parent } );
		}
	}
	
	public function setData(data:Object):Void {
		if(data != undefined) {
			this.data = data;
			updateAfterStateChange();
			
			this.visible = true;
			this._disabled = false;
		} else {
			this.visible = false;
			this._disabled = true;
		}
	}

	public function updateAfterStateChange():Void {
		if (data instanceof BaseItem) {
			currentExtension = (
				((selected /*|| state == "over" || state == "down"*/) && !disabled) ? 
				SELECTED_EXTENSION : EXTENSION
			);
			
			//Name	
			if (currentExtension == SELECTED_EXTENSION) {
				labMainName.htmlText = StringUtility.colorText (data.name, "#262626");
			} else {
				labMainName.text = data.name;
			}
			
			//Image
			if (uiMainImage instanceof UILoader) {				
				if (data.image == undefined || data.image == "") {
					ResolutionHelper.updateUILoader (uiMainImage, ImagePathManager.INVENTORY_ITEM_DEFAULT);
				} else {
					ResolutionHelper.updateUILoader (uiMainImage, ImagePathManager.INVENTORY_ITEM + data.image);
				}
			}			
			
			//Supported Class			
			var imgPath : String = "";
			switch (data.classId) {
				
				case BaseItem.CLASS_TANKER: {							
					imgPath = ImagePathManager.CLASS_ICON_TANKER + currentExtension;
					break;
				}
				case BaseItem.CLASS_SCOUT: {									
					imgPath = ImagePathManager.CLASS_ICON_SCOUT + currentExtension;
					break;
				}
				case BaseItem.CLASS_SNIPER: {					
					imgPath = ImagePathManager.CLASS_ICON_SNIPER + currentExtension;
					break;
				}
				default : {					
					uiMainSupportedClass.visible = false;
					break;
				}
			}
			ResolutionHelper.updateUILoader (uiMainSupportedClass, imgPath);
			
			//Extra
			if (data.hot) {
				ResolutionHelper.updateUILoader (uiExtra, ImagePathManager.HOT_LABEL);
			} else if (data.promotion) {
				ResolutionHelper.updateUILoader (uiExtra, ImagePathManager.PROMOTION_LABEL);
			} else {
				uiExtra.visible = false;
			}
			
			//Price
			if (localizer == undefined) {
				labMainPrice.text = "Uninitialized Localizer";
			} else {
				var day : Number = data.getMinRentingDays();
				if (cashInGameFirst) {
					var price : Number = data.howMuch (day, true);
					if (price != undefined) {				
						labMainPrice.text = localizer.localize ("PRICE_IN_GAME", [ {
							varName: "**PRICE**", 
							varValue: 	StringUtility.formatMoney(""+price)
						}]);
					} else {
						price = data.howMuch (day, false);
						if (price != undefined) {				
							labMainPrice.text = localizer.localize ("PRICE_IN_LIFE", [ {
								varName: "**PRICE**", 
								varValue: 	StringUtility.formatMoney("" + price)
							}]);
						} else {
							labMainPrice.text = "???";
						}
					}		
				} else {
					var price : Number = data.howMuch (day, false);
					if (price != undefined) {				
						labMainPrice.text = localizer.localize ("PRICE_IN_LIFE", [ {
							varName: "**PRICE**", 
							varValue: 	StringUtility.formatMoney(""+price)
						}]);
					} else {
						price = data.howMuch (day, true);
						if (price != undefined) {				
							labMainPrice.text = localizer.localize ("PRICE_IN_GAME", [ {
								varName: "**PRICE**", 
								varValue: 	StringUtility.formatMoney("" + price)
							}]);
						} else {
							labMainPrice.text = "???";
						}
					}
				}
			}
			if (currentExtension == SELECTED_EXTENSION) {
				labMainPrice.htmlText = StringUtility.colorText (labMainPrice.text, "#262626");
			}
			//Disable (for add-on)
			if (data.isDisabled) {
				mcLimitLevel._visible = true;
				mcLimitLevel.labUnlockLevel._visible = false;
				ddc.cancelDrag();
				
			} else if (data.classId != currentClassType && data.classId != BaseItem.CLASS_NONE) {
			} else {
				mcLimitLevel._visible = false;
				mcLimitLevel.labUnlockLevel._visible = false;
			}
			mcMask._visible = mcLimitLevel._visible;	
		}
	}	
	
	private function allowDrag () : Boolean {
		return data.allowBuy;
	}
	private function handleScrollItem():Void {
		//super.handleScroll(event);
		dispatchEvent( {
			type: "scroll", 
			target: this,
			data: data
		});
	}
}