import gfx.controls.ListItemRenderer;
import gfx.controls.Label;
import gfx.controls.SQUAD.MixedListItemRenderer;
import gfx.controls.UILoader;
import gfx.events.EventDispatcher;
import gfx.controls.Button;
import gfx.controls.SQUAD.NewStyle.XML.Localizer;
import gfx.controls.SQUAD.ResolutionHelper;
import gfx.controls.SQUAD.ImagePathManager;
import gfx.controls.SQUAD.NewStyle.Mission.MissionOfPlayer;
//import gfx.controls.SQUAD.NewStyle.XML.Localizer;

class gfx.controls.SQUAD.NewStyle.Menu.LIRPopupItem extends ListItemRenderer {	
	private var labName:Label;
	private var UIItem:UILoader;
	public static var localizer : 		Localizer = undefined;
	// gia tri tuong ung voi CatalogueID trong DB
	private var ECT_ALL:Number = 0;		// Bao gom tat ca catalog
	private var ECT_WEAPON:Number = 1;		// Catalogue: Vu khi -> BO SUNG THEM
	private var ECT_FASHION:Number = 2;	// Catalogue: Nhan vat
	private var ECT_ADDON:Number = 8;		// Catalogue: Addon
	private var ECT_MAIN_GUN:Number = 11;	// Catalogue: Sung chinh
	private var ECT_SUB_GUN:Number = 12;	// Catalogue: sung phu
	private var ECT_KNIFE:Number = 13;		// Catalogue: Dao
	private var ECT_GRENADE:Number = 14;	// Catalogue: Luu dan
	private var ECT_GP:Number = 61;	// Catalogue: GP
	private var ECT_EXP:Number = 62;	// Catalogue: EXP
	private var ECT_SPECIAL:Number = 63;
	
	public function LIRPopupItem() {
		super();
		EventDispatcher.initialize(this);
	}	
	
	private function configUI () : Void {
		super.configUI();
	}
	public function setData(data:Object):Void {
		if (data != undefined) {
			this.data = data;
			updateAfterStateChange();	
			this.visible = true;
			this._disabled = false;	
			
		}else {
			this.visible = false;
			this._disabled = true;
		}
	}
		
	public function updateAfterStateChange():Void {
		if (data != undefined) {
			if (labName instanceof Label ) {
				//trace("l")
				if (data.type == ECT_EXP) {
					labName.text = " + " + data.value +" " + data.name;
					if(UIItem instanceof UILoader){
						ResolutionHelper.updateUILoader(UIItem, ImagePathManager.INVENTORY_ITEM + data.image);
					}
				}else if( data.type == ECT_GP) {
					labName.text = " + " + data.value +" " + data.name;
					if(UIItem instanceof UILoader){
						ResolutionHelper.updateUILoader(UIItem, ImagePathManager.INVENTORY_ITEM + data.image);
					}
				}else if (data.type == ECT_ADDON) {
					labName.text = data.name ;
					if(UIItem instanceof UILoader){
						ResolutionHelper.updateUILoader(UIItem, ImagePathManager.INVENTORY_ITEM + data.image);
					}
				}else if(data.type == ECT_SPECIAL) {
						ResolutionHelper.updateUILoader(UIItem, ImagePathManager.INVENTORY_ITEM + data.image);
						labName.text = data.name ;
				}else {
					if(UIItem instanceof UILoader){
						ResolutionHelper.updateUILoader(UIItem, ImagePathManager.INVENTORY_ITEM + data.image);
						if (localizer instanceof Localizer) {
							labName.text = data.name +" ( " +localizer.localize("LABEL_DDM_RENTING_DAYS", [ { varName: "**DAYS**", varValue:data.rantingDay } ]) + ")";
						}
					}	
				}
			}
		}
	}
}
