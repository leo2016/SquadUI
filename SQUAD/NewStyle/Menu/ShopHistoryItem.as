﻿import gfx.controls.SQUAD.DateFormatter;
import gfx.controls.SQUAD.NewStyle.XML.Localizer;
class gfx.controls.SQUAD.NewStyle.Menu.ShopHistoryItem {
	private var m_bSelected : 					Boolean;
	private var m_iId : 								Number;
	private var m_iYear : 							Number;
	private var m_iMonth : 						Number;
	private var m_iDate : 							Number;
	private var m_iHour : 							Number;
	private var m_iMinute : 						Number;
	private var m_iSecond : 						Number;
	private var m_sItemName : 				String;
	private var m_iPrice : 							Number;
	private var m_sCashType : 					String;
	private var m_iNote : 							Number;
	
	private var m_sNote : 						String;
	private var m_sFullDateTime : 				String;
	
	public static var INVALID_NOTE_TAG : 	String = "INVALID_NOTE_TAG";
	private static var m_bInitialized : 			Boolean = false;
	private static var m_aEventTypes :		Array;
	private static var m_kLocalizer : 			Localizer;
	
	/**
	 * Hàm tạo. Phải khởi tạo danh sách các ghi chú trước bằng
	 * ShopHistoryItem.init (xxx, yyy);
	 * trong đó xxx là danh sách các ghi chú về loại sự kiện, lấy được = enums ["PlayerLogs"]["EventType"]
	 * còn yyy là localizer
	 * 
	 * @param	id
	 * @param	year
	 * @param	month
	 * @param	date
	 * @param	hour
	 * @param	minute
	 * @param	second
	 * @param	itemName
	 * @param	price
	 * @param	cashType
	 * @param	note
	 */
	public function ShopHistoryItem (
		id : 				Number, 
		year : 			Number, 
		month : 		Number, 
		date : 			Number, 
		hour : 			Number, 
		minute : 		Number, 
		second : 		Number, 
		itemName : 	String, 
		price : 			Number, 
		cashType : 	String, 
		note : 			Number		
	) {
		if (m_bInitialized) {
			m_iId 					= id;
			m_iYear					= year;
			m_iMonth  			= month;
			m_iDate 				= date;
			m_iHour				= hour;
			m_iMinute				= minute;
			m_iSecond 			= second;
			m_sItemName		= itemName;
			m_iPrice				= price;
			m_sCashType		= cashType;
			m_iNote				= note;
			m_bSelected 			= false;
			
			m_sFullDateTime 	= DateFormatter.format(
				m_iYear, m_iMonth, m_iDate, m_iHour, m_iMinute, m_iSecond, 
				m_kLocalizer.localize ("TRANS_HIST_SHOP_TIME_FORMAT")
			);			
			m_sNote				= m_kLocalizer.localize(noteTypeToNoteTag (m_iNote));
		} else {
			trace ("ShopHistoryItem wasn't initialized! Please use ShopHistoryItem.init (...); first!");
		}
	}
	
	public function noteTypeToNoteTag (noteType : Number) : String {		
		for (var x:String in m_aEventTypes) {
			if (m_aEventTypes[x] == noteType) {
				return "TRANS_HIST_SHOP_"+x;
			}
		}
		return INVALID_NOTE_TAG;
	}
	
	public function noteTagToNoteType (noteTag : String) : Number {
		return m_aEventTypes[noteTag];
	}	
	
	public static function init (eventTypes : Array, localizer : Localizer) : Void {
		DateFormatter.init (m_kLocalizer, false);
		m_aEventTypes = eventTypes;
		m_kLocalizer = localizer;
		m_bInitialized = true;		
	}
	
	public function get id () : Number {
		return m_iId;
	}
	
	public function get date () : Number {
		return m_iDate;
	}
	
	public function get month () : Number {
		return m_iMonth;
	}
	
	public function get year () : Number {
		return m_iYear;
	}
	
	public function get hour () : Number {
		return m_iHour;
	}
	
	public function get minute () : Number {
		return m_iMinute;
	}
	
	public function get second () : Number {
		return m_iSecond;
	}
	
	public function get fullDateTime () : String {
		return m_sFullDateTime;
	}
	
	public function get item () : String {
		return m_sItemName;
	}
	
	public function get price () : Number {
		return m_iPrice;
	}
	
	public function get cashType () : String {
		return m_sCashType;
	}
	
	public function get note () : String {
		return m_sNote;
	}	
	
	public function set selected (s : Boolean) : Void {
		m_bSelected = s;
	}
	
	public function get selected () : Boolean {
		return m_bSelected;
	}
	
	public function get initialzed () : Boolean {
		return m_bInitialized;
	}
	
	public function get noteType () : Number {
		return m_iNote;
	}
	
	public static function getRandomNoteType () : Number {//just for testing
		if (!m_bInitialized) {
			return 0;
		}
		var i : Number = random (5);
		switch (i) {
			case 0 :				
				return Number(m_aEventTypes["BuyItem"]);
			case 1 :
				return Number(m_aEventTypes["SellItem"]);
			case 2 :
				return Number(m_aEventTypes["RepairItem"]);
			case 3 :
				return Number(m_aEventTypes["ExtendItem"]);
			default : 
				return Number(m_aEventTypes["UpgradeGun"]);
		}		
	}
}