﻿import gfx.controls.SQUAD.DateFormatter;
import gfx.controls.SQUAD.NewStyle.XML.Localizer;
class gfx.controls.SQUAD.NewStyle.Menu.CashHistoryItem {
	private var m_bSelected : 							Boolean;
	private var m_iId : 										Number;
	private var m_iYear : 									Number;
	private var m_iMonth : 								Number;
	private var m_iDate : 									Number;
	private var m_iHour : 									Number;
	private var m_iMinute : 								Number;
	private var m_iSecond : 								Number;
	private var m_iCash : 									Number;
	private var m_iPrice : 									Number;	
	private var m_iMethod : 							Number;
	
	private var m_sMethod : 							String;
	private var m_sFullDateTime : 						String;
	
	public static var INVALID_METHOD_TAG : 	String = "INVALID_METHOD_TAG";
	private static var m_bInitialized : 					Boolean = false;
	private static var m_aEventTypes :				Array;
	private static var m_kLocalizer : 					Localizer;
	
	/**
	 * Hàm tạo. Phải khởi tạo danh sách các ghi chú trước bằng
	 * CashHistoryItem.init (xxx, yyy);
	 * trong đó xxx là danh sách các ghi chú về loại sự kiện, lấy được = enums ["PlayerLogs"]["EventType"]
	 * còn yyy là localizer
	 * 
	 * @param	id
	 * @param	year
	 * @param	month
	 * @param	date
	 * @param	hour
	 * @param	minute
	 * @param	second
	 * @param	cash
	 * @param	price
	 * @param	method
	 */
	public function CashHistoryItem (
		id : 				Number, 
		year : 			Number, 
		month : 		Number, 
		date : 			Number, 
		hour : 			Number, 
		minute : 		Number, 
		second : 		Number, 		
		cash : 			Number,
		price : 			Number, 		
		method : 		Number		
	) {
		if (m_bInitialized) {
			m_iId 					= id;
			m_iYear					= year;
			m_iMonth  			= month;
			m_iDate 				= date;
			m_iHour				= hour;
			m_iMinute				= minute;
			m_iSecond 			= second;
			m_iCash				= cash;
			m_iPrice				= price;			
			m_iMethod			= method;
			m_bSelected 			= false;
			
			m_sFullDateTime 	= DateFormatter.format(
				m_iYear, m_iMonth, m_iDate, m_iHour, m_iMinute, m_iSecond, 
				m_kLocalizer.localize ("TRANS_HIST_CASH_TIME_FORMAT")
			);			
			m_sMethod				= m_kLocalizer.localize(methodTypeToMethodTag (m_iMethod));
		} else {
			trace ("CashHistoryItem wasn't initialized! Please use ShopHistoryItem.init (...); first!");
		}
	}
	
	public function methodTypeToMethodTag (methodType : Number) : String {		
		for (var x:String in m_aEventTypes) {
			if (m_aEventTypes[x] == methodType) {
				return "TRANS_HIST_CASH_"+x;
			}
		}
		return INVALID_METHOD_TAG;
	}
	
	public function methodTagToMethodType (methodTag : String) : Number {
		return m_aEventTypes[methodTag];
	}	
	
	public static function init (eventTypes : Array, localizer : Localizer) : Void {
		DateFormatter.init (m_kLocalizer, false);
		m_aEventTypes = eventTypes;
		m_kLocalizer = localizer;
		m_bInitialized = true;		
	}
	
	public function get id () : Number {
		return m_iId;
	}
	
	public function get date () : Number {
		return m_iDate;
	}
	
	public function get month () : Number {
		return m_iMonth;
	}
	
	public function get year () : Number {
		return m_iYear;
	}
	
	public function get hour () : Number {
		return m_iHour;
	}
	
	public function get minute () : Number {
		return m_iMinute;
	}
	
	public function get second () : Number {
		return m_iSecond;
	}
	
	public function get fullDateTime () : String {
		return m_sFullDateTime;
	}
	
	public function get cash () : Number {
		return m_iCash;
	}
	
	public function get price () : Number {
		return m_iPrice;
	}
		
	public function get method () : String {
		return m_sMethod;
	}	
	
	public function set selected (s : Boolean) : Void {
		m_bSelected = s;
	}
	
	public function get selected () : Boolean {
		return m_bSelected;
	}
	
	public function get initialzed () : Boolean {
		return m_bInitialized;
	}
	
	public function get methodType () : Number {
		return m_iMethod;
	}
	
	public static function getRandomMethodType () : Number {//just for testing
		if (!m_bInitialized) {
			return 0;
		}
		var i : Number = random (2);
		switch (i) {
			case 0 :				
				return Number(m_aEventTypes["ChargeVCoin"]);			
			default : 
				return Number(m_aEventTypes["RewardVCoin"]);
		}		
	}
}