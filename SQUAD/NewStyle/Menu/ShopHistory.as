﻿import gfx.controls.CheckBox;
import gfx.controls.Label;
import gfx.controls.SQUAD.MixedListItemRenderer;
import gfx.controls.SQUAD.NewStyle.Menu.ShopHistoryItem;

class gfx.controls.SQUAD.NewStyle.Menu.ShopHistory extends MixedListItemRenderer {
	//GUI
	private var chkSelected : 			CheckBox;
	private var labTime : 					Label;
	private var labItem : 					Label;
	private var labPrice : 					Label;
	private var labCashType : 			Label;
	private var labNote : 					Label;
	
	public function ShopHistory () {
		super ();				
	}
	
	public function setData(data:Object):Void {
		if(data != undefined) {
			this.data = data;
			updateAfterStateChange();			
			this.visible = true;
			this._disabled = false;
		} else {
			this.visible = false;
			this._disabled = true;
		}
	}
	
	private function configUI () : Void {
		super.configUI ();
		
		chkSelected.addEventListener ("select", this, "handleSelectItem");
	}
	
	public function updateAfterStateChange():Void {
		if (data instanceof ShopHistoryItem) {
			//Check box
			chkSelected.selected = data.selected;			
			
			//Time
			labTime.text = data.fullDateTime;
			
			//Item
			labItem.text = data.item;
			
			//Price
			labPrice.text = "" + data.price;
			
			//Cash Type
			labCashType.text = data.cashType;
			
			//Note
			labNote.text = data.note;
		}
	}
	
	private function handleSelectItem (evt : Object) : Void {				
		data.selected = chkSelected.selected;
	}
}