﻿import gfx.controls.ListItemRenderer;
import gfx.controls.Label;
import gfx.controls.SQUAD.MixedListItemRenderer;
import gfx.controls.SQUAD.NewStyle.Mission.MissionStat;
import gfx.controls.UILoader;
import gfx.events.EventDispatcher;
import gfx.controls.Button;
import gfx.controls.SQUAD.ResolutionHelper;
import gfx.controls.SQUAD.ImagePathManager;
import gfx.controls.SQUAD.NewStyle.Mission.MissionOfPlayer;



class gfx.controls.SQUAD.NewStyle.Menu.MissionLevelUp extends ListItemRenderer {	
	private var labContent:Label;
	private var labQuantity:Label;
	private var UIStatus:UILoader;
	private var success:Number = 0;
	private var fail:Number = 1;
	public static var iscompletes:Number = 0;

	
	public function MissionLevelUp() {
		super();
		EventDispatcher.initialize(this);
	}	
	
	private function configUI () : Void {
		super.configUI();
	}
	public function setData(data:Object):Void {
		if(data != undefined) {
			this.data = data;
			updateAfterStateChange();	
			this.visible = true;
			this._disabled = false;	
			
		}else {
			this.visible = false;
			this._disabled = true;
		}
	}
		
	public function updateAfterStateChange():Void {
		if (data != undefined) {
			if (labContent instanceof Label ) {
				var stat : MissionStat = data.stats[0];
				var quantity : Number = stat.quantity;
				var maxQuantity : Number = stat.maxQuantity;
				labQuantity.text = stat.quantityToString();
				labContent.text =  data.getStatsSummary ();
			}
			if (data.completed) {
				ResolutionHelper.updateUILoader(UIStatus, ImagePathManager.STATUS_FOLDER +"icon_trangthai.png");
			}
		}
	}
}
