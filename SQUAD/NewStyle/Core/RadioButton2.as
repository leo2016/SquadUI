﻿import flash.external.ExternalInterface;
import gfx.controls.SQUAD.NewStyle.Core.Button2;
[InspectableList("disabled", "visible", "labelID", "selected", "inspectableGroupName", "data", "disableConstraints", "bindingEnabled")]
class gfx.controls.SQUAD.NewStyle.Core.RadioButton2 extends Button2 {
	public function RadioButton() { 
		super();
		if (_group == null) { group = "buttonGroup"; }
	}
	public function toString():String {
		return "[RadioButton2 " + _name + "]";
	}
	private function configUI():Void {
		if (bindingEnabled){			
			ExternalInterface.call("__registerControl", this._name, this, "RadioButton");
			addEventListener("select", this, "dispatchEventToGame");
			bindingEnabled = false;
		}
		super.configUI();	
	}
}