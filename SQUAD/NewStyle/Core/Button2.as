﻿import gfx.controls.Button;

/**
 * Hoạt động giống hệt Button nhưng ngay khi người dùng vừa click thì Button sẽ mất focus ngay.
 * Với Button (cũ), do khi click xong vẫn còn focus nên người dùng có thể nhấn ENTER để thực hiện click.
 */
class gfx.controls.SQUAD.NewStyle.Core.Button2 extends Button {	
	private function handleMouseRelease(mouseIndex:Number, button:Number):Void {
		super.handleMouseRelease (mouseIndex, button);
		focused = false;
	}
	
	private function handleRelease():Void {
		super.handleRelease ();
		focused = false;
	}
	
	private function handleReleaseOutside(mouseIndex:Number, button:Number):Void {
		super.handleReleaseOutside (mouseIndex, button);
		focused = false;		
	}
	
	public function toString():String {
		return "[Button2 " + _name + "]";
	}
}