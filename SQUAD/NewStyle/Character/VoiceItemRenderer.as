﻿import gfx.controls.Button;
import gfx.controls.CF.ItemRendererReplacer;
import gfx.controls.Label;
import gfx.controls.ListItemRenderer;
import gfx.controls.UILoader;
import gfx.events.EventDispatcher;
import gfx.controls.SQUAD.MixedListItemRenderer;
import gfx.interfaces.IListItemRenderer;
import gfx.controls.SQUAD.ResolutionHelper;
import gfx.controls.SQUAD.ImagePathManager;

class gfx.controls.SQUAD.NewStyle.Character.VoiceItemRenderer extends ListItemRenderer {	
	private var txtName: TextField;
	private var btnPlay:	Button;	
	private var btnBG: Button;	
	public var ID: Number;
	public var fileName: String;
	public var index:Number
	private var UIIcon:UILoader;
	private var play:Number = 0;
	private var stop:Number = 1;
				
	public function VoiceItemRenderer() {
		super();				
		
		EventDispatcher.initialize(this);
		//addEventListener ("onBtnPlayClick", _parent._parent);
		//addEventListener ("onbtnBGClick", _parent._parent);
	}	
	private function configUI () : Void {
		super.configUI();
				
		//btnPlay.addEventListener ("click", this, "onBtnPlayClick");
		//btnBG.addEventListener("click", this, "onbtnBGClick")
	}
	public function setData(data:Object):Void {
		if(data != undefined) {
			this.data = data;
			updateAfterStateChange();
			
			this.visible = true;
			this._disabled = false;
		} else {
			this.visible = false;
			this._disabled = true;
		}
	}
		
	public function updateAfterStateChange():Void {
		if(data != undefined){
			textField.text = data.name;
			ID = data.ID;
			fileName = data.fileName;
			index = data.index;
			
			if (UIIcon instanceof UILoader) {
				if(data.isPlay==play){
					ResolutionHelper.updateUILoader(UIIcon, ImagePathManager.PLAYSOUND );
					trace(ImagePathManager.PLAYSOUND);
				}else {
					ResolutionHelper.updateUILoader(UIIcon, ImagePathManager.PAUSESOUND);
					trace(ImagePathManager.PAUSESOUND)
				}
			}
			
		}
	}
	private function onBtnPlayClick () : Void {
		dispatchEvent ( {
			type :		"onBtnPlayClick", 
			target : 	this, 
			fileName :		fileName, 
			ID :		ID 
		});
	}
	private function onbtnBGClick () : Void {
		dispatchEvent ( { type :		"onbtnBGClick",
						target : 	this, 
						index :		index,
						ID: ID});
	}
	
}	
