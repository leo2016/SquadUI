﻿import gfx.controls.SQUAD.CPPSimulator;
import gfx.controls.SQUAD.ImagePathManager;
import gfx.controls.SQUAD.NewStyle.Data.Map;
import gfx.controls.SQUAD.NewStyle.MiniMap.*;
import gfx.controls.SQUAD.NewStyle.Radar.*;
import gfx.controls.SQUAD.StringUtility;

class gfx.controls.SQUAD.NewStyle.Navigation2D {
	//Customization
	private static var SHOW_ICON_TEMPORARILY : Number = 1500;
	private static var RADAR_MAP_SCALE : Number = 0.04;
	
	//Data (set directly from C++)
	public var m_aInfo : 				Array;//Number
	
	//Internal data
	private static var NAVIGATION_TYPE_COUNT : Number = 2; //Then list them below
	private var m_kRadar : 			SQUADRadar;
	private var m_kMiniMap : 		SQUADMiniMap;	
	
	private var m_nMyID : 			Number;
	private var m_nMaxIcons : 		Number;
	private var m_nInfoLength : 	Number;
	private var m_nInitStep : 		Number;
	private var m_fOnRequestInit : 	Function;
	
	//Enum NavigationIconInfoType
	private static var NIIT_IN_USE : 				Number;
	private static var NIIT_NID : 					Number;
	private static var NIIT_TEAM : 					Number;
	private static var NIIT_TYPE : 					Number;
	//private static var NIIT_INITIALIZED : 		Number;
	private static var NIIT_ITP_POSITION_X : 		Number;
	private static var NIIT_ITP_POSITION_Y : 		Number;
	private static var NIIT_ITP_PROGRESS : 			Number;
	private static var NIIT_POSITION_X : 			Number;
	private static var NIIT_POSITION_Y : 			Number;
	private static var NIIT_POSITION_Z : 			Number;
	private static var NIIT_ROTATION : 				Number;
	private static var NIIT_STATE : 				Number;
	private static var NIIT_VISIBLE_IN_RADAR : 		Number;	
	private static var NIIT_VISIBLE_IN_MAP : 		Number;	
	private static var NIIT_LEVEL_A_STAGE : 		Number;
	private static var NIIT_LEVEL_A_ACTIVE :		Number;
	private static var NIIT_COUNT : 				Number;
	
	//Enum NavigationBoolean
	private static var NBOOL_TRUE : 				Number = 1;
	private static var NBOOL_FALSE : 				Number = 0;
	
	//Enum NavigationIconVisible
	private static var NIV_START_HIDE : 			Number;
	private static var NIV_HIDE : 					Number;
	private static var NIV_START_SHOW : 			Number;
	private static var NIV_SHOW : 					Number;
	private static var NIV_START_SHOW_THEN_HIDE : 	Number;
	private static var NIV_SHOW_THEN_HIDE : 		Number;
	private static var NIV_COUNT : 					Number;
	
	//Alpha 
	private static var alpha:Number = 5;
	
	public function Navigation2D (radar : SQUADRadar, miniMap : SQUADMiniMap, onRequestInit : Function) {
		//Internal data
		m_aInfo 			= new Array ();
		m_nMyID 			= 0;
		m_nMaxIcons 		= 0;
		m_nInfoLength 		= 0;
		m_nInitStep 		= 0;
		m_fOnRequestInit 	= onRequestInit;
		
		//Enum
		var enum : Number = 0;
		NIIT_IN_USE 			= enum++;
		NIIT_NID 				= enum++;
		NIIT_TEAM 				= enum++;
		NIIT_TYPE 				= enum++;
		//NIIT_INITIALIZED 		= enum++;
		NIIT_ITP_POSITION_X 	= enum++;
		NIIT_ITP_POSITION_Y 	= enum++;
		NIIT_ITP_PROGRESS 		= enum++;
		NIIT_POSITION_X 		= enum++;
		NIIT_POSITION_Y 		= enum++;
		NIIT_POSITION_Z 		= enum++;
		NIIT_ROTATION 			= enum++;
		NIIT_STATE 				= enum++;
		NIIT_VISIBLE_IN_RADAR 	= enum++;		
		NIIT_VISIBLE_IN_MAP 	= enum++;	
		NIIT_LEVEL_A_STAGE 		= enum++;
		NIIT_LEVEL_A_ACTIVE 	= enum++;		
		NIIT_COUNT 				= enum;
		
		enum = 0;
		NIV_START_HIDE 				= enum++;
		NIV_HIDE 					= enum++;
		NIV_START_SHOW 				= enum++;
		NIV_SHOW 					= enum++;
		NIV_START_SHOW_THEN_HIDE 	= enum++;
		NIV_SHOW_THEN_HIDE 			= enum++;
		NIV_COUNT 					= enum;
		
		//Radar
		m_kRadar = radar;
		m_kRadar.addItemType (RadarItem.TEAM_RED, 		RadarItem.TYPE_PLAYER, 				"RIPlayerRed");
		m_kRadar.addItemType (RadarItem.TEAM_BLUE, 		RadarItem.TYPE_PLAYER, 				"RIPlayerBlue");
		m_kRadar.addItemType (RadarItem.TEAM_RED, 		RadarItem.TYPE_DOTA_BUNKER, 		"RIDotaBunkerRed");
		m_kRadar.addItemType (RadarItem.TEAM_BLUE, 		RadarItem.TYPE_DOTA_BUNKER, 		"RIDotaBunkerBlue");
		//m_kRadar.addItemType (RadarItem.TEAM_RED, 	RadarItem.TYPE_DOTA_AMOR_DEPOT, 	"RIDotaArmorRed");
		//m_kRadar.addItemType (RadarItem.TEAM_BLUE, 	RadarItem.TYPE_DOTA_AMOR_DEPOT, 	"RIDotaArmorBlue");
		//m_kRadar.addItemType (RadarItem.TEAM_RED, 	RadarItem.TYPE_DOTA_MEDIC_DEPOT, 	"RIDotaMedicRed");
		//m_kRadar.addItemType (RadarItem.TEAM_BLUE, 	RadarItem.TYPE_DOTA_MEDIC_DEPOT, 	"RIDotaMedicBlue");
		m_kRadar.addItemType (RadarItem.TEAM_RED, 		RadarItem.TYPE_CTF_FLAG, 			"RIFlagRed");
		m_kRadar.addItemType (RadarItem.TEAM_BLUE, 		RadarItem.TYPE_CTF_FLAG, 			"RIFlagBlue");
		m_kRadar.addItemType (RadarItem.TEAM_OTHER, 	RadarItem.TYPE_TANK_TANK, 			"RITank");
		m_kRadar.addItemType (RadarItem.TEAM_OTHER, 	RadarItem.TYPE_TANK_BAZOOKA, 		"RIBazooka");
		m_kRadar.addEventListener ("loadedMap", this, "a2cRequestInit");
		
		//Mini map
		m_kMiniMap = miniMap;
		m_kMiniMap.addItemType (RadarItem.TEAM_RED, 	RadarItem.TYPE_ME, 					"MIMe");
		m_kMiniMap.addItemType (RadarItem.TEAM_BLUE, 	RadarItem.TYPE_ME, 					"MIMe");
		m_kMiniMap.addItemType (RadarItem.TEAM_RED, 	RadarItem.TYPE_PLAYER, 				"MIPlayerRed");
		m_kMiniMap.addItemType (RadarItem.TEAM_BLUE, 	RadarItem.TYPE_PLAYER, 				"MIPlayerBlue");
		m_kMiniMap.addItemType (RadarItem.TEAM_RED, 	RadarItem.TYPE_DOTA_BUNKER, 		"MIDotaBunkerRed");
		m_kMiniMap.addItemType (RadarItem.TEAM_BLUE, 	RadarItem.TYPE_DOTA_BUNKER, 		"MIDotaBunkerBlue");
		//m_kMiniMap.addItemType (RadarItem.TEAM_RED, 	RadarItem.TYPE_DOTA_AMOR_DEPOT, 	"MIDotaArmorRed");
		//m_kMiniMap.addItemType (RadarItem.TEAM_BLUE, 	RadarItem.TYPE_DOTA_AMOR_DEPOT, 	"MIDotaArmorBlue");
		//m_kMiniMap.addItemType (RadarItem.TEAM_RED, 	RadarItem.TYPE_DOTA_MEDIC_DEPOT, 	"MIDotaMedicRed");
		//m_kMiniMap.addItemType (RadarItem.TEAM_BLUE, 	RadarItem.TYPE_DOTA_MEDIC_DEPOT, 	"MIDotaMedicBlue");
		m_kMiniMap.addItemType (RadarItem.TEAM_RED, 	RadarItem.TYPE_CTF_FLAG, 			"MIFlagRed");
		m_kMiniMap.addItemType (RadarItem.TEAM_BLUE, 	RadarItem.TYPE_CTF_FLAG, 			"MIFlagBlue");
		m_kMiniMap.addItemType (RadarItem.TEAM_OTHER, 	RadarItem.TYPE_TANK_TANK, 			"MITank");
		m_kMiniMap.addItemType (RadarItem.TEAM_OTHER, 	RadarItem.TYPE_TANK_BAZOOKA, 		"MIBazooka");
		m_kMiniMap.addEventListener ("loadedMap", this, "a2cRequestInit");
	}
	
	public function c2aAddIcon (NID : Number, team : Number, type : Number, visible : Boolean) : Void {
		//trace ("[NAV] add icon nid = " + NID + ", team = " + team + ", type = " + type + ", visible = " + visible);
		if (type == RadarItem.TYPE_ME) {
			m_nMyID = NID;
		} else {
			m_kRadar.addItem (team, type, NID, visible);
		}
		m_kMiniMap.addItem (team, type, NID, visible);
	}
	
	public function c2aRemoveIcon (NID : Number) : Void {
		m_kRadar.removeItem (NID);
		m_kMiniMap.removeItem (NID);
	}
	
	public function setMap (m : Map, maxIcons : Number, alwayShowIcon : Boolean) : Void {
		m_nMaxIcons = maxIcons;
		m_nInfoLength = m_nMaxIcons * NIIT_COUNT;
		
		RadarItem.ALWAYS_SHOW = alwayShowIcon;
		
		if (m instanceof Map) {
			m_kRadar.setRoot (m.x2d0, m.y2d0);
			m_kRadar.setAnchorPoint (m.x3d1, m.y3d1, m.x2d1, m.y2d1);
			m_kMiniMap.setRoot (m.x2d0, m.y2d0);
			m_kMiniMap.setAnchorPoint (m.x3d1, m.y3d1, m.x2d1, m.y2d1);
			
			m_kRadar.setMap(
				ImagePathManager.MAP_RADAR + m.image, 
				RADAR_MAP_SCALE);
			m_kMiniMap.setMap(
				StringUtility.convert(m.label, StringUtility.MODE_IN_UTF8, StringUtility.MODE_OUT_UPPER_VNI), 
				ImagePathManager.MAP_MINI_MAP + m.image);
		}
		//After this, wait for my signal (fscommand) to add initial icons.
	}
	
	public function c2aInvalidate () : Void {
		var offset : Number = 0;
		var id : Number = 0;
		
		for (var i : Number = 0; i < m_nMaxIcons; ++i ) {
			offset = MAP_NIIT_OFFSET (i);
			id = m_aInfo[offset + NIIT_NID];
			if (m_aInfo[offset + NIIT_IN_USE] == NBOOL_TRUE) {//If this information is valid
				if (m_aInfo[offset + NIIT_TYPE] == RadarItem.TYPE_ME) {//If this is me					
					//Move & rotate radar
					m_kRadar.moveTo (
						m_aInfo[offset + NIIT_POSITION_X], 
						m_aInfo[offset + NIIT_POSITION_Y], 
						m_aInfo[offset + NIIT_POSITION_Z]
					);
					m_kRadar.rotate (-m_aInfo[offset + NIIT_ROTATION]);
					//trace("Flash.mX:"+m_aInfo[offset + NIIT_POSITION_X]+" mY:"+m_aInfo[offset + NIIT_POSITION_Y] );
					//Move & rotate my icon in mini map
					m_kMiniMap.updateItemPosition (
						m_nMyID, 
						m_aInfo[offset + NIIT_POSITION_X], 
						m_aInfo[offset + NIIT_POSITION_Y], 
						m_aInfo[offset + NIIT_ROTATION]
					);
				} else {//If this is other icons
					//Update position
					var x : Number = m_aInfo[offset + NIIT_ITP_POSITION_X]
						+ (m_aInfo[offset + NIIT_POSITION_X] - m_aInfo[offset + NIIT_ITP_POSITION_X]) * m_aInfo[offset + NIIT_ITP_PROGRESS] / 100.0;
					var y : Number = m_aInfo[offset + NIIT_ITP_POSITION_Y]
						+ (m_aInfo[offset + NIIT_POSITION_Y] - m_aInfo[offset + NIIT_ITP_POSITION_Y]) * m_aInfo[offset + NIIT_ITP_PROGRESS] / 100.0;
					
					/*
					trace ("Move from (" + m_aInfo[offset + NIIT_ITP_POSITION_X] + ", " + m_aInfo[offset + NIIT_ITP_POSITION_Y]
						+ ") to (" + m_aInfo[offset + NIIT_POSITION_X] + ", " + m_aInfo[offset + NIIT_POSITION_Y]
						+ ") but in " + m_aInfo[offset + NIIT_ITP_PROGRESS] + "%, so move to (" + x + ", " + y + ")"
					);
					*/
					m_kRadar.updateRadarItemPosition (
						id, 
						x, 
						y, 
						m_aInfo[offset + NIIT_POSITION_Z], 
						m_aInfo[offset + NIIT_ROTATION]
					);
					m_kMiniMap.updateItemPosition (
						id, 
						x, 
						y, 
						m_aInfo[offset + NIIT_ROTATION]
					);

					//Update state
					var state : Number = m_aInfo[offset + NIIT_STATE];
					//Don't update if it's default state (STATUS_MOVING)
					
					if (state != RadarItem.STATUS_MOVING) {
						m_kRadar.updateRadarItemState (
							id, 
							state, 
							m_aInfo[offset + NIIT_LEVEL_A_STAGE], 
							getBool(m_aInfo[offset + NIIT_LEVEL_A_ACTIVE])
						);
						m_kMiniMap.updateItemState (
							id, 
							state, 
							m_aInfo[offset + NIIT_LEVEL_A_STAGE], 
							getBool(m_aInfo[offset + NIIT_LEVEL_A_ACTIVE])
						);	
					}
				}
				
				//-----------------------VISIBLE-----------------------
				//Update visible in radar
				var visible : Number = m_aInfo[offset + NIIT_VISIBLE_IN_RADAR];
				if (visible == NIV_START_HIDE) {//Request hide icon
					m_kRadar.setItemVisible (
						id, false);
				} else if (visible == NIV_START_SHOW) {//Request show icon
					m_kRadar.setItemVisible (
						id, true);
				} else if (visible == NIV_START_SHOW_THEN_HIDE) {//Request show icon for a fixed period of time
					m_kRadar.showItemTemporarily (
						id, SHOW_ICON_TEMPORARILY);
				}
				//else
				//NIV_SHOW. That means:
				//		It's being shown
				//NIV_HIDE. That means:
				//		It's being hidden
				//NIV_SHOW_THEN_HIDE. That means:
				//		Icon is being shown for a fixed period of time, then it'll be hidden
				//		OR
				//		Icon was shown for a fixed period of time, now it's hidden
				
				//Update visible in mini map (similar to radar)
				visible = m_aInfo[offset + NIIT_VISIBLE_IN_MAP];
				if (visible == NIV_START_HIDE) {						
					m_kMiniMap.setItemVisible (
						id, false);
				} else if (visible == NIV_START_SHOW) {
					m_kMiniMap.setItemVisible (
						id, true);
				} else if (visible == NIV_START_SHOW_THEN_HIDE) {
					m_kMiniMap.showItemTemporarily (
						id, SHOW_ICON_TEMPORARILY);
				}
			}
		}
	}
	
	private function checkPositionInMap(x:Number, y:Number):Boolean {
		return true;
	}
	
	private function MAP_NIIT (index : Number, infoType : Number) : Number {
		return index * NIIT_COUNT + infoType;
	}
	
	private function MAP_NIIT_OFFSET (index : Number) : Number {
		return index * NIIT_COUNT;
	}
	
	private function getBool (NIITData : Number) : Boolean {
		return (NIITData == NBOOL_TRUE);
	}
	
	private function a2cRequestInit () : Void {
		++m_nInitStep;
		
		//Initialization has been done for all navigation types		
		if (m_nInitStep == NAVIGATION_TYPE_COUNT) {
			CPPSimulator.fs ("requestInitNavigation");
			
			if (m_fOnRequestInit instanceof Function) {
				m_fOnRequestInit ();
			}
		}
	}
	
	//-------------------------------------OTHER FUNCTIONS--------------------------------------
	public function c2aSetNameForLevelAIcon (NID : Number, name : Number) : Void {
		m_kRadar.setNameForLevelAIcon (NID, name);
		m_kMiniMap.setNameForLevelAIcon (NID, name);
	}
}