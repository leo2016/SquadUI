﻿//Scaleform
import gfx.controls.Label;
import gfx.controls.ListItemRenderer;
import gfx.controls.SQUAD.ResolutionHelper;
import gfx.controls.TextArea;
import gfx.controls.UILoader;
//SQUAD Class
import gfx.controls.SQUAD.NewStyle.Inventory.LIRMainItem;
import gfx.controls.SQUAD.ImagePathManager;
import gfx.controls.SQUAD.NewStyle.Data.InventoryItem;
import gfx.controls.SQUAD.NewStyle.XML.Localizer;
import gfx.controls.SQUAD.StringUtility;

class gfx.controls.SQUAD.NewStyle.Inventory.LIRCompare extends ListItemRenderer {
	private var labName:							Label;
	private var labExpiration:					Label;
	private var uiImage:							UILoader;	
	private var uiMainSupportedClass : 	UILoader;	
	
	public static var EXTENSION 								: String = ".png";
	public static var SELECTED_EXTENSION 				: String = "-selected.png";
	private var currentExtension								: String;
	
	public static var localizer:					Localizer = undefined;
	
	public function LIRCompare () {
		super ();				
	}

	public function setData(data:Object):Void {
		if(data != undefined) {
			this.data = data;
			updateAfterStateChange();			
			this.visible = true;
			this._disabled = false;
		} else {
			this.visible = false;
			this._disabled = true;
		}
	}

	public function updateAfterStateChange():Void {
		if (data instanceof InventoryItem) {
			currentExtension = (
				((selected /*|| state == "over" || state == "down"*/) && !disabled) ? 
				SELECTED_EXTENSION : EXTENSION
			);
			
			//Name
			if (currentExtension == SELECTED_EXTENSION) {
				labName.htmlText = StringUtility.colorText (data.name, "#262626");
			} else {
				labName.text = data.name;
			}
			
			//Image
			if (uiImage instanceof UILoader) {
				if (data.image == undefined || data.image == "") {
					ResolutionHelper.updateUILoader (uiImage, ImagePathManager.INVENTORY_ITEM_BOTTOM_DEFAULT);					
				} else {
					ResolutionHelper.updateUILoader (uiImage, ImagePathManager.INVENTORY_ITEM_BOTTOM + data.image);					
				}
			}			
			
			//Expiration
			var expirationString : String = "Uninitialized Localizer";
			if (localizer != undefined) {				
				if (data.isRent) {
					if (data.expiredDays > 0) {
						expirationString = localizer.localize (
							"EXPIRED_DAYS", [{varName: "**EXPIRED_DAYS**", varValue: data.expiredDays}]);
					} else if (data.expiredHours > 0) {
						expirationString = localizer.localize (
							"EXPIRED_HOURS", [ { varName: "**EXPIRED_HOURS**", varValue: data.expiredHours } ]);				
					} else {
						expirationString = localizer.localize (
							"EXPIRED_MINUTES", [ { varName: "**EXPIRED_MINUTES**", varValue: data.remainingMinute } ]);							
					}
				} else {
					expirationString = "";
				} 			
			}
			if (currentExtension == SELECTED_EXTENSION) {
				labExpiration.htmlText = StringUtility.colorText (expirationString, "#262626");
			} else {
				labExpiration.text = expirationString;
			}
			
			//Supported Class
			var imgPath : String = "";
			switch (data.classId) {
				case InventoryItem.CLASS_TANKER: {
					imgPath = ImagePathManager.CLASS_COMPARE_TANKER + currentExtension;
					break;
				}
				case InventoryItem.CLASS_SCOUT: {										
					imgPath = ImagePathManager.CLASS_COMPARE_SCOUT + currentExtension;
					break;
				}
				case InventoryItem.CLASS_SNIPER: {
					imgPath = ImagePathManager.CLASS_COMPARE_SNIPER + currentExtension;
					break;
				}
				default : {										
					uiMainSupportedClass.visible = false;
					break;
				}
			}
			ResolutionHelper.updateUILoader (uiMainSupportedClass, imgPath);
		}
	}	
}