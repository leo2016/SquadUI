﻿//Default Scaleform UI
import gfx.controls.Label;
import gfx.controls.ProgressBar;
import gfx.controls.UILoader;
import gfx.controls.ListItemRenderer;
import gfx.events.EventDispatcher;
//SQUAD UI & Class
import gfx.controls.SQUAD.DragDropComponent;
import gfx.controls.SQUAD.ImagePathManager;
import gfx.controls.SQUAD.StringUtility;
import gfx.controls.SQUAD.ResolutionHelper;
//SQUAD Data
import gfx.controls.SQUAD.NewStyle.Data.InventoryItem;

class gfx.controls.SQUAD.NewStyle.Inventory.LIRMainItem extends ListItemRenderer {
	//GUI
	private var labMainName 									: Label;
	private var labMainExpiration								: Label;
	private var uiMainImage										: UILoader;
	private var uiMainSupportedClass						: UILoader;
	private var uiMainEquipedClass							: UILoader;
	private var uiUnlocked										: UILoader;
	private var ddc													: DragDropComponent;	
	private var pgbDurability									: ProgressBar;	
	private var mcMask											: MovieClip;
	private var labUnlockLevel										:Label;
	private var mcLimitLevel									:MovieClip;
	
	private var labMainPrice 									: Label;
	
	public static var currentClassType						: Number;
	public static var rankName								: String;
	
	//Will be read from localization file
	public static var PUC_EQUIP								: String = "";
	public static var PUC_UNEQUIP							: String = "";
	public static var PUC_EXTEND								: String = "";
	public static var PUC_REPAIR								: String = "";
	public static var PUC_SELL									: String = "";
	public static var PUC_UPGRADE 							: String = "";
	
	public static var L_EXPIRE_DAYS							: String = "";
	public static var L_EXPIRE_HOURS						: String = "";
	public static var L_EXPIRE_MINUTES					: String = "";
	
	public static var L_PRICE_IN_GAME 						: String = "";
	public static var L_PRICE_IN_LIFE 							: String = "";
	
	public static var EXTENSION 								: String = ".png";
	public static var SELECTED_EXTENSION 				: String = "-selected.png";
	private var currentExtension								: String;
	
	public function LIRMainItem () {
		super ();
		
		EventDispatcher.initialize (this);
		addEventListener ("handleItemDrag", _parent._parent);
	}
	
	private function configUI () : Void {
		super.configUI ();
		
		ddc.addEventListener("beginDrag", this, "beginDrag");
		ddc.addEventListener("drag", this, "drag");				
	}
	
	private function beginDrag(evt:Object):Void {				
		if (data == undefined || data == null || !allowDrag()) {			
			ddc.cancelDrag();
		}		
	}		
	
	private function drag(evt:Object):Void {
		if (!disabled) {
			dispatchEvent( { type: "handleItemDrag", target: this, data: data, from: _parent._parent } );
		}
	}
	
	public function setData(data:Object):Void {
		if(data != undefined) {
			this.data = data;
			updateAfterStateChange();
			
			this.visible = true;
			this._disabled = false;
		} else {
			this.visible = false;
			this._disabled = true;
		}
	}

	public function updateAfterStateChange():Void {
		if (data instanceof InventoryItem) {
			currentExtension = (
				((selected /*|| state == "over" || state == "down"*/) && !disabled) ? 
				SELECTED_EXTENSION : EXTENSION
			);
			
			//Name	
			if (currentExtension == SELECTED_EXTENSION) {
				labMainName.htmlText = StringUtility.colorText (data.name, "#262626");
			} else {
				labMainName.text = data.name;
			}
			/*
			if (labMainName instanceof Label) {
				if (data.useFor == 0) {
					labMainName.text = data.name;
				} else {
					labMainName.text = data.name + " (" + data.useForName + ")";
				}
			}
			*/
			
			//Image
			if (uiMainImage instanceof UILoader) {				
				if (data.image == undefined || data.image == "") {
					ResolutionHelper.updateUILoader (uiMainImage, ImagePathManager.INVENTORY_ITEM_DEFAULT);
				} else {
					ResolutionHelper.updateUILoader (uiMainImage, ImagePathManager.INVENTORY_ITEM + data.image);
				}
			}			
			
			//Expiration
			var expirationString : String = "";
			if (data.isRent) {
				if (data.expiredDays > 0) {
					expirationString = StringUtility.replaceAll (L_EXPIRE_DAYS, "**EXPIRED_DAYS**", data.expiredDays);
				} else if (data.expiredHours > 0) {
					expirationString = StringUtility.replaceAll (L_EXPIRE_HOURS, "**EXPIRED_HOURS**", data.expiredHours);
				} else {
					expirationString = StringUtility.replaceAll (L_EXPIRE_MINUTES, "**EXPIRED_MINUTES**", data.remainingMinute);
				}
			}
			if (currentExtension == SELECTED_EXTENSION) {
				labMainExpiration.htmlText = StringUtility.colorText (expirationString, "#262626");
			} else {
				labMainExpiration.text = expirationString;
			}
			
			//Supported Class
			var imgPath : String = "";
			switch (data.classId) {
				case InventoryItem.CLASS_TANKER: {
					imgPath = ImagePathManager.CLASS_ICON_TANKER + currentExtension;
					break;
				}
				case InventoryItem.CLASS_SCOUT: {										
					imgPath = ImagePathManager.CLASS_ICON_SCOUT + currentExtension;
					break;
				}
				case InventoryItem.CLASS_SNIPER: {
					imgPath = ImagePathManager.CLASS_ICON_SNIPER + currentExtension;
					break;
				}
				default : {
					uiMainSupportedClass.visible = false;
					break;
				}
			}			
			ResolutionHelper.updateUILoader (uiMainSupportedClass, imgPath);
			
			//Equipped Class			
			imgPath = "";
			switch (data.inClass) {
				case InventoryItem.CLASS_TANKER: {										
					imgPath = ImagePathManager.CLASS_EQUIPPED_ICON_TANKER + currentExtension;
					break;
				}
				case InventoryItem.CLASS_SCOUT: {										
					imgPath = ImagePathManager.CLASS_EQUIPPED_ICON_SCOUT + currentExtension;
					break;
				}
				case InventoryItem.CLASS_SNIPER: {
					imgPath = ImagePathManager.CLASS_EQUIPPED_ICON_SNIPER + currentExtension;
					break;
				}
				case InventoryItem.CLASS_NONE: {
					imgPath = ImagePathManager.CLASS_EQUIPPED_ICON_NONE + currentExtension;
					break;
				}
				default : {
					uiMainEquipedClass.visible = false;
					break;
				}
			}
			ResolutionHelper.updateUILoader (uiMainEquipedClass, imgPath);	
			
			//Unlock
			uiUnlocked._visible = !data.unlocked;
			
			if (data.isDisabled) {
				//disabled = true;
				mcLimitLevel._visible = true;
				mcLimitLevel.labUnlockLevel._visible = false;
				ddc.cancelDrag();
			} else if (data.classId != currentClassType && data.classId != InventoryItem.CLASS_NONE) {
				disabled = true;
			} else {
				//disabled = false;
				mcLimitLevel._visible = false;
				mcLimitLevel.labUnlockLevel._visible = false;
			}
			//trace(data.commingSoon);
			mcMask._visible = mcLimitLevel._visible;
			
			//Durability
			if (data.id == InventoryItem.SHOP_ITEM_ID) {
				pgbDurability._visible = false;
			} else {
				pgbDurability._visible = true;
				pgbDurability.setProgress (data.durability, InventoryItem.MAX_DURABILITY);
			}
			
			//Price (for shop item - in upgrade tab, shop category)
			if (data.id == InventoryItem.SHOP_ITEM_ID) {
				labMainPrice._visible = true;
				var text : String = "";
				if (L_PRICE_IN_GAME == "" || L_PRICE_IN_LIFE == "") {
					labMainPrice.text = "Uninitialized Localizer";
				} else {
					var day : Number = data.getMinRentingDays();
					var price : Number = data.howMuch (day, true);
					if (price != undefined) {
						text = L_PRICE_IN_GAME;
						text = StringUtility.replaceAll (text, "**PRICE**", StringUtility.formatMoney(""+price));
						labMainPrice.text = text;
					} else {
						price = data.howMuch (day, false);
						if (price != undefined) {
							text = L_PRICE_IN_LIFE;
							text = StringUtility.replaceAll (text, "**PRICE**", StringUtility.formatMoney(""+price));
							labMainPrice.text = text;
						} else {
							labMainPrice.text = "???";
						}
					}
				}
				if (currentExtension == SELECTED_EXTENSION) {
					labMainPrice.htmlText = StringUtility.colorText (labMainPrice.text, "#262626");
				}
			} else {
				labMainPrice._visible = false;
			}
		}
	}	
	
	private function allowDrag () : Boolean {
		if (data.inClass != InventoryItem.NOT_USE || data.useFor != 0) {
			return false;
		}
		return true;
	}
}