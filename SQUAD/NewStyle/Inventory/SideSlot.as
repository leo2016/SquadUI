﻿import gfx.controls.Button;
import gfx.controls.Label;
import gfx.controls.SQUAD.ResolutionHelper;
import gfx.controls.UILoader;
import gfx.controls.SQUAD.StringUtility;
import gfx.managers.DragManager;

import gfx.controls.SQUAD.NewStyle.Data.InventoryItem;
import gfx.controls.SQUAD.DragDropComponent;
import gfx.controls.SQUAD.ImagePathManager;

class gfx.controls.SQUAD.NewStyle.Inventory.SideSlot extends Button {	
	public var mcHint :		MovieClip;
	public var uiImage: 	UILoader;
	public var ddc: 			DragDropComponent;
	public var allowDrag:	Boolean;
	//public var labTip:		Label;
	
	private var item: 								InventoryItem;	
	public var suitableCatalogueId : 			Number;
	public var suitableCatalogueName : 	String;	
		
	public function SideSlot() {
		super();
		item = undefined;	
		hideHint();		
		allowDrag = true;
	}
	
	private function configUI():Void {
		super.configUI();
		
		ddc.addEventListener("beginDrag", this, "beginDrag");
		ddc.addEventListener("drag", this, "drag");
		DragManager.instance.addEventListener ("dragEnd", this, "hideHint");		
		
		label = "";
	}
	
	private function beginDrag(evt:Object):Void {
		if (item == undefined || item == null || !allowDrag) {
			ddc.cancelDrag();
		}
	}
	
	private function drag(evt:Object):Void {
		if (!_disabled && item != null && item != undefined) {			
			dispatchEvent ({type: "notifyDrag", target: this, data: item, from: "sideSlot"});
		}
	}
	
	public function setData (item : InventoryItem) : Void {	
		this.item = item;		
		updateAfterStateChange ();
	}
	
	public function unsetData () : Void {
		this.item = undefined;		
		updateAfterStateChange ();
	}
	
	private function updateAfterStateChange():Void {		
		// Redraw should only happen AFTER the initialization.
		if (!initialized) { return; }
		validateNow();// Ensure that the width/height is up to date.

		if (item == undefined) {			
			textField.text = StringUtility.convert(suitableCatalogueName, StringUtility.MODE_IN_UTF8, StringUtility.MODE_OUT_UPPER_VNI);
			if (uiImage.visible) {
				uiImage.visible = false;				
			}
			//labTip.text = "";			
		} else {		
			textField.text = "";
			if (item.image == undefined || item.image == "") {
				ResolutionHelper.updateUILoader (uiImage, ImagePathManager.INVENTORY_ITEM_SIDE_SLOTS_DEFAULT);
			} else {
				ResolutionHelper.updateUILoader (uiImage, ImagePathManager.INVENTORY_ITEM_SIDE_SLOTS + item.image);
			}			
			//labTip.text = item.name;			
		}		
		if (constraints != null) { 
			constraints.update(width, height);
		}
		dispatchEvent({type:"stateChange", state:state});
	}
	
	public function showHint () : Void {
		mcHint._visible = true;
	}
	
	public function hideHint () : Void {
		mcHint._visible = false;		
	}
	
	public function isHighlighted () : Boolean {
		return (mcHint._visible);
	}
	
	public function get data () : InventoryItem {
		return item;
	}	
}