﻿import gfx.managers.DragManager;
import gfx.controls.SQUAD.NewStyle.Data.InventoryItem;
import gfx.controls.SQUAD.ImagePathManager;
import gfx.controls.SQUAD.ResolutionHelper;
/*
import gfx.controls.SQUAD.NewStyle.Inventory.LIRMainItem;
import gfx.controls.SQUAD.StringUtility;

import gfx.controls.Label;
import gfx.controls.ProgressBar;
*/
import gfx.controls.UILoader;

/**
 * MovieClip mô phỏng vật phẩm đang được kéo
 */
class gfx.controls.SQUAD.NewStyle.Inventory.DraggingItem extends MovieClip {		
	//GUI
	private var uiMainImage										: UILoader;
	/*
	private var labMainName 									: Label;
	private var labMainExpiration								: Label;	
	private var uiMainSupportedClass						: UILoader;
	private var uiMainEquipedClass							: UILoader;
	private var uiUnlocked										: UILoader;	
	private var pgbDurability									: ProgressBar;
	*/
	
	private var _data 												: InventoryItem;
	public var bIsDragging										: Boolean;
	
	public function DraggingItem () {
		super ();		
		
		DragManager.instance.addEventListener("dragEnd", this, "hide");
	}
	
	public function setData (data : InventoryItem) : Void {
		if (data instanceof InventoryItem) {
			//Image
			if (uiMainImage instanceof UILoader) {
				if (data.image == undefined || data.image == "") {
					ResolutionHelper.updateUILoader (uiMainImage, ImagePathManager.INVENTORY_ITEM_DEFAULT);
				} else {
					ResolutionHelper.updateUILoader (uiMainImage, ImagePathManager.INVENTORY_ITEM + data.image);					
				}
			}
			
			/*
			//Name
			if (labMainName instanceof Label) {
				labMainName.text = data.name;
			}					
			
			//Expiration
			if (labMainExpiration instanceof Label) {
				if (data.isRent) {
					if (data.expiredDays > 0) {
						labMainExpiration.text = StringUtility.replaceAll (LIRMainItem.L_EXPIRE_DAYS, "**EXPIRED_DAYS**", "" + data.expiredDays);
					} else if (data.expiredHours > 0) {
						labMainExpiration.text = StringUtility.replaceAll (LIRMainItem.L_EXPIRE_HOURS, "**EXPIRED_HOURS**", "" + data.expiredHours);
					} else {
						labMainExpiration.text = StringUtility.replaceAll (LIRMainItem.L_EXPIRE_MINUTES, "**EXPIRED_MINUTES**", "" + data.remainingMinute);
					}
				} else {
					labMainExpiration.text = "";
				} 			
			}
			
			//Supported Class			
			switch (data.classId) {
				case InventoryItem.CLASS_TANKER: {
					uiMainSupportedClass.visible = true;
					uiMainSupportedClass.source = ImagePathManager.CLASS_ICON_TANKER + LIRMainItem.SELECTED_EXTENSION;
					break;
				}
				case InventoryItem.CLASS_SCOUT: {		
					uiMainSupportedClass.visible = true;
					uiMainSupportedClass.source = ImagePathManager.CLASS_ICON_SCOUT + LIRMainItem.SELECTED_EXTENSION;
					break;
				}
				case InventoryItem.CLASS_SNIPER: {		
					uiMainSupportedClass.visible = true;
					uiMainSupportedClass.source = ImagePathManager.CLASS_ICON_SNIPER + LIRMainItem.SELECTED_EXTENSION;
					break;
				}
				default : {
					uiMainSupportedClass.visible = false;
					break;
				}
			}
			
			//Equipped Class		
			switch (data.inClass) {
				case InventoryItem.CLASS_TANKER: {
					uiMainEquipedClass.visible = true;
					uiMainEquipedClass.source = ImagePathManager.CLASS_ICON_TANKER + LIRMainItem.SELECTED_EXTENSION;
					break;
				}
				case InventoryItem.CLASS_SCOUT: {
					uiMainEquipedClass.visible = true;
					uiMainEquipedClass.source = ImagePathManager.CLASS_ICON_SCOUT + LIRMainItem.SELECTED_EXTENSION;
					break;
				}
				case InventoryItem.CLASS_SNIPER: {
					uiMainEquipedClass.visible = true;
					uiMainEquipedClass.source = ImagePathManager.CLASS_ICON_SNIPER + LIRMainItem.SELECTED_EXTENSION;
					break;
				}
				default : {
					uiMainEquipedClass.visible = false;
					break;
				}
			}
						
			//Durability
			pgbDurability.setProgress (data.durability, InventoryItem.MAX_DURABILITY);
			*/
			
			_data = data;
			bIsDragging = true;
		}
	}
	
	public function hide () : Void {
		ResolutionHelper.moveOut (this);
		bIsDragging = false;
	}
	
	public function get data () : InventoryItem {
		return _data;
	}
}