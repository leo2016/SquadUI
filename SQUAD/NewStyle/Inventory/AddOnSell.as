﻿import gfx.controls.ListItemRenderer;
import gfx.controls.Label;
import gfx.controls.SQUAD.MixedListItemRenderer;
import gfx.controls.UILoader;
import gfx.events.EventDispatcher;
import gfx.controls.Button;
import gfx.controls.SQUAD.ResolutionHelper;
import gfx.controls.SQUAD.ImagePathManager;


class gfx.controls.SQUAD.NewStyle.Inventory.AddOnSell extends ListItemRenderer {	
	private var labName:Label;
	private var labPrice:Label;
	public function TileListClanAvatar() {
		super();
	}	
	private function configUI () : Void {
		super.configUI();
	}
	public function setData(data:Object):Void {
		if(data != undefined) {
			this.data = data;
			updateAfterStateChange();	
			this.visible = true;
			this._disabled = false;	
			
		}else {
			this.visible = false;
			this._disabled = true;
		}
	}
	public function updateAfterStateChange():Void {
		if (data != undefined) {
			if(labName instanceof Label){
				labName.text = data.name;
				labPrice.text = data.price;
			}
		}
	}
}
