﻿import gfx.controls.Label;
import gfx.controls.SQUAD.NewStyle.Core.Button2;
import gfx.controls.SQUAD.ResolutionHelper;
import gfx.controls.UILoader;
import gfx.controls.SQUAD.StringUtility;
import gfx.managers.DragManager;

import gfx.controls.SQUAD.NewStyle.Data.InventoryItem;
import gfx.controls.SQUAD.DragDropComponent;
import gfx.controls.SQUAD.ImagePathManager;

/**
 * Add-On Slot trong Inventory
 */
class gfx.controls.SQUAD.NewStyle.Inventory.AddOnSlot extends Button2 {
	/**
	 * UILoader để load ảnh add-on (ẩn nếu không có add-on)
	 */
	public var uiImage: 							UILoader;
	
	/**
	 * DragDropComponent, trang bị khả năng kéo thả add-on vào (ra) slot
	 */
	public var ddc: 									DragDropComponent;
	
	/**
	 * Cho phép kéo add-on ra hay không (súng nào cũng phải có đạn và băng đạn, không được phép kéo ra)
	 */
	public var allowDrag:							Boolean;
	//public var labTip:								Label;
	
	/**
	 * Background của slot đánh dấu add-on đang lắp là add-on mà người chơi sở hữu
	 */
	public var mcInventory :						MovieClip;		
	
	/**
	 * Background của slot đánh dấu add-on đang lắp là add-on mà người chơi lấy trong cửa hàng và lắp thử
	 */
	public var mcShop :							MovieClip;
	
	/**
	 * Highlight của slot đánh dấu add-on đang lắp là add-on mà người chơi sở hữu
	 */
	public var mcHintInventory :				MovieClip;		
	
	/**
	 * Highlight của slot đánh dấu add-on đang lắp là add-on mà người chơi lấy trong cửa hàng và lắp thử
	 */
	public var mcHintShop :						MovieClip;
	
	/**
	 * Tên slot
	 */
	public var txtInventory :						TextField;
	
	/**
	 * Add-On lắp vào slot khi mới truy cập màn hình nâng cấp (để hồi phục lại nếu người chơi hủy bỏ tất cả các thay đổi)
	 */
	private var originalItem:						InventoryItem;
	
	/**
	 * Add-On hiện tại
	 */
	private var item: 								InventoryItem;	
	
	/**
	 * CatalogueID của các add-on có thể lắp vào slot này
	 */
	public var suitableCatalogueId : 			Number;
	
	/**
	 * Tên của catalogue các add-on có thể lắp vào slot này
	 */
	public var suitableCatalogueName : 	String;
	
	/**
	 * Vị trí lắp add-on (Sử dụng InventoryItem.IIN_...)
	 */
	public var number :							Number;
	
	/**
	 * Slot có đang được highlight không
	 */
	public var isHighlighted : 					Boolean;
	
	/**
	 * Movie highlight khi rê chuột qua (nếu add-on đang lắp là của người chơi hoặc không có add-on)
	 */
	public var mcHighlightInventory :		MovieClip;
	
	/**
	 * Movie highlight khi rê chuột qua (nếu add-on đang lắp là từ cửa hàng)
	 */
	public var mcHighlightShop :				MovieClip;
	
	/**
	 * Tool tip
	 */
	public var labTip:								Label;
		
	public function AddOnSlot() {
		super();
		item = undefined;	
		hideHint();		
		allowDrag = true;
	}
	
	/**
	 * Bắt các sự kiện kéo thả
	 */
	private function configUI():Void {
		super.configUI();
		
		ddc.addEventListener("beginDrag", this, "beginDrag");
		ddc.addEventListener("drag", this, "drag");		
		DragManager.instance.addEventListener ("dragEnd", this, "hideHint");		
				
		hideHint ();
	}
	
	/**
	 * Quyết định có hủy bỏ sự kiện kéo thả không	 
	 */
	private function beginDrag(evt:Object):Void {
		if (item == undefined || item == null || !allowDrag) {
			ddc.cancelDrag();
		}
	}
	
	/**
	 * Bắt đầu kéo addon ra khỏi slot	 
	 */
	private function drag(evt:Object):Void {
		if (!_disabled && item != null && item != undefined) {
			dispatchEvent ({type: "notifyDrag", target: this, data: item, from: "addOnSlot"});
		}
	}
	
	/**
	 * Lắp add-on vào slot này
	 * 
	 * @param	item 			add-on
	 */
	public function setData (item : InventoryItem) : Void {
		this.item = item;
		updateAfterStateChange ();		
	}
	
	/**
	 * Gỡ bỏ add-on khỏi slot này
	 */
	public function unsetData () : Void {		
		item = undefined;
		updateAfterStateChange ();
	}
	
	private function updateAfterStateChange():Void {		
		// Redraw should only happen AFTER the initialization.
		if (!initialized) { return; }
		validateNow();// Ensure that the width/height is up to date.		
		
		if (item == undefined) {
			var s : String = StringUtility.convert(suitableCatalogueName, StringUtility.MODE_IN_UTF8, StringUtility.MODE_OUT_UPPER_VNI);
			txtInventory.text = s;
			mcInventory._visible = true;			
			mcShop._visible = false;
			uiImage.visible = false;			
			labTip.text = "";
			allowDrag = false;
			if (mcHighlightInventory instanceof MovieClip) {
				mcHighlightInventory._visible = true;
			}
			if (mcHighlightShop instanceof MovieClip) {
				mcHighlightShop._visible = false;
			}
		} else {
			if (item.id == InventoryItem.SHOP_ITEM_ID) {
				mcShop._visible = true;
				mcInventory._visible = false;				
				allowDrag = true;
				labTip.htmlText = StringUtility.colorText(item.name, "#E29B06");
				item.number = number;
				if (mcHighlightInventory instanceof MovieClip) {
					mcHighlightInventory._visible = false;
				}
				if (mcHighlightShop instanceof MovieClip) {
					mcHighlightShop._visible = true;
				}
			} else {
				mcShop._visible = false;
				mcInventory._visible = true;
				labTip.text = item.name;
				if (number != InventoryItem.IIN_AMMO && number != InventoryItem.IIN_CLIP) {
					allowDrag = true;					
				} else {
					allowDrag = false;					
				}
				if (mcHighlightInventory instanceof MovieClip) {
					mcHighlightInventory._visible = true;
				}
				if (mcHighlightShop instanceof MovieClip) {
					mcHighlightShop._visible = false;
				}
			}			
			txtInventory.text = "";
			if (item.image == undefined || item.image == "") {
				ResolutionHelper.updateUILoader (uiImage, ImagePathManager.INVENTORY_ITEM_SIDE_SLOTS_DEFAULT);
			} else {
				ResolutionHelper.updateUILoader (uiImage, ImagePathManager.INVENTORY_ITEM_SIDE_SLOTS + item.image);				
			}			
		}		
		if (constraints != null) { 
			constraints.update(width, height);
		}
		dispatchEvent( { type:"stateChange", state:state } );
		
		/*
		trace (
			"txtInventory = " + txtInventory.text + 
			", txtShop = " + txtShop.text + 
			", mcInventory = " + mcInventory._visible + 
			", mcShop = " + mcShop._visible + 
			", mcHintInventory = " + mcHintInventory._visible + 
			", mcHintShop = " + mcHintShop._visible			
		);
		*/
	}
	
	/**
	 * Hiển thị highlight
	 */
	public function showHint () : Void {
		if (item == undefined || item.id != InventoryItem.SHOP_ITEM_ID) {
			mcHintInventory._visible = true;
			mcHintShop._visible = false;			
		} else {
			mcHintShop._visible = true;
			mcHintInventory._visible = false;
		}
		isHighlighted = true;
	}
	
	/**
	 * Ẩn highlight
	 */
	public function hideHint () : Void {		
		mcHintInventory._visible 	= 
		mcHintShop._visible 		= 
		isHighlighted 				= false;
		labTip._visible = false;
	}
	
	/**
	 * Hiển thị slot
	 */
	public function show () : Void {		
		disabled 			= false;
	}
	
	/**
	 * Ẩn slot
	 */
	public function hide () : Void {		
		disabled 			= true;		
	}
	
	/**
	 * Lấy add-on đang gắn vào slot
	 */
	public function get data () : InventoryItem {
		return item;
	}
	
	/**
	 * Xác nhận thay đổi, nếu sau này có hủy bỏ thay đổi sẽ quay về thiết lập này
	 */
	public function saveData () : Void { 
		originalItem = item;
	}
	
	/**
	 * Hủy bỏ các thay đổi, quay về thiết lập trước đó đã lưu
	 */
	public function resetData () : Void {	
		setData (originalItem);
	}
	
	/**
	 * Lấy add-on mà khi hủy bỏ thay đổi thì slot sẽ nhận add-on đó
	 */
	public function get origin () : InventoryItem {
		return originalItem;
	}
	
	/**
	 * Kiểm tra xem dữ liệu trong slot đã bị thay đổi chưa
	 */
	public function isChanged () : Boolean {
		return (originalItem != item);
	}
	
	private function handleMouseRollOut(mouseIndex:Number):Void {
		super.handleMouseRollOut (mouseIndex);
		labTip._visible = false;
	}
	
	private function handleMouseRollOver(mouseIndex:Number):Void {
		super.handleMouseRollOver (mouseIndex);
		labTip._visible = true;		
	}
	
	private function handleDragOut(mouseIndex:Number):Void {
		super.handleDragOut (mouseIndex);
		labTip._visible = false;		
	}
	
	private function handleDragOver(mouseIndex:Number):Void {
		super.handleDragOver (mouseIndex);
		labTip._visible = true;		
	}
}