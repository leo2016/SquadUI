﻿import gfx.controls.ListItemRenderer;

class gfx.controls.SQUAD.NewStyle.Room.Players extends ListItemRenderer {
	public static var MIN_PLAYERS : Number;
	
	public function Players () {
		super ();				
	}

	public function setData(data:Object):Void {
		if(data != undefined) {
			this.data = data;
			updateAfterStateChange();			
			this.visible = true;
			this._disabled = false;
		} else {
			this.visible = false;
			this._disabled = true;
		}
	}

	public function updateAfterStateChange():Void {
		if (data != undefined) {
			if (MIN_PLAYERS != undefined) {
				disabled = (Number(data) < MIN_PLAYERS);
			}
		}
	}	
}