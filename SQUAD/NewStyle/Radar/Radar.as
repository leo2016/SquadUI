﻿import gfx.controls.SQUAD.CPPSimulator;
import gfx.controls.SQUAD.MathUtility;
import gfx.controls.SQUAD.NewStyle.Radar.RadarItem;
import gfx.controls.UILoader;
import gfx.core.UIComponent;
import gfx.utils.Delegate;

class gfx.controls.SQUAD.NewStyle.Radar.Radar extends UIComponent {
	//Inspectable	
	[Inspectable (name = "Radius", type = "Number", defaultValue = 150)]
	private var nRadius : 								Number;
	
	//Item Rotate Type	
	[Inspectable (name = "ItemRotateType", type = "String", enumeration = "RotateByMap,RealRotation,KeepOriginal,FromCenter,ToCenter,UserDefined", defaultValue = "KeepOriginal")]
	private var sItemRotateType : 				String;
	private static var IRT_BY_MAP : 				String = "RotateByMap";
	private static var IRT_REAL_ROTATION :	String = "RealRotation";
	private static var IRT_KEEP_ORIGINAL :		String = "KeepOriginal";
	private static var IRT_FROM_CENTER :		String = "FromCenter";
	private static var IRT_TO_CENTER :			String = "ToCenter";
	private static var IRT_USER_DEFINED : 		String = "UserDefined";
	
	//Height Status Rotate Type
	[Inspectable (name = "HeightStatusRotateType", type = "String", enumeration = "RotateByItem,KeepOriginal,UserDefined", defaultValue = "KeepOriginal")]
	private var sHeightStatusRotateType : 	String;
	private static var HSRT_BY_ITEM : 			String = "RotateByItem";
	private static var HSRT_KEEP_ORIGINAL :	String = "KeepOriginal";
	private static var HSRT_USER_DEFINED :	String = "UserDefined";
	
	//UI
	private var mcContent : 							MovieClip;
	private var uiMap : 								UILoader;
	private var mcItemContainer : 				MovieClip;//attached by code	
	private var mcPosCenter : 						MovieClip;
	
	//Data
	private var aItemTypes : 						Array;
	private var nX2D0 : 								Number;
	private var nY2D0 : 								Number;
	private var nX3D1 : 								Number;
	private var nY3D1 : 								Number;
	private var nX2D1 : 								Number;
	private var nY2D1 : 								Number;
	private var aRadarItems : 						Array;
	private var nScale : 								Number;
	private var nPlayerX2D : 						Number;
	private var nPlayerY2D : 						Number;
	private var nPlayerX3D : 						Number;
	private var nPlayerY3D : 						Number;
	private var nPlayerZ3D : 						Number;
	private var bReady : 								Boolean;
	private var nRotation : 							Number;
	private var bIsRefreshing : 						Boolean;
	private var nIntervalId : 							Number;
	
	public function Radar () {
		super ();
		init ();
		bReady = false;
	}
	
	private function configUI () : Void {
		super.configUI ();
		mcPosCenter = mcContent.mcPosCenter;
		uiMap = mcContent.uiMap;
		uiMap.addEventListener ("complete", this, "strechMap");
		mcItemContainer = uiMap.createEmptyMovieClip("ItemContainer", uiMap.getNextHighestDepth());
		mcItemContainer.topmostLevel = true;
		dispatchEvent ({type:"ready", target: this});
	}
	
	public function init () : Void {
		aRadarItems 			= [];
		nScale 					= -1;
		nX2D0					= 0;
		nY2D0					= 0;
		nX3D1 					= 0;
		nY3D1 					= 0;
		nX2D1 					= 0;
		nY2D1 					= 0;
		nPlayerX2D 			= 0;
		nPlayerY2D 			= 0;
		nPlayerX3D 			= 0;
		nPlayerY3D 			= 0;
		nPlayerZ3D 			= 0;
		nRotation				= 0;
		bIsRefreshing 		= true;
		
		aItemTypes 			= [];
		aItemTypes[RadarItem.TEAM_OTHER] 	= [];
		aItemTypes[RadarItem.TEAM_RED] 		= [];
		aItemTypes[RadarItem.TEAM_BLUE] 	= [];
	}
	
	public function setMap (mapImage : String, scale : Number) : Void {
		bReady 		= false;
		nScale 			= scale;
		if (uiMap.source != mapImage) {
			uiMap.source = mapImage;
		} else {
			handleMapLoaded ();
		}
	}
	
	public function rotate (degree : Number) : Void {
		if (!bReady) {
			return;
		}
		if (
		//Math.abs(mcContent._rotation - degree) >= iMinRotationDifferent || 
		bIsRefreshing) {
			CPPSimulator.trace ("Rotate " + degree, 8);
			nRotation = mcContent._rotation = degree;
			for (var x:String in aRadarItems) {
				if (aRadarItems[x] instanceof RadarItem) {
					rotateItem (aRadarItems[x]);
				}
			}
		}
	}
	
	public function moveTo (x3D : Number, y3D : Number, z3D : Number) : Void {
		if (!bReady) {
			return;
		}
		nPlayerX3D = x3D;
		nPlayerY3D = y3D;
		nPlayerZ3D = z3D;
		var _x2D : Number = x2D (x3D);
		var _y2D : Number = y2D (y3D);
		if (_x2D < 0) {
			_x2D = 0;
		}
		if (_y2D < 0) {
			_y2D = 0;
		}
		var distance : Number = MathUtility.distance3D (nPlayerX2D, nPlayerY2D, nPlayerZ3D, _x2D, _y2D, z3D);
		if (
		//distance < iMinPositionDifferent && 
		!bIsRefreshing) {
			return;
		}
		CPPSimulator.trace (
			"Move to (" +x3D + "," + y3D + ") in Block ~ (" + _x2D + "," + _y2D +
			") in Map (" + uiMap.width + "x" + uiMap.height +
			"). Distance = " + distance, 
			7
		);
		nPlayerX2D = _x2D;
		nPlayerY2D = _y2D;
		uiMap.content._x = mcItemContainer._x = - nPlayerX2D;
		uiMap.content._y = mcItemContainer._y = - nPlayerY2D;	
		invalidateItems ();		
	}
	
	public function addItem (team: Number, type : Number, id : Number, visible : Boolean) : MovieClip {
		var newItem : MovieClip = undefined;// RadarItem
		var identifier : String = aItemTypes[team][type];
		if (identifier != "" && identifier != undefined && identifier != null) {
			newItem = mcItemContainer.attachMovie (identifier, "item" + id, mcItemContainer.getNextHighestDepth(), { _visible: visible } );
			if (newItem instanceof RadarItem) {
				newItem.init (id, team, type, identifier, visible);
				newItem.addEventListener("changeState", this, "itemChangeStatus");
				newItem.addEventListener("backToDefaultStatus", this, "itemBackToDefaultStatus");
				aRadarItems.push (newItem);
				CPPSimulator.trace ("Add item '" + id + "', team = '"+team+"', type = '" + identifier + "', _visible = " + newItem._visible + ", v = " + newItem.visible + " to radar", 2);
			}
		}		
		return newItem;
	}
	
	public function updateRadarItemPosition (id : Number, x3D : Number, y3D : Number, z3D : Number, rotation : Number) : String {
		if (!bReady) {
			return;
		}
		CPPSimulator.trace ("Update radar item position of " + id + " at ("+x3D+","+y3D+","+z3D+") & r = " + rotation, 7);
		var radarItem : RadarItem = getItemById (id);		
		if (radarItem instanceof RadarItem) {
			if (radarItem.id == id) {
				radarItem.xBlock = x3D;
				radarItem.yBlock = y3D;
				radarItem.zBlock = z3D;				
				if (radarItem.traceEnabled) {
					if (
					//Math.abs (radarItem.rotation - rotation) >= iMinRotationDifferent || 
					bIsRefreshing) {
						radarItem.rotation = rotation;
					}
					radarItem.xMap = x2D(x3D);
					radarItem.yMap = y2D(y3D);
					var pos : Object = getPositionOnRadar (radarItem);
					var distance : Number = MathUtility.distance3D (radarItem._x, radarItem._y, radarItem.zBlock, pos.x, pos.y, z3D);
					if (
					//distance >= iMinPositionDifferent || 
					bIsRefreshing) {
						//Position
						radarItem._x = pos.x;
						radarItem._y = pos.y;
						/*
						if (MathUtility.distance(pos.x, pos.y, nPlayerX2D, nPlayerY2D) > nRadius + 30) {
							radarItem.visible = false;
						} else {
							radarItem.visible = true;
						}
						*/
						
						//Rotation
						rotateItem (radarItem);
						
						//Height
						if (isItemHigherThanMe(radarItem)) {
							radarItem.setHeightStatus (RadarItem.HEIGHT_HIGHER);
						} else if (isItemLowerThanMe(radarItem)) {
							radarItem.setHeightStatus (RadarItem.HEIGHT_LOWER);
						} else {
							radarItem.setHeightStatus (RadarItem.HEIGHT_EQUAL);
						}
					}
					return radarItem.statusFrame;
				} 
			} 
		}
		return "";
	}
	
	public function updateRadarItemState (id : Number, status : Number) : String {
		if (!bReady) {
			return;
		}		
		var radarItem : RadarItem = getItemById (id);
		if (radarItem instanceof RadarItem) {
			if (radarItem.id == id) {
				if (radarItem.visible) {
					CPPSimulator.trace ("Item '"+radarItem.toString()+"' set state = " + status, 3);
					radarItem.setStatus (status);
					return radarItem.statusFrame;
				} 
			} 
		}
		return "";
	}
	
	public function removeItem (id : Number) : Void {
		CPPSimulator.trace ("Remove item " + id, 2);
		var n : Number = aRadarItems.length;
		var temp : RadarItem;
		for (var i : Number = 0; i < n ; ++i ) {
			temp = aRadarItems[i];
			if (temp instanceof RadarItem) {
				if (temp.id == id) {
					temp.clearTimeout ();
					aRadarItems.splice (i, 1);
					temp.removeMovieClip ();
					delete temp;
					return;
				}
			}
		}
	}
	
	public function clearItems () : Void {
		CPPSimulator.trace ("Clear Items", 2);
		var temp;// : RadarItem;
		while (aRadarItems.length > 0) {
			temp = aRadarItems.pop ();
			if (temp instanceof RadarItem) {
				temp.clearTimeout ();
			}
			temp.removeMovieClip ();
			delete temp;
		}
	}
	
	public function x2D (x3D) : Number {
		return ((x3D * nScale) + (nX2D0 * uiMap.width));
	}
	
	public function y2D (y3D) : Number {
		return ((-y3D * nScale) + (nY2D0 * uiMap.height));
	}
	
	public function x3D (x2D) : Number {
		if (nScale == 0) {
			return x2D - (nX2D0 * uiMap.width);
		}
		return (x2D - (nX2D0 * uiMap.width)) / nScale;
	}
	
	public function y3D (y2D) : Number {
		if (nScale == 0) {
			return (nY2D0 * uiMap.height) - y2D;
		}
		return ((nY2D0 * uiMap.height) - y2D) / nScale;
	}
	
	public function isInRadar (x2D : Number, y2D : Number) : Boolean {
		var distance : Number = Math.sqrt ((x2D - nPlayerX2D) * (x2D - nPlayerX2D) + (y2D - nPlayerY2D) * (y2D - nPlayerY2D));
		return (distance <= nRadius);
	}
	
	private function getPositionOnRadar (radarItem : RadarItem) : Object {
		var x2D : Number = radarItem.xMap;
		var y2D : Number = radarItem.yMap;
		var pos : Object = { x:x2D, y:y2D };
		var newIsInRadar : Boolean = isInRadar(x2D, y2D);
		if (radarItem.isInRadar != newIsInRadar) {
			radarItem.isInRadar = newIsInRadar;
			radarItem.changeInOutState ();
		}		
		if (!radarItem.isInRadar) {
			//Determine the equation of line between (nPlayerX2D, nPlayerY2D) and (x2D, y2D) <=> y = ax + b
			//y0 = ax0 + b, y1 = ax1 + b
			//-> a = (y0 – y1) / (x0 – x1), b = y0 – ax0
			var a : Number = 0;
			if (nPlayerX2D != x2D) {
				a = (nPlayerY2D - y2D) / (nPlayerX2D - x2D);
			}
			var b : Number = nPlayerY2D - a * nPlayerX2D;
			//Determine the equation of radar circle <=> (x-x0)^2 + (y-y0)^2 = r^2 (1)			
			//but, y = ax + b = ax + y0 - ax0 = y0 + a(x-x0) -> y-y0 = a(x-x0)
			//-> (2) = (a^2+1)(x-x0)^2 = r^2 -> (x-x0)^2 = r^2/(a^2+1)
			//-> x = x0 +/- sqrt(r^2/(a^2+1)) = x0 +/- D
			var D : Number = Math.sqrt (nRadius * nRadius / (a * a + 1));			
			var x : Number = nPlayerX2D + D;
			if ((nPlayerX2D-x)*(x2D-x) <= 0) {//x is between nPlayerX2D and x2D
				pos.x = x;
				pos.y = a * x + b;
			} else {				
				x = nPlayerX2D - D;
				if ((nPlayerX2D - x) * (x2D - x) <= 0) {//x is between nPlayerX2D and x2D
					pos.x = x;
					pos.y = a * x + b;
				}
			}
		}
		return pos;
	}
	
	private function handleMapLoaded () : Void {
		//Get ratio = width/height of the map
		var ratio : Number = uiMap.content._width / uiMap.content._height;
		if (ratio > 0) {			
			//Get current scale of the 2D map with the 3D world
			var nCurrentScale : Number = calculateMapScale ();
			var nScaleFactor : Number = 0;
			if (nCurrentScale) {
				//But the expected scale is 'nScale', so we need to scale the map again, and scale factor = expected scale / current scale
				//trace ("current scale = "  + nCurrentScale + ", but expected scale = " + nScale + " -> need to scale again with factor = " + nScaleFactor);
				nScaleFactor = nScale / nCurrentScale;
			}
			//Re-calculate new width & height
			uiMap.content._width = uiMap.width = uiMap.content._width * nScaleFactor;
			uiMap.content._height = uiMap.height = uiMap.width / ratio;
			//trace ("now, new size: w = " + uiMap.width + ", h = " + uiMap.height);
			nScale = calculateMapScale ();
			//Set transparent
			uiMap.content._alpha = 70;
			uiMap.validateNow ();
			
			CPPSimulator.trace ("Map scale in radar = " + nScale, 1);
		}
	}
	
	public function invalidateItems () : Void {
		var radarItem : RadarItem;
		for (var x:String in aRadarItems) {
			radarItem = aRadarItems[x];
			updateRadarItemPosition (radarItem.id, radarItem.xBlock, radarItem.yBlock, radarItem.zBlock, radarItem.rotation);
		}
	}
	
	public function addItemType (team : Number, type : Number, identifier : String) : Void {
		aItemTypes[team][type] = identifier;
	}
	
	public function getItemById (id : Number) : RadarItem {
		for (var x : String in aRadarItems) {
			if (aRadarItems[x].id == id) {
				return aRadarItems[x];
			}
		}
		return undefined;
	}
	
	private function isItemHigherThanMe (radarItem : RadarItem) : Boolean {
		return (radarItem.zBlock - nPlayerZ3D)  >= 200;
	}
	
	private function isItemLowerThanMe (radarItem : RadarItem) : Boolean {
		return (nPlayerZ3D - radarItem.zBlock)  >= 200;
	}
	
	public function setRoot (x2D0 : Number, y2D0 : Number) : Void {
		nX2D0 = x2D0;
		nY2D0 = y2D0;
	}
	
	public function setAnchorPoint (x3D1 : Number, y3D1 : Number, x2D1 : Number, y2D1 : Number) : Void {
		nX3D1 = x3D1;
		nY3D1 = y3D1;
		nX2D1 = x2D1;
		nY2D1 = y2D1;
	}
	
	private function calculateMapScale () : Number {
		var x1 : Number = nX2D0 * uiMap.content._width;
		var y1 : Number = nY2D0 * uiMap.content._height;
		var x2 : Number = nX2D1 * uiMap.content._width;
		var y2 : Number = nY2D1 * uiMap.content._height;
		return MathUtility.distance (x1, y1, x2, y2) / MathUtility.distance (0, 0, nX3D1, nY3D1);
	}
	
	public function get mapWidth () : Number {
		return uiMap.width;
	}
	
	public function get scale () : Number {
		return nScale;
	}
	
	public function get myX () : Number {
		return nPlayerX2D;
	}
	
	public function get myY () : Number {
		return nPlayerY2D;
	}
	
	public function get x0 () : Number {
		return nX2D0;
	}
	
	public function get y0 () : Number {
		return nY2D0;
	}
	
	public function setVisible (v : Boolean) : Void {		
		this.visible = mcItemContainer._visible = v;
	}
	
	private function rotateItem (item : RadarItem) : Void {
		if (!(item instanceof RadarItem)) {
			return;
		}
		var r : Number;//rotation of item		
		var centerPoint : Object = { x : 0, y : 0 };
		mcPosCenter.localToGlobal (centerPoint);
		var itemPoint : Object = { x : 0, y : 0 };
		item.localToGlobal (itemPoint);		
		
		switch (sItemRotateType) {
			case IRT_BY_MAP : {
				r = 0;
				break;
			}
			case IRT_FROM_CENTER : {
				r = -MathUtility.angleBetweenVectors (0, 1, itemPoint.x - centerPoint.x, itemPoint.y - centerPoint.y)				
					+ 180 - mcContent._rotation;
				break;
			}
			case IRT_TO_CENTER : {
				r = -MathUtility.angleBetweenVectors (0, 1, itemPoint.x - centerPoint.x, itemPoint.y - centerPoint.y) - mcContent._rotation;
				break;
			}
			case IRT_KEEP_ORIGINAL : {
				r = -mcContent._rotation;
				break;
			}
			case IRT_REAL_ROTATION : {
				r = item.rotation - mcContent._rotation;
				break;
			}
		}
		
		item._rotation = r;
		rotateHeightStatus (item);
	}
	
	private function rotateHeightStatus (item : RadarItem) : Void {
		var r : Number = 0;
		switch (sHeightStatusRotateType) {
			case HSRT_BY_ITEM : {
				r = 0;
				break;
			}
			case HSRT_KEEP_ORIGINAL : {
				r = - item._rotation - mcContent._rotation;
				break;
			}
		}
		
		item.mcHeightStatus._rotation = r;
	}
	
	public function refresh () : Void {
		CPPSimulator.trace ("Refresh", 2);
		bIsRefreshing = true;
		moveTo (nPlayerX3D, nPlayerY3D, nPlayerZ3D);
		rotate (nRotation);
		invalidate ();
		bIsRefreshing = false;
	}
	
	private function itemBackToDefaultStatus (evt : Object) : Void {
		CPPSimulator.trace (evt.target + " back to default status", 4);
		rotateItem(evt.target);
	}
	
	private function itemChangeStatus (evt : Object) : Void {
		CPPSimulator.trace (evt.target + " change status to " + evt.newState, 4);
	}
	
	public function setItemVisible (itemId : Number, visible : Boolean) : Void {
		CPPSimulator.trace ("Set visible for item "+itemId + " = " + visible, 2);
		var item : RadarItem = getItemById (itemId);
		if (item instanceof RadarItem) {
			item.visible = item.traceEnabled = visible;
			if (visible) {
				invalidateItemPosition (item);
				invalidateItemState (item);
			}
		}
	}
	
	public function showItemTemporarily (itemId : Number, maxTime : Number) : Void {
		CPPSimulator.trace ("Show visible temporarily for item "+itemId + ", max time = " + maxTime, 2);
		var item : RadarItem = getItemById (itemId);
		if (item instanceof RadarItem) {
			item.clearTimeout ();
			item.visible = item.traceEnabled = true;			
			invalidateItemPosition (item);
			invalidateItemState (item);
			item.timeOutId = _global.setTimeout (
				Delegate.create(this, setItemVisible), 
				maxTime, 
				itemId, false);
		}
	}
	
	private function strechMap () : Void {
		nIntervalId = _global.setInterval (Delegate.create(this, checkMapProgress), 10);
	}
	
	private function checkMapProgress () : Void {
		//for some unknown reasons, when UILoader has completed loading, 
		//the contentHolder of UILoader still doesn't appear or it's not completed streching yet
		//so, we need to check several times to know when the contentHolder is visible to strech the map		
		handleMapLoaded ();		
		//trace ("w=" + uiMap.width + ",h=" + uiMap.height);
		var isStreched : Boolean = false;
		if (uiMap.content._width <= uiMap.width && Math.abs(uiMap.content._height - uiMap.height) < 1) {
			isStreched = true;
		} else if (Math.abs(uiMap.content._width - uiMap.width) < 1 && uiMap.content._height <= uiMap.height) {
			isStreched = true;
		}
		if (nScale > 0 && isStreched) {
			_global.clearTimeout (nIntervalId);
			bReady = true;
			dispatchEvent ( { type: "loadedMap" } );
		}		
	}
	
	private function invalidateItemPosition (item : RadarItem) : Void {
		if (item instanceof RadarItem) {
			updateRadarItemPosition (item.id, item.xBlock, item.yBlock, item.zBlock, item.rotation);
		}
	}
	
	private function invalidateItemState (item : RadarItem) : Void {
		if (item instanceof RadarItem) {
			updateRadarItemState (item.id, item.status);
		}
	}
}