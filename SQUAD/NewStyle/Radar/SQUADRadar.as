﻿import gfx.controls.SQUAD.CPPSimulator;
import gfx.controls.SQUAD.MathUtility;
import gfx.controls.SQUAD.NewStyle.Radar.RadarItem;
import gfx.controls.SQUAD.NewStyle.Radar.Radar;
import gfx.controls.SQUAD.NewStyle.Radar.RadarItemDOTA;

class gfx.controls.SQUAD.NewStyle.Radar.SQUADRadar extends Radar {
	public function SQUADRadar () {
		super ();
	}
	
	private function rotateItem (item : RadarItem) : Void {
		if (sItemRotateType == IRT_USER_DEFINED) {
			var r : Number;//rotation of item
			var centerPoint : Object = { x : 0, y : 0 };
			mcPosCenter.localToGlobal (centerPoint);
			var itemPoint : Object = { x : 0, y : 0 };
			item.localToGlobal (itemPoint);
			r = -mcContent._rotation;//keep original in other case
			switch (item.type) {
				case RadarItem.TYPE_PLAYER:{
					if (!(item.isInRadar)) {//out of radar
						if (item.status == RadarItem.STATUS_MOVING) {
							r = -MathUtility.angleBetweenVectors (0, 1, itemPoint.x - centerPoint.x, itemPoint.y - centerPoint.y)
								+ 180 - mcContent._rotation;
						}
					}
					break;
				}
			}
			item._rotation = r;
			rotateHeightStatus (item);
		} else {
			super.rotateItem (item);
		}
	}
	
	public function updateRadarItemState (id : Number, action : Number/*status*/, stage : Number, resourcePoint : Boolean) : String {
		if (!bReady) {
			return;
		}
		
		var radarItem/* : RadarItem*/ = getItemById (id);
		if (radarItem instanceof RadarItem) {
			if (radarItem.id == id) {	
				switch (radarItem.type) {	
					case RadarItem.TYPE_DOTA_BUNKER: {
						//CPPSimulator.trace ("Item '"+radarItem.toString()+"' change state to " + action + " in SQUADRadar", 3);
						radarItem.setStatus (action, stage, resourcePoint);
						return radarItem.statusFrame;
					}
					default: {
						if (radarItem.visible) {
							CPPSimulator.trace ("Item '"+radarItem.toString()+"' set state = " + action + " in SQUADRadar", 3);
							radarItem.setStatus (action);
							return radarItem.statusFrame;
						} else {
							return "";
						}
					}	
				}
			} 
		}
		return "";
	}
	
	private function invalidateItemState (item : RadarItem) : Void {
		if (item instanceof RadarItem) {
			if (item instanceof RadarItemDOTA) {
				var icon : RadarItemDOTA = RadarItemDOTA(item);				
				this.updateRadarItemState (icon.id, icon.status, icon.m_nStage, icon.m_bResourcePointEnabled);
			} else {
				super.updateRadarItemState (item.id, item.status);
			}
		}
	}
	
	public function setNameForLevelAIcon (id : Number, name : Number) : Void {
		var item/* : RadarItem*/ = getItemById (id);
		if (item instanceof RadarItemDOTA) {
			item.setName (name);
		}
	}
	
	private function itemChangeStatus (evt : Object) : Void {
		super.itemChangeStatus (evt);
		if (evt.onVisible == true) {//Level A Icon notifys that it has been set visible, so it need to be invalidated (position)
			invalidateItemPosition (evt.target);
		}
	}
}