﻿import gfx.core.UIComponent;
import gfx.utils.Delegate;
class gfx.controls.SQUAD.NewStyle.Radar.RadarItem extends UIComponent {
	//Data---------------------------------------------------------------------------------
	public static var TEAM_OTHER : 				Number = 0;
	public static var TEAM_BLUE : 				Number = 1;
	public static var TEAM_RED : 				Number = 2;
	
	public static var TYPE_INVALID : 			Number = 0;	
	public static var TYPE_ME : 				Number = 1;
	public static var TYPE_PLAYER : 			Number = 2;
	public static var TYPE_DOTA_BUNKER : 		Number = 3;
	public static var TYPE_DOTA_AMOR_DEPOT : 	Number = 4;
	public static var TYPE_DOTA_MEDIC_DEPOT :	Number = 5;
	public static var TYPE_CTF_FLAG : 			Number = 6;
	public static var TYPE_TANK_TANK : 			Number = 7;
	public static var TYPE_TANK_BAZOOKA : 		Number = 8;
	
	public static var STATUS_MOVING :			Number = 0;
	public static var STATUS_FIGHTING : 		Number = 1;
	public static var STATUS_DEAD : 			Number = 2;
	public static var STATUS_NOTIFYING : 		Number = 3;	
	public static var ACTION_NOT_INIT : 		Number = -1;//distinguise with other action. DOTA------------------------
	public static var ACTION_NO_ACTION : 		Number = 4;
	public static var ACTION_REPAIRNG : 		Number = 5;
	public static var ACTION_DESTROYING : 		Number = 6;
	public static var STATUS_CF_DROPPED : 		Number = 7;//CAPTURE FLAG------------
	public static var STATUS_CF_HELD_BY_BLUE : 	Number = 8;
	public static var STATUS_CF_HELD_BY_RED : 	Number = 9;
	
	public static var HEIGHT_HIGHER : 			Number = 0;
	public static var HEIGHT_EQUAL : 			Number = 1;
	public static var HEIGHT_LOWER : 			Number = 2;
	
	public static var ALWAYS_SHOW : 			Boolean = false;
	
	private var maxTimeInEachState:				Array;
	public var frameAtEachState : 				Array;
	public var heightStatus : 					String;
	
	//C++ Data Mapping
	public var id : 							Number;
	public var team : 							Number;
	public var type : 							Number;
	public var rotation : 						Number;
	public var xBlock : 						Number;
	public var yBlock : 						Number;
	public var zBlock : 						Number;
	public var status : 						Number;
	
	//AS Variable
	public  var name :							Number;
	public  var isInRadar : 					Boolean;	
	public  var xMap : 							Number;
	public  var yMap : 							Number;	
	private var defaultStatus : 				Number;
	public  var timeOutId : 					Number;
	public  var identifier : 					String;
	public  var traceEnabled : 					Boolean;
	private var hideWhenBackToDefault : 		Boolean;
	
	//UI---------------------------------------------------------------------------------
	public var mcHeightStatus : 				MovieClip;
	
	public function RadarItem () {
		super ();
		type 								= TYPE_INVALID;
		rotation 							= 
		name 	 							= 
		defaultStatus 						= 
		status 								= 
		timeOutId 							= 
		xBlock 								= 
		yBlock 								= 
		zBlock 								= 
		xMap 								= 
		yMap 								= 0;
		identifier 							=
		heightStatus 						= "";
		isInRadar 							= 
		traceEnabled 						= true;
		hideWhenBackToDefault 				= false;
		maxTimeInEachState 					= 
		frameAtEachState 					= [];
	}
	
	public function setStatus (newStatus : Number) : String {		
		var statusLabel : String = statusFrame;		
		if (newStatus != status) {			
			status = newStatus;
			_global.clearTimeout (timeOutId);
			var timeOut : Number = maxTimeInEachState[status];
			if (timeOut > 0) {
				timeOutId = _global.setTimeout (Delegate.create(this, backToDefaultStatus), timeOut);
			}
			statusLabel = statusFrame;
			gotoAndPlay (statusLabel);	
			dispatchEvent ({type : "changeState", target : this, newState : status});
		}
		return statusLabel;
	}
	
	public function backToDefaultStatus () : Void {
		status = defaultStatus;
		_global.clearTimeout (timeOutId);
		gotoAndPlay (statusFrame);
		dispatchEvent ( { type : "backToDefaultStatus", target : this } );
		if (hideWhenBackToDefault && _visible) {			
			visible = false;
			hideWhenBackToDefault = false;			
		}
	}
	
	public function getRandomStatus () : Number {
		return 0;
	}
	
	public function get statusFrame () : String {		
		return (frameAtEachState[status] + (isInRadar?"-In":"-Out"));
	}
	
	public function changeInOutState () : String {
		//continue timeout from previous state, don't re-set timeout here
		//this's the reason why we don't use 'setStatus' function
		var statusLabel : String = statusFrame;		
		gotoAndPlay (statusLabel);
		return statusLabel;
	}
	
	public function attachHeightStatus () : Void {
		if (identifier != "" && heightStatus != undefined && heightStatus != "") {
			mcHeightStatus = attachMovie (heightStatus, "HeightStatus", getNextHighestDepth());
			setHeightStatus (HEIGHT_EQUAL);
		}
	}
	
	public function setHeightStatus (hs : Number) : Void {
		if (mcHeightStatus instanceof MovieClip) {
			switch (hs) {
				case HEIGHT_HIGHER: {
					mcHeightStatus.gotoAndPlay ("Higher");
					break;
				}
				case HEIGHT_LOWER: {
					mcHeightStatus.gotoAndPlay ("Lower");
					break;
				}			
				default : {
					mcHeightStatus.gotoAndPlay ("Equal");
					break;
				}
			}
		}		
	}	
	
	public function clearTimeout () : Void {
		_global.clearTimeout (timeOutId);		
		visible
	}
	
	public function toString () : String {
		var s : String = "";
		s += "[CCK - RadarItem " + _name + "]";
		s += "\tType = " + type;
		//s += " ("+xBlock+","+yBlock+","+zBlock+") block ~ ("+xMap+", "+yMap+") map";
		return s;
	}
	
	public function get visible() : Boolean {
		return _visible;
	}
	
	public function set visible (value : Boolean) : Void {		
		if (value == false && ALWAYS_SHOW == true) {
			return;
		}
		super.visible = value;
	}
	
	public function init (myID : Number, myTeam : Number, myType : Number, myIdentifier : String, myVisibility : Boolean) : Void {
		team = myTeam;
		id = myID;
		type = myType;				
		identifier = myIdentifier;
		visible = traceEnabled = myVisibility;
		attachHeightStatus ();
	}
}