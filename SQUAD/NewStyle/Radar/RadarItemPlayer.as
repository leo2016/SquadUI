import gfx.controls.SQUAD.NewStyle.Radar.RadarItem;

class gfx.controls.SQUAD.NewStyle.Radar.RadarItemPlayer extends RadarItem {
	public function RadarItemPlayer () {
		super();
		defaultStatus = STATUS_MOVING;
		maxTimeInEachState = [];
		maxTimeInEachState[STATUS_MOVING] 		= 0;
		maxTimeInEachState[STATUS_FIGHTING] 		= 1500;
		maxTimeInEachState[STATUS_DEAD] 			= 1500;
		maxTimeInEachState[STATUS_NOTIFYING] 	= 1500;
		frameAtEachState = [];
		frameAtEachState[STATUS_MOVING] 			= "Moving";
		frameAtEachState[STATUS_FIGHTING] 			= "Fighting";
		frameAtEachState[STATUS_DEAD] 				= "Dead";
		frameAtEachState[STATUS_NOTIFYING] 		= "Notifying";
	}
	
	public function getRandomStatus () : Number {
		var x : Number = random (700);
		if (x < 50) {
			return STATUS_FIGHTING;
		} else if (x < 100) {
			return STATUS_DEAD;
		} else if (x < 150) {
			return STATUS_NOTIFYING;
		} else {
			return STATUS_MOVING;
		}
	}
	
	public function setStatus (newStatus : Number) : String {
		var statusLabel : String = super.setStatus (newStatus);		
		if (newStatus == STATUS_DEAD) {
			hideWhenBackToDefault = true;
		}
		return statusLabel;
	}
	
	public function init (myID : Number, myTeam : Number, myType : Number, myIdentifier : String, myVisibility : Boolean) : Void {
		if (myTeam == TEAM_RED) {
			heightStatus = "HeightStatusRed";
		} else {
			heightStatus = "HeightStatusBlue";
		}
		super.init (myID, myTeam, myType, myIdentifier, myVisibility);
	}
}