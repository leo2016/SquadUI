import gfx.controls.SQUAD.NewStyle.MiniMap.MiniMapItemDOTA;
import gfx.utils.Delegate;

class gfx.controls.SQUAD.NewStyle.Radar.RadarItemDOTA extends MiniMapItemDOTA {
	private static var m_nTimeBeforeHiding : Number = 1500;
	
	public function RadarItemDOTA () {
		super();			
	}
	
	public function setStatus ( action:Number, stage:Number, resourcePoint:Boolean) : Void {
		if (action == undefined || stage == undefined || resourcePoint == undefined) {
			return;
		}
		
		switch (status) {
			case ACTION_NOT_INIT: {//It's the 1st time
				status = action;				
				if (action == ACTION_NO_ACTION) {//Hide this icon immediately if initial status = NO_ACTION
					visible = false;
				} else {
					mcAction.gotoAndPlay(m_aActionLabels[status]);
				}
				dispatchEvent ( { type : "changeState", target : this, newState : status } );	
				break;
			}
			case ACTION_NO_ACTION: {//Nobody is reacting with this bunker
				if (action != ACTION_NO_ACTION) {
					//"NO ACTION" -> "DO SOMETHING" = show this icon and play animation
					//status can be = NO_ACTION but maybe it's visible and wait for hiding, so cancel that hiding command
					_global.clearTimeout (timeOutId); 
					visible = traceEnabled = true;
					status = action;
					mcAction.gotoAndPlay(m_aActionLabels[status]);
					dispatchEvent ( { type : "changeState", target : this, newState : status, onVisible : true } );
				}
				break;
			}
			default: {//Somebody is doing something with this bunker				
				if (action != ACTION_NO_ACTION) {//Now, the action is changing
					//"DO SOMETHING" -> "DO DIFFERENT TASK" = play animation
					status = action;
					mcAction.gotoAndPlay(m_aActionLabels[status]);
					dispatchEvent ( { type : "changeState", target : this, newState : status } );	
				} else {
					//"DO SOMETHING" -> "NO ACTION" = show this icon for a while, then hide it
					status = action;
					mcAction.gotoAndPlay(m_aActionLabels[status]);
					dispatchEvent ( { type : "changeState", target : this, newState : status } );	
					timeOutId = _global.setTimeout (
						Delegate.create(this, backToDefaultStatus), m_nTimeBeforeHiding);
				}
				break;
			}
		}
		
		if (m_bResourcePointEnabled != resourcePoint) {
			m_bResourcePointEnabled = mcResourcePoint._visible = resourcePoint;
		}
		
		if (m_nStage != stage) {
			m_nStage  = stage;
			mcStages.gotoAndPlay(m_aStageLabels[m_nStage]);	
		}
	}
	
	public function backToDefaultStatus () : Void {
		_global.clearTimeout (timeOutId);
		visible = traceEnabled = false;
		dispatchEvent ( { type : "backToDefaultStatus", target : this } );
	}
}