import gfx.controls.SQUAD.NewStyle.Radar.RadarItem;

class gfx.controls.SQUAD.NewStyle.Radar.RadarItemFlag extends RadarItem {
	public var holderTeam : Number;
	
	public function RadarItemFlag () {
		super();
		defaultStatus = STATUS_CF_DROPPED;
		frameAtEachState = [];
		frameAtEachState[STATUS_CF_DROPPED] 		= "Dropped";
		frameAtEachState[STATUS_CF_HELD_BY_BLUE] 	= "HeldByBlue";
		frameAtEachState[STATUS_CF_HELD_BY_RED] 	= "HeldByRed";
	}
	
	public function setStatus (newStatus : Number) : String {
		if (newStatus != status) {
			status = newStatus;
			var statusLabel : String = statusFrame;
			gotoAndPlay (statusLabel);
			dispatchEvent ({type : "changeState", target : this, newState : status});
			return statusLabel;
		}
		return statusFrame;
	}
	
	public function get statusFrame () : String {		
		return (frameAtEachState[status]);
	}
}