﻿import gfx.controls.SQUAD.NewStyle.Data.BaseItem;
import gfx.controls.SQUAD.NewStyle.Mission.Mission;
import gfx.controls.SQUAD.NewStyle.Mission.MissionsGroup;
import gfx.controls.SQUAD.NewStyle.Mission.MissionStat;
import gfx.controls.SQUAD.NewStyle.XML.BaseReader;
import gfx.controls.SQUAD.NewStyle.XML.Decryptor;

class gfx.controls.SQUAD.NewStyle.XML.MissionsReader extends BaseReader
{
	private var m_aMissionGroups:Array;//MissionsGroup
	
	/**
	 * Doc toan bo du lieu trong file XML
	 * @param	sFileName: Ten file XML
	 */
	public function MissionsReader(sFileName:String, encrypted : Boolean){
		super();
		
		m_bEncrypted = encrypted;
		m_aMissionGroups = [];
		addEventListener("open", readGroups);
		loadFile(sFileName);
	}
	
	/**
	 * Bắt đầu đọc dữ liệu từ file XML. 
	 * Đọc từng bản ghi một bằng hàm readGroups. 
	 * Khi đọc xong thì sinh sự kiện "complete".
	 * 
	 * @param	evt eventObject bắt được từ sự kiện "open"
	 */
	public function readGroups(evt:Object):Void {
		if (m_bEncrypted) {
			m_kDecryptor = new Decryptor (m_kRoot);
			var item: XMLNode = m_kDecryptor.getData().firstChild;
			if (item) {
				do {
					readGroupE (item);
					item = item.nextSibling;
				}while (item != undefined)		
			}
		} else {
			var item:XMLNode = m_kRoot.firstChild;
			if (item) {
				do {
					readGroup (item);
					item = item.nextSibling;
				}while (item != undefined)		
			}
		}
		m_aMissionGroups.reverse ();
		/*
		for (var x:String in m_aMissionGroups) {
			trace (m_aMissionGroups[x]);
		}
		*/
		dispatchEvent( { type:"complete", target:this, result:m_aMissionGroups } );
	}

	/**
	 * Đọc bản ghi. 
	 * Một bản ghi đọc từ XML vào là 1 instance của class MissionsGroup
	 * 
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	public function readGroup(itemRoot:XMLNode):Void {
		var properties:Array = convertIndices(itemRoot.childNodes);
		var group : MissionsGroup = new MissionsGroup (
			properties["GROUP_ID"].firstChild.nodeValue, 
			properties["TYPE"].firstChild.nodeValue
		);
		var missionNodes : Array = properties["MISSIONS"].childNodes;
		for (var x:String in missionNodes) {
			if (missionNodes[x] instanceof XMLNode) {
				group.addMission (readMission(missionNodes[x]));
			}
		}
		m_aMissionGroups.push (group);
	}
	
	public function readMission (itemRoot : XMLNode) : Mission {
		var properties : Array = convertIndices (itemRoot.childNodes);
		var m : Mission =  new Mission (
			properties["ID"].firstChild.nodeValue, 
			properties["LEVEL"].firstChild.nodeValue, 
			properties["NAME"].firstChild.nodeValue, 
			properties["EXP"].firstChild.nodeValue, 
			properties["GP"].firstChild.nodeValue, 
			properties["PRIZE_ID"].firstChild.nodeValue, 
			properties["RENTING_DAYS"].firstChild.nodeValue			
		);		
		var requirementNodes : Array = properties["REQUIREMENTS"].childNodes;
		for (var x:String in requirementNodes) {
			if (requirementNodes[x] instanceof XMLNode) {				
				m.addStat (readRequirement(requirementNodes[x]));
			}
		}
		m.m_aStats.reverse ();
		if (m.m_nRentingDays == undefined || m.m_nRentingDays == 0) {
			m.m_nRentingDays = BaseItem.RENT_FOREVER;
		}
		return m;
	}
	
	/**
	 * Lấy danh sách các bản ghi đọc được từ XML. (Chỉ sử dụng hàm này sau khi có sự kiện "complete"). 	 
	 * 
	 * @return Danh sách các bản ghi
	 */
	public function getGroups ():Array {
		return m_aMissionGroups;
	}	
	
	/**
	 * Đọc bản ghi. (Mã hóa)
	 * Một bản ghi đọc từ XML vào là 1 instance của class MissionsGroup
	 * 
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	public function readGroupE(itemRoot:XMLNode):Void {
		var properties:Array = convertEncryptedIndices(itemRoot.childNodes);
		var group : MissionsGroup = new MissionsGroup (
			Number(m_kDecryptor.decrypt(properties["GROUP_ID"].firstChild.nodeValue)), 
			Number(m_kDecryptor.decrypt(properties["TYPE"].firstChild.nodeValue))
		);
		var missionNodes : Array = properties["MISSIONS"].childNodes;
		for (var x:String in missionNodes) {
			if (missionNodes[x] instanceof XMLNode) {
				group.addMission (readMissionE(missionNodes[x]));
			}
		}
		m_aMissionGroups.push (group);
	}
	
	public function readMissionE (itemRoot : XMLNode) : Mission {
		var properties : Array = convertEncryptedIndices (itemRoot.childNodes);
		var m : Mission = new Mission (
			Number(m_kDecryptor.decrypt(properties["ID"].firstChild.nodeValue)), 
			Number(m_kDecryptor.decrypt(properties["LEVEL"].firstChild.nodeValue)), 
			m_kDecryptor.decrypt(properties["NAME"].firstChild.nodeValue), 
			Number(m_kDecryptor.decrypt(properties["EXP"].firstChild.nodeValue)), 
			Number(m_kDecryptor.decrypt(properties["GP"].firstChild.nodeValue)), 
			Number(m_kDecryptor.decrypt(properties["PRIZE_ID"].firstChild.nodeValue)),
			Number(m_kDecryptor.decrypt(properties["RENTING_DAYS"].firstChild.nodeValue))			
		);		
		var requirementNodes : Array = properties["REQUIREMENTS"].childNodes;
		for (var x:String in requirementNodes) {
			if (requirementNodes[x] instanceof XMLNode) {
				m.addStat (readRequirementE(requirementNodes[x]));
			}
		}
		m.m_aStats.reverse ();
		if (m.m_nRentingDays == undefined || m.m_nRentingDays == 0) {
			m.m_nRentingDays = BaseItem.RENT_FOREVER;
		}
		return m;
	}
	
	public function readRequirement (itemRoot : XMLNode) : MissionStat {		
		var properties : Array = convertIndices (itemRoot.childNodes);
		return new MissionStat (
			Number(properties["ID"].firstChild.nodeValue),
			Number(properties["TYPE"].firstChild.nodeValue),
			Number(properties["MAX_QUANTITY"].firstChild.nodeValue),
			Number(properties["MODE"].firstChild.nodeValue),
			Number(properties["MAP"].firstChild.nodeValue)
		);
	}
	
	public function readRequirementE (itemRoot : XMLNode) : MissionStat {
		var properties : Array = convertEncryptedIndices (itemRoot.childNodes);
		return new MissionStat (
			Number(m_kDecryptor.decrypt(properties["ID"].firstChild.nodeValue)),
			Number(m_kDecryptor.decrypt(properties["TYPE"].firstChild.nodeValue)),
			Number(m_kDecryptor.decrypt(properties["MAX_QUANTITY"].firstChild.nodeValue)),
			Number(m_kDecryptor.decrypt(properties["MODE"].firstChild.nodeValue)),
			Number(m_kDecryptor.decrypt(properties["MAP"].firstChild.nodeValue))
		);
	}
}