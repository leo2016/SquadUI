﻿import gfx.controls.SQUAD.NewStyle.XML.BaseReader;
import gfx.controls.SQUAD.NewStyle.Data.Catalogue;
import gfx.controls.SQUAD.NewStyle.XML.Decryptor;

/**
 * Class đọc các Catalogue từ XML.
 * Đọc ngay khi khởi tạo instance mới nhưng phải bắt sự kiện "complete" rồi chờ tới khi đọc xong mới nhận được dữ liệu để xử lý.
 */
class gfx.controls.SQUAD.NewStyle.XML.CataloguesReader extends BaseReader {
	private var m_aCatalogues: Array;
	
	/**
	 * Hàm tạo và mở file ngay lập tức.
	 * Ngay khi file được mở xong, bắt sự kiện "open" và đọc ngay.
	 *
	 * @param	sFileName đường dẫn file XML
	 */
	public function CataloguesReader (sFileName: String, encrypted : Boolean) {
		super();
		
		m_bEncrypted = encrypted;
		m_aCatalogues = [];
		addEventListener ("open", readCatalogues);
		loadFile (sFileName);
	}
	
	/**
	 * Bắt đầu đọc dữ liệu từ file XML.
	 * Đọc từng bản ghi một bằng hàm readCatalogue.
	 * Khi đọc xong thì gọi hàm buildCatalogueTree để xây dựng cây phân cấp Catalogue.
	 * Hoàn tất thì sinh sự kiện "complete".
	 *
	 * @param	evt eventObject bắt được từ sự kiện "open"
	 */
	private function readCatalogues (evt: Object):Void {
		if (m_bEncrypted) {
			m_kDecryptor = new Decryptor (m_kRoot);
			var item: XMLNode = m_kDecryptor.getData().firstChild;
			if (item) {
				do {
					readCatalogueE (item);
					item = item.nextSibling;
				} while (item != undefined) ;
			}
		} else {
			var item: XMLNode = m_kRoot.firstChild;
			if (item) {
				do {
					readCatalogue (item);
					item = item.nextSibling;
				} while (item != undefined) ;				
			}
		}
		buildCatalogueTree ();		
		mapIDToName ();
		dispatchEvent ( { type: "complete", target: this, result: m_aCatalogues } ) ;
	}
	
	/**
	 * Đọc bản ghi.
	 * Một bản ghi đọc từ XML vào là một instance của class Catalogue
	 *
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	private function readCatalogue (itemRoot: XMLNode):Void {
		var properties:Array = convertIndices (itemRoot.childNodes);
		m_aCatalogues [properties["ID"].firstChild] = new Catalogue (
			properties["ID"].firstChild.nodeValue,
			properties["PARENT"].firstChild.nodeValue,
			properties["NAME"].firstChild.nodeValue,
			properties["TAGS"].firstChild.nodeValue,
			properties["DISPLAY"].firstChild.nodeValue == true
		);
	}
	
	/**
	 * Xây dựng cây phân cấp Catalogue.
	 * Mỗi Catalogue đã có ID của Catalogue cha nhưng chưa có danh sách các Catalogue con.
	 * Dựa vào dữ liệu đọc được, xây dựng danh sách các Catalogue con của tất cả các Catalogue.
	 */
	private function buildCatalogueTree ():Void {
		for (var id:String in m_aCatalogues) {
			m_aCatalogues[m_aCatalogues[id].parent].addChild (m_aCatalogues[id]);
		}
	}
	
	/**
	 * Lấy danh sách các bản ghi đọc được từ XML. (Chỉ sử dụng hàm này sau khi có sự kiện "complete").
	 * Phải đợi đọc xong cả Catalogue và Common để gọi phân loại các item vào các catalogue nhằm tránh việc lọc nhiều lần các item trong các catalogue khác nhau.
	 * Phân loại các item bằng Catalogue.classify ( .. )
	 *
	 * @return Danh sách các catalogue đã được phân cấp
	 */
	public function getAllCatalogues ():Array {
		return m_aCatalogues;
	}
	
	/**
	 * Đọc bản ghi. (Mã hóa)
	 * Một bản ghi đọc từ XML vào là một instance của class Catalogue
	 *
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	private function readCatalogueE (itemRoot: XMLNode):Void {
		var properties:Array = convertEncryptedIndices (itemRoot.childNodes);
		var id : Number = Number(m_kDecryptor.decrypt(properties["ID"].firstChild.nodeValue));
		m_aCatalogues [id] = new Catalogue (
			id,
			Number(m_kDecryptor.decrypt(properties["PARENT"].firstChild.nodeValue)),
			m_kDecryptor.decrypt(properties["NAME"].firstChild.nodeValue),
			m_kDecryptor.decrypt(properties["TAGS"].firstChild.nodeValue),
			getBool(m_kDecryptor.decrypt(properties["DISPLAY"].firstChild.nodeValue))
		);
	}
	
	private function getName (id : Number) : String {
		//trace("catalogID=" + id);
		return m_aCatalogues[id].name;
	}
	
	private function mapIDToName () : Void {
		Catalogue.CTL_ALL 									= getName (0);
		Catalogue.CTL_WEAPON 								= getName (1);
		Catalogue.CTL_MAIN_GUN 								= getName (11);
		Catalogue.CTL_SNIPER_GUN 							= getName (35);
		Catalogue.CTL_HEAVY_MACHINE_GUN 					= getName (36);
		Catalogue.CTL_MACHINE_GUN 							= getName (37);
		Catalogue.CTL_SUB_MACHINE_GUN 						= getName (38);
		Catalogue.CTL_SHOTGUN_GUN 							= getName (39);
		Catalogue.CTL_SUB_GUN 								= getName (12);
		Catalogue.CTL_KNIFE 								= getName (13);
		Catalogue.CTL_GRENADE 								= getName (14);
		Catalogue.CTL_SMOKE 								= getName (40);
		Catalogue.CTL_FRAG 									= getName (41);
		Catalogue.CTL_STUN 									= getName (42);
		Catalogue.CTL_FLASH 								= getName (43);
		Catalogue.CTL_CHARACTER 							= getName (2);
		Catalogue.CTL_UNIFORM 								= getName (15);
		Catalogue.CTL_CAMOVYLAGE 							= getName (16);
		Catalogue.CTL_HEAD_PART 							= getName (17);
		Catalogue.CTL_HAT 									= getName (44);
		Catalogue.CTL_FASHIONAL_HAT							= getName (50);
		Catalogue.CTL_HEAD_ARMOR 							= getName (51);
		Catalogue.CTL_FACE 									= getName (45);	
		Catalogue.CTL_GLASSES								= getName (52);
		Catalogue.CTL_MASK 									= getName (53);
		Catalogue.CTL_AVATAR								= getName (58);
		Catalogue.CTL_BODY_PART 							= getName (32);
		Catalogue.CTL_ARMOR 								= getName (46);
		Catalogue.CTL_SHOULDER 								= getName (47);
		Catalogue.CTL_HIP 									= getName (48);
		Catalogue.CTL_BACK 									= getName (49);
		Catalogue.CTL_LEG_PART 								= getName (19);				
		Catalogue.CTL_EQUIPMENT 							= getName (3);
		Catalogue.CTL_NAME_PLATE 							= getName (20);
		Catalogue.CTL_SPRAY 								= getName (21);
		Catalogue.CTL_OTHER_EQUIPMENT 						= getName (22);
		Catalogue.CTL_PACK 									= getName (9);
		Catalogue.CTL_COLLECTION 							= getName (4);
		Catalogue.CTL_ACCUMULATED_ITEMS 					= getName (5);
		Catalogue.CTL_MEDALS 								= getName (6);
		Catalogue.CTL_TANKER								= getName (23);
		Catalogue.CTL_SNIPER								= getName (24);
		Catalogue.CTL_SCOUT									= getName (25);
		Catalogue.CTL_ASSASSIN								= getName (26);
		Catalogue.CTL_MASTER								= getName (27);
		Catalogue.CTL_TREASURE 								= getName (7);
		Catalogue.CTL_ADD_ON 								= getName (8);
		Catalogue.CTL_BULLET 								= getName (28);
		Catalogue.CTL_CLIP 									= getName (29);
		Catalogue.CTL_SCOPE 								= getName (30);
		Catalogue.CTL_BARREL 								= getName (31);
		Catalogue.CTL_SIDE 									= getName (32);
		Catalogue.CTL_MIDDLE_SIDE							= getName (54);
		Catalogue.CTL_LEFT_SIDE								= getName (56);
		Catalogue.CTL_RIGHT_SIDE							= getName (55);
		Catalogue.CTL_BOTTOM_SIDE							= getName (57);
		Catalogue.CTL_COLOR 								= getName (33);
		Catalogue.CTL_DECAL 								= getName (34);
		Catalogue.CTL_OTHERS								= getName (10);
	}	
}