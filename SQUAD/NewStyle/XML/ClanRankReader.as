﻿import gfx.controls.SQUAD.NewStyle.Data.ClanRank;
import gfx.controls.SQUAD.NewStyle.XML.BaseReader;
import gfx.controls.SQUAD.NewStyle.XML.Decryptor;

class gfx.controls.SQUAD.NewStyle.XML.ClanRankReader extends BaseReader
{
	private var m_aClanRanks:Array;
	
	/**
	 * Doc toan bo du lieu trong file XML
	 * @param	sFileName: Ten file XML
	 */
	public function ClanRankReader(sFileName:String, encrypted : Boolean) {
		//trace(sFileName);
		super();
		m_bEncrypted = encrypted;
		m_aClanRanks = [];
		addEventListener("open", readClanRanks);
		loadFile(sFileName);
	}
	
	/**
	 * Bắt đầu đọc dữ liệu từ file XML. 
	 * Đọc từng bản ghi một bằng hàm readCommonItem. 
	 * Khi đọc xong thì sinh sự kiện "complete".
	 * 
	 * @param	evt eventObject bắt được từ sự kiện "open"
	 */
	public function readClanRanks(evt:Object):Void {
		if (m_bEncrypted) {
			m_kDecryptor = new Decryptor (m_kRoot);
			var item: XMLNode = m_kDecryptor.getData().firstChild;
			if (item) {
				do {
					readClanRankE (item);
					item = item.nextSibling;
				}while (item != undefined)		
			}
		} else {
			var item:XMLNode = m_kRoot.firstChild;
			if (item) {
				do {
					readClanRank(item);
					item = item.nextSibling;
				}while (item != undefined)		
			}
		}		
		dispatchEvent( { type:"complete", target:this, result:m_aClanRanks } );
	}

	/**
	 * Đọc bản ghi. 
	 * Một bản ghi đọc từ XML vào là 1 instance của class BaseItem
	 * 
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	public function readClanRank(itemRoot:XMLNode):Void {
		var properties:Array = convertIndices(itemRoot.childNodes);
		m_aClanRanks[properties["ID"].firstChild.nodeValue] = new ClanRank(
			properties["ID"].firstChild.nodeValue,
			properties["RANK_NAME"].firstChild.nodeValue, 
			properties["MIN_PLAYER"].firstChild.nodeValue, 
			properties["MAX_PLAYER"].firstChild.nodeValue,
			properties["MAX_SENIOR_PLAYER"].firstChild.nodeValue, 
			properties["MAX_COMMANDER"].firstChild.nodeValue, 
			properties["MAX_COMMMITTEE"].firstChild.nodeValue,
			properties["MAX_CHIEF"].firstChild.nodeValue, 
			properties["MAX_CITY"].firstChild.nodeValue
		);
	}
		/**
	 * Lấy danh sách các bản ghi đọc được từ XML. (Chỉ sử dụng hàm này sau khi có sự kiện "complete"). 	 
	 * 
	 * @return Danh sách các bản ghi
	 */
	public function getClanRanks ():Array {
		return m_aClanRanks;
	}
	
	/**
	 * Đọc bản ghi (Mã hóa)
	 * Một bản ghi đọc từ XML vào là 1 instance của class BaseItem
	 * 
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	public function readClanRankE(itemRoot:XMLNode):Void {
		var properties:Array = convertEncryptedIndices(itemRoot.childNodes);
		var id : Number = Number(m_kDecryptor.decrypt(properties["ID"].firstChild.nodeValue));
		m_aClanRanks[id] = new ClanRank(
			id,
			properties["RANK_NAME"].firstChild.nodeValue, 
			properties["MIN_PLAYER"].firstChild.nodeValue, 
			properties["MAX_PLAYER"].firstChild.nodeValue,
			properties["MAX_SENIOR_PLAYER"].firstChild.nodeValue, 
			properties["MAX_COMMANDER"].firstChild.nodeValue, 
			properties["MAX_COMMMITTEE"].firstChild.nodeValue,
			properties["MAX_CHIEF"].firstChild.nodeValue, 
			properties["MAX_CITY"].firstChild.nodeValue
		);
	}
}