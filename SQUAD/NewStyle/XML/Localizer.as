﻿import gfx.controls.SQUAD.NewStyle.XML.Decryptor;
import gfx.controls.SQUAD.StringUtility;
import gfx.controls.SQUAD.NewStyle.XML.BaseReader;

/**
 * Class thực hiện bước "Localizing" từ XML
 */
class gfx.controls.SQUAD.NewStyle.XML.Localizer extends BaseReader {
	public var m_aMap :									Array;	//Map chuyển đổi giữa tag và text hiển thị trên GUI
	public static var LOCALIZATION_FOLDER :	String = "Localization/";
	
	/**
	 * Hàm tạo.
	 * Tự động mở file và đọc, chờ sự kiện "complete" để thực hiện các bước tiếp theo
	 * 
	 * @param	sFileName		Đường dẫn tới file "Locallizing"	 
	 */
	public function Localizer (sFileName : String, encrypted : Boolean) {
		super();
		
		m_bEncrypted = encrypted;
		m_aMap = [];
		addEventListener ("open", readContent);
		loadFile (sFileName);
	}
	
	/**
	 * Hàm đọc nội dung từ file XML sau khi có thông báo đã tìm và mở được file
	 * 
	 * @param	evt	Biến lưu giữ thông tin về thông báo đã tìm và mở được file
	 */
	private function readContent (evt: Object):Void {
		if (m_bEncrypted) {
			m_kDecryptor = new Decryptor (m_kRoot);
			var temp: Array = m_kDecryptor.getData().childNodes;
			var length : Number = temp.length;
			for (var i : Number  = 0 ; i < length ; ++i ) {
				//m_aMap[TAG] = NODE_VALUE
				m_aMap [m_kDecryptor.decrypt(temp[i].nodeName.substr(1))] = m_kDecryptor.decrypt(temp[i].firstChild.nodeValue);				
			}
		} else {
			var temp : Array = m_kRoot.childNodes;		
			var length : Number = temp.length;
			for (var i : Number  = 0 ; i < length ; ++i ) {
				//m_aMap[TAG] = NODE_VALUE
				m_aMap [temp[i].nodeName] = temp[i].firstChild.nodeValue;
			}
		}
		
		dispatchEvent ( { type: "complete", target: this, result: this} ) ;
	}
	
	/**
	 * Hàm chuyển đổi từ tag sang text (và đồng thời thay thế các biến bằng giá trị của chúng ngay)
	 * 
	 * @param	tag					Tag
	 * @param	applyingArray	Mảng chứa danh sách các biến và giá trị của biến để thay thế.
	 * 											Mảng này có cấu trúc như sau:
	 * 												Mỗi phần tử là 1 Object bao gồm 2 trường : 
	 * 													varName :		Tên biến sẽ tìm để thay thế
	 * 													varValue :		Giá trị sẽ thay thế vào vị trí của tên biến
	 * 												Nếu được, tốt nhất cả 2 trường này đều nên là kiểu String
	 * 
	 * @return		text đã được chuyển đổi và thay thế giá trị biến
	 */
	public function localize (tag : String, applyingArray : Array) : String {
		if (m_aMap[tag] == undefined) {			
			return tag + " not found!";
		}
		if (applyingArray == undefined || applyingArray == null || applyingArray.length < 1) {			
			return m_aMap[tag];
		} else {
			return applyVars (m_aMap[tag], applyingArray);
		}
	}

	/**
	 * Hàm thay thế giá trị của biến vào vị trí của tên biến 
	 * 
	 * @param	text			Chuỗi mà hàm sẽ thực hiện tìm kiếm và thay thế
	 * @param	varName	Tên biến sẽ tìm để thay thế
	 * @param	varValue	Giá trị sẽ thay thế vào vị trí của tên biến
	 * 
	 * @return		text đã được chuyển đổi và thay thế giá trị biến
	 */
	public function applyVar (text : String, varName : String, varValue : String) : String {
		return StringUtility.replaceAll (text, varName, varValue);
	}
	
	/**
	 * Hàm thay thế giá trị của biến vào vị trí của tên biến 
	 * 
	 * @param	text					Chuỗi mà hàm sẽ thực hiện tìm kiếm và thay thế
	 * @param	applyingArray	Mảng chứa danh sách các biến và giá trị của biến để thay thế.
	 * 											Mảng này có cấu trúc như sau:
	 * 												Mỗi phần tử là 1 Object bao gồm 2 trường : 
	 * 													varName :		Tên biến sẽ tìm để thay thế
	 * 													varValue :		Giá trị sẽ thay thế vào vị trí của tên biến
	 * 												Nếu được, tốt nhất cả 2 trường này đều nên là kiểu String
	 * 
	 * @return		text đã được chuyển đổi và thay thế giá trị biến
	 */
	public function applyVars (text : String, applyingArray : Array) : String {
		for (var x : String in applyingArray) {
			text = StringUtility.replaceAll (text, applyingArray[x].varName, applyingArray[x].varValue);
		}
		return text;
	}
	
	/**
	 * Nối thêm dữ liệu LOCALIZATION đọc được từ file XML khác
	 * 
	 * @param	addedMap	dữ liệu thêm vào
	 */
	public function append (addedMap : Array) : Void {
		for (var x : String in addedMap) {				
			m_aMap[x] = addedMap[x];			
		}		
	}
	
	/**
	 * Nội dung của các map
	 * 
	 * @return Xâu thể hiện các map
	 */
	public function toString () : String {
		var s : String = "";
		for (var x : String in m_aMap) {
			s += ("Map [" + x + "] = " + m_aMap[x]) + "\n";
		}
		return s;
	}
	
	public function trace () : Void {
		for (var x : String in m_aMap) {
			trace ("Map [" + x + "] = " + m_aMap[x]);
		}
	}
}

//-------------------------------------------------------------------------
//Ví dụ sử dụng
//-------------------------------------------------------------------------
/*
import gfx.controls.SQUAD.XML.Localizer;

var l : Localizer = new Localizer (Localizer.LOCALIZATION_FOLDER + "PlayerInfo.xml");
l.addEventListener ("complete", this, "test");

function test () : Void {
	var cach : Number = 3;
	
	switch (cach) {
		case 1: {
			var s2 : String = l.localize ("TEST");
			s2 = l.applyVar (s2, "**1**", "Thaogau");
			s2 = l.applyVar (s2, "**ITEM_PRICE**", "33000 GP");
			break;
		}
		case 2: {
			var s2 : String = l.localize ("TEST");
			var aa : Array = [
				{varName:"**1**", varValue:"thaogau"}, 
				{varName:"**ITEM_PRICE**", varValue:"33000 GP"}
			];
			s2 = l.applyVars (s2, aa);
			break;
		}
		case 3: {
			var aa : Array = [
				{varName:"**1**", varValue:"thaogau"}, 
				{varName:"**ITEM_PRICE**", varValue:"33000 GP"}
			];
			var s2 : String = l.localize ("TEST", aa);
			break;
		}		
	}
	
	trace (s2);	
}
*/