﻿import gfx.controls.SQUAD.NewStyle.XML.BaseReader;

class gfx.controls.SQUAD.NewStyle.XML.XMLSettingReader extends BaseReader {
	var m_bUseEncryptedData : Boolean;
		
	public function XMLSettingReader (sFileName: String) {
		super();
		
		addEventListener ("open", readSetting);
		loadFile (sFileName);
	}
	
	private function readSetting (evt: Object):Void {				
		m_bUseEncryptedData = getBool (m_kRoot.firstChild.firstChild.nodeValue);		
		dispatchEvent ( { type: "complete", target: this, result: m_bUseEncryptedData } ) ;
	}
	
	public function useEXML () : Boolean {
		return m_bUseEncryptedData;
	}
}