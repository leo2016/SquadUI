import gfx.controls.SQUAD.NewStyle.XML.BaseReader;
import gfx.controls.SQUAD.NewStyle.Data.Gear;
import gfx.controls.SQUAD.NewStyle.Data.GearProperty;
import gfx.controls.SQUAD.NewStyle.XML.Decryptor;
import gfx.events.EventDispatcher;

class gfx.controls.SQUAD.NewStyle.XML.GearReader extends BaseReader {
	private var m_aGears: Array;	
	
	/**
	 * Hàm tạo và mở file ngay lập tức. 
	 * Ngay khi file được mở xong, bắt sự kiện "open" và đọc ngay. 
	 * 
	 * @param	sFileName đường dẫn file XML
	 */
	public function GearReader (sFileName: String, encrypted : Boolean) {
		super();
		m_bEncrypted = encrypted;
		m_aGears = [];
		addEventListener ("open", readGears);
		loadFile (sFileName);
	}
	
	/**
	 * Bắt đầu đọc dữ liệu từ file XML. 
	 * Đọc từng bản ghi một bằng hàm readCommonItem. 
	 * Khi đọc xong thì sinh sự kiện "complete".
	 * 
	 * @param	evt eventObject bắt được từ sự kiện "open"
	 */
	private function readGears (evt:Object):Void {
		if (m_bEncrypted) {
			m_kDecryptor = new Decryptor (m_kRoot);
			var item: XMLNode = m_kDecryptor.getData().firstChild;
			var tempGear :Gear;
			var i:Number = 1;
			if (item) {
				do {
					//tempGear =			
					m_aGears[i++] =  readGearE (item);		
					item = item.nextSibling;			
				} while (item != undefined) ;
			}
/*			if (!(m_aGears[0] instanceof Gear)) {
				m_aGears.shift ();
			}*/
		} else {
			var item: XMLNode = m_kRoot.firstChild;		
			var tempGear : Gear;
			var i:Number = 1;
			if (item) {
				do {
					tempGear = readGear (item);
					m_aGears[i++] = readGear (item);
					item = item.nextSibling;
				} while (item != undefined) ;
			}
		}
		dispatchEvent ( { type: "complete", target: this, result: m_aGears } ) ;
	}		
	
	/**
	 * Đọc bản ghi. 
	 * Một bản ghi đọc từ XML vào là 1 instance của class BaseItem
	 * 
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	private function readGear (itemRoot: XMLNode):Gear {
		var properties:Array = convertIndices (itemRoot.childNodes);
		return new Gear (
			properties["ID"].firstChild.nodeValue, 
			readPropertys (properties["PROPERTIES"].firstChild)
		);
	}
	
	private function readPropertys (PropertysRoot : XMLNode) : Array {
		var item: XMLNode = PropertysRoot;		
		var temp : Array = [];
		var tempProperty: GearProperty;
		do {
			temp.push(reaProperty(item));			
			item = item.nextSibling;
		} while (item != undefined) ;
		return temp;
	}
	private function reaProperty (PropertRoot: XMLNode):GearProperty {
		var properties:Array = convertIndices (PropertRoot.childNodes);		
		//read Property
		return new GearProperty(
			properties["PROPERTY_ID"].firstChild.nodeValue, 
			properties["NAME"].firstChild.nodeValue, 
			properties["VALUE"].firstChild.nodeValue
		);	
	}
	
	/**
	 * Lấy danh sách các bản ghi đọc được từ XML. (Chỉ sử dụng hàm này sau khi có sự kiện "complete"). 	 
	 * 
	 * @return Danh sách các bản ghi
	 */
	public function getGears ():Array {
		return m_aGears;
	}
	
	/**
	 * Đọc bản ghi (Mã hóa) 
	 * Một bản ghi đọc từ XML vào là 1 instance của class BaseItem
	 * 
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	private function readGearE (itemRoot: XMLNode):Gear {
		var properties:Array = convertEncryptedIndices (itemRoot.childNodes);				
		return new Gear (
			Number(m_kDecryptor.decrypt(properties["ID"].firstChild.nodeValue)), 
			readPropertysE (properties["PROPERTIES"].firstChild)
		);		
	}
	
	private function readPropertysE (PropertysRoot : XMLNode) : Array {
		var item: XMLNode = PropertysRoot;		
		var temp : Array = [];
		var tempProperty: GearProperty;
		do {
			temp.push(readPropertyE(item));			
			item = item.nextSibling;
		} while (item != undefined) ;
		return temp;
	}
	
	private function readPropertyE (PropertyRoot: XMLNode):GearProperty {
		var properties:Array = convertEncryptedIndices (PropertyRoot.childNodes);		
		//read Property		
		return new GearProperty(
			Number(m_kDecryptor.decrypt(properties["PROPERTY_ID"].firstChild.nodeValue)), 
			m_kDecryptor.decrypt(properties["NAME"].firstChild.nodeValue), 
			Number(m_kDecryptor.decrypt(properties["VALUE"].firstChild.nodeValue))
		);		
	}
}