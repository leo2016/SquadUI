﻿import gfx.controls.SQUAD.CPPSimulator;
import gfx.controls.SQUAD.NewStyle.XML.*;
import gfx.controls.SQUAD.NewStyle.Data.*;
import gfx.events.EventDispatcher;

/**
 * Class đọc và ghép dữ liệu cho tất cả các phần dữ liệu từ XML
 */
class gfx.controls.SQUAD.NewStyle.XML.TotalReader {
	private static var ENCRYPTED : 		Boolean = false;//sẽ được thay đổi khi đọc thiết lập từ XMLSetting.xml
	
	//----------------------------------------------------------------------------------------Thư mục chứa các file XML và đuôi file
	private static var CIPHER_PATH : 		String = "Data/";
	private static var PLAIN_PATH:			String = "XML/";
	private static var FOLDER:				String = ENCRYPTED ? CIPHER_PATH : PLAIN_PATH;//sẽ được thay đổi khi ENCRYPTED bị thay đổi
	private static var CIPHER_EXT : 		String = ".dat";
	private static var PLAIN_EXT : 			String = ".xml";
	private static var EXTENSION: 			String = ENCRYPTED ? CIPHER_EXT : PLAIN_EXT;//sẽ được thay đổi khi ENCRYPTED bị thay đổi
	//----------------------------------------------------------------------------------------Tên các file XML
	private static var F_COMMON: 		String = "Common";
	private static var F_CATALOGUES: 	String = "Catalogues";
	private static var F_ENUMS: 		String = "Enums";
	private static var F_PRICES: 		String = "ItemPrices";
	private static var F_ADDON:			String = "AddOns";
	private static var F_ARMOR:			String = "Gears";
	private static var F_COLLECTION:	String = "Collections";
	
	private static var F_CHARACTER:		String = "Characters";
	private static var F_FASHION:		String = "FashionItems";
	private static var F_FUNCTIONAL:	String = "FunctionalItems";
	private static var F_GRENADE:		String = "Grenades";
	private static var F_GUN:			String = "Guns";
	private static var F_KNIFE:			String = "Knives";
	private static var F_SET:			String = "Sets";
	private static var F_MODE: 			String = "ModeMap";
	private static var F_WEAPON:		String = "WeaponInGame";
	private static var F_ROOM: 			String = "RoomName";
	private static var F_LEVELS:		String = "Ranks";
	private static var F_UNIFORMS:		String = "Uniforms";
	private static var F_HOT_ITEMS:		String = "HotAndNew";
	private static var F_CONFIG:		String = "Config";
	private static var F_SPLASH_SCREEN:	String = "SplashScreenContent";
	private static var F_MISSIONS : 	String = "Missions";
	private static var F_MEDALS : 		String = "Medals";
	private static var F_CLAN : 			String = "avatarClan";
	private static var F_CLAN_POSITION : 			String = "ClanPosition";
	private static var F_CLAN_RANK : 			String = "ClanRanks";
	private static var F_SKILL : 			String = "Skills";
	private static var F_EVENT : 			String = "Events";
	private static var F_GEAR : 			String = "Gears";
	//private static var F_CHARACTER : 			String = "Character";
	//---------------------LOCALIZATION---------------------------------
	private static var F_L_MAIN:		String = Localizer.LOCALIZATION_FOLDER + "Main";
	private static var F_L_INVENTORY:	String = Localizer.LOCALIZATION_FOLDER + "Inventory";
	private static var F_L_SHOP:		String = Localizer.LOCALIZATION_FOLDER + "Shop";
	private static var F_L_PLAYER_INFO:	String = Localizer.LOCALIZATION_FOLDER + "PlayerInfo";
	private static var F_L_UNIFORM:		String = Localizer.LOCALIZATION_FOLDER + "Uniform";
	private static var F_L_MENU:		String = Localizer.LOCALIZATION_FOLDER + "Menu";
	private static var F_L_LOBBY:		String = Localizer.LOCALIZATION_FOLDER + "Lobby";
	private static var F_L_INGAME: 		String = Localizer.LOCALIZATION_FOLDER + "Ingame";
	private static var F_L_MISSIONS: 	String = Localizer.LOCALIZATION_FOLDER + "Missions";
	private static var F_L_CHAT: 		String = Localizer.LOCALIZATION_FOLDER + "Chat";
	private static var F_L_FRIEND: 		String = Localizer.LOCALIZATION_FOLDER + "Friend";
	private static var F_L_EMAIL: 		String = Localizer.LOCALIZATION_FOLDER + "Email";
	private static var F_L_CONFIG: 		String = Localizer.LOCALIZATION_FOLDER + "Config";
	private static var F_L_LOGIN : 		String = Localizer.LOCALIZATION_FOLDER + "Login";
	private static var F_L_CHANNEL_SERVER : 	String = Localizer.LOCALIZATION_FOLDER + "ChannelServer";
	private static var F_L_CREATE_CHARACTER : 	String = Localizer.LOCALIZATION_FOLDER + "CreateCharacter";
	private static var F_L_MESSAGE_SYSTEM : 	String = Localizer.LOCALIZATION_FOLDER + "MessageSystem";
	private static var F_L_LANGAUGE : 	String = Localizer.LOCALIZATION_FOLDER + "Language";
	
	//----------------------------------------------------------------------------------------
	private static var STEP_READING: 	Number = 0;		//File đang được đọc
	private static var STEP_READ: 			Number = 1;		//File đã được đọc xong nhưng chưa liên kết với file khác
	private static var STEP_MERGED:		Number = 2;		//File đã được ghép nối với file khác
	//----------------------------------------------------------------------------------------
	private var readers: 	Array;							//Các class đọc file XML
	private var data: 		Array;							//Các dữ liệu tương ứng
	private var steps:		Array;							//Bước thực hiện hiện tại của các class đọc file XML
	private var files:		Array;						//Danh sách các file sẽ được đọc (sử dụng PRESET_* để chỉ định)
	private var handlers:	Array;							//Các hàm kết hợp dữ liệu của các file XML với nhau
	//----------------------------------------------------------------------------------------
	private var finishCount: Number;
	//----------------------------------------------------------------------------------------
	public static var PRESET_ALL : Array = [
		F_COMMON,			F_CATALOGUES,			F_ENUMS,				F_PRICES,
		F_ADDON,				F_ARMOR,					F_COLLECTION,		F_CHARACTER,
		F_FASHION,			F_FUNCTIONAL,			F_GRENADE,			F_GUN,
		F_KNIFE,				F_SET, 						F_MODE,				F_WEAPON,
		F_ROOM,				F_LEVELS, 					F_UNIFORMS, 		F_SPLASH_SCREEN,
		F_HOT_ITEMS,		F_CONFIG, 				F_MISSIONS, 			F_MEDALS,
		//Localization
		F_L_MAIN, 			F_L_SHOP, 				F_L_MENU,	F_L_LOBBY, 
		F_L_PLAYER_INFO, 	F_L_INGAME, 			F_L_INVENTORY, 	F_L_MISSIONS,
		F_L_CHAT, F_L_FRIEND, F_L_EMAIL, F_L_CONFIG, F_L_LOGIN, F_L_CHANNEL_SERVER,
		F_L_CREATE_CHARACTER,F_L_MESSAGE_SYSTEM
	];
	public static var PRESET_INVENTORY : Array = [
		F_COMMON,
		F_CATALOGUES,
		F_ENUMS,
		F_PRICES,		
		F_ADDON, 
		//F_ARMOR,		
		F_GRENADE,
		F_GUN,
		F_KNIFE, 
		F_UNIFORMS, 		
		F_HOT_ITEMS,
		//F_MODE, 
		F_L_MAIN, 
		F_L_INVENTORY,
		F_LEVELS,
		F_GEAR
		//F_CHARACTER
	];
	public static var PRESET_LOBBY : Array = [
		F_COMMON,
		F_CATALOGUES,
		F_ENUMS,
		F_PRICES,		
		F_ADDON, 
		F_ARMOR,		
		F_GRENADE,
		F_GUN,
		F_KNIFE, 
		F_UNIFORMS, 		
		F_ROOM,		
		F_MODE, 
		F_LEVELS,
		F_L_MAIN,
		F_L_LOBBY
		//F_L_LANGAUGE
		
	];

	public static var PRESET_MENU : Array = [
		F_COMMON,		
		F_UNIFORMS, 
		F_LEVELS, 
		F_PRICES,
		F_ENUMS, 
		F_L_MAIN,
		F_L_UNIFORM,
		F_L_MENU,
		F_MISSIONS,
		F_MODE,
		F_L_MISSIONS
	];
	public static var PRESET_PLAYER_INFO : Array = [
		F_COMMON,		
		F_CATALOGUES, 
		F_UNIFORMS, 
		F_LEVELS, 
		F_MODE, 		
		F_MISSIONS, 
		F_MEDALS, 
		F_L_MAIN,
		F_L_PLAYER_INFO,		
		F_L_MISSIONS,
		F_L_CHAT,
		F_L_FRIEND,
		F_L_EMAIL,
		F_L_MESSAGE_SYSTEM,
		F_SKILL
	];
	public static var PRESET_PLAYER_CHAT: Array = [
		F_LEVELS, 
		F_L_MAIN,
		F_L_PLAYER_INFO,		
		F_L_CHAT,
		F_L_FRIEND
	];
	
	public static var PRESET_SHOP : Array = [
		F_COMMON,
		F_CATALOGUES,
		F_ENUMS,
		F_PRICES,		
		F_ADDON, 
		//F_ARMOR,		
		F_GRENADE,
		F_GUN,
		F_KNIFE, 
		F_UNIFORMS, 
		F_L_MAIN, 
		F_L_SHOP, 
		F_HOT_ITEMS,
		F_LEVELS,
		F_GEAR
		//F_CHARACTER
	];
	public static var PRESET_INGAME : Array = [
		F_COMMON,
		F_CATALOGUES,
		F_LEVELS,
		F_MODE,	
		F_MISSIONS, 
		F_L_MAIN,
		F_L_INGAME, 
		F_L_MISSIONS,
		F_L_MESSAGE_SYSTEM,
		F_L_CHAT
	];
	public static var PRESET_POPUPlOBBY : Array = [
		F_COMMON,		
		F_CATALOGUES, 
		F_UNIFORMS, 
		F_LEVELS, 
		F_MODE, 		
		F_MISSIONS, 
		F_MEDALS, 
		F_L_MAIN,
		F_L_PLAYER_INFO,		
		F_L_MISSIONS,
		F_L_CHAT,
		F_L_FRIEND,
		F_L_EMAIL,
		F_L_MESSAGE_SYSTEM,
		F_L_LOBBY,
		F_EVENT
	];
	public static var PRESET_SETTING : Array = [
		F_CONFIG,
		F_L_CONFIG,
		F_L_MAIN,
		F_L_MESSAGE_SYSTEM,
		F_L_CHAT,
		F_L_LANGAUGE
	];
	public static var PRESET_LOADING : Array = [
		F_MODE
	];
	public static var PRESET_CHANNEL_SERVER : Array = [
		F_L_CHANNEL_SERVER,
		F_L_MAIN
	];
	public static var PRESET_LOGIN : Array = [
		F_SPLASH_SCREEN,
		F_L_LOGIN,
		F_L_MAIN
	];
	public static var PRESET_CREATE_CHARACTER : Array = [
		F_L_CREATE_CHARACTER,
		F_L_MAIN,
		F_COMMON
		//F_CHARACTER
	];
	public static var PRESET_CLAN : Array = [
		F_CLAN,
		F_LEVELS,
		F_CLAN_POSITION,
		F_CLAN_RANK
	];
	function dispatchEvent() {};
 	function addEventListener() {};
 	function removeEventListener() { };
		
	/**
	 * Hàm tạo.
	 * Ngay lập tức mở và đọc tất cả các file XML cần thiết.
	 * Mỗi khi hoàn tất việc đọc 1 file XML, chuyển sang hàm prepareToMergeData để ghép thông tin (nếu được)
	 * 
	 * 
	 * @param	preset	Danh sách các file XML cần đọc được thiết lập sẵn (có thể tự tạo thêm biến PRESET_* trong class này)
	 */
	public function TotalReader (preset : Array) {	
		EventDispatcher.initialize (this);
		
		if (preset == undefined || preset == null) {
			preset = PRESET_ALL;
		}		
		CPPSimulator.trace ("Reading data from XML files.......(preset " + preset + ")", 10);		
		readers 	= [];
		data 		= [];
		steps 		= [];
		files 			= preset;
		handlers 	= [];
		finishCount = 0;
		
		handlers [F_ADDON] 			= attachAsAddon;
		//handlers [F_ARMOR] 			= attachAsArmor;
		handlers [F_COLLECTION] 	= attachAsCollection;
		//handlers [F_CHARACTER]  	= attachAsCharacter;
		handlers [F_FASHION] 		= attachAsFashion;
		handlers [F_FUNCTIONAL] 	= attachAsFunctional;
		handlers [F_GRENADE] 		= attachAsGrenade;
		handlers [F_GUN] 			= attachAsGun;
		handlers [F_KNIFE] 			= attachAsKnife;
		handlers [F_SET] 			= attachAsSet;
		handlers [F_UNIFORMS] 		= attachAsUniform;
		handlers [F_MEDALS] 		= attachAsMedal;
		//---------------------------LOCALIZATION--------------------------
		handlers [F_L_INVENTORY]	= attachToMainLocalizer;
		handlers [F_L_SHOP]			= attachToMainLocalizer;
		handlers [F_L_MENU]			= attachToMainLocalizer;
		handlers [F_L_PLAYER_INFO] 	= attachToMainLocalizer;
		handlers [F_L_UNIFORM]   	= attachToMainLocalizer;
		handlers [F_L_LOBBY]		= attachToMainLocalizer;
		handlers[F_L_INGAME]        = attachToMainLocalizer;
		handlers[F_L_MISSIONS] 		= attachToMainLocalizer;
		handlers[F_L_CHAT] 			= attachToMainLocalizer;
		handlers[F_L_FRIEND] 		= attachToMainLocalizer;
		handlers[F_L_EMAIL] 		= attachToMainLocalizer;
		handlers[F_L_CONFIG] 		= attachToMainLocalizer;
		handlers[F_L_LOGIN] 		= attachToMainLocalizer;
		handlers[F_L_CHANNEL_SERVER] 		= attachToMainLocalizer;
		handlers[F_L_CREATE_CHARACTER] 		= attachToMainLocalizer;
		handlers[F_L_MESSAGE_SYSTEM] 		= attachToMainLocalizer;
		handlers[F_L_LANGAUGE] 		= attachToMainLocalizer;
		
		
		var kXMLSettingReader : XMLSettingReader = new XMLSettingReader ("DataSetting.dat");
		kXMLSettingReader.addEventListener ("complete", this, "getXMLSetting");
	}
	
	private function getXMLSetting (evt : Object) : Void {
		ENCRYPTED = Boolean(evt.result);
		FOLDER = ENCRYPTED ? CIPHER_PATH : PLAIN_PATH;
		EXTENSION = ENCRYPTED ? CIPHER_EXT : PLAIN_EXT;
		init ();
	}
	
	private function init () : Void {
		if (isInFiles(F_COMMON)) {
			readers [F_COMMON] 		= new CommonReader (FOLDER + F_COMMON + EXTENSION, ENCRYPTED);
		}		
		if (isInFiles(F_CATALOGUES)) {
			readers [F_CATALOGUES] 	= new CataloguesReader (FOLDER + F_CATALOGUES + EXTENSION, ENCRYPTED);
		}		
		if (isInFiles(F_ENUMS)) {
			readers [F_ENUMS] 			= new EnumReader (FOLDER + F_ENUMS + EXTENSION, ENCRYPTED);
		}
		if (isInFiles(F_PRICES)) {
			readers [F_PRICES] 			= new PricesReader (FOLDER + F_PRICES + EXTENSION, ENCRYPTED);
		}
		if (isInFiles(F_ADDON)) {
			readers [F_ADDON] 			= new AddOnsReader (FOLDER + F_ADDON + EXTENSION, ENCRYPTED);		
		}		
	/*	if (isInFiles(F_ARMOR)) {
			readers [F_ARMOR] 			= new ArmorsReader (FOLDER + F_ARMOR + EXTENSION, ENCRYPTED);
		}	*/
		
		//if (isInFiles(F_COLLECTION)) {
			//readers [F_COLLECTION] 	= new CollectionsReader (FOLDER + F_COLLECTION + EXTENSION, ENCRYPTED);
		//}
		//if (isInFiles(F_CHARACTER)) {
			//readers [F_CHARACTER] 	= new CharactersReader (FOLDER + F_CHARACTER + EXTENSION, ENCRYPTED);
		//}
		//if (isInFiles(F_FASHION)) {
			//readers [F_FASHION] 			= new FashionItemsReader (FOLDER + F_FASHION + EXTENSION, ENCRYPTED);
		//}
		//if (isInFiles(F_FUNCTIONAL)) {
			//readers [F_FUNCTIONAL] 	= new FunctionalItemsReader (FOLDER + F_FUNCTIONAL + EXTENSION, ENCRYPTED);
		//}		
		
		if (isInFiles(F_GRENADE)) {		
			readers [F_GRENADE] 		= new GrenadesReader (FOLDER + F_GRENADE + EXTENSION, ENCRYPTED);
		}
		if (isInFiles(F_GUN)) {
			readers [F_GUN] 				= new GunsReader (FOLDER + F_GUN + EXTENSION, ENCRYPTED);
		}
		if (isInFiles(F_KNIFE)) {
			readers [F_KNIFE] 				= new KnivesReader (FOLDER + F_KNIFE + EXTENSION, ENCRYPTED);
		}		
		
		//if (isInFiles(F_SET)) {
			//readers [F_SET] 					= new CollectionsReader (FOLDER + F_SET + EXTENSION, ENCRYPTED);		
		//}
		//if (isInFiles(F_WEAPON)) {
			//readers[F_WEAPON]			= new IconWeaponInGameReader(FOLDER + F_WEAPON + EXTENSION, ENCRYPTED);		
		//}		
		
		if (isInFiles(F_ROOM)) {
			readers[F_ROOM]				= new RoomNameReader(FOLDER + F_ROOM + EXTENSION, ENCRYPTED);
		}		
		if (isInFiles(F_MODE)) {
			readers [F_MODE]				= new ModesReader (FOLDER + F_MODE + EXTENSION, ENCRYPTED);
		}		
		if (isInFiles(F_LEVELS)) {
			readers[F_LEVELS]				= new LevelsReader(FOLDER + F_LEVELS + EXTENSION, ENCRYPTED);
		}
		if (isInFiles(F_UNIFORMS)) {
			readers[F_UNIFORMS]		= new UniformsReader(FOLDER + F_UNIFORMS + EXTENSION, ENCRYPTED);
		}
		if (isInFiles(F_HOT_ITEMS)) {
			readers[F_HOT_ITEMS]		= new HotItemsReader(FOLDER + F_HOT_ITEMS + EXTENSION, ENCRYPTED);
		}
		if (isInFiles(F_CONFIG)) {
			readers[F_CONFIG]		= new KeyBoardReader(FOLDER + F_CONFIG + EXTENSION, ENCRYPTED);
		}
		if (isInFiles(F_SPLASH_SCREEN)) {
			readers[F_SPLASH_SCREEN]	= new SSContentReader(FOLDER + F_SPLASH_SCREEN + EXTENSION, ENCRYPTED);
		}
		if (isInFiles(F_MISSIONS)) {
			readers[F_MISSIONS]	= new MissionsReader(FOLDER + F_MISSIONS + EXTENSION, ENCRYPTED);
		}
		if (isInFiles(F_MEDALS)) {
			readers[F_MEDALS]	= new MedalsReader(FOLDER + F_MEDALS + EXTENSION, ENCRYPTED);
		}
			if (isInFiles(F_CLAN)) {
			readers[F_CLAN]				= new ClanAvatarReader(FOLDER + F_CLAN + EXTENSION, ENCRYPTED);
		}
		if (isInFiles(F_CLAN_POSITION)) {
			readers[F_CLAN_POSITION]			= new ClanpositionReader(FOLDER + F_CLAN_POSITION + EXTENSION, ENCRYPTED);
		}
		if (isInFiles(F_CLAN_RANK)) {
			readers[F_CLAN_RANK]			= new ClanRankReader(FOLDER + F_CLAN_RANK + EXTENSION, ENCRYPTED);
		}
		if (isInFiles(F_SKILL)) {
			readers[F_SKILL]			= new SkillReader(FOLDER + F_SKILL + EXTENSION, ENCRYPTED);
		}
		if (isInFiles(F_EVENT)) {
			readers[F_EVENT]			= new EventReader(FOLDER + F_EVENT + EXTENSION, ENCRYPTED);
		}
		if (isInFiles(F_GEAR)) {
			readers[F_GEAR]			= new GearReader(FOLDER + F_GEAR + EXTENSION, ENCRYPTED);
		}
		if (isInFiles(F_CHARACTER)) {
			readers[F_CHARACTER]			= new SpecialItemReader(FOLDER + F_CHARACTER + EXTENSION, ENCRYPTED);
		}
		
		//------------------------------------LOCALIZE-----------------------------------------------------------------
		if (isInFiles(F_L_MAIN)) {			
			readers[F_L_MAIN]				= new Localizer (FOLDER + F_L_MAIN + EXTENSION, ENCRYPTED);		
		}
		if (isInFiles(F_L_INVENTORY)) {
			readers[F_L_INVENTORY]	= new Localizer (FOLDER + F_L_INVENTORY + EXTENSION, ENCRYPTED);
		}
		if (isInFiles(F_L_SHOP)) {
			readers[F_L_SHOP]				= new Localizer (FOLDER + F_L_SHOP + EXTENSION, ENCRYPTED);
		}
		if (isInFiles(F_L_MENU)) {
			readers[F_L_MENU]			= new Localizer (FOLDER + F_L_MENU + EXTENSION, ENCRYPTED);
		}
		if (isInFiles(F_L_PLAYER_INFO)) {
			readers[F_L_PLAYER_INFO]	= new Localizer (FOLDER + F_L_PLAYER_INFO +EXTENSION, ENCRYPTED);
		}
		if (isInFiles(F_L_UNIFORM)) {
			readers[F_L_UNIFORM]		= new Localizer (FOLDER + F_L_UNIFORM +EXTENSION, ENCRYPTED);
		}
		if (isInFiles(F_L_LOBBY)) {
			readers[F_L_LOBBY]			= new Localizer (FOLDER + F_L_LOBBY +EXTENSION, ENCRYPTED);
		}
		if (isInFiles(F_L_INGAME)) {
			readers[F_L_INGAME]         = new Localizer (FOLDER + F_L_INGAME + EXTENSION, ENCRYPTED);
		}
		if (isInFiles(F_L_MISSIONS)) {
			readers[F_L_MISSIONS]         = new Localizer (FOLDER + F_L_MISSIONS + EXTENSION, ENCRYPTED);
		}
		if (isInFiles(F_L_CHAT)) { 
			readers[F_L_CHAT]         = new Localizer (FOLDER + F_L_CHAT + EXTENSION, ENCRYPTED);
		}
		if (isInFiles(F_L_FRIEND)) {
			readers[F_L_FRIEND]         = new Localizer (FOLDER + F_L_FRIEND + EXTENSION, ENCRYPTED);			
		}
		if (isInFiles(F_L_EMAIL)) {
			readers[F_L_EMAIL]         = new Localizer (FOLDER + F_L_EMAIL + EXTENSION, ENCRYPTED);			
		}
		if (isInFiles(F_L_CONFIG)) {
			readers[F_L_CONFIG]         = new Localizer (FOLDER + F_L_CONFIG + EXTENSION, ENCRYPTED);			
		}
		if (isInFiles(F_L_LOGIN)) {
			readers[F_L_LOGIN]         = new Localizer (FOLDER + F_L_LOGIN + EXTENSION, ENCRYPTED);			
		}
		if (isInFiles(F_L_CHANNEL_SERVER)) {
			readers[F_L_CHANNEL_SERVER]         = new Localizer (FOLDER + F_L_CHANNEL_SERVER + EXTENSION, ENCRYPTED);			
		}
		if (isInFiles(F_L_CREATE_CHARACTER)) {
			readers[F_L_CREATE_CHARACTER]         = new Localizer (FOLDER + F_L_CREATE_CHARACTER + EXTENSION, ENCRYPTED);			
		}
		if (isInFiles(F_L_MESSAGE_SYSTEM)) {
			readers[F_L_MESSAGE_SYSTEM]         = new Localizer (FOLDER + F_L_MESSAGE_SYSTEM + EXTENSION, ENCRYPTED);			
		}
		if (isInFiles(F_L_LANGAUGE)) {
			readers[F_L_LANGAUGE]         = new Localizer (FOLDER + F_L_LANGAUGE + EXTENSION, ENCRYPTED);			
		}
			
		for (var x: String in files) {
			readers	[files[x]].addEventListener ("complete", this, "prepareToMergeData");				
			data 	[files[x]] = null;
			steps 	[files[x]] = STEP_READING;
		}		
	}

	/**
	 * Ghép nối thông tin
	 *
	 * @param	evt	Đối tượng sinh bởi sự kiện "complete"
	 */
	private function prepareToMergeData (evt: Object) : Void {
		++finishCount;
		
		for (var x: String in files) {
			var fileX:String = files[x];
			if (evt.target == readers[fileX]) {
				CPPSimulator.trace (finishCount + ". " + fileX, 11);
				steps [fileX] = STEP_READ;
				data [fileX] = evt.result;				
				break;
			}			
		}
		
		if (steps [F_COMMON] == STEP_READ) {			
			for (var x:String in handlers) {				
				if (steps[x] == STEP_READ && x.indexOf(Localizer.LOCALIZATION_FOLDER) == -1) {
					if (handlers[x] != undefined) {						
						handlers[x] (data[F_COMMON], data[x]);						
						finish (x);
					}
				}
			}
		}
		
		if (steps [F_L_MAIN] == STEP_READ) {
			for (var x:String in handlers) {				
				if (steps[x] == STEP_READ && x.indexOf(Localizer.LOCALIZATION_FOLDER) == 0) {					
					if (handlers[x] != undefined) {						
						handlers[x] (data[F_L_MAIN], data[x]);
						finish (x);
					}
				}
			}
		}
		
		if (finishCount == files.length) {
			attachPrices (data[F_COMMON], data[F_PRICES]);
			CPPSimulator.trace ("Finish reading data from XML completely!", 10);
			dispatchEvent ( { type: "complete", target: this } );	
		}
	}

	/**
	 * Ghép giá tiền
	 *
	 * @param	baseItems	danh sách vật phẩm
	 * @param	prices	giá tiền
	 */
	private function attachPrices (baseItems: Array, prices: Array) : Void {		
		for (var x:String in prices) {
			if (baseItems[x]) {				
				baseItems[x].attachPrices ( prices[x].prices );
			}
		}
	}

	/**
	 * Ghép thông tin về phụ kiện
	 *
	 * @param	baseItems	danh sách vật phẩm
	 * @param	extraInfo	thông tin bổ sung
	 */
	private function attachAsAddon (baseItems: Array, extraInfo: Array) : Void {
		for (var x:String in extraInfo) {
			var extra: Object = extraInfo[x];
			baseItems[x] = new AddOn (baseItems[x], extra.properties, extra.holders);
		}
	}

	/**
	 * Ghép thông tin về giáp
	 *
	 * @param	baseItems	danh sách vật phẩm
	 * @param	extraInfo	thông tin bổ sung
	 */
	private function attachAsArmor (baseItems: Array, extraInfo: Array) : Void {
		for (var x:String in extraInfo) {			
			baseItems[x] = new Armor (baseItems[x], extraInfo[x].damageReduction);
		}
	}

	/**
	 * Ghép thông tin về nhân vật
	 *
	 * @param	baseItems	danh sách vật phẩm
	 * @param	extraInfo	thông tin bổ sung
	 */
	private function attachAsCharacter (baseItems: Array, extraInfo: Array) : Void {
		for (var x:String in extraInfo) {
			//baseItems[x] = new Character (baseItems[x], extraInfo[x].baseForce, extraInfo[x].isMale);
		}
	}

	/**
	 * Ghép thông tin về bộ sưu tập
	 *
	 * @param	baseItems	danh sách vật phẩm
	 * @param	extraInfo	thông tin bổ sung
	 */
	private function attachAsCollection (baseItems: Array, extraInfo: Array) : Void {
		for (var x:String in extraInfo) {
			//baseItems[x] = new Collection (baseItems[x], extraInfo[x].items);
		}
	}

	/**
	 * Ghép thông tin về vật phẩm thời trang
	 *
	 * @param	baseItems	danh sách vật phẩm
	 * @param	extraInfo	thông tin bổ sung
	 */
	private function attachAsFashion (baseItems: Array, extraInfo: Array) : Void {
		for (var x:String in extraInfo) {
			var extra: Object = extraInfo[x];
			/*
			baseItems[x] = new FashionItem (
				baseItems[x],
				extra.isSexLimited,
				extra.isMale,
				extra.extraType,
				extra.extraValue,
				extra.isCharacterLimited,
				extra.characterIds);
				*/
		}
	}

	/**
	 * Ghép thông tin về vật phẩm chức năng
	 *
	 * @param	baseItems	danh sách vật phẩm
	 * @param	extraInfo	thông tin bổ sung
	 */
	private function attachAsFunctional (baseItems: Array, extraInfo: Array) : Void {
		for (var x:String in extraInfo) {
			//baseItems[x] = new FunctionalItem (baseItems[x], extraInfo[x].properties);
		}
	}

	/**
	 * Ghép thông tin về lựu đạn
	 *
	 * @param	baseItems	danh sách vật phẩm
	 * @param	extraInfo	thông tin bổ sung
	 */
	private function attachAsGrenade (baseItems: Array, extraInfo: Array) : Void {
		for (var x:String in extraInfo) {
			var extra: Object = extraInfo[x];
			baseItems[x] = new Grenade (baseItems[x], extra.effectRadius, extra.baseDamage, extra.lifeTime, extra.distance, extra.effect);
		}
	}

	/**
	 * Ghép thông tin về súng
	 *
	 * @param	baseItems	danh sách vật phẩm
	 * @param	extraInfo	thông tin bổ sung
	 */
	private function attachAsGun (baseItems: Array, extraInfo: Array) : Void {
		for (var x:String in extraInfo) {
			var extra: Object = extraInfo[x];
			baseItems[x] = new Gun (
				baseItems[x],
				extra.ammos,
				extra.speed,
				extra.recoil,
				extra.accuracy,
				extra.minRank,
				extra.damageAmplifying,
				extra.reloadTime, 
				extra.slots);
		}
	}

	/**
	 * Ghép thông tin về dao
	 *
	 * @param	baseItems	danh sách vật phẩm
	 * @param	extraInfo	thông tin bổ sung
	 */
	private function attachAsKnife (baseItems: Array, extraInfo: Array) : Void {
		for (var x:String in extraInfo) {
			var extra: Object = extraInfo[x];
			baseItems[x] = new Knife (
				baseItems[x],
				extra.baseDamage,
				extra.speed,
				extra.length,
				extra.specialModeDamage,
				extra.specialModeSpeed);
		}
	}

	/**
	 * Ghép thông tin về gói
	 *
	 * @param	baseItems	danh sách vật phẩm
	 * @param	extraInfo	thông tin bổ sung
	 */
	private function attachAsSet (baseItems: Array, extraInfo: Array) : Void {
		for (var x:String in extraInfo) {
			//baseItems[x] = new Set (baseItems[x], extraInfo[x].items);
		}
	}
	
	/**
	 * Ghép thông tin về quân phục
	 *
	 * @param	baseItems	danh sách vật phẩm
	 * @param	extraInfo	thông tin bổ sung
	 */
	private function attachAsUniform (baseItems: Array, extraInfo: Array) : Void {
		for (var x:String in extraInfo) {
			baseItems[x] = new Uniform (baseItems[x], extraInfo[x].properties);
		}
	}
	private function attachAsSteting (baseItems: Array, extraInfo: Array) : Void {
		for (var x:String in extraInfo) {
			baseItems[x] = new Uniform (baseItems[x], extraInfo[x].properties);
		}
	}
	
	/**
	 * Ghép thông tin LOCALIZATION
	 *
	 * @param	main		dữ liệu đang có
	 * @param	extra		dữ liệu bổ sung
	 */
	private function attachToMainLocalizer (main: Localizer, extra: Localizer) : Void {				
		main.append (extra.m_aMap);				
	}		

	/**
	 * Đánh dấu đã ghép xong thông tin cho 1 loại vật phẩm, đồng thời xóa dữ liệu thừa để giải phóng bộ nhớ
	 *
	 * @param	file	loại vật phẩm đã ghép xong
	 */
	private function finish (file: String) : Void {
		delete readers[file];		
		data[file] = null;
		steps[file] = STEP_MERGED;
		CPPSimulator.trace ("Finish merging " + file, 11);
	}
	
	/**
	 * Lấy danh sách các enum đọc được. Chỉ sử dụng hàm này sau khi có sự kiện "complete"
	 *
	 * @return danh sách các enum đọc được
	 */
	public function getEnums(): Array {
		return data[F_ENUMS];
	}
	
	/**
	 * Lấy danh sách các catalogue đọc được. Chỉ sử dụng hàm này sau khi có sự kiện "complete"
	 *
	 * @return danh sách các catalogue đọc được (đã phân cấp)
	 */
	public function getCatalogues(): Array {
		return data[F_CATALOGUES];
	}
	
	/**
	 * Lấy danh sách các vật phẩm đọc được. Chỉ sử dụng hàm này sau khi có sự kiện "complete"
	 *
	 * @return danh sách các vật phẩm đọc được (đã ghép đủ thông tin)
	 */
	public function getItems(): Array {
		return data[F_COMMON];
	}
	/**
	 * 
	 * @return danh sach Mode cua Game trong Lobby
	 */
	public function getModes () : Array {
		return data[F_MODE];
	}
	/**
	 * 
	 * @return danh sach vu khi trong InGame
	 */
	public function getWeaponInGame():Array {
		return data[F_WEAPON];
	}
	
	/**
	 * 
	 * @return danh sach phong trong Lobby
	 */
	public function getRoomName():Array {
		return data[F_ROOM];
	}
	//setting 
		public function getDefaultSetting():Object {
		return data[F_CONFIG];
	}
	
	/**
	 * 
	 * @return Level cua Player
	 */
	public function getLevel():Array {
		return data[F_LEVELS];
	}
	/**	 
	 * @return Localizer đã được ghép nối
	 */
	public function getLocalizer () : Localizer {
		return data[F_L_MAIN];
	}
	
	/**	 
	 * @return danh sách mã các vật phẩm HOT nhất
	 */
	public function getHotItems () : Array {
		return data [F_HOT_ITEMS];
	}
	
	/**
	 * Kiểm tra file có nằm trong danh sách cần đọc không
	 * 
	 * @param	fileName	đường dẫn tới file
	 * @return		true - có nằm trong danh sách, false - ngược lại
	 */
	private function isInFiles (fileName : String) : Boolean {
		for (var x:String in files) {
			if (files[x] == fileName) {
				return true;
			}
		}
		return false;
	}
	
	public function getSSContent () : Object {
		return data[F_SPLASH_SCREEN];
	}
	
	public function getMissions () : Array/*MissionGroups*/{
		
		return data[F_MISSIONS];
	}
	//avatar clan
	public function getAvatar():Array {
		return data[F_CLAN];
	}
	//position Clan
	public function getPosition():Array {
		return data[F_CLAN_POSITION];
	}
	//rank Clan
		public function getClanRank():Array {
		return data[F_CLAN_RANK];
	}
	//Skill
	public function getSkill():Array {
		return data[F_SKILL];
	}
	//Event
		public function getEvents():Array {
		return data[F_EVENT];
	}
	//gear
		public function getGears():Array {
		return data[F_GEAR];
	}
	//character Item
		public function getcharacterItem():Array {
		return data[F_CHARACTER];
	}

	/**
	 * Ghép thông tin về huân chương
	 *
	 * @param	baseItems	danh sách vật phẩm
	 * @param	extraInfo	thông tin bổ sung
	 */
	private function attachAsMedal (baseItems: Array, extraInfo: Array) : Void {
		for (var x:String in extraInfo) {
			var extra: Object = extraInfo[x];
			baseItems[x] = new Medal (baseItems[x], extra.properties);
		}
	}
}
