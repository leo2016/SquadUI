﻿import gfx.controls.SQUAD.Data.Grenade;
import gfx.controls.SQUAD.NewStyle.XML.BaseReader;
import gfx.controls.SQUAD.NewStyle.XML.Decryptor;

/**
 * Class đọc dữ liệu bổ sung cho LỰU ĐẠN từ XML.
 * Đọc ngay khi khởi tạo instance mới nhưng phải bắt sự kiện "complete" rồi chờ tới khi đọc xong mới nhận được dữ liệu để xử lý.
 */
class gfx.controls.SQUAD.NewStyle.XML.GrenadesReader extends BaseReader {
	private var m_aGrenades: Array;
	
	/**
	 * Hàm tạo và mở file ngay lập tức.
	 * Ngay khi file được mở xong, bắt sự kiện "open" và đọc ngay.
	 *
	 * @param	sFileName đường dẫn file XML
	 */
	public function GrenadesReader (sFileName: String, encrypted : Boolean) {
		super();
		
		m_bEncrypted = encrypted;
		m_aGrenades = [];
		addEventListener ("open", readGrenades);
		loadFile (sFileName);
	}
	
	/**
	 * Bắt đầu đọc dữ liệu từ file XML.
	 * Đọc từng bản ghi một bằng hàm readGrenade.
	 * Khi đọc xong thì sinh sự kiện "complete".
	 *
	 * @param	evt eventObject bắt được từ sự kiện "open"
	 */
	private function readGrenades (evt: Object):Void {
		if (m_bEncrypted) {
			m_kDecryptor = new Decryptor (m_kRoot);
			var item: XMLNode = m_kDecryptor.getData().firstChild;
			if (item) {
				do {
					readGrenadeE (item);
					item = item.nextSibling;
				} while (item != undefined) ;
			}
		} else {
			var item: XMLNode = m_kRoot.firstChild;
			if (item) {
				do {
					readGrenade (item);
					item = item.nextSibling;
				} while (item != undefined) ;
			}
		}		
		dispatchEvent ( { type: "complete", target: this, result: m_aGrenades } ) ;
	}
	
	/**
	 * Đọc bản ghi.
	 * Một bản ghi đọc từ XML vào có các trường:
	 * + id
	 * + effectRadius
	 * + baseDamage
	 *
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	private function readGrenade (itemRoot: XMLNode):Void {
		var properties:Array = convertIndices (itemRoot.childNodes);
		var id :Number 		= Number(properties["ID"].firstChild.nodeValue);
		m_aGrenades [id] = {
				id			 : id,
				effectRadius : Number(properties["EFFECT_RADIUS"].firstChild.nodeValue),
				baseDamage   : Number(properties["BASE_DAMAGE"].firstChild.nodeValue),
				lifeTime     : Number(properties["LIFE_TIME"].firstChild.nodeValue),
				distance	 : Number(properties["DISTANCE"].firstChild.nodeValue),
				effect	     : String(properties["EFFECT"].firstChild.nodeValue)
		 };
	}
	
	/**
	 * Lấy danh sách các bản ghi đọc được từ XML. (Chỉ sử dụng hàm này sau khi có sự kiện "complete").
	 * Các bản ghi này chỉ là dữ liệu bổ sung nên phải đợi đọc xong "Common.xml" mới ghép được dữ liệu.
	 *
	 * @return Danh sách các bản ghi
	 */
	public function getAllGrenades ():Array {
		return m_aGrenades;
	}
	
	/**
	 * Đọc bản ghi (Mã hóa)
	 * Một bản ghi đọc từ XML vào có các trường:
	 * + id
	 * + effectRadius
	 * + baseDamage
	 *
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	private function readGrenadeE (itemRoot: XMLNode):Void {
		var properties:Array = convertEncryptedIndices (itemRoot.childNodes);
		var id : Number = Number(m_kDecryptor.decrypt(properties["ID"].firstChild.nodeValue));
		m_aGrenades [id] = {
			id			:	id, 
			effectRadius:	Number(m_kDecryptor.decrypt(properties["EFFECT_RADIUS"].firstChild.nodeValue)),
			baseDamage	: 	Number(m_kDecryptor.decrypt(properties["BASE_DAMAGE"].firstChild.nodeValue)),
			lifeTime 	:	Number(m_kDecryptor.decrypt(properties["LIFE_TIME"].firstChild.nodeValue)),
			baseDamage	: 	Number(m_kDecryptor.decrypt(properties["DISTANCE"].firstChild.nodeValue)),
			effect		: 	String(m_kDecryptor.decrypt(properties["EFFECT"].firstChild.nodeValue))
		};
	}
}