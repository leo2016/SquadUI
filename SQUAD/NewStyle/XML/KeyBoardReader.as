﻿import gfx.controls.SQUAD.NewStyle.XML.BaseReader;
import gfx.controls.SQUAD.NewStyle.XML.Decryptor;

class gfx.controls.SQUAD.NewStyle.XML.KeyBoardReader extends BaseReader{
	var m_aKeyboard:Array;
	var m_aSound:Array;
	
	private static var TAB_CONTROL : String = "TAB_CONTROL";
	private static var TAB_SOUND : String = "TAB_SOUND";
	
	public function KeyBoardReader (sFileName: String, encrypted : Boolean) {
		super();
		m_bEncrypted = encrypted;
		m_aKeyboard = [];
		m_aSound = [];
		addEventListener ("open", readKeyboards);
		loadFile (sFileName);
	}
	
	public function readKeyboards(evt: Object) {
		if (m_bEncrypted) {
			m_kDecryptor = new Decryptor (m_kRoot);
			var tabs : Array = convertEncryptedIndices (m_kDecryptor.getData().childNodes);
			readTabControlE (tabs[TAB_CONTROL]);
			readTabSoundE (tabs[TAB_SOUND]);
		} else {
			var tabs : Array = convertIndices (m_kRoot.childNodes);
			readTabControl (tabs[TAB_CONTROL]);
			readTabSound(tabs[TAB_SOUND]);
		}		
		dispatchEvent ( { 
			type: "complete", 
			target: this, 
			result:	{
				tabControlKeyboard: m_aKeyboard,
				tabSound:m_aSound
			}			
		} ) ;
	}
	private function readTabSound(root : XMLNode) {
		var parts : Array = convertIndices (root.childNodes);
		//sound default
		var sound : Array = parts["SOUNDS"].childNodes;		
		var props : Array;
		for (var x : String in sound) {			
			props = convertIndices (sound[x].childNodes);
			m_aSound[props["ACTION"].firstChild.nodeValue] = props["CODE"].firstChild.nodeValue;
		}
	}
	private function readTabControl (root : XMLNode) : Void {
		var parts : Array = convertIndices (root.childNodes);
		//KEYBOARD DEFAULT
		var keys : Array = parts["KEYS"].childNodes;		
		var props : Array;
		for (var x : String in keys) {			
			props = convertIndices (keys[x].childNodes);
			m_aKeyboard[props["ACTION"].firstChild.nodeValue] = props["CODE"].firstChild.nodeValue;
		}
		var mouse: Array = parts["MOUSE_SPEED"].childNodes;		
		var props : Array;
		for (var x : String in mouse) {			
			props = convertIndices (mouse[x].childNodes);
			m_aKeyboard[props["ACTION"].firstChild.nodeValue] = props["CODE"].firstChild.nodeValue;
		}
		var scope : Array = parts["SCOPE_SPEED"].childNodes;		
		var propsscope : Array;
		for (var x : String in scope) {			
			propsscope = convertIndices (scope[x].childNodes);
			m_aKeyboard[propsscope["ACTION"].firstChild.nodeValue] = propsscope["CODE"].firstChild.nodeValue;
		}
	}
	public function getDefaultKey():Array {
		return m_aKeyboard;
	}
	public function getDefaultSound():Array {
		return m_aSound;
	}
	
	private function readTabSoundE(root : XMLNode) {
		var parts : Array = convertEncryptedIndices (root.childNodes);
		//sound default
		var sound : Array = parts["SOUNDS"].childNodes;
		var props : Array;
		var action, code : Number;
		for (var x : String in sound) {			
			props = convertEncryptedIndices (sound[x].childNodes);
			action = Number (m_kDecryptor.decrypt(props["ACTION"].firstChild.nodeValue));
			code = Number (m_kDecryptor.decrypt(props["CODE"].firstChild.nodeValue));
			m_aSound[action] = code;			
		}
	}
	private function readTabControlE (root : XMLNode) : Void {
		var parts : Array = convertEncryptedIndices (root.childNodes);
		
		//DEFAULT KEYBOARD SETTING
		var keys : Array = parts["KEYS"].childNodes;		
		var props : Array;
		var action, code : Number;
		for (var x : String in keys) {			
			props = convertEncryptedIndices (keys[x].childNodes);
			action = Number (m_kDecryptor.decrypt(props["ACTION"].firstChild.nodeValue));
			code = Number (m_kDecryptor.decrypt(props["CODE"].firstChild.nodeValue));
			m_aKeyboard[action] = code;
		}
		//DEFAULT MOUSE SPEED SETTING
		var mouse: Array = parts["MOUSE_SPEED"].childNodes;		
		for (var x : String in mouse) {			
			props = convertEncryptedIndices (mouse[x].childNodes);
			action = Number (m_kDecryptor.decrypt(props["ACTION"].firstChild.nodeValue));
			code = Number (m_kDecryptor.decrypt(props["CODE"].firstChild.nodeValue));
			m_aKeyboard[action] = code;
		}
		//DEFAULT SCOPE SPEED SETTING
		var scope : Array = parts["SCOPE_SPEED"].childNodes;				
		for (var x : String in scope) {
			props = convertEncryptedIndices (scope[x].childNodes);
			action = Number (m_kDecryptor.decrypt(props["ACTION"].firstChild.nodeValue));
			code = Number (m_kDecryptor.decrypt(props["CODE"].firstChild.nodeValue));
			m_aKeyboard[action] = code;			
		}
	}
}