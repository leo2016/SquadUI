﻿import gfx.controls.SQUAD.NewStyle.XML.BaseReader;
import gfx.controls.SQUAD.NewStyle.Data.Map;
import gfx.controls.SQUAD.NewStyle.Data.Mode;
import gfx.controls.SQUAD.NewStyle.Data.Type;
import gfx.controls.SQUAD.NewStyle.XML.Decryptor;
import gfx.events.EventDispatcher;

class gfx.controls.SQUAD.NewStyle.XML.ModesReader extends BaseReader {
	private var m_aModes: Array;	
	
	/**
	 * Hàm tạo và mở file ngay lập tức. 
	 * Ngay khi file được mở xong, bắt sự kiện "open" và đọc ngay. 
	 * 
	 * @param	sFileName đường dẫn file XML
	 */
	public function ModesReader (sFileName: String, encrypted : Boolean) {
		super();
		
		m_bEncrypted = encrypted;
		m_aModes = [];				
		addEventListener ("open", readModes);
		loadFile (sFileName);
	}
	
	/**
	 * Bắt đầu đọc dữ liệu từ file XML. 
	 * Đọc từng bản ghi một bằng hàm readCommonItem. 
	 * Khi đọc xong thì sinh sự kiện "complete".
	 * 
	 * @param	evt eventObject bắt được từ sự kiện "open"
	 */
	private function readModes (evt:Object):Void {
		if (m_bEncrypted) {
			m_kDecryptor = new Decryptor (m_kRoot);
			var item: XMLNode = m_kDecryptor.getData().firstChild;
			var tempMode : Mode;
			var i:Number = 1;
			if (item) {
				do {
					tempMode = readModeE (item);					
					m_aModes[i++] = tempMode;
					item = item.nextSibling;			
				} while (item != undefined) ;
			}
			if (!(m_aModes[0] instanceof Mode)) {
				m_aModes.shift ();
			}
		} else {
			var item: XMLNode = m_kRoot.firstChild;		
			var tempMode : Mode;
			var i:Number = 1;
			if (item) {
				do {
					tempMode = readMode (item);
					m_aModes[i++] = tempMode;
					item = item.nextSibling;
				} while (item != undefined) ;
			}
			if (!(m_aModes[0] instanceof Mode)) {
				m_aModes.shift ();
			}
		}
		/*
		for (var x:String in m_aModes) {
			trace (x + " = " + m_aModes[x]);
		}
		*/
		dispatchEvent ( { type: "complete", target: this, result: m_aModes } ) ;
	}		
	
	/**
	 * Đọc bản ghi. 
	 * Một bản ghi đọc từ XML vào là 1 instance của class BaseItem
	 * 
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	private function readMode (itemRoot: XMLNode):Mode {		
		var properties:Array = convertIndices (itemRoot.childNodes);
		return new Mode (
			properties["ID"].firstChild.nodeValue, 
			properties["MODE_NAME"].firstChild.nodeValue, 
			readMaps (properties["MAPS_LIST"].firstChild)
		);		
	}
	
	private function readMaps (mapsRoot : XMLNode) : Array {
		var item: XMLNode = mapsRoot;		
		var temp : Array = [];
		var tempMap: Map;
		do {
			temp.push(readMap(item));			
			item = item.nextSibling;
		} while (item != undefined) ;
		return temp;
	}
	private function readMap (mapRoot: XMLNode):Map {
		var properties:Array = convertIndices (mapRoot.childNodes);		
		//read map
		return new Map(
			properties["MAP_ID"].firstChild.nodeValue, 
			properties["MAP_NAME"].firstChild.nodeValue, 
			properties["IMAGE"].firstChild.nodeValue,
			properties["MIN_PLAYER"].firstChild.nodeValue, 
			properties["MAX_PLAYER"].firstChild.nodeValue, 
			properties["HOP_PLAYER"].firstChild.nodeValue,
			properties["MIN_TARGET"].firstChild.nodeValue, 
			properties["MAX_TARGET"].firstChild.nodeValue, 
			properties["HOP_TARGET"].firstChild.nodeValue,
			properties["TARGET_TYPE"].firstChild.nodeValue,
			properties["MIN_TIME"].firstChild.nodeValue, 
			properties["MAX_TIME"].firstChild.nodeValue, 
			properties["HOP_TIME"].firstChild.nodeValue,
			properties["WEAPON"].firstChild.nodeValue, 			
			Number(properties["X2D0"].firstChild.nodeValue), 
			Number(properties["Y2D0"].firstChild.nodeValue), 
			Number(properties["X3D1"].firstChild.nodeValue), 
			Number(properties["Y3D1"].firstChild.nodeValue), 
			Number(properties["X2D1"].firstChild.nodeValue), 
			Number(properties["Y2D1"].firstChild.nodeValue), 
			Number(properties["BLOCK_ID"].firstChild.nodeValue)
		);		
	}
	
	/**
	 * Lấy danh sách các bản ghi đọc được từ XML. (Chỉ sử dụng hàm này sau khi có sự kiện "complete"). 	 
	 * 
	 * @return Danh sách các bản ghi
	 */
	public function getModes ():Array {
		return m_aModes;
	}
	
	/**
	 * Đọc bản ghi (Mã hóa) 
	 * Một bản ghi đọc từ XML vào là 1 instance của class BaseItem
	 * 
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	private function readModeE (itemRoot: XMLNode):Mode {
		var properties:Array = convertEncryptedIndices (itemRoot.childNodes);				
		return new Mode (
			Number(m_kDecryptor.decrypt(properties["ID"].firstChild.nodeValue)), 
			m_kDecryptor.decrypt(properties["MODE_NAME"].firstChild.nodeValue), 
			readMapsE (properties["MAPS_LIST"].firstChild)
		);		
	}
	
	private function readMapsE (mapsRoot : XMLNode) : Array {
		var item: XMLNode = mapsRoot;		
		var temp : Array = [];
		var tempMap: Map;
		do {
			temp.push(readMapE(item));			
			item = item.nextSibling;
		} while (item != undefined) ;
		return temp;
	}
	
	private function readMapE (mapRoot: XMLNode):Map {
		var properties:Array = convertEncryptedIndices (mapRoot.childNodes);		
		//read map		
		return new Map(
			Number(m_kDecryptor.decrypt(properties["MAP_ID"].firstChild.nodeValue)), 
			m_kDecryptor.decrypt(properties["MAP_NAME"].firstChild.nodeValue), 
			m_kDecryptor.decrypt(properties["IMAGE"].firstChild.nodeValue),
			Number(m_kDecryptor.decrypt(properties["MIN_PLAYER"].firstChild.nodeValue)), 
			Number(m_kDecryptor.decrypt(properties["MAX_PLAYER"].firstChild.nodeValue)), 
			Number(m_kDecryptor.decrypt(properties["HOP_PLAYER"].firstChild.nodeValue)), 
			Number(m_kDecryptor.decrypt(properties["MIN_TARGET"].firstChild.nodeValue)), 
			Number(m_kDecryptor.decrypt(properties["MAX_TARGET"].firstChild.nodeValue)), 
			Number(m_kDecryptor.decrypt(properties["HOP_TARGET"].firstChild.nodeValue)), 
			Number(m_kDecryptor.decrypt(properties["TARGET_TYPE"].firstChild.nodeValue)), 
			Number(m_kDecryptor.decrypt(properties["MIN_TIME"].firstChild.nodeValue)), 
			Number(m_kDecryptor.decrypt(properties["MAX_TIME"].firstChild.nodeValue)), 
			Number(m_kDecryptor.decrypt(properties["HOP_TIME"].firstChild.nodeValue)), 
			Number(m_kDecryptor.decrypt(properties["WEAPON"].firstChild.nodeValue)), 	
			Number(m_kDecryptor.decrypt(properties["X2D0"].firstChild.nodeValue)), 
			Number(m_kDecryptor.decrypt(properties["Y2D0"].firstChild.nodeValue)), 
			Number(m_kDecryptor.decrypt(properties["X3D1"].firstChild.nodeValue)), 
			Number(m_kDecryptor.decrypt(properties["Y3D1"].firstChild.nodeValue)), 
			Number(m_kDecryptor.decrypt(properties["X2D1"].firstChild.nodeValue)), 
			Number(m_kDecryptor.decrypt(properties["Y2D1"].firstChild.nodeValue)), 
			Number(m_kDecryptor.decrypt(properties["BLOCK_ID"].firstChild.nodeValue))
		);		
	}
}