﻿import gfx.controls.SQUAD.NewStyle.Data.Level;
import gfx.controls.SQUAD.NewStyle.XML.BaseReader;
import gfx.controls.SQUAD.NewStyle.XML.Decryptor;

class gfx.controls.SQUAD.NewStyle.XML.LevelsReader extends BaseReader
{
	private var m_aLevels:Array;
	
	/**
	 * Doc toan bo du lieu trong file XML
	 * @param	sFileName: Ten file XML
	 */
	public function LevelsReader(sFileName:String, encrypted : Boolean){
		super();
		
		m_bEncrypted = encrypted;
		m_aLevels = [];
		addEventListener("open", readLevels);
		loadFile(sFileName);
	}
	
	/**
	 * Bắt đầu đọc dữ liệu từ file XML. 
	 * Đọc từng bản ghi một bằng hàm readCommonItem. 
	 * Khi đọc xong thì sinh sự kiện "complete".
	 * 
	 * @param	evt eventObject bắt được từ sự kiện "open"
	 */
	public function readLevels(evt:Object):Void {
		if (m_bEncrypted) {
			m_kDecryptor = new Decryptor (m_kRoot);
			var item: XMLNode = m_kDecryptor.getData().firstChild;
			if (item) {
				do {
					readLevelE (item);
					item = item.nextSibling;
				}while (item != undefined)		
			}
		} else {
			var item:XMLNode = m_kRoot.firstChild;
			if (item) {
				do {
					readLevel(item);
					item = item.nextSibling;
				}while (item != undefined)		
			}
		}		
		dispatchEvent( { type:"complete", target:this, result:m_aLevels } );
	}

	/**
	 * Đọc bản ghi. 
	 * Một bản ghi đọc từ XML vào là 1 instance của class BaseItem
	 * 
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	public function readLevel(itemRoot:XMLNode):Void {
		var properties:Array = convertIndices(itemRoot.childNodes);
		m_aLevels[properties["ID"].firstChild.nodeValue] = new Level(
			properties["ID"].firstChild.nodeValue, 
			properties["NAME"].firstChild.nodeValue, 
			properties["IMAGE"].firstChild.nodeValue, 
			properties["FROM_EXP"].firstChild.nodeValue,
			properties["TO_EXP"].firstChild.nodeValue,	
			properties["LEVEL"].firstChild.nodeValue,
			properties["NAME_LEVEL"].firstChild.nodeValue
		);
	}
		/**
	 * Lấy danh sách các bản ghi đọc được từ XML. (Chỉ sử dụng hàm này sau khi có sự kiện "complete"). 	 
	 * 
	 * @return Danh sách các bản ghi
	 */
	public function getLevels ():Array {
		return m_aLevels;
	}
	
	/**
	 * Đọc bản ghi (Mã hóa)
	 * Một bản ghi đọc từ XML vào là 1 instance của class BaseItem
	 * 
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	public function readLevelE(itemRoot:XMLNode):Void {
		var properties:Array = convertEncryptedIndices(itemRoot.childNodes);
		var id : Number = Number(m_kDecryptor.decrypt(properties["ID"].firstChild.nodeValue));
		m_aLevels[id] = new Level(
			id,
			m_kDecryptor.decrypt(properties["NAME"].firstChild.nodeValue), 
			m_kDecryptor.decrypt(properties["IMAGE"].firstChild.nodeValue), 
			Number(m_kDecryptor.decrypt(properties["FROM_EXP"].firstChild.nodeValue)),
			Number(m_kDecryptor.decrypt(properties["TO_EXP"].firstChild.nodeValue)),
			Number(m_kDecryptor.decrypt(properties["LEVEL"].firstChild.nodeValue)),
			m_kDecryptor.decrypt(properties["NAME_LEVEL"].firstChild.nodeValue)
		);
	}
}