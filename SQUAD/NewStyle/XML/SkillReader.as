﻿import gfx.controls.SQUAD.NewStyle.XML.BaseReader;
import gfx.controls.SQUAD.NewStyle.Data.Skill;
import gfx.controls.SQUAD.NewStyle.Data.Property;
import gfx.controls.SQUAD.NewStyle.XML.Decryptor;
import gfx.events.EventDispatcher;

class gfx.controls.SQUAD.NewStyle.XML.SkillReader extends BaseReader {
	private var m_aSkills: Array;	
	
	/**
	 * Hàm tạo và mở file ngay lập tức. 
	 * Ngay khi file được mở xong, bắt sự kiện "open" và đọc ngay. 
	 * 
	 * @param	sFileName đường dẫn file XML
	 */
	public function SkillReader (sFileName: String, encrypted : Boolean) {
		super();
		m_bEncrypted = encrypted;
		m_aSkills = [];				
		addEventListener ("open", readSkills);
		loadFile (sFileName);
	}
	
	/**
	 * Bắt đầu đọc dữ liệu từ file XML. 
	 * Đọc từng bản ghi một bằng hàm readCommonItem. 
	 * Khi đọc xong thì sinh sự kiện "complete".
	 * 
	 * @param	evt eventObject bắt được từ sự kiện "open"
	 */
	private function readSkills (evt:Object):Void {
		if (m_bEncrypted) {
			m_kDecryptor = new Decryptor (m_kRoot);
			var item: XMLNode = m_kDecryptor.getData().firstChild;
			var tempSkill :Skill;
			var i:Number = 1;
			if (item) {
				do {
					//tempSkill =			
					m_aSkills[i++] =  readSkillE (item);		
					item = item.nextSibling;			
				} while (item != undefined) ;
			}
/*			if (!(m_aSkills[0] instanceof Skill)) {
				m_aSkills.shift ();
			}*/
		} else {
			var item: XMLNode = m_kRoot.firstChild;		
			var tempSkill : Skill;
			var i:Number = 1;
			if (item) {
				do {
					tempSkill = readSkill (item);
					m_aSkills[i++] = readSkill (item);
					item = item.nextSibling;
				} while (item != undefined) ;
			}
		}
		dispatchEvent ( { type: "complete", target: this, result: m_aSkills } ) ;
	}		
	
	/**
	 * Đọc bản ghi. 
	 * Một bản ghi đọc từ XML vào là 1 instance của class BaseItem
	 * 
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	private function readSkill (itemRoot: XMLNode):Skill {
		var properties:Array = convertIndices (itemRoot.childNodes);
		return new Skill (
			properties["SKILL_ID"].firstChild.nodeValue, 
			properties["CLASS_ID"].firstChild.nodeValue,
			properties["NAME"].firstChild.nodeValue,
			properties["DESCRITION"].firstChild.nodeValue,
			properties["IS_ACTIVE"].firstChild.nodeValue,
			readPropertys (properties["PROPERTIES"].firstChild)
		);
	}
	
	private function readPropertys (PropertysRoot : XMLNode) : Array {
		var item: XMLNode = PropertysRoot;		
		var temp : Array = [];
		var tempProperty: Property;
		do {
			temp.push(reaProperty(item));			
			item = item.nextSibling;
		} while (item != undefined) ;
		return temp;
	}
	private function reaProperty (PropertRoot: XMLNode):Property {
		var properties:Array = convertIndices (PropertRoot.childNodes);		
		//read Property
		return new Property(
			properties["PROPERTY_ID"].firstChild.nodeValue, 
			properties["NAME"].firstChild.nodeValue, 
			properties["VALUE"].firstChild.nodeValue,
			properties["LEVEL"].firstChild.nodeValue
		);	
	}
	
	/**
	 * Lấy danh sách các bản ghi đọc được từ XML. (Chỉ sử dụng hàm này sau khi có sự kiện "complete"). 	 
	 * 
	 * @return Danh sách các bản ghi
	 */
	public function getSkills ():Array {
		return m_aSkills;
	}
	
	/**
	 * Đọc bản ghi (Mã hóa) 
	 * Một bản ghi đọc từ XML vào là 1 instance của class BaseItem
	 * 
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	private function readSkillE (itemRoot: XMLNode):Skill {
		var properties:Array = convertEncryptedIndices (itemRoot.childNodes);				
		return new Skill (
			Number(m_kDecryptor.decrypt(properties["SKILL_ID"].firstChild.nodeValue)), 
			Number(m_kDecryptor.decrypt(properties["CLASS_ID"].firstChild.nodeValue)),
			m_kDecryptor.decrypt(properties["NAME"].firstChild.nodeValue),
			m_kDecryptor.decrypt(properties["DESCRITION"].firstChild.nodeValue),
			Number(m_kDecryptor.decrypt(properties["IS_ACTIVE"].firstChild.nodeValue)),
			readPropertysE (properties["PROPERTIES"].firstChild)
		);		
	}
	
	private function readPropertysE (PropertysRoot : XMLNode) : Array {
		var item: XMLNode = PropertysRoot;		
		var temp : Array = [];
		var tempProperty: Property;
		do {
			temp.push(readPropertyE(item));			
			item = item.nextSibling;
		} while (item != undefined) ;
		return temp;
	}
	
	private function readPropertyE (PropertyRoot: XMLNode):Property {
		var properties:Array = convertEncryptedIndices (PropertyRoot.childNodes);		
		//read Property		
		return new Property(
			Number(m_kDecryptor.decrypt(properties["PROPERTY_ID"].firstChild.nodeValue)), 
			m_kDecryptor.decrypt(properties["NAME"].firstChild.nodeValue), 
			Number(m_kDecryptor.decrypt(properties["VALUE"].firstChild.nodeValue)),
			Number(m_kDecryptor.decrypt(properties["LEVEL"].firstChild.nodeValue))
		);		
	}
}