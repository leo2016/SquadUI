﻿import gfx.controls.SQUAD.NewStyle.Data.ClanAvatar;
import gfx.controls.SQUAD.NewStyle.XML.BaseReader;
import gfx.controls.SQUAD.NewStyle.XML.Decryptor;

class gfx.controls.SQUAD.NewStyle.XML.ClanAvatarReader extends BaseReader
{
	private var m_aAvatars:Array;
	
	/**
	 * Doc toan bo du lieu trong file XML
	 * @param	sFileName: Ten file XML
	 */
	public function ClanAvatarReader(sFileName:String, encrypted : Boolean) {
		//trace(sFileName);
		super();
		m_bEncrypted = encrypted;
		m_aAvatars = [];
		addEventListener("open", readAvatars);
		loadFile(sFileName);
	}
	
	/**
	 * Bắt đầu đọc dữ liệu từ file XML. 
	 * Đọc từng bản ghi một bằng hàm readCommonItem. 
	 * Khi đọc xong thì sinh sự kiện "complete".
	 * 
	 * @param	evt eventObject bắt được từ sự kiện "open"
	 */
	public function readAvatars(evt:Object):Void {
		if (m_bEncrypted) {
			m_kDecryptor = new Decryptor (m_kRoot);
			var item: XMLNode = m_kDecryptor.getData().firstChild;
			if (item) {
				do {
					readAvatarE (item);
					item = item.nextSibling;
				}while (item != undefined)		
			}
		} else {
			var item:XMLNode = m_kRoot.firstChild;
			if (item) {
				do {
					readAvatar(item);
					item = item.nextSibling;
				}while (item != undefined)		
			}
		}		
		dispatchEvent( { type:"complete", target:this, result:m_aAvatars } );
	}

	/**
	 * Đọc bản ghi. 
	 * Một bản ghi đọc từ XML vào là 1 instance của class BaseItem
	 * 
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	public function readAvatar(itemRoot:XMLNode):Void {
		var properties:Array = convertIndices(itemRoot.childNodes);
		m_aAvatars[properties["ID"].firstChild.nodeValue] = new ClanAvatar(
			properties["ID"].firstChild.nodeValue, 
			properties["NAME"].firstChild.nodeValue, 
			properties["IMAGE"].firstChild.nodeValue
		);
	}
		/**
	 * Lấy danh sách các bản ghi đọc được từ XML. (Chỉ sử dụng hàm này sau khi có sự kiện "complete"). 	 
	 * 
	 * @return Danh sách các bản ghi
	 */
	public function getAvatars ():Array {
		return m_aAvatars;
	}
	
	/**
	 * Đọc bản ghi (Mã hóa)
	 * Một bản ghi đọc từ XML vào là 1 instance của class BaseItem
	 * 
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	public function readAvatarE(itemRoot:XMLNode):Void {
		var properties:Array = convertEncryptedIndices(itemRoot.childNodes);
		var id : Number = Number(m_kDecryptor.decrypt(properties["ID"].firstChild.nodeValue));
		m_aAvatars[id] = new ClanAvatar(
			id,
			m_kDecryptor.decrypt(properties["NAME"].firstChild.nodeValue), 
			m_kDecryptor.decrypt(properties["IMAGE"].firstChild.nodeValue)
		);
	}
	
}