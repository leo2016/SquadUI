
import gfx.controls.SQUAD.NewStyle.Data.ItemId;
import gfx.controls.SQUAD.NewStyle.Data.SpecialItem;
import gfx.controls.SQUAD.NewStyle.XML.BaseReader;
import gfx.controls.SQUAD.NewStyle.XML.Decryptor;
/**
 * ...
 * @author ...
 */

class  gfx.controls.SQUAD.NewStyle.XML.SpecialItemReader extends BaseReader
{
	
	private var m_aSpecialItem: Array;	
	
	/**
	 * Hàm tạo và mở file ngay lập tức. 
	 * Ngay khi file được mở xong, bắt sự kiện "open" và đọc ngay. 
	 * 
	 * @param	sFileName đường dẫn file XML
	 */
	public function SpecialItemReader (sFileName: String, encrypted : Boolean) {
		super();
		m_bEncrypted = encrypted;
		m_aSpecialItem = [];				
		addEventListener ("open", readSpecialItems);
		loadFile (sFileName);
	}
	
	/**
	 * Bắt đầu đọc dữ liệu từ file XML. 
	 * Đọc từng bản ghi một bằng hàm readCommonItem. 
	 * Khi đọc xong thì sinh sự kiện "complete".
	 * 
	 * @param	evt eventObject bắt được từ sự kiện "open"
	 */
	private function readSpecialItems (evt:Object):Void {
		if (m_bEncrypted) {
			m_kDecryptor = new Decryptor(m_kRoot);
			var item: XMLNode = m_kDecryptor.getData().firstChild;
			var tempSkill :SpecialItem;
			var i:Number = 1;
			if (item) {
				do {		
					m_aSpecialItem[i++] =  readSpecialItemE(item);		
					item = item.nextSibling;			
				} while (item != undefined) ;
			}
		} else {
			var item: XMLNode = m_kRoot.firstChild;		
			var tempSpecialItem : SpecialItem;
			var i:Number = 1;
			if (item) {
				do {
					tempSpecialItem = readSpecialItem (item);
					m_aSpecialItem[i++] = readSpecialItem (item);
					item = item.nextSibling;
				} while (item != undefined) ;
			}
		}
		dispatchEvent ( { type: "complete", target: this, result: m_aSpecialItem } ) ;
	}		
	
	/**
	 * Đọc bản ghi. 
	 * Một bản ghi đọc từ XML vào là 1 instance của class
	 * 
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	private function readSpecialItem (itemRoot: XMLNode):SpecialItem {
		var properties:Array = convertIndices (itemRoot.childNodes);
		//trace("voiceID=" + properties["VOICE_ID"].firstChild.nodeValue);
		return new SpecialItem (
			properties["CHARACTER_ID"].firstChild.nodeValue,
			readItemIds(properties["ITEMS"].firstChild),
			properties["IS_DEFAULT"].firstChild.nodeValue,
			properties["WEIGHT"].firstChild.nodeValue,
			properties["HEALTH"].firstChild.nodeValue,
			properties["PHYSICAL"].firstChild.nodeValue,
			properties["MODEL_NAME"].firstChild.nodeValue,
			properties["VOICE_ID"].firstChild.nodeValue
		);
	}
	
	private function readItemIds (PropertysRoot : XMLNode) : Array {
		var item: XMLNode = PropertysRoot;		
		var temp : Array = [];
		var tempItemId: ItemId;
		do {
			temp.push(readItemId(item));			
			item = item.nextSibling;
		} while (item != undefined) ;
		return temp;
	}
	private function readItemId (itemRoot: XMLNode):ItemId {
		var properties:Array = convertIndices (itemRoot.childNodes);		
		//read ItemId
		return new ItemId(properties["ITEM_ID"].firstChild.nodeValue,
				properties["IMAGE_2D"].firstChild.nodeValue,
				properties["IS_SPECIAL"].firstChild.nodeValue
		);	
	}
	
	/**
	 * Lấy danh sách các bản ghi đọc được từ XML. (Chỉ sử dụng hàm này sau khi có sự kiện "complete"). 	 
	 * 
	 * @return Danh sách các bản ghi
	 */
	public function getSpecialItem ():Array {
		return m_aSpecialItem;
	}
	
	/**
	 * Đọc bản ghi (Mã hóa) 
	 * Một bản ghi đọc từ XML vào là 1 instance của class 
	 * 
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	private function readSpecialItemE (itemRoot: XMLNode):SpecialItem {
		var properties:Array = convertEncryptedIndices (itemRoot.childNodes);				
		return new SpecialItem (
		/*	properties["CHARACTER_ID"].firstChild.nodeValue,
			readItemIds(properties["ITEMS"].firstChild),
			properties["IS_DEFAULT"].firstChild.nodeValue,
			properties["WEIGHT"].firstChild.nodeValue,
			properties["HEALTH"].firstChild.nodeValue,
			properties["PHYSICAL"].firstChild.nodeValue,
			properties["MODEL_NAME"].firstChild.nodeValue,
			properties["VOICE_ID"].firstChild.nodeValue*/
			Number(m_kDecryptor.decrypt(properties["CHARACTER_ID"].firstChild.nodeValue)),
			readItemIdsE(properties["ITEMS"].firstChild),
			Number(m_kDecryptor.decrypt(properties["IS_DEFAULT"].firstChild.nodeValue)),
			Number(m_kDecryptor.decrypt(properties["WEIGHT"].firstChild.nodeValue)),
			Number(m_kDecryptor.decrypt(properties["HEALTH"].firstChild.nodeValue)),
			Number(m_kDecryptor.decrypt(properties["PHYSICAL"].firstChild.nodeValue)),
			m_kDecryptor.decrypt(properties["MODEL_NAME"].firstChild.nodeValue),
			Number(m_kDecryptor.decrypt(properties["VOICE_ID"].firstChild.nodeValue))
		);		
	}
	
	private function readItemIdsE (ItemsRoot : XMLNode) : Array {
		var item: XMLNode = ItemsRoot;		
		var temp : Array = [];
		var tempItemId: ItemId;
		do {
			temp.push(readItemIdE(item));			
			item = item.nextSibling;
		} while (item != undefined) ;
		return temp;
	}
	
	private function readItemIdE (ItemRoot: XMLNode):ItemId {
		var properties:Array = convertEncryptedIndices (ItemRoot.childNodes);		
		//read Property		
		return new ItemId(
			Number(m_kDecryptor.decrypt(properties["ITEM_ID"].firstChild.nodeValue)),
			m_kDecryptor.decrypt(properties["IMAGE_2D"].firstChild.nodeValue),
			Boolean(m_kDecryptor.decrypt(properties["IS_SPECIAL"].firstChild.nodeValue))
		);		
	}
}