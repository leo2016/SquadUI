﻿import gfx.controls.SQUAD.NewStyle.XML.BaseReader;
import gfx.controls.SQUAD.NewStyle.XML.Decryptor;

/**
 * Class đọc dữ liệu bổ sung cho quân phục từ XML.
 * Đọc ngay khi khởi tạo instance mới nhưng phải bắt sự kiện "complete" rồi chờ tới khi đọc xong mới nhận được dữ liệu để xử lý.
 */
class gfx.controls.SQUAD.NewStyle.XML.UniformsReader extends BaseReader {
	private var m_aUniforms: Array;
	
	private var EA_TYPE : String;
	
	/**
	 * Hàm tạo và mở file ngay lập tức.
	 * Ngay khi file được mở xong, bắt sự kiện "open" và đọc ngay.
	 *
	 * @param	sFileName đường dẫn file XML
	 */
	public function UniformsReader (sFileName: String, encrypted : Boolean) {
		super();
		
		m_bEncrypted = encrypted;
		m_aUniforms = [];
		addEventListener ("open", readUniforms);
		loadFile (sFileName);
	}
	
	/**
	 * Bắt đầu đọc dữ liệu từ file XML.
	 * Đọc từng bản ghi một bằng hàm readUniform.
	 * Khi đọc xong thì sinh sự kiện "complete".
	 *
	 * @param	evt eventObject bắt được từ sự kiện "open"
	 */
	private function readUniforms (evt: Object):Void {
		if (m_bEncrypted) {
			m_kDecryptor = new Decryptor (m_kRoot);
			EA_TYPE = m_kDecryptor.encryptAttribute ("TYPE");
			var item: XMLNode = m_kDecryptor.getData().firstChild;
			if (item) {
				do {
					readUniformE (item);
					item = item.nextSibling;
				} while (item != undefined) ;
			}
		} else {
			var item: XMLNode = m_kRoot.firstChild;
			if (item) {
				do {
					readUniform (item);
					item = item.nextSibling;
				} while (item != undefined) ;
			}
		}
		dispatchEvent ( { type: "complete", target: this, result: m_aUniforms } ) ;
	}
	
	/**
	 * Đọc bản ghi.
	 * 
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	private function readUniform (itemRoot: XMLNode):Void {
		var properties:Array = convertIndices (itemRoot.childNodes);
		var id : Number = properties["UNIFORM_ID"].firstChild.nodeValue;
		properties = convertAttributeIndices(properties["PROPERTIES"].childNodes)
		var data : Array = [];		
		for (var x : String in properties) {			
			data[properties[x].property] = parseFloat(properties[x].value);
		}		
		m_aUniforms[id] = {
			id: 				id, 
			properties:	data};
	}
	
	/**
	 * Lấy danh sách các bản ghi đọc được từ XML. (Chỉ sử dụng hàm này sau khi có sự kiện "complete").
	 * Các bản ghi này chỉ là dữ liệu bổ sung nên phải đợi đọc xong "Common.xml" mới ghép được dữ liệu.
	 *
	 * @return Danh sách các bản ghi
	 */
	public function getData ():Array {
		return m_aUniforms;
	}
	
	/**
	 * Đọc bản ghi (Mã hóa)
	 * 
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	private function readUniformE (itemRoot: XMLNode):Void {
		var properties:Array = convertEncryptedIndices (itemRoot.childNodes);
		var id : Number = Number(m_kDecryptor.decrypt(properties["UNIFORM_ID"].firstChild.nodeValue));		
		properties = convertEncryptedAttributeIndices(properties["PROPERTIES"].childNodes, EA_TYPE);
		var data : Array = [];		
		for (var x : String in properties) {
			data[properties[x].property] = parseFloat(properties[x].value);
		}		
		m_aUniforms[id] = {
			id: 				id, 
			properties:	data
		};		
	}
}