﻿import gfx.controls.SQUAD.NewStyle.XML.BaseReader;
import gfx.controls.SQUAD.NewStyle.XML.Decryptor;

/**
 * Class đọc dữ liệu cho nội dung của phần Splash Screen
 * Đọc ngay khi khởi tạo instance mới nhưng phải bắt sự kiện "complete" rồi chờ tới khi đọc xong mới nhận được dữ liệu để xử lý.
 */
class gfx.controls.SQUAD.NewStyle.XML.SSContentReader extends BaseReader {
	private var m_sTitle : 		String;
	private var m_sVersion :	String;
	private var m_sContent :	String;	
	
	/**
	 * Hàm tạo và mở file ngay lập tức.
	 * Ngay khi file được mở xong, bắt sự kiện "open" và đọc ngay.
	 *
	 * @param	sFileName đường dẫn file XML
	 */
	public function SSContentReader (sFileName: String, encrypted : Boolean) {
		super();
		
		m_bEncrypted = encrypted;
		m_sTitle 		= "";
		m_sVersion 	= "";
		m_sContent	= "";
		addEventListener ("open", readContent);
		loadFile (sFileName);
	}
	
	/**
	 * Bắt đầu đọc dữ liệu từ file XML.	 
	 * Hoàn tất thì sinh sự kiện "complete".
	 *
	 * @param	evt eventObject bắt được từ sự kiện "open"
	 */
	private function readContent (evt: Object):Void {
		if (m_bEncrypted) {
			m_kDecryptor = new Decryptor (m_kRoot);
			var tags:Array = convertEncryptedIndices (m_kDecryptor.getData().childNodes);
			m_sTitle 		= m_kDecryptor.decrypt(tags["TITLE"].firstChild.nodeValue);
			m_sVersion 	= m_kDecryptor.decrypt(tags["VERSION"].firstChild.nodeValue);
			m_sContent 	= m_kDecryptor.decrypt(tags["CONTENT"].firstChild.nodeValue);		
		} else {
			var tags:Array = convertIndices (m_kRoot.childNodes);
			m_sTitle 		= tags["TITLE"].firstChild.nodeValue;
			m_sVersion 	= tags["VERSION"].firstChild.nodeValue;
			m_sContent 	= tags["CONTENT"].firstChild.nodeValue;		
		}
		
		dispatchEvent ( { 
			type: "complete", 
			target: this, 
			result : {
				title : m_sTitle, 
				version : m_sVersion, 
				content : m_sContent
			}
		} ) ;
	}	
}