﻿import gfx.controls.SQUAD.NewStyle.Data.ClanPosition;
import gfx.controls.SQUAD.NewStyle.XML.BaseReader;
import gfx.controls.SQUAD.NewStyle.XML.Decryptor;

class gfx.controls.SQUAD.NewStyle.XML.ClanpositionReader extends BaseReader
{
	private var m_aPositions:Array;
	
	/**
	 * Doc toan bo du lieu trong file XML
	 * @param	sFileName: Ten file XML
	 */
	public function ClanpositionReader(sFileName:String, encrypted : Boolean) {
		//trace(sFileName);
		super();
		m_bEncrypted = encrypted;
		m_aPositions = [];
		addEventListener("open", readPositions);
		loadFile(sFileName);
		trace(sFileName);
	}
	
	/**
	 * Bắt đầu đọc dữ liệu từ file XML. 
	 * Đọc từng bản ghi một bằng hàm readCommonItem. 
	 * Khi đọc xong thì sinh sự kiện "complete".
	 * 
	 * @param	evt eventObject bắt được từ sự kiện "open"
	 */
	public function readPositions(evt:Object):Void {
		if (m_bEncrypted) {
			m_kDecryptor = new Decryptor (m_kRoot);
			var item: XMLNode = m_kDecryptor.getData().firstChild;
			if (item) {
				do {
					readPositionE (item);
					item = item.nextSibling;
				}while (item != undefined)		
			}
		} else {
			var item:XMLNode = m_kRoot.firstChild;
			if (item) {
				do {
					readPosition(item);
					item = item.nextSibling;
				}while (item != undefined)		
			}
		}		
		dispatchEvent( { type:"complete", target:this, result:m_aPositions } );
	}

	/**
	 * Đọc bản ghi. 
	 * Một bản ghi đọc từ XML vào là 1 instance của class BaseItem
	 * 
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	public function readPosition(itemRoot:XMLNode):Void {
		var properties:Array = convertIndices(itemRoot.childNodes);
		m_aPositions[properties["ID"].firstChild.nodeValue] = new ClanPosition(
			properties["ID"].firstChild.nodeValue, 
			properties["NAME"].firstChild.nodeValue, 
			properties["LEVEL"].firstChild.nodeValue,
			properties["TYPE"].firstChild.nodeValue
		);
		//trace("name:" + properties["NAME"].firstChild.nodeValue);
	}
		/**
	 * Lấy danh sách các bản ghi đọc được từ XML. (Chỉ sử dụng hàm này sau khi có sự kiện "complete"). 	 
	 * 
	 * @return Danh sách các bản ghi
	 */
	public function getPositions ():Array {
		return m_aPositions;
	}
	
	/**
	 * Đọc bản ghi (Mã hóa)
	 * Một bản ghi đọc từ XML vào là 1 instance của class BaseItem
	 * 
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	public function readPositionE(itemRoot:XMLNode):Void {
		var properties:Array = convertEncryptedIndices(itemRoot.childNodes);
		var id : Number = Number(m_kDecryptor.decrypt(properties["ID"].firstChild.nodeValue));
		m_aPositions[id] = new ClanPosition(
			id, 
			properties["NAME"].firstChild.nodeValue, 
			properties["LEVEL"].firstChild.nodeValue,
			properties["TYPE"].firstChild.nodeValue
		);
	}
	
}