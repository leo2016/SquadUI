import gfx.controls.SQUAD.NewStyle.Data.Event;
import gfx.controls.SQUAD.NewStyle.XML.BaseReader;
import gfx.controls.SQUAD.NewStyle.XML.Decryptor;

class gfx.controls.SQUAD.NewStyle.XML.EventReader extends BaseReader
{
	private var m_aEvent:Array;
	
	/**
	 * Doc toan bo du lieu trong file XML
	 * @param	sFileName: Ten file XML
	 */
	public function EventReader(sFileName:String, encrypted : Boolean) {
		//trace("sFileName=" + sFileName);
		super();
		m_bEncrypted = encrypted;
		m_aEvent = [];
		addEventListener("open", readEvents);
		loadFile(sFileName);
	}
	
	/**
	 * Bắt đầu đọc dữ liệu từ file XML. 
	 * Đọc từng bản ghi một bằng hàm readCommonItem. 
	 * Khi đọc xong thì sinh sự kiện "complete".
	 * 
	 * @param	evt eventObject bắt được từ sự kiện "open"
	 */
	public function readEvents(evt:Object):Void {
		if (m_bEncrypted) {
			m_kDecryptor = new Decryptor (m_kRoot);
			var item: XMLNode = m_kDecryptor.getData().firstChild;
			if (item) {
				do {
					readEventE (item);
					item = item.nextSibling;
				}while (item != undefined)		
			}
		} else {
			var item:XMLNode = m_kRoot.firstChild;
			if (item) {
				do {
					readEvent(item);
					item = item.nextSibling;
				}while (item != undefined || item != null)		
			}
		}		
		dispatchEvent( { type:"complete", target:this, result:m_aEvent } );
	}

	/**
	 * Đọc bản ghi. 
	 * Một bản ghi đọc từ XML vào là 1 instance của class BaseItem
	 * 
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	public function readEvent(itemRoot:XMLNode):Void {
		var properties:Array = convertIndices(itemRoot.childNodes);
		m_aEvent[properties["ID"].firstChild.nodeValue] = new Event(
			properties["ID"].firstChild.nodeValue,
			properties["NAME"].firstChild.nodeValue, 
			properties["DESCRIPTION"].firstChild.nodeValue, 
			properties["FROMRANK"].firstChild.nodeValue, 
			properties["TORANK"].firstChild.nodeValue,
			properties["BEGINTIME"].firstChild.nodeValue,	
			properties["ENDTIME"].firstChild.nodeValue,
			properties["ICON"].firstChild.nodeValue
		);
	}
		/**
	 * Lấy danh sách các bản ghi đọc được từ XML. (Chỉ sử dụng hàm này sau khi có sự kiện "complete"). 	 
	 * 
	 * @return Danh sách các bản ghi
	 */
	public function getEvent ():Array {
		return m_aEvent;
	}
	
	/**
	 * Đọc bản ghi (Mã hóa)
	 * Một bản ghi đọc từ XML vào là 1 instance của class BaseItem
	 * 
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	public function readEventE(itemRoot:XMLNode):Void {
		var properties:Array = convertEncryptedIndices(itemRoot.childNodes);
		var id : Number = Number(m_kDecryptor.decrypt(properties["ID"].firstChild.nodeValue));
		m_aEvent[id] = new Event(
			id,
			m_kDecryptor.decrypt(properties["NAME"].firstChild.nodeValue), 
			m_kDecryptor.decrypt(properties["DESCRIPTION"].firstChild.nodeValue), 
			Number(m_kDecryptor.decrypt(properties["FROMRANK"].firstChild.nodeValue)),
			Number(m_kDecryptor.decrypt(properties["TORANK"].firstChild.nodeValue)),
			m_kDecryptor.decrypt(properties["BEGINTIME"].firstChild.nodeValue),
			m_kDecryptor.decrypt(properties["ENDTIME"].firstChild.nodeValue),
			m_kDecryptor.decrypt(properties["ICON"].firstChild.nodeValue)
		);
	}
}