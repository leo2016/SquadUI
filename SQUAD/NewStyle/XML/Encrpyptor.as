﻿import gfx.controls.SQUAD.CPPSimulator;
import gfx.controls.SQUAD.NewStyle.XML.BaseReader;
class gfx.controls.SQUAD.NewStyle.XML.Encrpyptor extends BaseReader {
	private static var ELEMENT_NODE : Number	= 1;
	private var DELIMITER : String;
	private var SHIFT : Number;	
	
	public function Encrpyptor (sFileName: String) {
		super();
		
		DELIMITER = "_";
		addEventListener ("open", encrypt);
		loadFile (sFileName);
		CPPSimulator.TRACE_DEBUG_LEVEL = 100;
	}
	
	/**
	 * Bắt đầu đọc dữ liệu từ file XML.
	 * Mã hóa dữ liệu và in ra
	 *
	 * @param	evt eventObject bắt được từ sự kiện "open"
	 */
	private function encrypt (evt: Object):Void {		
		var delimiterTag : String = ""+random (9999)+""+random(9999);
		SHIFT = random (50) + 10;
		trace ("<DATA>");
		trace ("	<T"+delimiterTag+">"+DELIMITER.charCodeAt(0)+"</T"+delimiterTag+">");
		trace ("	<T" + toCharCode("SHIFT") + ">" + SHIFT + "</T" + toCharCode("SHIFT") + ">");		
		encryptTag (m_kRoot, 1);
		trace ("</DATA>");		
		dispatchEvent ( { type: "complete", target: this, result: "" } ) ;
	}
	
	private function encryptTag (tag : XMLNode, level : Number) : Void {
		var tagName : String = tag.nodeName;
		var output : String = "<T" + toCharCode(tagName/*, DELIMITER, SHIFT*/);
		var attributes : Object = tag.attributes;
		for (var x:String in attributes) {
			output += " A" + toCharCode(x) + "=\""+toCharCode(attributes[x])+"\"";
		}		
		output += ">";
		var children : Array = tag.childNodes;
		var n : Number = children.length;
		if (n > 0 && children[0].nodeType == ELEMENT_NODE) {
			trace (CPPSimulator.getTrace (output, level));
			var child : XMLNode;
			for (var i : Number = 0; i < n; ++i ) {
				child = children[i];
				if (child instanceof XMLNode) {
					encryptTag (child, level+1);
				}
			}
			output = CPPSimulator.getTrace ("</T" + toCharCode (tagName/*, DELIMITER, SHIFT*/) + ">", level);
			trace (output);
		} else {
			output += toCharCode (tag.firstChild.nodeValue/*, DELIMITER, SHIFT*/);
			output += "</T" + toCharCode (tagName/*, DELIMITER, SHIFT*/) + ">";			
			trace (CPPSimulator.getTrace (output, level));
		}		
	}
	
	private function toCharCode (input : String/*, delimiter : String, shift : Number*/) : String {
		var output : String = "";
		//output += delimiter + "_" + shift + "_";
		var n : Number = input.length;
		for (var i : Number = 0 ; i < n; ++i) {
			if (i == 0) {
				output = (input.charCodeAt(i) + SHIFT);
			} else {
				output += DELIMITER + (input.charCodeAt(i) + SHIFT);
			}
		}
		return output;
	}

	private function fromCharCode (input : String) : String {		
		var charCodes : Array = input.split (DELIMITER);
		var output : String = "";
		var n : Number = charCodes.length;
		for (var i : Number = 0 ; i<n; ++i) {
			output += String.fromCharCode(charCodes[i] - SHIFT);
		}
		return output;
	}
}