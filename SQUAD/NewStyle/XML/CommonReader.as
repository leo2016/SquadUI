﻿import gfx.controls.SQUAD.NewStyle.XML.BaseReader;
import gfx.controls.SQUAD.NewStyle.Data.BaseItem;
import gfx.controls.SQUAD.NewStyle.XML.Decryptor;

/**
 * Class đọc dữ liệu bổ sung cho BaseItems từ XML.
 * Đọc ngay khi khởi tạo instance mới nhưng phải bắt sự kiện "complete" rồi chờ tới khi đọc xong mới nhận được dữ liệu để xử lý.
 */
class gfx.controls.SQUAD.NewStyle.XML.CommonReader extends BaseReader {
	private var m_aCommonItems: Array;
	
	/**
	 * Hàm tạo và mở file ngay lập tức.
	 * Ngay khi file được mở xong, bắt sự kiện "open" và đọc ngay.
	 *
	 * @param	sFileName đường dẫn file XML
	 */
	public function CommonReader (sFileName: String, encrypted : Boolean) {
		super();
		
		m_bEncrypted = encrypted;
		m_aCommonItems = [];
		addEventListener ("open", readCommonItems);
		loadFile (sFileName);
	}
	
	/**
	 * Bắt đầu đọc dữ liệu từ file XML.
	 * Đọc từng bản ghi một bằng hàm readCommonItem.
	 * Khi đọc xong thì sinh sự kiện "complete".
	 *
	 * @param	evt eventObject bắt được từ sự kiện "open"
	 */
	private function readCommonItems (evt:Object):Void {
		if (m_bEncrypted) {
			m_kDecryptor = new Decryptor (m_kRoot);
			var item: XMLNode = m_kDecryptor.getData().firstChild;
			if (item) {
				do {
					readCommonItemE (item);
					item = item.nextSibling;			
				} while (item != undefined) ;
			}
		} else {
			var item: XMLNode = m_kRoot.firstChild;
			if (item) {
				do {
					readCommonItem (item);
					item = item.nextSibling;			
				} while (item != undefined) ;
			}
		}
		dispatchEvent ( { type: "complete", target: this, result: m_aCommonItems } ) ;
	}
	
	/**
	 * Đọc bản ghi.
	 * Một bản ghi đọc từ XML vào là 1 instance của class BaseItem
	 *
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	private function readCommonItem (itemRoot: XMLNode):Void {
		var properties:Array = convertIndices (itemRoot.childNodes);
		m_aCommonItems[properties["ID"].firstChild.nodeValue] = new BaseItem (
			properties["ID"].firstChild.nodeValue,
			properties["CATALOGUE"].firstChild.nodeValue,
			properties["DETAIL_TABLE"].firstChild.nodeValue,			
			properties['NAME'].firstChild.nodeValue,
			properties["SHORT_DESCRIPTION"].firstChild.nodeValue,
			properties["DESCRIPTION"].firstChild.nodeValue,
			properties["WEIGHT"].firstChild.nodeValue,
			properties["DAY_ADD_ALLOW"].firstChild.nodeValue == true,
			properties["BUY_ALLOW"].firstChild.nodeValue == true,
			properties["SELL_ALLOW"].firstChild.nodeValue == true,
			properties["GIVE_ALLOW"].firstChild.nodeValue == true, 
			properties["REPAIR_ALLOW"].firstChild.nodeValue == true,
			properties["IMAGE"].firstChild.nodeValue, 
			properties["UNLOCK_CONDITION"].firstChild.nodeValue, 
			properties["UNLOCK_REQUIREMENT"].firstChild.nodeValue, 
			properties["CLASS_ID"].firstChild.nodeValue, 
			properties["MODEL"].firstChild.nodeValue,
			properties["PRICE"].firstChild.nodeValue,
			properties["COMING_SOON"].firstChild.nodeValue
		);
	}
	
	/**
	 * Lấy danh sách các bản ghi đọc được từ XML. (Chỉ sử dụng hàm này sau khi có sự kiện "complete").
	 * Các bản ghi này là dữ liệu chung mà tất cả các loại item đều cần phải có. Do đó, khi đọc xong dữ liệu này thì có thể ghép với các dữ liệu bổ sung khác để tạo nên dữ liệu hoàn chỉnh cho 1 item.
	 *
	 * @return Danh sách các bản ghi
	 */
	public function getAllItems ():Array {
		return m_aCommonItems;
	}
	
	/**
	 * Đọc bản ghi (Mã hóa)
	 * Một bản ghi đọc từ XML vào là 1 instance của class BaseItem
	 *
	 * @param	itemRoot Node chứa dữ liệu của 1 bản ghi
	 */
	private function readCommonItemE (itemRoot: XMLNode):Void {
		var properties:Array = convertEncryptedIndices (itemRoot.childNodes);
		var id : Number = Number(m_kDecryptor.decrypt(properties["ID"].firstChild.nodeValue));
		m_aCommonItems[id] = new BaseItem (
			id,
			Number(m_kDecryptor.decrypt(properties["CATALOGUE"].firstChild.nodeValue)),
			Number(m_kDecryptor.decrypt(properties["DETAIL_TABLE"].firstChild.nodeValue)),
			m_kDecryptor.decrypt(properties['NAME'].firstChild.nodeValue),
			m_kDecryptor.decrypt(properties["SHORT_DESCRIPTION"].firstChild.nodeValue),
			m_kDecryptor.decrypt(properties["DESCRIPTION"].firstChild.nodeValue),
			Number(m_kDecryptor.decrypt(properties["WEIGHT"].firstChild.nodeValue)),
			getBool(m_kDecryptor.decrypt(properties["DAY_ADD_ALLOW"].firstChild.nodeValue)),
			getBool(m_kDecryptor.decrypt(properties["BUY_ALLOW"].firstChild.nodeValue)),
			getBool(m_kDecryptor.decrypt(properties["SELL_ALLOW"].firstChild.nodeValue)),
			getBool(m_kDecryptor.decrypt(properties["GIVE_ALLOW"].firstChild.nodeValue)),
			getBool(m_kDecryptor.decrypt(properties["REPAIR_ALLOW"].firstChild.nodeValue)),
			m_kDecryptor.decrypt(properties["IMAGE"].firstChild.nodeValue), 
			Number(m_kDecryptor.decrypt(properties["UNLOCK_CONDITION"].firstChild.nodeValue)), 
			Number(m_kDecryptor.decrypt(properties["UNLOCK_REQUIREMENT"].firstChild.nodeValue)), 
			Number(m_kDecryptor.decrypt(properties["CLASS_ID"].firstChild.nodeValue)), 
			m_kDecryptor.decrypt(properties["MODEL"].firstChild.nodeValue),
			Number(m_kDecryptor.decrypt(properties["PRICE"].firstChild.nodeValue)),
			getBool(m_kDecryptor.decrypt(properties["COMING_SOON"].firstChild.nodeValue))
		);
	}
}