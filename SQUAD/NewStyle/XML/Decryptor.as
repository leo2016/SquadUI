﻿class gfx.controls.SQUAD.NewStyle.XML.Decryptor {	
	private var DELIMITER : String;
	private var SHIFT : Number;	
	
	private var dataNode : XMLNode;
	
	public function Decryptor (root : XMLNode) {
		var delimiterNode : XMLNode = root.childNodes[0];		
		var keyNode : XMLNode = root.childNodes[1];		
		dataNode = root.childNodes[2];
		DELIMITER = String.fromCharCode (Number(delimiterNode.firstChild.nodeValue));
		SHIFT = Number(keyNode.firstChild.nodeValue);
		/*
		trace ("open to decrypt with delimiter = '" + DELIMITER + "' ("+DELIMITER.charCodeAt(0)+") and shift = " + SHIFT
			+ " and data tag is '" + decrypt(dataNode.nodeName.substr(1))+"'");
		*/
	}
	
	private function toCharCode (input : String) : String {
		var output : String = "";		
		var n : Number = input.length;
		for (var i : Number = 0 ; i < n; ++i) {
			if (i == 0) {
				output = (input.charCodeAt(i) + SHIFT);
			} else {
				output += DELIMITER + (input.charCodeAt(i) + SHIFT);
			}
		}
		return output;
	}
	
	private function fromCharCode (input : String) : String {		
		if (typeof(input) != "string") {
			return "";
		}		
		var charCodes : Array = [];
		charCodes = input.split ("_");
		//trace ("split " + input + " by delimiter = '" + DELIMITER + "' -> " + charCodes.length + " parts by using method " + input.split);		
		var output : String = "";
		var n : Number = charCodes.length;
		for (var i : Number = 0 ; i < n; ++i) {			
			output += String.fromCharCode(charCodes[i] - SHIFT);
		}		
		return output;
	}
	
	public function getData () : XMLNode {
		return dataNode;
	}
	
	public function encryptTag (plainText : String) : String {
		return toCharCode (plainText);
	}
	
	public function encryptAttribute (plainText : String) : String {
		return "A" + toCharCode (plainText);		
	}
	
	public function decrypt (cipherText : String) : String {
		return fromCharCode (cipherText);
	}
	
	public function get delimiter () : String {
		return DELIMITER;
	}
	
	public function get shift () : Number {
		return SHIFT;
	}
}