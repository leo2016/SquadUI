import gfx.controls.ListItemRenderer;
import gfx.controls.Label;
import gfx.controls.SQUAD.MixedListItemRenderer;
import gfx.controls.UILoader;
import gfx.events.EventDispatcher;
import gfx.controls.Button;
import gfx.controls.SQUAD.NewStyle.XML.Localizer;
import gfx.controls.SQUAD.ResolutionHelper;
import gfx.controls.SQUAD.ImagePathManager;
import gfx.controls.SQUAD.NewStyle.Mission.MissionOfPlayer;
//import gfx.controls.SQUAD.NewStyle.XML.Localizer;

class gfx.controls.SQUAD.NewStyle.Event.LIREvent extends ListItemRenderer {	
	private var labTime:Label;
	private var UILoaderEvent:UILoader;
	public static var localizer : 		Localizer = undefined;
	
	var EVENT_STATUS_INIT:Number = 0;	// Dung de khoi tao cac bien event status
	var EVENT_STATUS_PROGRESS:Number = 1;
	var EVENT_STATUS_DONE:Number = 2;
	var EVENT_STATUS_GIFT:Number = 3;
	var EVETN_STATUS_EXPIRED:Number = 4;

	
	public function LIREvent() {
		super();
		EventDispatcher.initialize(this);
		addEventListener ("itemDragOver", _parent._parent);
		addEventListener ("itemDragOut", _parent._parent);
	}	
	
	private function configUI () : Void {
		super.configUI();
	}
	public function setData(data:Object):Void {
		if(data != undefined) {
			this.data = data;
			updateAfterStateChange();	
			this.visible = true;
			this._disabled = false;	
			
		}else {
			this.visible = false;
			this._disabled = true;
		}
	}
		
	public function updateAfterStateChange():Void {
		if (data != undefined) {
			if (labTime instanceof Label) {
				labTime.text = "";
			}
			if (UILoaderEvent instanceof UILoader) {
				if (data.status == EVENT_STATUS_DONE) {
					ResolutionHelper.updateUILoader(UILoaderEvent, ImagePathManager.EVENT_HIGHLIGHTED_FORDER +data.image);
				}else {
					ResolutionHelper.updateUILoader(UILoaderEvent, ImagePathManager.EVENT_NORMAL_FORDER +data.image);
				}
			}
		}
	}
	private function handleDragOut () : Void {
		super.handleDragOut();
		dispatchEvent ({type: "itemDragOut"});
	}
	
	private function handleDragOver () : Void {
		super.handleDragOver ();
		dispatchEvent ({type: "itemDragOver", item: data});
	}
}
