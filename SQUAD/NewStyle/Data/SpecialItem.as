/**
 * ...
 * @author ...
 */

class gfx.controls.SQUAD.NewStyle.Data.SpecialItem  
{
	var _characterID:Number;
	var _itemID:Array;
	var _isDefault:Number;
	var _weight:Number;
	var _health:Number;
	var _plysical:Number;
	var _mode_Name:String;
	var _voiceId:Number;
	public function SpecialItem(characterID:Number,ItemID:Array,isDefault:Number,weight:Number,health:Number,plysical:Number,mode_Name:String,voiceId:Number) {
		_characterID = characterID;
		_itemID = ItemID;
		_isDefault = isDefault;
		_weight = weight;
		_health = health;
		_plysical = plysical;
		_mode_Name = mode_Name;
		_voiceId = voiceId;
	}
	public function get itemId () : Array {
		return _itemID;
	}
	
	public function get characterId () : Number {
		return _characterID;
	}
	
	public function get isDefault () : Number {
		return _isDefault;
	}
	public function get weigth () : Number {
		return _weight;
	}
	public function get health () : Number {
		return _health;
	}
	
	public function get plysical () : Number {
		return _plysical;
	}
	public function get modeName () : String {
		return _mode_Name;
	}
	
	public function get voiceId () : Number {
		return _voiceId;
	}
	public function toString () : String {
		var s : String = "";
		s += "characterID = " + _characterID + "\n";
		s += "isDefault = " + _isDefault + "\n";
		s += "weight = " + _weight + "\n";
		s += "health = " + _health + "\n";
		s += "plysical = " + _plysical + "\n";
		s += "modeName = " + _mode_Name + "\n";
		s += "voiceId = " + _voiceId + "\n";
		for (var x : String in _itemID) {
			s += "\t\t" + _itemID[x] + "\n";
		}
		return s;
	}
}