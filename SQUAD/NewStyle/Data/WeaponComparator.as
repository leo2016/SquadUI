﻿import gfx.controls.SQUAD.MathUtility;
import gfx.controls.SQUAD.NewStyle.Data.AddOn;
import gfx.controls.SQUAD.NewStyle.Data.Catalogue;
import gfx.controls.SQUAD.NewStyle.Data.Grenade;
import gfx.controls.SQUAD.NewStyle.Data.Gun;
import gfx.controls.SQUAD.NewStyle.Data.InventoryItem;
import gfx.controls.SQUAD.NewStyle.Data.Knife;
import mx.data.types.Int;

/**
 * Class hỗ trợ việc xác định thông số về súng, dao, lựu đạn
 * 
 * Trong class này có rất nhiều hàm có tác dụng giống nhau:
 * 		power						Sức sát thương của súng, đưa vào súng, danh sách add-on đang có, danh sách toàn bộ vật phẩm (dùng trong inventory)
 * 		powerInInventory		Sức sát thương của súng, đưa vào súng, danh sách Base Add-On ID và danh sách toàn bộ vật phẩm (dùng trong inventory hoặc shop, chủ yếu là inventory)
 * 		powerInShop				Sức sát thương của súng, đưa vào súng, danh sách Base Add-On (dùng trong shop và inventory, chủ yếu là shop)
 * 		powerOfDefaultGun	Sức sát thương của súng, đưa vào súng và danh sách toàn bộ vật phẩm (dùng trong shop, khi súng chưa lắp thêm add-on)
 * 
 * -> Chỉ chú thích cho các hàm dạng 1, 3 dạng còn lại tương tự
 */
class gfx.controls.SQUAD.NewStyle.Data.WeaponComparator {
	public static var MAX_GUN_POWER : 					Number = -1;
	public static var MAX_GUN_ACCURACY : 				Number = -1;
	public static var MAX_GUN_RELOADING_SPEED : 		Number = -1;
	public static var MAX_GUN_SPEED : 					Number = -1;
	public static var MAX_GUN_RECOIL : 					Number = -1;
	public static var MAX_GUN_WEIGHT : 					Number = -1;
	public static var MAX_KNIFE_DAMAGE : 				Number = -1;
	public static var MAX_KNIFE_SPEED : 				Number = -1;
	public static var MAX_KNIFE_LENGTH : 				Number = -1;
	public static var MAX_KNIFE_WEIGHT : 				Number = -1;
	public static var MAX_GRENADE_DAMAGE :				Number = -1;
	public static var MAX_GRENADE_RADIUS : 				Number = -1;
	public static var MAX_GRENADE_WEIGHT: 				Number = -1;
	public static var MAX_ADDON_WEIGHT :				Number = -1;
	public static var MAX_GRENADE_LIFE_TIME    :		Number = -1;
	public static var MAX_GRENADE_DISTANCE     :		Number = -1;     
	/**
	 * Những súng có speed =0, bên core chuyển thành 1000 trong DB, khi gặp phải reset nó về 0
	 */
	public static var SPEED_GUN_DEFAULT:Number				   = 1000;
	/**
	 * Hàm khởi tạo. Phải được gọi trước khi sử dụng!
	 * 
	 * @param	itemList		danh sách tất cả các vật phẩm
	 */
	public static function init (itemList : Array) : Void {
		for (var x : String in itemList) {
			var y = itemList[x];
			if (y instanceof Gun) {			
				if (MAX_GUN_ACCURACY < y.accuracy ) {
					MAX_GUN_ACCURACY = y.accuracy;
				}
				if (MAX_GUN_RELOADING_SPEED < y.reloadTime(0) ) {
					MAX_GUN_RELOADING_SPEED = y.reloadTime(0) ;				
				}
				if (MAX_GUN_SPEED < y.speed ) {
					if (y.speed != SPEED_GUN_DEFAULT){
						MAX_GUN_SPEED = y.speed ;					
					}	
				}
				if (MAX_GUN_RECOIL < y.recoil ) {
					MAX_GUN_RECOIL = y.recoil ;
				}			
				if (MAX_GUN_WEIGHT < y.weight) {
					MAX_GUN_WEIGHT = y.weight;				
				}
			} else if (y instanceof Knife) {			
				if (MAX_KNIFE_SPEED < y.speed) {
					MAX_KNIFE_SPEED = y.speed;
				}
				if (MAX_KNIFE_LENGTH < y.length) {
					MAX_KNIFE_LENGTH = y.length;
				}
				if (MAX_KNIFE_WEIGHT < y.weight) {
					MAX_KNIFE_WEIGHT = y.weight;
				}
			} else if (y instanceof Grenade) {
				if (MAX_GRENADE_RADIUS < y.effectRadius) {
					MAX_GRENADE_RADIUS = y.effectRadius;
				}
				if (MAX_GRENADE_WEIGHT < y.weight) {
					MAX_GRENADE_WEIGHT = y.weight;
				}
				
				if (MAX_GRENADE_LIFE_TIME < y.lifeTime) {
					MAX_GRENADE_LIFE_TIME  = y.lifeTime;
				}
				if (MAX_GRENADE_DISTANCE < y.distance) {
					MAX_GRENADE_DISTANCE  = y.distance;	
				}
			} else if (y instanceof AddOn) {
				if (MAX_ADDON_WEIGHT < y.weight) {
					MAX_ADDON_WEIGHT = y.weight;
				}
			}
		}
		
		++MAX_GUN_ACCURACY;
		++MAX_GUN_RELOADING_SPEED;
		++MAX_GUN_SPEED;
		++MAX_GUN_RECOIL;
		++MAX_GUN_WEIGHT;
		++MAX_GRENADE_DISTANCE
		++MAX_GRENADE_LIFE_TIME;
		MAX_GUN_WEIGHT += (MAX_ADDON_WEIGHT * 9);
		++MAX_KNIFE_SPEED;
		++MAX_KNIFE_LENGTH;		
		++MAX_KNIFE_WEIGHT;
		++MAX_GRENADE_RADIUS;
		++MAX_GRENADE_WEIGHT;
		
		MAX_GUN_POWER = 150;
		MAX_KNIFE_DAMAGE = 150;
		MAX_GRENADE_DAMAGE = 150;
	}
	
	/**
	 * Tìm add-on đang gắn vào súng
	 * 
	 * @param	gun				súng
	 * @param	slot				vị trí lắp add-on (sử dụng InventoryItem.IIN_....)
	 * @param	addOnList		danh sách các add-on mà người chơi sở hữu
	 * 
	 * @return		add-on đang gắn vào vị trí 'slot' của súng 'gun' nếu tìm thấy hoặc undefined nếu ngược lại
	 */
	public static function findAddOnOfII (gun : InventoryItem, slot : Number, addOnList : Array) : InventoryItem {
		//trace ("\t finding add-on in slot " + slot);
		var addOn : InventoryItem;
		var notUse1 : Number = InventoryItem.CLASS_NONE;
		var notUse2 : Number = InventoryItem.NOT_USE;
		for (var x : String in addOnList) {
			addOn = addOnList[x];
			//if (addOn.inClass != notUse1 && addOn.inClass != notUse2) {
				if (addOn.useFor == gun.id && addOn.number == slot) {
					//trace ("\t\t found");
					return addOn;
				}
			//}
		}
		//trace ("\t\t not found");
		return undefined;
	}
	
	/**
	 * Tìm add-on  theo ID
	 * 
	 * @param	itemId      
	 * @param	addOnList		danh sách các add-on mà người chơi sở hữu
	 * 
	 * @return		add-on đang gắn vào vị trí 'slot' của súng 'gun' nếu tìm thấy hoặc undefined nếu ngược lại
	 */
	public static function findAddOnOfItemId (itemId : Number, addOnList : Array) : InventoryItem {
		var addOn : InventoryItem;
		for (var x : String in addOnList) {
			addOn = addOnList[x];
			if (addOn.id == itemId) {
				return addOn;
			}
		}
		return undefined;
	}
	
	/**
	 * Tìm base add-on id đang gắn vào súng
	 * 
	 * @param	gun				súng
	 * @param	slot				vị trí lắp add-on (sử dụng InventoryItem.IIN_....)
	 * @param	addOnList		danh sách các add-on mà người chơi sở hữu
	 * 
	 * @return		base add-on id đang gắn vào vị trí 'slot' của súng 'gun' nếu tìm thấy hoặc undefined nếu ngược lại
	 */
	public static function findBaseAddOnIdOfII (gun : InventoryItem, slot : Number, addOnList : Array) : Number {
		for (var x : String in addOnList) {
			if (addOnList[x].useFor == gun.id && addOnList[x].number == slot) {
				return addOnList[x].baseItemId;
			}
		}
		return undefined;
	}
	
	/**
	 * Sức sát thương của súng trong inventory của người chơi
	 * 
	 * @param	gun				súng
	 * @param	addOnList		danh sách add-on mà người chơi sở hữu
	 * @param	allItems			danh sách tất cả các vật phẩm
	 * 
	 * @return		Sức sát thương của súng
	 */
	public static function power (gun : InventoryItem, addOnList : Array, allItems : Array) : Number {		
		var ammoBaseDamage : Number = 0;
		var powerAffect : Number = 0;
		var properties : Array;
		var property : String;
		var baseAddOn : AddOn;		
		for (var x : String in addOnList) {			
			if (addOnList[x].useFor == gun.id) {				
				baseAddOn = allItems[addOnList[x].baseItemId]; 				
				properties = baseAddOn.properties;				
				for (var y : String in properties) {	
					property = properties[y].property;
					switch (property) {
						case AddOn.AOP_AMMO_BASE_DAMAGE: {							
							ammoBaseDamage = baseAddOn.property(property);
							break;
						}						
						//handle other properties that affect power of the gun here!
						case AddOn.AOP_BARREL_DAMAGE_AFFECT: {
							powerAffect += baseAddOn.property (property);
						}
					}
				}
			}
		}
		return Math.abs(MathUtility.round(allItems[gun.baseItemId].power (ammoBaseDamage) * (100 + powerAffect) / 100, 2));
	}
	
	/**
	 * Tốc độ thay đạn của súng trong inventory của người chơi
	 * 
	 * @param	gun				súng
	 * @param	addOnList		danh sách add-on mà người chơi sở hữu
	 * @param	allItems			danh sách tất cả các vật phẩm
	 * 
	 * @return		Tốc độ thay đạn của súng
	 */
	public static function reloadingSpeed (gun : InventoryItem, addOnList : Array, allItems : Array) : Number {
		var speedAffect : Number = 0;
		var properties : Array;
		var property : String;
		var baseAddOn : AddOn;		
		for (var x : String in addOnList) {
			if (addOnList[x].useFor == gun.id) {
				baseAddOn = allItems[addOnList[x].baseItemId]; 
				properties = baseAddOn.properties;
				for (var y : String in properties) {
					property = properties[y].property;
					switch (property) {
						case AddOn.AOP_CLIP_RELOADING_SPEED_AFFECT: {							
							speedAffect += baseAddOn.property(property);
							break;
						}
						//handle other properties that affect reloading speed of the gun here!
					}
				}
			}
		}
		return Math.abs(MathUtility.round(allItems[gun.baseItemId].reloadTime (speedAffect), 2));
	}
	
	/**
	 * Độ chính xác của súng trong inventory của người chơi
	 * 
	 * @param	gun				súng
	 * @param	addOnList		danh sách add-on mà người chơi sở hữu
	 * @param	allItems			danh sách tất cả các vật phẩm
	 * 
	 * @return		Độ chính xác của súng
	 */
	public static function accuracy (gun : InventoryItem, addOnList : Array, allItems : Array) : Number {				
		var accuracyAffect : Number = 0;
		var properties : Array;
		var property : String;
		var baseAddOn : AddOn;		
		for (var x : String in addOnList) {
			if (addOnList[x].useFor == gun.id) {
				baseAddOn = allItems[addOnList[x].baseItemId]; 
				properties = baseAddOn.properties;
				for (var y : String in properties) {
					property = properties[y].property;
					switch (property) {
						case AddOn.AOP_SCOPE_ACCURACY: {
							accuracyAffect += baseAddOn.property(property);
							break;
						}
						//handle other properties that affect accuracy of the gun here!
					}
				}
			}
		}
		return Math.abs(MathUtility.round(allItems[gun.baseItemId].accuracy * (100.0 + accuracyAffect) / 100.0, 2));
	}
	
	/**
	 * Độ giật của súng trong inventory của người chơi
	 * 
	 * @param	gun				súng
	 * @param	addOnList		danh sách add-on mà người chơi sở hữu
	 * @param	allItems			danh sách tất cả các vật phẩm
	 * 
	 * @return		Độ giật của súng
	 */
	public static function recoil (gun : InventoryItem, addOnList : Array, allItems : Array) : Number {				
		return Math.abs(MathUtility.round(allItems[gun.baseItemId].recoil, 2));
	}
	
	/**
	 * Tốc độ bắn của súng trong inventory của người chơi
	 * 
	 * @param	gun				súng
	 * @param	addOnList		danh sách add-on mà người chơi sở hữu
	 * @param	allItems			danh sách tất cả các vật phẩm
	 * 
	 * @return		Tốc độ bắn của súng
	 */
	public static function speed (gun : InventoryItem, addOnList : Array, allItems : Array) : Number {
		var speedAffect : Number = 0;
		var properties : Array;
		var property : String;
		var baseAddOn : AddOn;
		for (var x : String in addOnList) {
			if (addOnList[x].useFor == gun.id) {
				
				baseAddOn = allItems[addOnList[x].baseItemId]; 
				properties = baseAddOn.properties;
				for (var y : String in properties) {
					property = properties[y].property;
					switch (property) {
						case AddOn.AOP_BARREL_RATE_OF_FIRE: {
							speedAffect += baseAddOn.property(property);
							break;
						}
						case AddOn.AOP_SCOPE_RATE_OF_FIRE : {
							speedAffect += baseAddOn.property(property);
							break;
						}
						//handle other properties that affect speed of the gun here!
					}
				}
			}
		}
		return Math.abs(MathUtility.round(allItems[gun.baseItemId].speed * (100.0 + speedAffect) / 100.0, 2));
	}	
	
	/**
	 * Số lượng đạn của súng trong inventory của người chơi
	 * 
	 * @param	gun				súng
	 * @param	addOnList		danh sách add-on mà người chơi sở hữu
	 * @param	allItems			danh sách tất cả các vật phẩm
	 * 
	 * @return		Số lượng đạn của súng
	 */
	public static function ammos (gun : InventoryItem, addOnList : Array, allItems : Array) : Number {		
		var extraAmmos : Number = 0;
		var properties : Array;
		var property : String;
		var baseAddOn : AddOn;		
		for (var x : String in addOnList) {			
			if (addOnList[x].useFor == gun.id) {				
				baseAddOn = allItems[addOnList[x].baseItemId]; 				
				properties = baseAddOn.properties;				
				for (var y : String in properties) {	
					property = properties[y].property;
					switch (property) {
						case AddOn.AOP_CLIP_EXTRA_AMMOS: {							
							extraAmmos = baseAddOn.property (property);
							break;
						}
					}
				}
			}
		}
		return Math.abs(Math.round(allItems[gun.baseItemId].ammos) + Math.floor(extraAmmos));
	}
	
	/**
	 * Cân nặng của súng trong inventory của người chơi
	 * 
	 * @param	gun				súng
	 * @param	addOnList		danh sách add-on mà người chơi sở hữu
	 * @param	allItems			danh sách tất cả các vật phẩm
	 * 
	 * @return		Cân nặng của súng (tính cả add-on)
	 */
	public static function weight (gun : InventoryItem, addOnList : Array, allItems : Array) : Number {
		var result : Number = gun.weight;
		var properties : Array;
		var property : String;
		var baseAddOn : AddOn;		
		for (var x : String in addOnList) {			
			if (addOnList[x].useFor == gun.id) {				
				result += Number(addOnList[x].weight);
			}
		}		
		return Math.abs(Number (result));
	}
	
	/**
	 * Kiểm tra xem súng có ống phóng lựu không
	 * 
	 * @param	bullet	BaseItemID của đạn đang lắp
	 * @param	clip		BaseItemID của băng đạn đang lắp
	 * @param	scope	BaseItemID của ống ngắm đang lắp
	 * @param	barrel	BaseItemID của nòng đang lắp
	 * @param	left		BaseItemID của thân trái đang lắp
	 * @param	right		BaseItemID của thân phải đang lắp
	 * @param	bottom	BaseItemID của thân dưới đang lắp
	 * @param	color	BaseItemID của màu sắc đang lắp
	 * @param	decal	BaseItemID của đề-can đang lắp
	 * @param	grenadeLauncherCatalogue	CatalogueID của ống phóng lựu
	 * @param	allItems	Danh sách tất cả các vật phẩm
	 * 
	 * @return		true nếu súng có ống phóng lựu, false nếu ngược lại
	 */
	public static function hasGrenadeLauncher (		
		bullet : 									Number, 
		clip : 									Number, 
		scope : 								Number, 
		barrel : 								Number, 
		left : 										Number, 
		right : 									Number, 
		bottom : 								Number, 
		color : 									Number, 
		decal : 									Number, 
		grenadeLauncherCatalogue : 	Number, 
		allItems:								Array) : Boolean {
		if (allItems[left].catalogue == grenadeLauncherCatalogue) {
			return true;
		}
		if (allItems[right].catalogue == grenadeLauncherCatalogue) {
			return true;
		}
		if (allItems[bottom].catalogue == grenadeLauncherCatalogue) {
			return true;
		}		
		return false;
	}
		
	public static function powerInShop (
		gun :			Gun, 
		bullet : 		AddOn, 
		clip : 		AddOn, 
		scope : 	AddOn, 
		barrel : 	AddOn, 
		left : 			AddOn, 
		right : 		AddOn, 
		bottom : 	AddOn, 
		color : 		AddOn, 
		decal : 		AddOn		
	) : Number {
		if (!(gun instanceof Gun) || !(bullet instanceof AddOn)) {
			return 0;
		}
		var result : Number = 0;		
		result = gun.power (bullet.property(AddOn.AOP_AMMO_BASE_DAMAGE));
		if (barrel != undefined) {
			result *= (100 + barrel.property (AddOn.AOP_BARREL_DAMAGE_AFFECT)) / 100;
		}		
		return Math.abs(Number (result));
	}
	
	public static function accuracyInShop (
		gun :			Gun, 
		bullet : 		AddOn, 
		clip : 		AddOn, 
		scope : 	AddOn, 
		barrel : 	AddOn, 
		left : 			AddOn, 
		right : 		AddOn, 
		bottom : 	AddOn, 
		color : 		AddOn, 
		decal : 		AddOn		
	) : Number {
		var result : Number = 0;
		if (gun instanceof Gun) {
			result = gun.accuracy;
			if (scope instanceof AddOn) {
				result *= (100.0 + scope.property(AddOn.AOP_SCOPE_ACCURACY)) / 100.0;
			}
		}
		return Math.abs(Number (result));
	}
	
	public static function speedInShop (
		gun :			Gun, 
		bullet : 		AddOn, 
		clip : 		AddOn, 
		scope : 	AddOn, 
		barrel : 	AddOn, 
		left : 			AddOn, 
		right : 		AddOn, 
		bottom : 	AddOn, 
		color : 		AddOn, 
		decal : 		AddOn		
	) : Number {
		var result : Number = 0;		
		if (gun instanceof Gun) {			
			result = gun.speed;
			var affect : Number = 0;
			if (barrel instanceof AddOn) {
				affect += barrel.property (AddOn.AOP_BARREL_RATE_OF_FIRE);
			}
			if (scope instanceof AddOn) {
				affect += scope.property (AddOn.AOP_SCOPE_RATE_OF_FIRE);
			}
		}
		//trace("speedInShop=" + Math.abs(Number (result * Number(100.0 + affect) / 100.0));
		return Math.abs(Number (result * Number(100.0 + affect) / 100.0));
	}
	
	public static function recoilInShop (
		gun :			Gun, 
		bullet : 		AddOn, 
		clip : 		AddOn, 
		scope : 	AddOn, 
		barrel : 	AddOn, 
		left : 			AddOn, 
		right : 		AddOn, 
		bottom : 	AddOn, 
		color : 		AddOn, 
		decal : 		AddOn		
	) : Number {
		var result : Number = 0;
		if (gun instanceof Gun) {
			result = gun.recoil;			
		}
		return Math.abs(Number (result));
	}
	
	public static function ammosInShop (
		gun :			Gun, 
		bullet : 		AddOn, 
		clip : 		AddOn, 
		scope : 	AddOn, 
		barrel : 	AddOn, 
		left : 			AddOn, 
		right : 		AddOn, 
		bottom : 	AddOn, 
		color : 		AddOn, 
		decal : 		AddOn		
	) : Number {
		var result : Number = 0;
		if (!(gun instanceof Gun)) {
			return 0;
		}
		result = gun.ammos;
		if (clip instanceof AddOn) {
			result += Number(clip.property(AddOn.AOP_CLIP_EXTRA_AMMOS));
		}
		return Math.abs(Number (result));
	}
	
	public static function reloadingSpeedInShop (
		gun :			Gun, 
		bullet : 		AddOn, 
		clip : 		AddOn, 
		scope : 	AddOn, 
		barrel : 	AddOn, 
		left : 			AddOn, 
		right : 		AddOn, 
		bottom : 	AddOn, 
		color : 		AddOn, 
		decal : 		AddOn		
	) : Number {
		var result : Number = 0;
		if (gun instanceof Gun && clip instanceof AddOn) {			
			result = gun.reloadTime (clip.property(AddOn.AOP_CLIP_RELOADING_SPEED_AFFECT));
		}
		return Math.abs(Number (result));
	}
	
	public static function weightInShop (
		gun :			Gun, 
		bullet : 		AddOn, 
		clip : 		AddOn, 
		scope : 	AddOn, 
		barrel : 	AddOn, 
		left : 			AddOn, 
		right : 		AddOn, 
		bottom : 	AddOn, 
		color : 		AddOn, 
		decal : 		AddOn		
	) : Number {
		var result : Number = gun.weight;
		if (scope != undefined) {
			result += Number(scope.weight);
		}
		if (barrel != undefined) {
			result += Number(barrel.weight);
		}
		if (left != undefined) {
			result += Number(left .weight);
		}
		if (right != undefined) {
			result += Number(right.weight);
		}
		if (bottom != undefined) {
			result += Number(bottom.weight);
		}
		if (bullet != undefined) {
			result += Number(bullet.weight);
		}	
		if (clip != undefined) {
			result += Number(clip.weight);
		}	
		return Math.abs(Number (result));
	}
	
	public static function powerOfDefaultGun (gun : Gun, allItems : Array) : Number {
		var addOnId : Number = gun.getDefaultAddOnInSlot (Catalogue.CTL_BULLET);
		if (addOnId != undefined) {
			var addOn: AddOn = allItems [addOnId];
			if (addOn instanceof AddOn) {				
				var result : Number = gun.power (addOn.property (AddOn.AOP_AMMO_BASE_DAMAGE));
				addOnId = gun.getDefaultAddOnInSlot (Catalogue.CTL_BARREL);
				if (addOnId != undefined) {
					addOn = allItems [addOnId];
					if (addOn instanceof AddOn) {
						result *= (100 + Number(addOn.property(AddOn.AOP_BARREL_DAMAGE_AFFECT))) / 100;
					}
				}
				return Math.abs(result);
			}			
		} 
		return 0;		
	}
	
	public static function accuracyOfDefaultGun (gun : Gun, allItems : Array) : Number {
		var addOnId:Number = gun.getDefaultAddOnInSlot(Catalogue.CTL_SCOPE);
		var result : Number = 0;
		result = gun.accuracy;
		if (addOnId != undefined) {
			var addOn:AddOn = allItems[addOnId];
			if (addOn instanceof AddOn) {
				result *= (100 + Number(addOn.property(AddOn.AOP_SCOPE_ACCURACY))) / 100;
			}
		}
		return Math.abs(Number (result));
	}
	
	public static function speedOfDefaultGun (gun : Gun, allItems : Array) : Number {
		var addOnId:Number = gun.getDefaultAddOnInSlot(Catalogue.CTL_SCOPE);
		var result : Number = 0;
		var effect:Number = 0;
		result = gun.speed;
		if (addOnId != undefined) {
			var addOn:AddOn = allItems[addOnId];
			if (addOn instanceof AddOn) {
				result *= (100 + Number(addOn.property(AddOn.AOP_SCOPE_RATE_OF_FIRE))) / 100;
			}
		}
		return Math.abs(Number (result));
		
	/*  if (barrel instanceof AddOn) {
				affect += barrel.property (AddOn.AOP_BARREL_RATE_OF_FIRE);
			}
		if (scope instanceof AddOn) {
				affect += scope.property (AddOn.AOP_SCOPE_RATE_OF_FIRE);
			}*/
	}
	
	public static function recoilOfDefaultGun (gun : Gun, allItems : Array) : Number {
		var result : Number = 0;
		result = gun.recoil;
		return Math.abs(Number (result));
	}
	
	public static function ammosOfDefaultGun (gun : Gun, allItems : Array) : Number {
		var result : Number = gun.ammos;
		var addOnId : Number = gun.getDefaultAddOnInSlot (Catalogue.CTL_CLIP);
		if (addOnId != undefined) {
			var addOn: AddOn = allItems [addOnId];
			if (addOn instanceof AddOn) {				
				result += Number(addOn.property(AddOn.AOP_CLIP_EXTRA_AMMOS));
			}			
		}		
		return Math.abs(Number (result));
	}
	
	public static function reloadingSpeedOfDefaultGun (gun : Gun, allItems : Array) : Number {
		var result : Number = gun.reloadTime (0);
		var addOnId : Number = gun.getDefaultAddOnInSlot (Catalogue.CTL_CLIP);
		if (addOnId != undefined) {
			var addOn: AddOn = allItems [addOnId];
			if (addOn instanceof AddOn) {				
				result = gun.reloadTime(addOn.property(AddOn.AOP_CLIP_RELOADING_SPEED_AFFECT));
			}			
		}
		return Math.abs(Number (result));
	}
	
	public static function weightOfDefaultGun (gun : Gun, allItems : Array) : Number {
		var result : Number = gun.weight;
		var slots : Array = gun.slots;
		var addOnId : Number;
		for (var x:String in slots) {
			addOnId = slots[x].defaultAddOn;
			if (addOnId != undefined) {
				if (allItems[addOnId] != undefined) {
					result += Number(allItems[addOnId].weight);
				}				
			}
		}
		return Math.abs(Number (result));
	}
	
	public static function powerInInventory (
		gun :			Number, 
		bullet : 		Number, 
		clip : 		Number, 
		scope : 	Number, 
		barrel : 	Number, 
		left : 			Number, 
		right : 		Number, 
		bottom : 	Number, 
		color : 		Number, 
		decal : 		Number, 
		allItems:	Array) : Number {
		return powerInShop (
			allItems [gun], 
			allItems [bullet], 
			allItems [clip], 
			allItems [scope], 
			allItems [barrel], 
			allItems [left], 
			allItems [right], 
			allItems [bottom], 
			allItems [color], 
			allItems [decal]
		);
	}	
	
	public static function accuracyInInventory (
		gun :			Number, 
		bullet : 		Number, 
		clip : 		Number, 
		scope : 	Number, 
		barrel : 	Number, 
		left : 			Number, 
		right : 		Number, 
		bottom : 	Number, 
		color : 		Number, 
		decal : 		Number, 
		allItems:	Array) : Number {
		return accuracyInShop (
			allItems [gun], 
			allItems [bullet], 
			allItems [clip], 
			allItems [scope], 
			allItems [barrel], 
			allItems [left], 
			allItems [right], 
			allItems [bottom], 
			allItems [color], 
			allItems [decal]
		);
	}	
	
	public static function ammosInInventory (
		gun :			Number, 
		bullet : 		Number, 
		clip : 		Number, 
		scope : 	Number, 
		barrel : 	Number, 
		left : 			Number, 
		right : 		Number, 
		bottom : 	Number, 
		color : 		Number, 
		decal : 		Number, 
		allItems:	Array) : Number {
		return ammosInShop (
			allItems [gun], 
			allItems [bullet], 
			allItems [clip], 
			allItems [scope], 
			allItems [barrel], 
			allItems [left], 
			allItems [right], 
			allItems [bottom], 
			allItems [color], 
			allItems [decal]
		);
	}	
	
	public static function recoilInInventory (
		gun :			Number, 
		bullet : 		Number, 
		clip : 		Number, 
		scope : 	Number, 
		barrel : 	Number, 
		left : 			Number, 
		right : 		Number, 
		bottom : 	Number, 
		color : 		Number, 
		decal : 		Number, 
		allItems:	Array) : Number {
		return recoilInShop (
			allItems [gun], 
			allItems [bullet], 
			allItems [clip], 
			allItems [scope], 
			allItems [barrel], 
			allItems [left], 
			allItems [right], 
			allItems [bottom], 
			allItems [color], 
			allItems [decal]
		);
	}
	
	public static function speedInInventory (
		gun :			Number, 
		bullet : 		Number, 
		clip : 		Number, 
		scope : 	Number, 
		barrel : 	Number, 
		left : 			Number, 
		right : 		Number, 
		bottom : 	Number, 
		color : 		Number, 
		decal : 		Number, 
		allItems:	Array) : Number {
		return speedInShop (
			allItems [gun], 
			allItems [bullet], 
			allItems [clip], 
			allItems [scope], 
			allItems [barrel], 
			allItems [left], 
			allItems [right], 
			allItems [bottom], 
			allItems [color], 
			allItems [decal]
		);
	}
	
	public static function reloadingSpeedInInventory (
		gun :			Number, 
		bullet : 		Number, 
		clip : 		Number, 
		scope : 	Number, 
		barrel : 	Number, 
		left : 			Number, 
		right : 		Number, 
		bottom : 	Number, 
		color : 		Number, 
		decal : 		Number, 
		allItems:	Array) : Number {
		return reloadingSpeedInShop (
			allItems [gun], 
			allItems [bullet], 
			allItems [clip], 
			allItems [scope], 
			allItems [barrel], 
			allItems [left], 
			allItems [right], 
			allItems [bottom], 
			allItems [color], 
			allItems [decal]
		);
	}
	
	public static function weightInInventory (
		gun :			Number, 
		bullet : 		Number, 
		clip : 		Number, 
		scope : 	Number, 
		barrel : 	Number, 
		left : 			Number, 
		right : 		Number, 
		bottom : 	Number, 
		color : 		Number, 
		decal : 		Number, 
		allItems:	Array) : Number {
		return weightInShop (
			allItems [gun], 
			allItems [bullet], 
			allItems [clip], 
			allItems [scope], 
			allItems [barrel], 
			allItems [left], 
			allItems [right], 
			allItems [bottom], 
			allItems [color], 
			allItems [decal]
		);
	}
}