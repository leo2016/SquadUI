class gfx.controls.SQUAD.NewStyle.Data.Property  {
	private var _id:		Number;
	private var _name:		String;	
	private var _value:		Number;
	private var _level:		Number;
	public function Property(
		id: Number,
		name:String,
		value: Number,
		level:		Number
		) {
		_id = id;
		_name = name;
		_value = value;
		_level = level;
	}		
	
	public function get id () : Number {
		return _id;
	}
	public function get name () : String {
		return _name;
	}
	public function get values () : Number {
		return _value;
	}
	public function get levelPoperty () : Number {
		return _level;
	}
	
	public function toString () : String {
		var s : String = "";
		s += "ID = " + _id + "\n";
		s += "Value = " + _value + "\n";
		s += "Name = " + _name + "\n";
		s += "level = " + _level + "\n";
		return s;
	}
}