﻿class gfx.controls.SQUAD.NewStyle.Data.Level  {
	private var _id:		Number;
	private var _image:		String;
	private var _name:		String;	
	private var _from:		Number;
	private var _to:		Number;
	private var _level:		Number;
	private var _levelName:	String;
	
	public function Level(
		id: Number,
		name:String,
		image: String, 
		from: Number,
		to: Number,
		level:Number,
		levelName:String
		) {
		_id = id;
		_name = name;
		_image = image;
		_from = from;
		_to = to;
		_level = level;
		_levelName = levelName;
	}		
	
	public function get id () : Number {
		return _id;
	}
	
	public function get name () : String {
		return _name;
	}
	
	public function get image () : String {
		return _image;
	}
	
	public function get from () : Number {
		return _from;
	}
	
	public function get to () : Number {
		return _to;
	}
	public function get levels () : Number {
		return _level;
	}
	public function get levelName () : String {
		return _levelName;
	}
	
	public function toString () : String {
		var s : String = "";
		s += "ID = " + _id + "\n";
		s += "Name = " + _name + "\n";
		s += "EXP = " + _from + "->" + _to + "\n";
		s += "Level = " + _level + "\n";
		s += "LevelName = " + _levelName + "\n";
		return s;
	}
}