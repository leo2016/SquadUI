﻿class gfx.controls.SQUAD.NewStyle.Data.ClanRank  {
	private var _id:		Number;
	private var _rankName:		String;
	private var _minPlayer:		Number;	
	private var _maxPlayer:		Number;
	private var _maxSeniorPlayer: Number;
	private var  _maxCommander:		Number;	
	private var _maxCommittee:		Number;
	private var _maxChief:		Number;	
	private var _maxCity:		Number;
	
	public function ClanRank(
		id:		Number,
		rankName:		String,
		minPlayer:		Number,	
		maxPlayer:		Number,
		maxSeniorPlayer: Number,
		maxCommander:		Number,	
		maxCommittee:		Number,
		maxChief:		Number,
		maxCity:		Number
		) {
		_id = id;
		_rankName = rankName;
		_minPlayer = minPlayer;
		_maxPlayer = maxPlayer;
		_maxSeniorPlayer = maxSeniorPlayer;
		_maxCommander = maxCommander;
		_maxCommittee = maxCommittee;
		_maxChief = maxChief;
		_maxCity = maxCity;
	}		
	
	public function get id () : Number {
		return _id;
	}
	
	public function get rankName () : String {
		return _rankName;
	}
	
	public function get minPlayer () : Number {
		return _minPlayer;
	}
	public function get maxPlayer () : Number {
		return _maxPlayer;
	}
	
	public function get maxSeniorPlayer () : Number {
		return _maxSeniorPlayer;
	}
	
	public function get maxCommander () : Number {
		return _maxCommander;
	}
	public function get maxCommittee () : Number {
		return _maxCommittee
	}
	
	public function get maxChief () : Number {
		return maxChief;
	}
	
	public function get maxCity () : Number {
		return _maxCity;
	}
	
	public function toString () : String {
		var s : String = "";
		s += "ID = " + _id + "\n";
		s += "Name = " + _rankName + "\n";
		s += "minPlayer = " + _minPlayer + "\n";
		s += "maxPlayer = " + _maxPlayer + "\n";
		s += "maxSeniorPlayer = " + _maxSeniorPlayer + "\n";
		s += "maxCommander = " + _maxCommander + "\n";
		s += "maxCommittee = " + _maxCommittee + "\n";
		s += "maxChief = " + maxChief + "\n";
		s += "maxCity = " + _maxCity + "\n";
		return s;
	}
}