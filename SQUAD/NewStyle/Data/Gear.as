class gfx.controls.SQUAD.NewStyle.Data.Gear  {
	private var _id:		Number;
	public var _propertys: 			Array;
	
	public function Gear(
		id: Number,
		propertys:Array
		) {
		_id = id;
		_propertys = propertys;
	}
	public function get id () : Number {
		return _id;
	}
	public function get propertys () : Array {
		return _propertys;
	}
	public function toString () : String {
		var s : String = "";
		s += "ID = " + _id + "\n";;
		for (var x : String in _propertys) {
			s += "\t\t" + _propertys[x] + "\n";
		}
		return s;
	}
}