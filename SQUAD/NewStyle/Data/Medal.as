﻿import gfx.controls.SQUAD.NewStyle.Data.BaseItem;
import gfx.controls.SQUAD.NewStyle.Data.Catalogue;

/**
 * Class lưu trữ dữ liệu về HUÂN CHƯƠNG
 */
class gfx.controls.SQUAD.NewStyle.Data.Medal extends BaseItem {
	private var _properties: Array;	
	private static var LOCALIZATION_VALUE_PARAM : String = "**VALUE**";
	
	//----------------Danh sách các thuộc tính của huân chương
	public static var MP_EXTRA_EXP :	String = "MedalExtraEXP";
	public static var MP_EXTRA_GP :		String = "MedalExtraGP";
	
	/**
	 * Hàm tạo
	 *
	 * @param	baseItem BaseItem với dữ liệu đọc từ Common.xml
	 * @param	properties	Danh sách các thuộc tính của phụ kiện (mỗi phần tử có { property, value } )
	 * @param	holders	Danh sách các Catalogue hoặc vật phẩm mà phụ kiện hỗ trợ (mỗi phần tử có { id, isCatalogue, isIncluded } )
	 */
	public function Medal(baseItem: BaseItem, properties: Array) {
		super.copyFrom(baseItem);
		_properties = properties;		
	}
	
	/**
	 * @return Danh sách các thuộc tính của vật phẩm
	 */
	public function get properties():Array {
		return _properties;
	}
	
	/**
	 * Lấy giá trị của một thuộc tính của huân chương
	 * 
	 * @param	propertyName	tên thuộc tính (nên dùng các biến public static ở phía trên, dạng MP_...)
	 * 
	 * @return 	Giá trị dạng Number của thuộc tính (nếu có) hoặc 0 (nếu không có)
	 */
	public function property (propertyName : String) : Number {
		for (var x: String in _properties) {
			if (_properties[x].property == propertyName) {
				return parseFloat(_properties[x].value);
			}
		}
		return 0;
	}	
	
	/**
	 * @return Mô tả phụ kiện
	 */
	public function toString():String {
		var s:String = super.toString();
		for (var x:String in _properties) {
			s += "\t" + _properties[x].property + " = " + _properties[x].value + "\n";
		}
		return s;
	}
	
	/**
	 * Lấy tag để localize cho một thuộc tính của huân chương
	 * 
	 * @param	propertyName	tên thuộc tính (nên dùng các biến public static ở phía trên, dạng MP_...)
	 * 
	 * @return 	XML TAG dùng cho locaizer
	 */
	public function getLocalizationTag (propertyName : String) : String {
		switch (propertyName) {
			case MP_EXTRA_EXP :
				return "MP_EXTRA_EXP";
			case MP_EXTRA_GP : 
				return "MP_EXTRA_GP";
		}
		return "";
	}
}