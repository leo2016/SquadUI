/**
 * Class chứa base item id của các add-on đang lắp trong một 1 khẩu súng
 */
class gfx.controls.SQUAD.NewStyle.Data.AdaptedAddOns {
	public var bullet;
	public var clip;
	public var scope;
	public var barrel;
	public var left;
	public var right;
	public var bottom;
	public var color;
	public var decal;
	
	public function AdaptedAddOns (
		bullet 	: Number, 
		clip 	: Number, 
		scope 	: Number, 
		barrel 	: Number, 
		left 	: Number, 
		right 	: Number, 
		bottom 	: Number, 
		color 	: Number, 
		decal 	: Number
	) {
		this.bullet 	= bullet;
		this.clip 		= clip;
		this.scope 		= scope;
		this.barrel 	= barrel;
		this.left 		= left;
		this.right 		= right;
		this.bottom 	= bottom;
		this.color 		= color;
		this.decal 		= decal;
	}
}