﻿import gfx.controls.SQUAD.NewStyle.Data.InventoryItem;

/**
 * Kho đồ của người chơi
 */
class gfx.controls.SQUAD.NewStyle.Data.Inventory {
	/**
	 * Danh sách các vật phẩm trong kho đồ
	 */
	private var _items : Array;
	
	public function Inventory () {
		_items = [];
	}
	
	/**
	 * Thêm 1 vật phẩm mới vào kho đồ (lúc khởi tạo kho đồ, mua vật phẩm)
	 * 
	 * @param	item	vật phẩm mới
	 */
	public function addItem (item: InventoryItem) : Void {
		_items.push (item);
	}
	
	/**
	 * Loại bỏ 1 vật phẩm ra khỏi kho đồ (lúc bán, hết hạn....)
	 * 
	 * @param	itemId	InventoryID của vật phẩm muốn loại bỏ
	 */
	public function removeItem (itemId : Number) : Void {
		var length : Number = _items.length;
		for (var i:Number = 0; i < length; ++i ) {
			if (_items[i].id == itemId) {
				_items.splice(i, 1);
				break;
			}
		}		
	}
	
	/**	 
	 * @return Lấy danh sách vật phẩm trong kho đồ
	 */
	public function getItems () : Array {
		return _items;
	}
	
	/**
	 * Lấy 1 vật phẩm trong kho đồ
	 * 
	 * @param	itemId	InventoryID của vật phẩm
	 * 
	 * @return		vật phẩm có InventoryID = itemId
	 */
	public function item (itemId : Number) : InventoryItem {
		for (var x : String in _items) {
			if (_items[x].id == itemId) {
				return _items[x];
			}
		}
		return undefined;
	}
	
	/**
	 * Xóa sạch kho đồ
	 */
	public function clear () : Void {
		while (_items.length > 0) {
			_items.pop ();
		}
		_items = [];
	}
	
	/**
	 * Lấy số vật phẩm trong kho đồ	 
	 */
	public function getNumberOfItems () : Number {
		var count : Number = 0;
		for (var x : String in _items) {
			++count;
		}
		return count;
	}
	
	public function getSharedItem (number : Number) : InventoryItem {
		for (var x : String in _items) {
			if (_items[x].number == number && _items[x].inClass == InventoryItem.CLASS_NONE) {
				return _items[x];
			}
		}
		return undefined;
	}
	
	public function replaceSharedItem (number : Number, newItem : InventoryItem) : InventoryItem {
		var oldItem : InventoryItem;
		for (var x : String in _items) {
			oldItem = _items[x];
			if (oldItem.number == number && oldItem.inClass == InventoryItem.CLASS_NONE) {
				oldItem.inClass = InventoryItem.NOT_USE;
				newItem.inClass = InventoryItem.CLASS_NONE;
				return newItem;
			}
		}
		newItem.inClass = InventoryItem.CLASS_NONE;
		return newItem;
	}
	
	public function unequipSharedItem (number : Number) : Void {
		for (var x : String in _items) {
			if (_items[x].number == number && _items[x].inClass == InventoryItem.CLASS_NONE) {
				_items[x].inClass = InventoryItem.NOT_USE;
				break;
			}
		}
	}
}