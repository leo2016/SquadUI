﻿import gfx.controls.SQUAD.NewStyle.Data.BaseItem;

/**
 * Class lưu trữ dữ liệu về LỰU ĐẠN
 */
class gfx.controls.SQUAD.NewStyle.Data.Grenade extends BaseItem {
	private var _effectRadius: 	Number;
	private var _baseDamage	 : 	Number;
	private var _lifeTime  	 :	Number;	// Thêm mới tuannq
	private var _distance	 :	Number; // Thêm mới tuannq
	private var _effect		 :	String; // Thêm mới tuannq
	
	/**
	 * Hàm tạo
	 *
	 * @param	baseItem BaseItem với dữ liệu đọc từ Common.xml
	 * @param	effectRadius Phạm vi ảnh hưởng
	 * @param	baseDamage	Sát thương cơ bản
	 */
	public function Grenade(
		baseItem	: BaseItem,
		effectRadius: Number,
		baseDamage	: Number,
		lifeTime	: Number,
		distance	: Number,
		effect		: String) {
		copyFrom(baseItem);
		_effectRadius = effectRadius;
		_baseDamage   = baseDamage;
		_lifeTime     = lifeTime;
		_distance     = distance;
		if (effect == undefined) effect = "";
		_effect   	  = effect;
	}
	 
	/**
	 * @return Tác dụng  
	 */
	public function get effect():String {
		return String(_effect);	
	}
	/**
	 * @return Phạm vi ảnh hưởng
	 */
	public function get effectRadius():Number {
		return Number(_effectRadius);
	}
	
	/**
	 * @return Sát thương cơ bản
	 */
	public function get baseDamage():Number {
		return Number(_baseDamage);
	}
	
	/**
	 * @return Khoảng cách ném 
	 */
	public function get distance():Number {
		return Number(_distance);
	}
	
	/**
	 * @return Thời gian nổ
	 */
	public function get lifeTime():Number {
		return Number(_lifeTime);
	}
	
	/**
	 * @return Mô tả lựu đạn
	 */
	public function toString():String {
		var s:String = super.toString();
		s += "\tEffect Radius = " + _effectRadius + "\n";
		s += "\tBase Damage = " + _baseDamage + "\n";
		s += "\tTime = " + _lifeTime + "\n";
		return s;
	}
}