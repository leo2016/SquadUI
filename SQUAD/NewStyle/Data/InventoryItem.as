﻿import gfx.controls.SQUAD.NewStyle.Data.BaseItem;
import gfx.controls.SQUAD.NewStyle.Data.Catalogue;

/**
 * Class lưu trữ dữ liệu về một vật phẩm mà người chơi sở hữu
 */
class gfx.controls.SQUAD.NewStyle.Data.InventoryItem {			
	/**
	 * Đối tượng chứa tất cả các thông tin cơ bản của vật phẩm
	 */
	private var _baseItem : 				BaseItem;
	
	/**
	 * Mã vật phẩm
	 */
	private var _itemId : 					Number;
	
	/**
	 * Nhân vật thuộc class nào hiện đang sử dụng vật phẩm này
	 */
	public var inClass :						Number;	
	
	/**
	 * Độ bền hiện tại của vật phẩm
	 */
	public var durability : 				Number;	
	
	/**
	 * Vật phẩm đang được thuê (true) hay mua (false)
	 */
	public var isRent : 						Boolean;	
	
	/**
	 * Thời gian còn được sử dụng (tính bằng phút)
	 */
	public var remainingMinute : 		Number;	
	
	/**
	 * Add-on đang được lắp cho súng nào
	 */
	public var useFor : 					Number;	
	
	/**
	 * Nếu là lựu đạn thì ở slot mấy (1, 2, 3)
	 * Nếu là súng thì là súng chính (4) hay súng phụ (5)
	 * Nếu là dao thì là 6
	 * Nếu là add-on thì 7-15
	 * Còn lại, = 0
	 * 
	 * Là giá trị chọn từ IIN_*
	 */
	public var number : 					Number;	
	
	/**
	 * Vật phẩm đã được mở khóa chưa
	 */
	public var unlocked : 					Boolean;
	
	/**
	 * Bị vô hiệu hóa (mờ đi, không tương tác được, dùng với add-on khi không phù hợp với súng)
	 */
	public var isDisabled :				Boolean;
	
	/**
	 * Tên của vật phẩm tương ứng với trường useFor (thường là tên súng mà add-on gắn vào)
	 */
	public var useForName :			String;
	
	/**
	 * ID đánh dấu vật phẩm chưa mua mà chỉ lấy từ cửa hàng và lắp thử (trong nâng cấp súng)
	 */
	public static var SHOP_ITEM_ID : Number		= 0;
	
	/**
	 * Tính trước số phút 1 ngày (vì lưu hạn sử dụng theo số phút và hiển thị theo số ngày)
	 */
	public static var MINUTES_PER_DAY : Number = 24 * 60;
	
	public var isDefault : Boolean;
	public var isGift : Boolean;
	public var m_ulDefaultOfGunID : Number;
	
	/**
	 * Danh sách các enum chỉ định loại vật phẩm
	 */
	public static var IIN_NOT_USE : 						Number = 0;
	public static var IIN_GRENADE1 : 						Number = 1;
	public static var IIN_GRENADE2 : 						Number = 2;
	public static var IIN_GRENADE3 : 						Number = 3;
	public static var IIN_MAIN_GUN : 						Number = 4;
	public static var IIN_SUB_GUN : 						Number = 5;
	public static var IIN_KNIFE : 							Number = 6;
	public static var IIN_AMMO : 							Number = 7;
	public static var IIN_CLIP : 							Number = 8;
	public static var IIN_SCOPE : 							Number = 9;
	public static var IIN_BARREL : 							Number = 10;
	public static var IIN_LEFT_SIDE : 						Number = 11;
	public static var IIN_RIGHT_SIDE : 					Number = 12;
	public static var IIN_BOTTOM_SIDE : 				Number = 13;
	public static var IIN_COLOR : 							Number = 14;
	public static var IIN_DECAL : 							Number = 15;
	public static var IIN_NOT_USE_GRENADE:			Number = 16;
	public static var IIN_NOT_USE_AMMO : 			Number = 17;
	public static var IIN_NOT_USE_CLIP : 				Number = 18;
	public static var IIN_NOT_USE_SCOPE : 			Number = 19;
	public static var IIN_NOT_USE_BARREL : 			Number = 20;
	public static var IIN_NOT_USE_LEFT_SIDE : 		Number = 21;
	public static var IIN_NOT_USE_RIGHT_SIDE : 	Number = 22;
	public static var IIN_NOT_USE_BOTTOM_SIDE : Number = 23;
	public static var IIN_NOT_USE_COLOR : 			Number = 24;
	public static var IIN_NOT_USE_DECAL : 			Number = 25;
	public static var IIN_UNIFORM :						Number = 26;
	public static var IIN_NOT_USE_SIDE :				Number = 27;
	public static var IIN_NOT_USE_MIDDLE_SIDE :			Number = 28;	
	public static var IIN_HAT :								Number = 29;
	public static var IIN_FACE :								Number = 30;
	public static var IIN_SHOULDER :						Number = 31;
	public static var IIN_HIP :								Number = 32;
	public static var IIN_ARMOR :							Number = 33;
	public static var IIN_LEG_PART :						Number = 34;
	public static var IIN_CAMOVYLAGE :				Number = 35;
	public static var IIN_PACK :								Number = 36;//Never happens when it's an inventory item, but it can when it's shop item
	public static var IIN_AVATAR :								Number = 38;
	
	/**
	 * Danh sách các enum chỉ định vật phẩm đang được dùng hay không, dùng trong class nào
	 */
	public static var NOT_USE:			Number = -1;
	public static var CLASS_NONE:		Number = BaseItem.CLASS_NONE;
	public static var CLASS_TANKER :	Number = BaseItem.CLASS_TANKER;
	public static var CLASS_SCOUT:		Number = BaseItem.CLASS_SCOUT;
	public static var CLASS_SNIPER :	Number = BaseItem.CLASS_SNIPER;
	
	public static var MAX_DURABILITY : 	Number = 10000;
	
	public function InventoryItem (
		baseItem:					BaseItem,
		itemId : 					Number,
		inClass :					Number,
		durability : 				Number,
		isRent : 					Boolean,
		remainingMinute : 			Number,
		useFor : 					Number, 
		number :					Number,
		unlocked :					Boolean,
		isDefault :					Boolean,
		isGift : 					Boolean
		//m_ulDefaultOfGunID          Number
		) {
				
		_baseItem 					= baseItem;
		_itemId							= itemId;
		this.inClass					= inClass;
		this.durability					= durability;
		this.isRent						= isRent;
		this.remainingMinute		= remainingMinute;
		this.useFor					= useFor;
		this.number					= number;
		this.unlocked					= unlocked;
		isDisabled 					= false;
		useForName					= "";	
		this.isDefault				= isDefault;
		this.isGift					= isGift;
		//this.m_ulDefaultOfGunID				= m_ulDefaultOfGunID;
	}
	
	/**
	 * Chuyển đổi 1 BaseItem thành InventoryItem để có thẻ lắp vào AddOnSlot (với itemId = SHOP_ITEM_ID)
	 * 
	 * @param	baseItem				BaseItem
	 * @param	catalogueName		Tên catalogue của vật phẩm
	 */
	public function setUpAsShopItem (baseItem: BaseItem, catalogueName : String) : Void {
		_baseItem 					= baseItem;
		_itemId							= SHOP_ITEM_ID;
		inClass							= NOT_USE;
		durability						= MAX_DURABILITY;
		isRent							= false;
		remainingMinute			= 0;
		useFor							= 0;		
		unlocked						= true;		
		useForName					= "";
		isDisabled 					= false;		
		switch (catalogueName) {
			case Catalogue.CTL_BULLET:
				number = IIN_NOT_USE_AMMO;
				break;
			case Catalogue.CTL_CLIP:
				number = IIN_NOT_USE_CLIP;
				break;
			case Catalogue.CTL_SCOPE:
				number = IIN_NOT_USE_SCOPE;
				break;
			case Catalogue.CTL_BARREL:
				number = IIN_NOT_USE_BARREL;
				break;
			case Catalogue.CTL_LEFT_SIDE:
				number = IIN_NOT_USE_LEFT_SIDE;
				break;
			case Catalogue.CTL_RIGHT_SIDE:
				number = IIN_NOT_USE_RIGHT_SIDE;
				break;
			case Catalogue.CTL_BOTTOM_SIDE:
				number = IIN_NOT_USE_BOTTOM_SIDE;
				break;
			case Catalogue.CTL_SIDE:
				number = IIN_NOT_USE_SIDE;
				break;
			case Catalogue.CTL_MIDDLE_SIDE:
				number = IIN_NOT_USE_MIDDLE_SIDE;
				break;
			case Catalogue.CTL_COLOR:
				number = IIN_NOT_USE_COLOR;
				break;
			case Catalogue.CTL_DECAL:
				number = IIN_NOT_USE_DECAL;
				break;
			default:
				number = NOT_USE;
		}
	}
	
	/**
	 * @return BaseItem
	 */
	public function get baseItem(): BaseItem {
		return _baseItem;
	}
	
	/**
	 * @return Mã vật phẩm
	 */
	public function get baseItemId(): Number {
		return _baseItem.id;
	}
	
	/**
	 * @return Mã Catalogue của vật phẩm
	 */
	public function get catalogue():Number {
		return _baseItem.catalogue;
	}
	
	/**
	 * @return	Enum bảng thông tin chi tiết (dùng để xác định loại vật phẩm)
	 */
	public function get detailTable () : Number {
		return _baseItem.detailTable;
	}
	
	/**
	 * @return Mô tả ngắn về vật phẩm
	 */
	public function get shortDescription():String {
		return _baseItem.shortDescription;
	}
	
	/**
	 * @return Tên vật phẩm
	 */
	public function get name():String {		
		return _baseItem.name;
	}
	
	/**
	 * @return Miêu tả vật phẩm
	 */
	public function get description():String {
		return _baseItem.description;
	}
	
	/**
	 * @return Cân nặng
	 */
	public function get weight():Number {
		return _baseItem.weight;
	}
	
	/**
	 * @return Có được gia hạn không
	 */
	public function get allowExtend():Boolean {
		return _baseItem.allowExtend;
	}
	
	/**
	 * @return Có được mua không
	 */
	public function get allowBuy():Boolean {
		return _baseItem.allowBuy;
	}
	
	/**
	 * @return Có được bán không
	 */
	public function get allowSell():Boolean {
		return _baseItem.allowSell;
	}
	
	/**
	 * @return Có được tặng không
	 */
	public function get allowGive():Boolean {
		return _baseItem.allowGive;
	}
	
	/**
	 * @return Có được sửa không
	 */
	public function get allowRepair () : Boolean {
		return _baseItem.allowRepair;
	}
	
	/**
	 * @return Ảnh vật phẩm
	 */
	public function get image():String {
		return _baseItem.image;
	}
	public function setImage(image:String) {
		_baseItem._image = image;
	}
	
	
	/**
	 * @return Loại điều kiện để mở khóa vật phẩm
	 */
	public function get unlockCondition():Number {		
		return _baseItem.unlockCondition;
	}
	
	/**
	 * @return yêu cầu cần đạt được để mở khóa vật phẩm
	 */
	public function get unlockRequirement():Number {
		return Number(_baseItem.unlockRequirement);
	}
	
	/**
	 * @return Class mà vật phẩm này hỗ trợ tốt nhất
	 */
	public function get classId():Number {
		return _baseItem.classId;
	}
	
	/**
	 * @return Inventory ID của vật phẩm
	 */
	public function get id():Number {
		return _itemId;
	}
	
	/**
	 * @return Thời gian thuê còn lại (tính bằng ngày)
	 */
	public function get expiredDays () : Number {
		return Math.round(remainingMinute / MINUTES_PER_DAY);
	}	
	
	/**
	 * @return Thời gian thuê còn lại (tính bằng giờ)
	 */
	public function get expiredHours () : Number {
		return Math.round(remainingMinute / 60);
	}	
	
	/**
	 * @return 3D model của vật phẩm
	 */
	public function get model () : String {
		return _baseItem.model;
	}
	
	public function get isSharedItem () : Boolean {
		switch (number) {
			case IIN_HAT:
			case IIN_FACE:
			case IIN_SHOULDER:
			case IIN_ARMOR:
			case IIN_HIP:
			case IIN_LEG_PART:
			case IIN_CAMOVYLAGE:
			case IIN_AVATAR:
				return true;
			default:
				return false;
		}
	}
	
	public function getMinRentingDays () : Number {
		return _baseItem.getMinRentingDays ();
	}
	
	public function howMuch (rentingDays: Number, isGP: Boolean): Number {
		return _baseItem.howMuch (rentingDays, isGP);
	}
	
	public function toString () : String {
		var s : String =  _baseItem.toString ();
		s += "\tInventoryID = " + _itemId + "\n";
		s += "\tinClass = " + inClass + "\n";
		return s;
	}
}