﻿import gfx.controls.SQUAD.NewStyle.Data.BaseItem;
import gfx.controls.SQUAD.NewStyle.Data.Catalogue;

/**
 * Class lưu trữ dữ liệu về QUÂN PHỤC
 */
class gfx.controls.SQUAD.NewStyle.Data.Uniform extends BaseItem {
	private var m_aProperties: 	Array;	
	private var m_iLevel :			Number;
	
	/**
	 * Danh sách các thuộc tính mà 1 bộ quân phục có thể có
	 */
	public static var UO_EXTRA_PERCENTAGE_HP :								String = "ExtraPercentageArmor";
	public static var UO_EXTRA_PERCENTAGE_MOVEMENT_SPEED :	String = "ExtraPercentageMovementSpeed";
	public static var UO_EXTRA_PERCENTAGE_POWER :						String = "ExtraPercentagePower";
	public static var UO_EXTRA_PERCENTAGE_ACCURACY :					String = "ExtraPercentageAccuracy";
	public static var UO_EXTRA_PERCENTAGE_RECOIL :						String = "ExtraPercentageRecoil";
	public static var UO_EXTRA_PERCENTAGE_RELOADING_SPEED :		String = "ExtraPercentageReloadingSpeed";		
	public static var UO_EXTRA_PERCENTAGE_ARMOR :						String = "ExtraPercentageArmor";
	public static var UO_LEVEL :														String = "Level";
	
	/**
	 * Hàm tạo
	 *
	 * @param	baseItem 	BaseItem với dữ liệu đọc từ Common.xml
	 * @param	properties	Danh sách các thuộc tính của quân phục (mỗi phần tử có { property, value } )	 	 
	 */
	public function Uniform(
		baseItem: 		BaseItem,
		properties: 	Array) {
		super.copyFrom(baseItem);
		m_aProperties = properties;
		m_iLevel = Math.floor(property (UO_LEVEL));
	}
	
	/**
	 * Kiểm tra sự tồn tại của 1 thuộc tính
	 * 
	 * @param	propertyName	tên thuộc tính (sử dụng UO_....)
	 * 
	 * @return		true nếu bộ quân phục có thuộc tính propertyName, false nếu ngược lại
	 */
	public function hasProperty (propertyName : String) : Boolean {
		var x : Number = parseFloat(m_aProperties[propertyName]);
		return !isNaN(x);
	}	
	
	/**
	 * Lấy giá trị của 1 thuộc tính của bộ quân phục
	 * 
	 * @param	propertyName	tên thuộc tính (sử dụng UO_....)
	 * 
	 * @return		Giá trị dạng Number của thuộc tính hoặc 0 nếu không tồn tại thuộc tính
	 */
	public function property (propertyName : String) : Number {
		var x : Number = parseFloat(m_aProperties[propertyName]);
		if (isNaN(x)) {
			return 0;
		} else {
			return x;
		}
	}	
	
	/**
	 * @return Mô tả quân phục
	 */
	public function toString():String {
		var s:String = super.toString();		
		for (var x:String in m_aProperties) {			
			s += "\t" + x + " = " + m_aProperties[x] + "\n";
		}		
		return s;
	}
	
	/**
	 * Kiểm tra xem bộ quân phục này đã được mở khóa với người chơi chưa
	 *
	 * @param	playerLevel	Cấp bậc class của người chơi
	 * 
	 * @return		true nếu bộ quân phục đã được mở khóa, false nếu ngược lại
	 */
	public function isUnlocked (playerLevel : Number) : Boolean {
		return (unlockRequirement <= playerLevel);
	}
	
	/**
	 * Lấy cấp bậc của bộ quân phục này
	 */
	public function get level () : Number {
		return m_iLevel;
	}
}