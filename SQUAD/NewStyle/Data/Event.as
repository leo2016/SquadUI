﻿class gfx.controls.SQUAD.NewStyle.Data.Event  {
	private var _id:		Number;
	private var _name:		String;
	private var _description:		String;	
	private var _fromRank:		Number;
	private var _toRank:		Number;
	private var _beginTime:		String;
	private var _endTime:		String;
	private var _icon:		String;
	
	public function Event(
		id: Number,
		name:String,
		description: String, 
		fromRank: Number,
		toRank: Number,
		beginTime:String,
		endTime:String,
		icon: String
		) {
		_id = id;
		_name = name;
		_description = description;
		_fromRank = fromRank;
		_toRank = toRank;
		_beginTime = beginTime;
		_endTime = endTime;
		_icon = icon;
	}		
	
	public function get id () : Number {
		return _id;
	}
	
	public function get name () : String {
		return _name;
	}
	
	public function get description () : String {
		return _description;
	}
	
	public function get fromRank () : Number {
		return _fromRank;
	}
	
	public function get toRank () : Number {
		return _toRank;
	}
	public function get beginTime () : String {
		return _beginTime;
	}
	
	public function get endTime () : String {
		return _endTime;
	}
	public function get image () : String {
		return _icon;
	}
	
	public function toString () : String {
		var s : String = "";
		s += "ID = " + _id + "\n";
		s += "Name = " + _name + "\n";
		s += "Rank = " + _fromRank + "->" + _toRank + "\n";
		s += "beginTime = " + _beginTime + "\n";
		s += "endTime = " + _endTime + "\n" ;
		s += "image = " + _icon + "\n";
		return s;
	}
}