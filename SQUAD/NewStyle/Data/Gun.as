﻿import gfx.controls.SQUAD.NewStyle.Data.AddOn;
import gfx.controls.SQUAD.NewStyle.Data.BaseItem;
import gfx.controls.SQUAD.NewStyle.Data.InventoryItem;
import gfx.controls.SQUAD.NewStyle.Data.Catalogue;

/**
 * Class lưu trữ dữ liệu về SÚNG
 */
class gfx.controls.SQUAD.NewStyle.Data.Gun extends BaseItem {
	private var _ammos:					Number;
	private var _speed: 					Number;
	private var _recoil: 					Number;
	private var _accuracy:					Number;
	private var _slots: 						Array;
	private var _minRank:					Number;
	private var _damageAmplifying :	Number;
	private var _reloadTime : 			Number;
	
	/**
	 * Hàm tạo
	 *
	 * @param	baseItem					BaseItem với dữ liệu đọc từ Common.xml
	 * @param	ammos						Lượng đạn cơ bản
	 * @param	speed						Tốc độ bắn
	 * @param	recoil						Độ giật
	 * @param	accuracy					Độ chính xác
	 * @param	minRank					Cấp độ yêu cầu tối thiểu
	 * @param	damageAmplifying		Mức độ khuếch đại sát thương
	 * @param	reloadTime				Tốc độ thay đạn
	 * @param	slots							Danh sách các vị trí lắp thêm addon
	 */
	public function Gun(
		baseItem: 					BaseItem,
		ammos: 					Number,
		speed: 						Number,
		recoil:						Number,
		accuracy: 					Number,
		minRank: 					Number,
		damageAmplifying: 	Number,
		reloadTime : 				Number,
		slots: 						Array) {
		
		copyFrom(baseItem);
		_ammos 					= ammos;
		_speed 						= speed;
		_recoil 						= recoil;
		_accuracy 					= accuracy;
		_minRank 					= minRank;
		_damageAmplifying 	= damageAmplifying;
		_reloadTime 				= reloadTime;
		_slots 						= slots;
	}
	
	/**
	 * @return Lượng đạn cơ bản
	 */
	public function get ammos():Number {
		return Number(_ammos);
	}
	
	/**
	 * @return Tốc độ bắn
	 */
	public function get speed():Number {
		return Number(_speed);
	}
	
	/**
	 * @return Độ giật
	 */
	public function get recoil():Number {
		return Number(_recoil);
	}
	
	/**
	 * @return Độ chính xác
	 */
	public function get accuracy():Number {
		return Number(_accuracy);
	}
	
	/**
	 * @return Cấp độ yêu cầu tối thiểu
	 */
	public function get minRank():Number {
		return Number(_minRank);
	}
	
	/**
	 * @return Mức độ khuếch đại sát thương
	 */
	public function get damageAmplifying():Number {
		return Number(_damageAmplifying);
	}
	
	/**
	 * @param ammoBaseDamage	Sát thương cơ bản của đạn
	 * 
	 * @return Sát thương của súng = (100 + damageAmplifying) /100 * ammoBaseDamage
	 */
	public function power(ammoBaseDamage : Number) : Number {
		//trace ("ampl = " + ((1+(_damageAmplifying / 100))) + ", and base = " + ammoBaseDamage + " -> power = " + ((1+(_damageAmplifying / 100)) * ammoBaseDamage));
		return ((1+(_damageAmplifying / 100)) * ammoBaseDamage);
	}
	
	public function reloadTime (reloadTimeAffect : Number) : Number {
		if (reloadTimeAffect == undefined || reloadTimeAffect == null || reloadTimeAffect == 0) {
			return Number(_reloadTime);
		}
		return (1 + (reloadTimeAffect / 100)) * _reloadTime;
	}
		
	/**
	 * @return Danh sách các vị trí lắp thêm addon
	 */
	public function get slots():Array {
		return _slots;
	}
	
	/**
	 * Kiểm tra xem súng có slot để lắp loại phụ kiện thuộc catalogue nào đó không
	 *
	 * @param	catalogueName	Tên slot (tên catalogue)
	 *
	 * @return	true = súng có slot để lắp phụ kiện thuộc catalogue có tên "catalogueName", false = súng không có slot
	 */
	public function hasSlot (catalogueName: String): Boolean {		
		for (var x:String in _slots) {
			/*
			if (catalogueName == Catalogue.CTL_SIDE) {
				switch (_slots[x].type) {
					case Catalogue.CTL_LEFT_SIDE:
					case Catalogue.CTL_RIGHT_SIDE:
					case Catalogue.CTL_BOTTOM_SIDE:
						return true;
				}
			} else if (catalogueName == Catalogue.CTL_MIDDLE_SIDE) {
				switch (_slots[x].type) {
					case Catalogue.CTL_LEFT_SIDE:
					case Catalogue.CTL_RIGHT_SIDE:					
						return true;
				}
			} else {
				if (_slots[x].type == catalogueName) {
					return true;
				}
			}
			*/
			if (_slots[x].type == catalogueName) {
				return true;
			}
		}
		return false;
	}
	
	public function hasDefaultAddOnInSlot (catalogueName : String) : Boolean {
		for (var x:String in _slots) {
			if (_slots[x].type == catalogueName) {
				if (_slots[x].defaultAddOn != undefined) {
					return true;
				} else {
					return false;
				}
			}
		}
		return false;
	}
	
	/**
	 * Lấy add-on mặc định lắp trong slot
	 * 
	 * @param	catalogueName		tên catalogue của slot muốn tìm add-on mặc định
	 * 
	 * @return		Nếu có add-on mặc định, trả về BaseItemID, nếu không trả về undefined
	 */
	public function getDefaultAddOnInSlot (catalogueName : String) : Number {
		for (var x:String in _slots) {
			if (_slots[x].type == catalogueName) {
				return _slots[x].defaultAddOn;
			}
		}
		return undefined;
	}
	
	/**
	 * Kiểm tra xem add-on này có phải add-on mặc định trong slot nào đó không
	 * 
	 * @param	addOnId				BaseitemId của add-on
	 * @param	catalogueName		tên catalogue của slot muốn kiểm tra
	 * @return		true nếu đúng là mặc định, false nếu ngược lại
	 */
	public function isDefaultAddOn (addOnId : Number, catalogueName : String) : Boolean {
		for (var x:String in _slots) {
			if (_slots[x].type == catalogueName) {
				return (_slots[x].defaultAddOn == addOnId);
			}
		}
		return false;
	}
	
	/**
	 * Kiểm tra xem súng có thể lắp 1 phụ kiện nào đó không.
	 * Chú ý: Phải kiểm tra 2 chiều:
	 * +	Phụ kiện có hỗ trợ súng này không
	 * +	Súng này có slot để lắp phụ kiện đó không
	 *
	 * @param	addon			Phụ kiện
	 * @param	catalogues	Danh sách catalogue
	 * @return		true = Có thể lắp, false = Không thể lắp
	 */
	public function canAdaptAddon (addon: AddOn, catalogues: Array): Boolean {
		return addon.canAdaptIntoGun (this, catalogues);
	}
	
	/**
	 * Kiểm tra xem cấp độ nhân vật có phù hợp để sử dụng súng này không
	 *
	 * @param	userRank cấp độ nhân vật
	 *
	 * @return true = có thể sử dụng, false = không thể sử dụng
	 */
	public function isSuitableForRank (userRank: Number) : Boolean {
		if (_minRank == null || _minRank == undefined) {
			return true;
		} else {
			return ( _minRank <= userRank );
		}
	}
		
	/**
	 * @return Mô tả súng
	 */
	public function toString():String {
		var s:String = super.toString();
		s += "\tAmmo = " + _ammos + "\n";
		s += "\tSpeed = " + _speed + "\n";
		s += "\tRecoil = " + _recoil + "\n";
		s += "\tAccuracy = " + _accuracy + "\n";
		s += "\tMinRank = " + _minRank + "\n";
		s += "\tDamageAmplifying = " + _damageAmplifying + "\n";
		s += "\tSlots = ";
		for (var x:String in _slots) {
			s += _slots[x].type + (_slots[x].defaultAddOn != undefined ? (", (default = " + _slots[x].defaultAddOn + ")") : "");
		}
		s += "\n";
		return s;
	}
	
	public function traceSlots () : Void {
		var s : String = "";
		for (var x:String in _slots) {
			s += _slots[x].type + "\n";
			//+ (_slots[x].defaultAddOn != undefined ? (", (default = " + _slots[x].defaultAddOn + ")") : "") + "\n";
		}
		s += "\n";
		trace (s);
	}
}