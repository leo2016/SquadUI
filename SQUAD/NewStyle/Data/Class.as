﻿import gfx.controls.SQUAD.NewStyle.Data.Catalogue;
import gfx.controls.SQUAD.NewStyle.Data.InventoryItem;

/**
 * Class lưu trữ các thiết lập của người dùng với mỗi lớp nhân vật
 */
class gfx.controls.SQUAD.NewStyle.Data.Class {
	/**
	 * Lớp nhân vật (Sử dụng BaseItem.CLASS_...)
	 */
	private var m_iClassType :						Number;
	
	/**
	 * Cấp bậc của lớp nhân vật
	 */
	private var m_iClassLevel :						Number;
	
	/**
	 * Súng chính
	 */
	private var m_kMainGun :						InventoryItem;
	
	/**
	 * Súng phụ
	 */
	private var m_kSubGun :							InventoryItem;
	
	/**
	 * Dao
	 */
	private var m_kKnife :								InventoryItem;	
	
	/**
	 * Danh sách lựu đạn
	 */
	private var m_aGrenades :						Array;
	
	/**
	 * Quân phục
	 */
	private var m_kUniform :						InventoryItem;
	
	/**
	 * Vật phẩm không tồn tại
	 */
	public static var UNDEFINED_ITEM :		InventoryItem = undefined;
	
	/**
	 * Số lựu đạn tối đa mỗi lớp
	 */
	private static var MAX_GRENADES :			Number = 3;
	
	/**
	 * Số thứ tự của slot lựu đạn rỗng nếu không còn slot nào
	 */
	public static var FREE_NONE :					Number = -1;
	
	public function Class (	classType :	Number ) {		
		
		m_iClassType										= classType;			
		m_iClassLevel									= 0;
		m_kMainGun										= UNDEFINED_ITEM;
		m_kSubGun										= UNDEFINED_ITEM;
		m_kKnife										= UNDEFINED_ITEM;
		m_kUniform										= UNDEFINED_ITEM;
		m_aGrenades 									= [];		
		for (var i : Number = 1; i <= MAX_GRENADES; ++i) {
			m_aGrenades[i] = UNDEFINED_ITEM;
		}
	}
	
	/**
	 * Thay thế súng chính
	 * 
	 * @param	newGun	súng chính mới
	 */
	public function replaceMainGun (newGun : InventoryItem) : Void {
		if (m_kMainGun != UNDEFINED_ITEM) {
			m_kMainGun.inClass = InventoryItem.NOT_USE;			
		}
		m_kMainGun = newGun;
		m_kMainGun.inClass = m_iClassType;
	}
	
	/**
	 * Thay thế súng phụ
	 * 
	 * @param	newGun	súng phụ mới
	 */
	public function replaceSubGun (newGun : InventoryItem) : Void {
		if (m_kSubGun != UNDEFINED_ITEM) {
			m_kSubGun.inClass = InventoryItem.NOT_USE;
		}		
		m_kSubGun = newGun;
		m_kSubGun.inClass = m_iClassType;
	}
		
	/**
	 * Thay thế dao
	 * 
	 * @param	newKnife	dao mới
	 */
	public function replaceKnife (newKnife : InventoryItem) : Void {
		if (m_kKnife != UNDEFINED_ITEM) {
			m_kKnife.inClass = InventoryItem.NOT_USE;
		}		
		m_kKnife = newKnife;
		m_kKnife.inClass = m_iClassType;
	}
	
	/**
	 * Thay thế quân phục
	 * 
	 * @param	newUniform	quân phục mới
	 * @param	newLevel		cấp bậc mới (không dùng)
	 */
	public function replaceUniform (newUniform : InventoryItem, newLevel : Number) : Void {
		if (m_kUniform != UNDEFINED_ITEM) {
			m_kUniform.inClass = InventoryItem.NOT_USE;
		}
		m_kUniform = newUniform;
		m_kUniform.inClass = m_iClassType;
		//m_iClassLevel = newLevel;
	}
	
	/**
	 * Tìm slot lựu đạn rỗng	 	 
	 */
	public function getFreeGrenadeSlot () : Number {
		for (var i : Number = 1; i <= MAX_GRENADES; ++i) {
			if (m_aGrenades[i] == UNDEFINED_ITEM) {
				return i;
			}
		}
		return FREE_NONE;
	}
	
	/**
	 * Thêm lựu đạn vào 1 slot chỉ định
	 * 
	 * @param	newGrenade		lựu đạn
	 * @param	slot					số thứ tự slot (1, 2, ...MAX_GRENADES)
	 */
	public function addGrenade (newGrenade : InventoryItem, slot : Number) : Void {
		if (slot < 1 || slot > MAX_GRENADES) {
			return;
		}		
		if (m_aGrenades[slot] != UNDEFINED_ITEM) {
			m_aGrenades[slot].inClass = InventoryItem.NOT_USE;
			m_aGrenades[slot].number = InventoryItem.IIN_NOT_USE_GRENADE;
		}
		m_aGrenades[slot] = newGrenade;
		m_aGrenades[slot].inClass = m_iClassType;
		m_aGrenades[slot].number = slot;		
	}
	
	/**
	 * Thêm lựu đạn vào slot trống
	 * 
	 * @param	grenade	lựu đạn
	 */
	public function addGrenadeToFreeSlot (grenade : InventoryItem) : Void {
		var freeSlot : Number = getFreeGrenadeSlot();
		if (freeSlot != FREE_NONE) {
			addGrenade(grenade, freeSlot);
		}
	}
	
	/**
	 * Gỡ bỏ lựu đạn ở 1 slot
	 * 
	 * @param	currentNumber	slot muốn gỡ lựu đạn
	 */
	public function removeGrenade (currentNumber : Number) : Void {
		var tempGrenade : InventoryItem;
		switch (currentNumber) {
			case InventoryItem.IIN_GRENADE1: {
				m_aGrenades[1].inClass = InventoryItem.NOT_USE;
				m_aGrenades[1].number = InventoryItem.IIN_NOT_USE_GRENADE;
				m_aGrenades[1] = UNDEFINED_ITEM;				
				if (m_aGrenades[2] != UNDEFINED_ITEM) {					
					m_aGrenades[1] = m_aGrenades[2];
					m_aGrenades[1].number = InventoryItem.IIN_GRENADE1;
					m_aGrenades[2] = UNDEFINED_ITEM;
				}
				if (m_aGrenades[3] != UNDEFINED_ITEM) {					
					m_aGrenades[2] = m_aGrenades[3];
					m_aGrenades[2].number = InventoryItem.IIN_GRENADE2;
					m_aGrenades[3] = UNDEFINED_ITEM;
				}				
				break;
			}
			case InventoryItem.IIN_GRENADE2 : {
				m_aGrenades[2].inClass = InventoryItem.NOT_USE;
				m_aGrenades[2].number = InventoryItem.IIN_NOT_USE_GRENADE;				
				m_aGrenades[2] = UNDEFINED_ITEM;
				if (m_aGrenades[3] != UNDEFINED_ITEM) {					
					m_aGrenades[2] = m_aGrenades[3];
					m_aGrenades[2].number = InventoryItem.IIN_GRENADE2;
					m_aGrenades[3] = UNDEFINED_ITEM;
				}
				break;
			}
			case InventoryItem.IIN_GRENADE3 : {
				m_aGrenades[3].inClass = InventoryItem.NOT_USE;
				m_aGrenades[3].number = InventoryItem.IIN_NOT_USE_GRENADE;								
				m_aGrenades[3] = UNDEFINED_ITEM;
				break;
			}
		}		
	}

	/**
	 * Súng chính
	 */
	public function get mainGun () : InventoryItem {
		return m_kMainGun;
	}
	
	/**
	 * Súng phụ
	 */
	public function get subGun () : InventoryItem {
		return m_kSubGun;
	}
	
	/**
	 * Dao
	 */
	public function get knife () : InventoryItem {
		return m_kKnife;
	}
	
	/**
	 * Lựu đạn số 1
	 */
	public function get grenade1 () : InventoryItem {
		return m_aGrenades[1];
	}
	
	/**
	 * Lựu đạn số 2
	 */
	public function get grenade2 () : InventoryItem {
		return m_aGrenades[2];
	}
	
	/**
	 * Lựu đạn sô 3
	 */
	public function get grenade3 () : InventoryItem {
		return m_aGrenades[3];
	}
	
	/**
	 * Quân phục
	 */
	public function get uniform () : InventoryItem {
		return m_kUniform;
	}
	
	/**
	 * Số lựu đạn tối đa
	 */
	public function get maxGrenades () : Number {
		return MAX_GRENADES;
	}
	
	/**
	 * Tên của bộ quân phục hoặc loại lớp nhân vật (để debug)
	 */
	public function get name () : String {
		if (m_kUniform) {
			return m_kUniform.name;
		} else {
			return "" + m_iClassType;
		}
	}
	
	/**
	 * Cấp bậc của lớp nhân vật
	 */
	public function get level () : Number {
		return m_iClassLevel;
	}
	
	/**
	 * Loại lớp nhân vật
	 */
	public function get type () : Number {
		return m_iClassType;
	}
	
	/**
	 * Lắp add-on vào súng
	 * 
	 * @param	gun				súng
	 * @param	addOn			add-on
	 * @param	newNumber	vị trí lắp (sử dụng các biến InventoryItem.IIN_....)
	 */
	public static function adaptAddOn (gun : InventoryItem, addOn : InventoryItem, newNumber : Number) : Void {
		addOn.inClass = gun.inClass;
		addOn.useFor = gun.id;
		addOn.number = newNumber;
		addOn.useForName = gun.name;
		addOn.isDisabled = true;
	}
	
	/**
	 * Gỡ bỏ add-on ra khỏi súng
	 * 
	 * @param	addOn							add-on
	 * @param	addOnCatalogueName	tên catalogue của add-on
	 */
	public static function removeAddOn (addOn : InventoryItem, addOnCatalogueName : String) : Void {		
		if (addOn == undefined) {
			return;
		}
		var removed : Boolean = false;
		var newNumber : Number = -1;		
		switch (addOnCatalogueName) {
			case Catalogue.CTL_BULLET: {
				removed = true;
				newNumber = InventoryItem.IIN_NOT_USE_AMMO;
				break;
			}
			case Catalogue.CTL_CLIP : {
				removed = true;
				newNumber = InventoryItem.IIN_NOT_USE_CLIP;
				break;
			}
			case Catalogue.CTL_SCOPE : {
				removed = true;
				newNumber = InventoryItem.IIN_NOT_USE_SCOPE;
				break;
			}
			case Catalogue.CTL_BARREL : {
				removed = true;
				newNumber = InventoryItem.IIN_NOT_USE_BARREL;
				break;
			}
			case Catalogue.CTL_COLOR : {
				removed = true;
				newNumber = InventoryItem.IIN_NOT_USE_COLOR;
				break;
			}
			case Catalogue.CTL_DECAL : {
				removed = true;
				newNumber = InventoryItem.IIN_NOT_USE_DECAL;
				break;
			}
			/*case Catalogue.CTL_SIDE : {
				removed = true;
				newNumber = InventoryItem.IIN_NOT_USE_SIDE;
				break;
			}
			case Catalogue.CTL_MIDDLE_SIDE : {
				removed = true;
				newNumber = InventoryItem.IIN_NOT_USE_MIDDLE_SIDE;
				break;
			}*/
			case Catalogue.CTL_LEFT_SIDE : {
				removed = true;
				newNumber = InventoryItem.IIN_NOT_USE_LEFT_SIDE;
				break;
			}
			case Catalogue.CTL_RIGHT_SIDE : {
				removed = true;
				newNumber = InventoryItem.IIN_NOT_USE_RIGHT_SIDE;
				break;
			}
			case Catalogue.CTL_BOTTOM_SIDE : {
				removed = true;
				newNumber = InventoryItem.IIN_NOT_USE_BOTTOM_SIDE;
				break;
			}
		}
		if (removed) {
			addOn.inClass 	 = InventoryItem.NOT_USE
			addOn.useFor 	 = 0;	
			addOn.number 	 = newNumber;
			addOn.useForName = "";
			addOn.isDisabled = false;
		}
	}
	
	public function countGrenades (allTypes : Boolean, catalogues : Array, catalogueId : Number) : Number {
		var count : Number = 0;
		var g : InventoryItem;
		for (var x:String in m_aGrenades) {
			g = m_aGrenades[x];
			if (g instanceof InventoryItem) {
				if (g != UNDEFINED_ITEM) {
					if (allTypes || catalogues[g.catalogue].isDescendantOf (catalogueId)) {
						++count;
					}
				}
			}
		}
		return count;
	}
}