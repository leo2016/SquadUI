﻿class gfx.controls.SQUAD.NewStyle.Data.Map {
	public var id : 		Number;
	public var label : 		String;	
	public var image :		String;		
	public var targettype:	Number;
	public var weapon: 		Number;
	
	private var minplayer:	Number;
	private var maxplayer:	Number;
	private var hopplayer: 	Number;
	public var players :	Array;
	
	private var mintarget: 	Number;
	private var maxtarget:	Number;
	private var hoptarget:	Number;
	public var targets :	Array;
	
	private var mintime: 	Number;
	private var maxtime: 	Number;
	private var hoptime: 	Number;
	public var times :		Array;
		
	public var x2d0 : 		Number;
	public var y2d0 : 		Number;
	public var x3d1 : 		Number;
	public var y3d1 : 		Number;
	public var x2d1 : 		Number;
	public var y2d1 : 		Number;	
	
	public var mapBlockID : Number;
	
	public function Map (
		 id : Number, 
		 name : String, 
		 image : String,
		 min_player:	Number,
		 max_player:	Number,
		 hop_player: 	Number,
		 min_target: 	Number,
		 max_target:	Number,
		 hop_target:	Number,
		 target_type:	Number,
		 min_time: 	Number,
		 max_time: 	Number,
		 hop_time: 	Number,
		 _weapon: 		Number, 		 
		 x2d0 : 		Number,
		 y2d0 : 		Number,
		 x3d1 : 		Number,
		 y3d1 : 		Number,
		 x2d1 : 		Number,
		 y2d1 : 		Number, 
		 mapBlockID : Number
	) {
		this.id 			= Math.floor(id);
		this.label 			= name;
		this.image 			= image;
		this.minplayer 		= Math.floor(min_player);
		this.maxplayer 		= Math.floor(max_player);
		this.hopplayer 		= Math.floor(hop_player);
		this.mintarget 		= Math.floor(min_target);
		this.maxtarget 		= Math.floor(max_target);
		this.hoptarget 		= Math.floor(hop_target);
		this.targettype 	= Math.floor(target_type);
		this.mintime 		= Math.floor(min_time);
		this.maxtime 		= Math.floor(max_time);
		this.hoptime 		= Math.floor(hop_time);
		this.weapon 		= Math.floor(_weapon);
		this.x2d0			= Number (x2d0);
		this.y2d0			= Number (y2d0);
		this.x2d1			= Number (x2d1);
		this.y2d1			= Number (y2d1);
		this.x3d1			= Number (x3d1);
		this.y3d1			= Number (y3d1);
		this.mapBlockID = Number (mapBlockID);
		
		generatePlayersList ();
		generateTargetList();
		generateTimeList();		
	}
	
	public static function findMapById (maps : Array, id : Number) : Map {
		for (var x : String in maps) {
			if (maps[x].id == id) {
				return maps[x];
			}
		}
		return null;
	}
	
	public function toString () : String {
		var s : String = "";
		s = id + "\n";
		s += label +"\n";
		s += image +"\n";
		s +=  String(maxplayer) +"\n";
		s += String(hopplayer) +"\n";
		s += String(mintarget)+"\n";
		s += String(maxtarget)+"\n";
		s += String(hoptarget)+"\n";
		s += String(targettype)+"\n";
		s += String(mintime)+"\n";
		s += String(maxtime)+"\n";
		s += String(hoptime)+"\n";
		s += String(weapon);		
		s += String(x2d0) + "\n";
		s += String(y2d0) + "\n";
		s += String(x3d1) + "\n";
		s += String(y3d1) + "\n";
		s += String(x2d1) + "\n";
		s += String(y2d1) + "\n";
		s += String(mapBlockID) + "\n";
		s += "----------------------------------------------------------";
		return s;
	}
	
	private function generatePlayersList () : Void {
		players = [];
		for (var i : Number = maxplayer; i >= minplayer ; i-= hopplayer) {
			players.push (i);
			//trace("aa" + i);
		}
	}
	private function generateTargetList () : Void {
		targets = [];
		for (var i : Number = mintarget; i <= maxtarget ; i += hoptarget) {
			targets.push (i);
		}
	}
	
	private function generateTimeList () : Void {
		times = [];		
		for (var i : Number = maxtime; i>= mintime ; i -= hoptime) {
			times.push (i);			
		}
	}
	
	public static function targetTypeString (targetType : Number) : String {
		switch (targetType) {
			case 1:
				return "LOBBY_WAITING_ROOM_LIFE";
			case 2:
				return "LOBBY_WAITING_ROOM_ROUND";
			case 3:
				return "LOBBY_WAITING_ROOM_SCORCE";
		}
		return "???";
	}
	public static function PlayerTypeString (player : Number) : String {
				return "LOBBY_WAITING_ROOM_DDL_PLAYER";
	}
}