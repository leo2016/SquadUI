class gfx.controls.SQUAD.NewStyle.Data.Skill  {
	private var _id:		Number;
	private var _classId:		Number;
	private var _name:		String;	
	private var _description:		String;
	private var _isActive:		Number;
	private var _level:		Number;
	private var _type:		Number;
	public var _propertys: 			Array;
	
	public function Skill(
		id: Number,
		classId: Number,
		name:String,
		description: String, 
		isActive: Number,
		propertys:Array
		) {
		_id = id;
		_classId = classId;
		_name = name;
		_description = description;
		_isActive = isActive;
		_propertys = propertys;
	}		
	
	public function get id () : Number {
		return _id;
	}
	public function get classId () : Number {
		return _classId;
	}
	
	public function get name () : String {
		return _name;
	}
	
	public function get description () : String {
		return _description;
	}
	
	public function get isActive () : Number {
		return _isActive;
	}
	
	public function get propertys () : Array {
		return _propertys;
	}
	
	public function toString () : String {
		var s : String = "";
		s += "ID = " + _id + "\n";
		s += "Class = " + _classId + "\n";
		s += "Name = " + _name + "\n";
		s += "Description = " + _description + "\n";
		s += "IsActive = " + _isActive + "\n";
		for (var x : String in _propertys) {
			s += "\t\t" + _propertys[x] + "\n";
		}
		return s;
	}
}