﻿import gfx.controls.SQUAD.NewStyle.Data.BaseItem;
import gfx.controls.SQUAD.NewStyle.Data.InventoryItem;

/**
 * Class hỗ trợ sắp xếp các vật phẩm trong danh sách theo 1 quy tắc nhất định
 */
class gfx.controls.SQUAD.NewStyle.Data.Sorter {
	private function Sorter () {}
	
	//Value for parameter "criteria" in main function "sort"
	public static var SORT_BY_NAME_ALPHA_BETA : 		String 	= "";// "Xếp giảm dần theo tên,không bị ảnh hưởng bởi class"; 
	public static var SORT_BY_NAME : 					String 	= "";// "Xếp tăng dần theo tên"; 
	public static var SORT_BY_NAME_DESC : 				String 	= "";// "Xếp giảm dần theo tên"; 
	public static var SORT_BY_DURABILITY_ASC : 			String 	= "";// "Xếp tăng dần theo độ bền"; 
	public static var SORT_BY_DURABILITY_DESC : 		String 	= "";// "Xếp giảm dần theo độ bền"; 
	public static var SORT_BY_EXPIRATION_ASC : 			String 	= "";// "Xếp tăng dần theo hạn sử dụng"; 
	public static var SORT_BY_EXPIRATION_DESC : 		String	= "";// "Xếp giảm dần theo hạn sử dụng"; 
	public static var SORT_BY_CASH_IN_GAME_ASC :		String 	= "";// "Xếp tăng dần theo giá GP";
	public static var SORT_BY_CASH_IN_GAME_DESC :		String 	= "";// "Xếp giảm dần theo giá GP";
	public static var SORT_BY_CASH_IN_LIFE_ASC :		String 	= "";// "Xếp tăng dần theo giá VCoin";
	public static var SORT_BY_CASH_IN_LIFE_DESC :		String 	= "";// "Xếp giảm dần theo giá VCoin";	
	public static var SORT_BY_SUITABLE_ADD_ON:			String	= "";// "Xếp theo độ phù hợp của phụ kiện";
	
	public static var m_eSortByClass :							Number= BaseItem.CLASS_NONE;		//Sort by class, choose BaseItem.CLASS_*
		
	public static var UNLOCKED_FIRST : 						Boolean = false;								//Unlocked item appears before locked item	
	
	private static var ORDER_12: 									Number = -1;									//Item 1 appears before item 2
	private static var ORDER_MESS:								Number = 0;										//Random order
	private static var ORDER_21: 									Number = 1;										//Item 1 appears after item 2
	
	public static function sort (data : Array, criteria : String) : Array {
		var cmpFunc : Function = undefined;
		switch (criteria) {
			case SORT_BY_NAME_ALPHA_BETA:
				cmpFunc = compareByNameAlphaBeta;
				break;
				
			case SORT_BY_NAME : 
				if (data[0] instanceof InventoryItem) {
					cmpFunc = compareByName;
				} else {//if (data[0] instanceof BaseItem) {
					cmpFunc = compareByNameOfBaseItem;					
				}
				break;
			case SORT_BY_NAME_DESC : 
				if (data[0] instanceof InventoryItem) {
					cmpFunc = compareByNameDESC;
				} else {//if (data[0] instanceof BaseItem) {
					cmpFunc = compareByNameOfBaseItemDESC;
				}
				break;
			case SORT_BY_DURABILITY_ASC : 
				cmpFunc = compareByDurabilityASC;
				break;
			case SORT_BY_DURABILITY_DESC : 
				cmpFunc = compareByDurabilityDESC;
				break;
			case SORT_BY_EXPIRATION_ASC : 
				cmpFunc = compareByExpirationASC;
				break;
			case SORT_BY_EXPIRATION_DESC : 
				cmpFunc = compareByExpirationDESC;
				break;
			case SORT_BY_CASH_IN_GAME_ASC : 
				cmpFunc = compareByGP_ASC;
				break;
			case SORT_BY_CASH_IN_GAME_DESC:
				cmpFunc = compareByGP_DESC;
				break;
			case SORT_BY_CASH_IN_LIFE_ASC:
				cmpFunc = compareByVCoin_ASC;
				break;
			case SORT_BY_CASH_IN_LIFE_DESC:
				cmpFunc = compareByVCoin_DESC;
				break;			
			case SORT_BY_SUITABLE_ADD_ON :
				cmpFunc = compareBySuitableOfAddOn;
				break;			
		}
		if (cmpFunc == undefined) {
			return data;
		} else {
			return data.sort (cmpFunc, Array.RETURNINDEXEDARRAY);
		}
	}
	
	private static function compareByName (item1 : InventoryItem, item2 : InventoryItem) : Number {
		//var result : Number = checkUnlockedFirst (item1, item2);
		//if (result != ORDER_MESS) {
		//	return result;
		//}
		
		//result = checkClass (item1, item2);
		//if (result != ORDER_MESS) {
		//	return result;
		//}
		
		return compareByNameOfBaseItem (item1.baseItem, item2.baseItem);
	}
	//------------------tuannq add ----------------------------------------------------
	//Hàm sắp xếp theo thứ tự A->Z . Dùng cho tab So sánh và khuyên dùng 
	private static function compareByNameAlphaBeta (item1 : BaseItem, item2 : BaseItem) : Number {		
		//var result : Number = checkUnlockedFirstForBaseItem (item1, item2);
		//if (result != ORDER_MESS) {
		//	return result;
		//}
		var result : Number = compareByCatalogueID(item1, item2);
		if (result != ORDER_MESS){
			return result;
		}
		else{	
			var name1 : String = item1.name ;
			var name2 : String = item2.name ;
			var length1 : Number = name1.length;
			var length2 : Number = name2.length;
			var length : Number;
			if (length1 > length2 ) {
				length = length2;
			} else {
				length = length1;
			}
			for (var i = 0; i < length; ++i ) {
				if (name1.charCodeAt(i) < name2.charCodeAt(i)) {
					return ORDER_12;
				} else if (name1.charCodeAt(i) > name2.charCodeAt(i)) {
					return ORDER_21;
				}
			}
			if (length1 == length2) {
				return ORDER_MESS;
			} else {
				if (length1 < length2) {
					return ORDER_12;
				} else {
					return ORDER_21;
				}
			}
		}
	}
	// ---------------------------------------tuannq end -----------------------------------------------
	
	private static function compareByNameOfBaseItem (item1 : BaseItem, item2 : BaseItem) : Number {		
		//var result : Number = checkUnlockedFirstForBaseItem (item1, item2);
		//if (result != ORDER_MESS) {
		//	return result;
		//}
		
		//result = checkClassForBaseItem (item1, item2);
		//if (result != ORDER_MESS) {
		//	return result;
		//}
		var result : Number = compareByCatalogueID(item1, item2);
		if (result != ORDER_MESS){
			return result;
		}
		else{
			var name1 : String = item1.name ;
			var name2 : String = item2.name ;
			var length1 : Number = name1.length;
			var length2 : Number = name2.length;
			var length : Number;
			if (length1 > length2 ) {
				length = length2;
			} else {
				length = length1;
			}
			for (var i = 0; i < length; ++i ) {
				if (name1.charCodeAt(i) < name2.charCodeAt(i)) {
					return ORDER_12;
				} else if (name1.charCodeAt(i) > name2.charCodeAt(i)) {
					return ORDER_21;
				}
			}
			if (length1 == length2) {
				return ORDER_MESS;
			} else {
				if (length1 < length2) {
					return ORDER_12;
				} else {
					return ORDER_21;
				}
			}
		}
		
	}
	
	private static function compareByNameDESC (item1 : InventoryItem, item2 : InventoryItem) : Number {
		//var result : Number = checkUnlockedFirst (item1, item2);
		//if (result != ORDER_MESS) {
		//	return result;
		//}
		//result = checkClass (item1, item2);
		//if (result != ORDER_MESS) {
		//	return result;
		//}
		return compareByNameOfBaseItemDESC (item1.baseItem, item2.baseItem);		
	}
	
	private static function compareByNameOfBaseItemDESC (item1 : BaseItem, item2 : BaseItem) : Number {
		//var result : Number = checkUnlockedFirstForBaseItem (item1, item2);
		//if (result != ORDER_MESS) {
		//	return result;
		//}
		//result = checkClassForBaseItem (item1, item2);
		//if (result != ORDER_MESS) {
		//	return result;
		//}
		var result : Number = compareByCatalogueID(item1, item2);
		if (result != ORDER_MESS){
			return result;
		}
		else{
			var name1 : String = item1.name ;
			var name2 : String = item2.name ;
			var length1 : Number = name1.length;
			var length2 : Number = name2.length;
			var length : Number;
			if (length1 > length2 ) {
				length = length2;
			} else {
				length = length1;
			}
			for (var i = 0; i < length; ++i ) {
				if (name1.charCodeAt(i) < name2.charCodeAt(i)) {
					return ORDER_21;
				} else if (name1.charCodeAt(i) > name2.charCodeAt(i)) {
					return ORDER_12;
				}
			}
			if (length1 == length2) {
				return ORDER_MESS;
			} else {
				if (length1 < length2) {
					return ORDER_21;
				} else {
					return ORDER_12;
				}
			}
		}
	}
	
	private static function compareByDurabilityASC (item1 : InventoryItem, item2 : InventoryItem) : Number {
		//var result : Number = checkUnlockedFirst (item1, item2);
		//if (result != ORDER_MESS) {
		//	return result;
		//}
		//result = checkClass (item1, item2);
		//if (result != ORDER_MESS) {
		//	return result;
		//}
		var result : Number = compareByCatalogueID(item1.baseItem, item2.baseItem);
		if (result != ORDER_MESS) {
			return result;
		}else {
			if (item1.durability < item2.durability) {
				return ORDER_12;
			} else if (item1.durability > item2.durability) {
				return ORDER_21;
			}
			return compareByNameOfBaseItem (item1.baseItem, item2.baseItem);
		}
		
	}

	private static function compareByDurabilityDESC (item1 : InventoryItem, item2 : InventoryItem) : Number {
		//var result : Number = checkUnlockedFirst (item1, item2);
		//if (result != ORDER_MESS) {
		//	return result;
		//}
		//result = checkClass (item1, item2);
		//if (result != ORDER_MESS) {
		//	return result;
		//}
		var result : Number = compareByCatalogueID(item1.baseItem, item2.baseItem);
		if (result != ORDER_MESS) {
			return result;
		}else {
			if (item1.durability < item2.durability) {
				return ORDER_21;
			} else if (item1.durability > item2.durability) {
				return ORDER_12;
			}
			return compareByNameOfBaseItem (item1.baseItem, item2.baseItem);
		}
	}
	
	private static function compareByExpirationASC (item1 : InventoryItem, item2 : InventoryItem) : Number {
		//var result : Number = checkUnlockedFirst (item1, item2);
		//if (result != ORDER_MESS) {
		//	return result;
		//}
		//result = checkClass (item1, item2);
		//if (result != ORDER_MESS) {
		//	return result;
		//}
		var result : Number = compareByCatalogueID(item1.baseItem, item2.baseItem);
		if (result != ORDER_MESS) {
			return result;
		}else {
			if (item1.remainingMinute <= 0 && item2.remainingMinute <= 0 ) {
				return compareByNameOfBaseItem(item1.baseItem, item2.baseItem);
			}
			else if (item1.remainingMinute <= 0  && item2.remainingMinute > 0) {
				return ORDER_21;
			} else if (item1.remainingMinute > 0 && item2.remainingMinute <= 0) {
				return ORDER_12;
			}
			else if (item1.remainingMinute > 0 && item2.remainingMinute > 0){
					if (item1.remainingMinute < item2.remainingMinute) {
						return ORDER_12;
					} else if (item1.remainingMinute > item2.remainingMinute) {
						return ORDER_21;
					}
			}
			return compareByNameOfBaseItem (item1.baseItem, item2.baseItem);
		}
	}
	
	private static function compareByExpirationDESC (item1 : InventoryItem, item2 : InventoryItem) : Number {
		//var result : Number = checkUnlockedFirst (item1, item2);
		//if (result != ORDER_MESS) {
		//	return result;
		//}
		//result = checkClass (item1, item2);
		//if (result != ORDER_MESS) {
		//	return result;
		//}
		var result : Number = compareByCatalogueID(item1.baseItem, item2.baseItem);
		if (result != ORDER_MESS) {
			return result;
		}else {
			if (item1.remainingMinute <= 0 && item2.remainingMinute <= 0 ) {
				return compareByNameOfBaseItem(item1.baseItem, item2.baseItem);
			}
			else if (item1.remainingMinute <= 0  && item2.remainingMinute > 0) {
				return ORDER_12;
			} else if (item1.remainingMinute > 0 && item2.remainingMinute <= 0) {
				return ORDER_21;
			}
			else if (item1.remainingMinute > 0 && item2.remainingMinute > 0){
				if (item1.remainingMinute < item2.remainingMinute) {
					return ORDER_21;
				} else if (item1.remainingMinute > item2.remainingMinute) {
					return ORDER_12;
				}
				return compareByNameOfBaseItem (item1.baseItem, item2.baseItem);
			}
		}
	}
	
	private static function compareByGP_ASC (item1 : BaseItem, item2 : BaseItem) : Number {
		//var result : Number = checkUnlockedFirstForBaseItem (item1, item2);
		//if (result != ORDER_MESS) {
		//	trace ("ORDER_MESS 1");
		//	return result;
		//}
		//result = checkClassForBaseItem (item1, item2);
		//if (result != ORDER_MESS) {
		//	trace ("ORDER_MESS 2");
		//	return result;
		//}	
	
		var result : Number = compareByCatalogueID(item1, item2);
		if (result != ORDER_MESS){
			return result;
		}
		else
		{
			var p1 : Number = Number(item1.howMuch(item1.getMinRentingDays(), true));
			var p2 : Number = Number(item2.howMuch(item2.getMinRentingDays(), true));
			
			//trace("item1 = " + item1.id + ":" + p1 + " - item2 = " + item2.id + ":" + p2);
					
			if (p1 == -1 || p1 == NaN) {			
				return ORDER_21;
			} else if (p2 == -1 || p2 == NaN) {			
				return ORDER_12;
			} else if (p1 < p2) {			
				return ORDER_12;
			} else if (p1 > p2) {
				return ORDER_21;
			} else {			
				return compareByNameOfBaseItem (item1, item2);
			}
		}
	}
	
	private static function compareByGP_DESC (item1 : BaseItem, item2 : BaseItem) : Number {
		//var result : Number = checkUnlockedFirstForBaseItem (item1, item2);
		//if (result != ORDER_MESS) {
		//	return result;
		//}
		//result = checkClassForBaseItem (item1, item2);
		//if (result != ORDER_MESS) {
		//	return result;
		//}
		var result : Number = compareByCatalogueID(item1, item2);
		if (result != ORDER_MESS){
			return result;
		}
		else{
			var p1 : Number = Number(item1.howMuch(item1.getMinRentingDays(), true));
			var p2 : Number = Number(item2.howMuch(item2.getMinRentingDays(), true));
			if (p1 == -1 || p1 == NaN) {			
				return ORDER_21;
			} else if (p2 == -1 || p2 == NaN) {			
				return ORDER_12;
			} else if (p1 > p2) {			
				return ORDER_12;
			} else if (p1 < p2) {
				return ORDER_21;
			} else {			
				return compareByNameOfBaseItem (item1, item2);
			}
		}
	}
	
	private static function compareByVCoin_ASC (item1 : BaseItem, item2 : BaseItem) : Number {		
		//var result : Number = checkUnlockedFirstForBaseItem (item1, item2);
		//if (result != ORDER_MESS) {
		//	return result;
		//}		
		//result = checkClassForBaseItem (item1, item2);
		//if (result != ORDER_MESS) {
		//	return result;
		//}		
		var result : Number = compareByCatalogueID(item1, item2);
		if (result != ORDER_MESS){
			return result;
		}
		else{
			var p1 : Number = Number(item1.howMuch(item1.getMinRentingDays(), false));
			var p2 : Number = Number(item2.howMuch(item2.getMinRentingDays(), false));		
			if (p1 == -1 || p1 == NaN) {
				return ORDER_21;
			} else if (p2 == -1 || p2 == NaN) {	
				return ORDER_12;
			} else if (p1 < p2) {
				return ORDER_12;
			} else if (p1 > p2) {
				return ORDER_21;
			} 
		}
	}
	
	private static function compareByVCoin_DESC (item1 : BaseItem, item2 : BaseItem) : Number {
		//var result : Number = checkUnlockedFirstForBaseItem (item1, item2);
		//if (result != ORDER_MESS) {
		//	return result;
		//}
		//result = checkClassForBaseItem (item1, item2);
		//if (result != ORDER_MESS) {
		//	return result;
		//}
		var result : Number = compareByCatalogueID(item1, item2);
		if (result != ORDER_MESS){
			return result;
		}
		else{
		
			var p1 : Number = Number(item1.howMuch(item1.getMinRentingDays(), false));
			var p2 : Number = Number(item2.howMuch(item2.getMinRentingDays(), false));
			if (p1 == -1 || p1 == NaN) {			
				return ORDER_21;
			} else if (p2 == -1 || p2 == NaN) {			
				return ORDER_12;
			} else if (p1 > p2) {			
				return ORDER_12;
			} else if (p1 < p2) {
				return ORDER_21;
			} else {			
				return compareByNameOfBaseItem (item1, item2);
			}
		}
	}	
	
	private static function checkUnlockedFirst (item1 : InventoryItem, item2 : InventoryItem) : Number {
		if (UNLOCKED_FIRST) {
			if (item1.unlocked && !item2.unlocked) {
				return ORDER_12;
			} else if (!item1.unlocked && item2.unlocked) {
				return ORDER_21;
			}
		}
		return ORDER_MESS;
	}
	
	private static function checkUnlockedFirstForBaseItem (item1 : BaseItem, item2 : BaseItem) : Number {
		if (UNLOCKED_FIRST) {
			if (item1.allowBuy && !item2.allowBuy) {
				return ORDER_12;
			} else if (!item1.allowBuy && item2.allowBuy) {
				return ORDER_21;
			}
		}
		return ORDER_MESS;
	}
	
	private static function checkClass (item1 : InventoryItem, item2 : InventoryItem) : Number {
		return checkClassForBaseItem (item1.baseItem, item2.baseItem);
	}
	
	private static function checkClassForBaseItem (item1 : BaseItem, item2 : BaseItem) : Number {
		if (item1.classId == m_eSortByClass) {
			if (item2.classId == m_eSortByClass) {
				return ORDER_MESS;
			} else {
				return ORDER_12;
			}
		} else if (item1.classId == BaseItem.CLASS_NONE) {
			if (item2.classId == m_eSortByClass) {
				return ORDER_21;
			} else if (item2.classId == BaseItem.CLASS_NONE) {
				return ORDER_MESS;
			} else {
				return ORDER_12;
			}
		} else {
			if (item2.classId == m_eSortByClass) {
				return ORDER_21;
			} else if (item2.classId == BaseItem.CLASS_NONE) {
				return ORDER_21;
			} else {
				return ORDER_MESS;
			}
		}		
	}
	
	private static function compareBySuitableOfAddOn (item1 : InventoryItem, item2 : InventoryItem) : Number {		
		if (item1.isDisabled && !item2.isDisabled) {
			return ORDER_21;
		} else if (!item1.isDisabled && item2.isDisabled) {
			return ORDER_12;
		}
		if (item1.inClass == InventoryItem.NOT_USE && item2.inClass != InventoryItem.NOT_USE) {
			return ORDER_12;
		} else if (item1.inClass != InventoryItem.NOT_USE && item2.inClass == InventoryItem.NOT_USE) {
			return ORDER_21;
		}
		return compareByName (item1, item2);		
	}
	
	private static function compareByCatalogueID(item1 : BaseItem, item2 : BaseItem) : Number {
		var p1 : Number = getPriorityByCatalogue (item1);
		var p2 : Number = getPriorityByCatalogue (item2);
		if (p1 < p2) {
			return ORDER_12;
		} else if (p1 > p2) {
			return ORDER_21;
		} else {
			return ORDER_MESS;
		}
	}
	
	private static function getPriorityByCatalogue (item : BaseItem) : Number {
		switch (item.catalogue) {
			case 35:
			case 36: 
			case 37:
			case 38:
			case 39:{
				return 1;
			}
			case 12 : {
				return 2;
			}
			case 13 : {
				return 3;
			}
			default:{
				return 4;
			}
			
		}
	}
}