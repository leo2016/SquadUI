class gfx.controls.SQUAD.NewStyle.Data.GearProperty {
	private var _id:		Number;
	private var _name:		String;	
	private var _value:		Number;
	public function GearProperty(
		id: Number,
		name:String,
		value: Number
		) {
		_id = id;
		_name = name;
		_value = value;
	}		
	
	public function get id () : Number {
		return _id;
	}
	public function get name () : String {
		return _name;
	}
	public function get values () : Number {
		return _value;
	}
	public function toString () : String {
		var s : String = "";
		s += "ID = " + _id + "\n";
		s += "Value = " + _value + "\n";
		s += "Name = " + _name + "\n";
		return s;
	}
}