/**
 * Class chứa các thông số của súng
 */
class gfx.controls.SQUAD.NewStyle.Data.GunStats {
	public var power;
	public var speed;
	public var recoil;
	public var accuracy;
	public var reload;
	public var ammos;
	public var launcher;
	public var weight;
	public var maxAmmos;
	
	private static var DEFAULT_EXTRA_CLIPS : Number = 3;
	
	public function GunStats (
		power 		: Number, 
		speed 		: Number, 
		recoil 		: Number, 
		accuracy 	: Number, 
		reload 		: Number, 
		ammos 		: Number, 
		launcher 	: Boolean, 
		weight 		: Number
	) {
		this.power 		= power;
		this.speed 		= speed;
		this.recoil 	= recoil;
		this.accuracy 	= accuracy;
		this.reload 	= reload;
		this.ammos 		= ammos;
		this.launcher 	= launcher;
		this.weight 	= weight;
		
		this.maxAmmos 	= ammos * DEFAULT_EXTRA_CLIPS;
	}
}