﻿/**
 * Class lưu trữ dữ liệu chung của tất cả các loại vật phẩm
 */
class gfx.controls.SQUAD.NewStyle.Data.BaseItem {
	private var _id: 					Number;
	private var _catalogue: 			Number;
	private var _detailTable:			Number;	
	private var _name: 					String;
	private var _shortDescription:		String;
	private var _description: 			String;
	private var _weight: 				Number;
	private var _isExtendAllowed:		Boolean;
	private var _isBuyAllowed: 			Boolean;
	private var _isSellAllowed:			Boolean;
	private var _isGiveAllowed:			Boolean;
	private var _isRepairAllowed:		Boolean;
	public var _image:					String;
	private var _unlockCondition:		Number;
	private var _unlockRequirement: 	Number;
	private var _class:					Number;
	private var _model:					String;
	private var _prices:				Array;
	private var _priceSell:				Number;
	private var _commingSoon:			Boolean;
	
	/**
	 * Có nằm trong danh sách các vật phẩm hot không
	 */
	public var hot :							Boolean;
	
	/**
	 * Có nằm trong danh sách các vật phẩm đang được khuyến mại không
	 */
	public var promotion :				Boolean;
	
	// disable item
	public var isDisabled :				Boolean;
	
	/**
	 * Đánh dấu thuê vĩnh viễn
	 */
	public static var RENT_FOREVER:		Number = -1;
	
	/**
	 * Vật phẩm chuyên dùng cho class nào
	 */
	public static var CLASS_NONE:		Number = 0;
	public static var CLASS_TANKER :	Number = 1;
	public static var CLASS_SCOUT:		Number = 2;
	public static var CLASS_SNIPER :	Number = 3;
	
	/**
	 * Tag để localize cho tên của các class
	 */
	public static var CLASS_NAME_TANKER : 	String 	= "CLASS_NAME_TANKER";
	public static var CLASS_NAME_SCOUT : String		= "CLASS_NAME_SCOUT";
	public static var CLASS_NAME_SNIPER : String		= "CLASS_NAME_SNIPER";
	
	/**
	 * Hàm tạo.
	 * BaseItem chỉ lưu trữ các thông tin chung nhất của vật phẩm (đọc từ Common.xml), do đó với mỗi loại vật phẩm,
	 * cần bổ sung các thông tin khác nhau thì mới tạo nên thông tin hoàn chỉnh của vật phẩm đó.
	 * 
	 * @param	id							Mã vật phẩm
	 * @param	catalogue 				Mã Catalogue của vật phẩm
	 * @param	detailTable			Enum bảng thông tin chi tiết (dùng để xác định loại vật phẩm)	 
	 * @param	name					Tên vật phẩm
	 * @param	shortDescription	Mô tả ngắn vật phẩm
	 * @param	description			Miêu tả vật phẩm
	 * @param	weight					Cân nặng
	 * @param	isExtendAllowed	Có được gia hạn không
	 * @param	isBuyAllowed			Có được mua không
	 * @param	isSellAllowed			Có được bán không
	 * @param	isGiveAllowed		Có được tặng không
	 * @param	isRepairAllowed		Có được sửa không
	 * @param	image					Ảnh vật phẩm
	 * @param	unlockCondition		Loại điều kiện để mở khóa vật phẩm
	 * @param	unlockRequirement	Yêu cầu cần đạt được để mở khóa vật phẩm	
	 * @param	classId					Class mà vật phẩm này hỗ trợ tốt nhất 
	 * @param	model					Tên mô hình 3D của vật phẩm
	 */
	/**
	 * Hàm tạo.
	 * BaseItem chỉ lưu trữ các thông tin chung nhất của vật phẩm (đọc từ Common.xml), do đó với mỗi loại vật phẩm,
	 * cần bổ sung các thông tin khác nhau thì mới tạo nên thông tin hoàn chỉnh của vật phẩm đó.
	 * 
	 * @param	id							Mã vật phẩm
	 * @param	catalogue 				Mã Catalogue của vật phẩm
	 * @param	detailTable			Enum bảng thông tin chi tiết (dùng để xác định loại vật phẩm)	 
	 * @param	name					Tên vật phẩm
	 * @param	shortDescription	Mô tả ngắn vật phẩm
	 * @param	description			Miêu tả vật phẩm
	 * @param	weight					Cân nặng
	 * @param	isExtendAllowed	Có được gia hạn không
	 * @param	isBuyAllowed			Có được mua không
	 * @param	isSellAllowed			Có được bán không
	 * @param	isGiveAllowed		Có được tặng không
	 * @param	isRepairAllowed		Có được sửa không
	 * @param	image					Ảnh vật phẩm
	 * @param	unlockCondition		Loại điều kiện để mở khóa vật phẩm
	 * @param	unlockRequirement	Yêu cầu cần đạt được để mở khóa vật phẩm	
	 * @param	classId					Class mà vật phẩm này hỗ trợ tốt nhất	 
	 */
	public function BaseItem(
		id: 					Number,
		catalogue: 				Number,
		detailTable: 			Number,		
		name: 					String,
		shortDescription: 		String,
		description: 			String,
		weight: 				Number,
		isExtendAllowed:		Boolean,
		isBuyAllowed: 			Boolean,
		isSellAllowed: 			Boolean,
		isGiveAllowed: 			Boolean, 
		isRepairAllowed:		Boolean,
		image:					String, 
		unlockCondition:		Number, 
		unlockRequirement:	    Number, 
		classId:				Number, 
		model:					String,
		priceSell:				Number,
		commingSoon:			Boolean
		
		) {
			
		_id 							= id;
		_catalogue 				= catalogue;
		_detailTable 				= detailTable;		
		_name 						= name;
		_shortDescription 		= shortDescription
		_description 				= description;
		_weight 					= weight;
		_isExtendAllowed 		= isExtendAllowed == true;
		_isBuyAllowed 			= isBuyAllowed == true;
		_isSellAllowed 			= isSellAllowed == true;
		_isGiveAllowed 			= isGiveAllowed == true;
		_isRepairAllowed		= isRepairAllowed == true;
		_image 						= image;
		_unlockCondition 		= unlockCondition;		
		_unlockRequirement 	= unlockRequirement;
		_class						= Number(classId);
		_model						= model;
		_priceSell					= priceSell;
		_commingSoon				= commingSoon;
		_prices 						= undefined;
		
		hot							= false;
		promotion				= false;
	}
	/**
	 * Tạo bản sao các thuộc tính chung của vật phẩm (sử dụng cho việc mở rộng thêm thông tin của từng loại vật phẩm)
	 *
	 * @param	otherItem Vật phẩm muốn sao chép thông tin
	 */
	public function copyFrom(otherItem: BaseItem):Void {
		_id 							= otherItem.id;
		_catalogue 				= otherItem.catalogue;
		_detailTable 				= otherItem.detailTable;
		_name 						= otherItem.name;
		_shortDescription 		= otherItem.shortDescription;
		_description 				= otherItem.description;
		_weight 					= otherItem.weight;
		_isExtendAllowed 		= otherItem.allowExtend;
		_isBuyAllowed 			= otherItem.allowBuy;
		_isSellAllowed 			= otherItem.allowSell;
		_isGiveAllowed 			= otherItem.allowGive;
		_isRepairAllowed		= otherItem.allowRepair;
		_image 						= otherItem.image;
		_unlockCondition 		= otherItem.unlockCondition;
		_unlockRequirement 	= otherItem.unlockRequirement;
		_class						= otherItem.classId;
		_model						= otherItem.model;
		_priceSell					= otherItem.priceSell;
		_commingSoon					= otherItem.commingSoon;
		_prices 					= otherItem.prices;
	}
	
	/**
	 * @return Mã vật phẩmvne
	 */
	public function get id(): Number {
		return _id;
	}
	
	/**
	 * @return Mã Catalogue của vật phẩm
	 */
	public function get catalogue():Number {
		return Number(_catalogue);
	}
	
	/**
	 * @return	Enum bảng thông tin chi tiết (dùng để xác định loại vật phẩm)
	 */
	public function get detailTable () : Number {
		return _detailTable;
	}
	
	/**
	 * @return Mô tả ngắn về vật phẩm
	 */
	public function get shortDescription():String {
		return _shortDescription;
	}
	
	/**
	 * @return Tên vật phẩm
	 */
	public function get name():String {
		return _name;
	}
	
	/**
	 * @return Miêu tả vật phẩm
	 */
	public function get description():String {
		return _description;
	}
	
	/**
	 * @return Cân nặng
	 */
	public function get weight():Number {
		return Number(_weight);
	}
	
	/**
	 * @return Có được gia hạn không
	 */
	public function get allowExtend():Boolean {
		return _isExtendAllowed;
	}
	
	/**
	 * @return Có được mua không
	 */
	public function get allowBuy():Boolean {
		return _isBuyAllowed;
	}
	
	/**
	 * @return Có được bán không
	 */
	public function get allowSell():Boolean {
		return _isSellAllowed;
	}
	
	/**
	 * @return Có được tặng không
	 */
	public function get allowGive():Boolean {
		return _isGiveAllowed;
	}
	
	/**
	 * @return Có được sửa không
	 */
	public function get allowRepair () : Boolean {
		return _isRepairAllowed;
	}
	
	/**
	 * @return Ảnh vật phẩm
	 */
	public function get image():String {
		return _image;
	}
	public function setImage(image):Void{
		_image = image;
	}
	/**
	 * @return Tên mô hình 3D của vật phẩm
	 */
	public function get model():String {
		return _model;
	}
	
	/**
	 * @return Loại điều kiện để mở khóa vật phẩm
	 */
	public function get unlockCondition():Number {		
		return _unlockCondition;
	}
	
	/**
	 * @return yêu cầu cần đạt được để mở khóa vật phẩm
	 */
	public function get unlockRequirement():Number {
		return Number(_unlockRequirement);
	}
	
	/**
	 * @return Class mà vật phẩm này hỗ trợ tốt nhất
	 */
	public function get classId():Number {
		return _class;
	}
	/**
	 * @return priceSell giá bán của vật phẩm này
	 */
	public function get priceSell():Number {
		return Number(_priceSell);
	}
	/**
	 * @return commingSoon san pham sap ra mắt
	 */
	public function get commingSoon():Boolean {
		return _commingSoon;
	}
	
	/**
	 * Gắn kèm giá vật phẩm
	 *
	 * @param	prices 1 ma trận 2 chiều nx2 chứa giá vật phẩm
	 * 		Với chỉ số hàng là số ngày thuê (mua đứt thì chỉ số = "-1")
	 * 		Chỉ số cột là "GP", "VCOIN"
	 * 
	 * XML như sau:
	 * <ITEM_PRICE>
			<ID>22</ID>
			<PRICES>
				<PRICE>
					<GP>58000</GP>
					<VCOIN>58</VCOIN>
				</PRICE>
				<PRICE>
					<RENTING_DAYS>7</RENTING_DAYS>
					<GP>19000</GP>
					<VCOIN>0</VCOIN>
				</PRICE>
				<PRICE>
					<RENTING_DAYS>3</RENTING_DAYS>
					<GP>0</GP>
					<VCOIN>30</VCOIN>
				</PRICE>
			</PRICES>
		</ITEM_PRICE>
	 *
	 * Chẳng hạn để lấy giá GP khi thuê 3 ngày thì dùng prices["3"]["GP"]
	 * Chẳng hạn để lấy giá VCOIN khi mua đứt thì dùng prices["-1"]["VCOIN"]
	 */
	public function attachPrices (prices: Array): Void {
		_prices = prices;
		/*
		for (var x:String in _prices) {
			for (var y:String in _prices[x]) {
				if (parseInt(_prices[x][y]) == 0) {
					_prices[x][y] = undefined;
				}
			}
		}
		*/
	}
	
	/**
	 * @return Mảng chứa giá bán vật phẩm
	 */
	public function get prices (): Array {
		return _prices;
	}
	
	/**
	 * Truy vấn giá tiền của vật phẩm
	 *
	 * @param	rentingDays	Số ngày thuê (nếu mua đứt, có thể truyền vào tham số BaseItem.RENT_FOREVER)
	 * @param	isGP				Giá truy vấn có phải tính bằng GP không
	 * 
	 * @return		Giá tiền theo ngày thuê và đơn vị tính truyền vào
	 */
	public function howMuch (rentingDays: Number, isGP: Boolean): Number {
		var row: String = "" + rentingDays;
		if (_prices[row] == undefined || _prices[row] == null) {
			return undefined;
		}
		
		var col: String;
		if (isGP) {
			col = "GP";
		} else {
			col = "VCOIN";
		}		
		var price: Number = parseInt(_prices[row][col]);
		
		if (price == undefined || price == null) {
			return undefined;
		} else if (price <= 0){
			return undefined;
		} else {
			return price;
		}
	}
	
	/**
	 * @return Mô tả vật phẩm
	 */
	public function toString():String {
		var s:String = "";
		s += "BASE ITEM:\n";
		s += "Name:" + _name + "\n";
		s += "commingSoon:"+_commingSoon+"\n";
		for (var x : String in this) {
			if (typeof(this[x]) != "Function") {
				s += "\t" + x + " = " + this[x] + "\n";
			}
		}
		if (_prices != undefined) {
			for (var x:String in _prices) {
				s += "\t\tRenting days = " + x + ", GP = " + _prices[x]["GP"] + ", Vcoin = " + _prices[x]["VCOIN"] +"\n";
			}
		}
		return s;
	}
	
	/**
	 * Lấy số ngày thuê nhỏ nhất của vật phẩm	 
	 * 
	 * @return
	 * 		Nếu vật phẩm không có giá tiền, trả về 0
	 * 		Nếu vật phẩm chỉ có thể mua mà không thể thuê, trả về BaseItem.RENT_FOREVER
	 * 		Nếu không phải các trường hợp trên, trả về số ngày thuê nhỏ nhất của vật phẩm
	 */
	public function getMinRentingDays () : Number {
		if (_prices != undefined) {
			var min : Number = Number.MAX_VALUE;
			var canBuy : Boolean = false;
			for (var x:String in _prices) {
				var y : Number = Number(x);
				if (y == -1) {
					canBuy = true;
				} else if (y != NaN) {
					if (min > y) {
						min = y;
					}
				}
			}
			if (min == Number.MAX_VALUE) {//item doesn't have price or just can be bought, not rented
				if (canBuy) {
					return RENT_FOREVER;
				} else {
					return 0;
				}
			}
			return min;
		} else {
			return 0;
		}
	}
	
	/**
	 * Lấy danh sách số ngày thuê cho phép của vật phẩm
	 */
	public function get rentingDays () : Array {
		//Collect
		var result : Array = [];
		for (var x:String in _prices) {
			result.push (parseInt(x));
		}
		//Sort
		var l : Number = result.length;
		var t : Number;
		for (var i : Number = 0; i < l-1 ; ++i ) {
			for (var j : Number = i + 1;  j < l ; ++j ) {				
				if (result[i] > result[j] && result[j] != RENT_FOREVER) {					
					t = result[i];
					result[i] = result[j];
					result[j] = t;
				}
			}
		}
		return result;
	}
}