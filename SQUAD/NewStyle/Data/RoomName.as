﻿/**
 * ...
 * @author ...
 */

class  gfx.controls.SQUAD.NewStyle.Data.RoomName
{
	public var id		:Number;
	public var label	:String;
	
	public function RoomName(
	id:Number,
	name:String
	)
	{
		this.id = id;
		this.label = name;
	}
	public function toString () : String {
		var s : String = "";
		s = label + "\n";
		s += "\tRoomName:\n";
		for (var x : String in label) {
			s += "\t\t" +label [x] + "\n";
		}
		return s;
	}
}