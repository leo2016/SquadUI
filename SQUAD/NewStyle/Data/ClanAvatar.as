﻿class gfx.controls.SQUAD.NewStyle.Data.ClanAvatar  {
	private var _id:		Number;
	private var _image:		String;
	private var _name:		String;	
	
	public function ClanAvatar(
		id: Number,
		name:String,
		image: String
		) {
		_id = id;
		_name = name;
		_image = image;
	}		
	
	public function get id () : Number {
		return _id;
	}
	
	public function get name () : String {
		return _name;
	}
	
	public function get image () : String {
		return _image;
	}
	
	public function toString () : String {
		var s : String = "";
		s += "ID = " + _id + "\n";
		s += "Name = " + _name + "\n";
		return s;
	}
}