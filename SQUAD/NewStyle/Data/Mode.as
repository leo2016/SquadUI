﻿class gfx.controls.SQUAD.NewStyle.Data.Mode {
	public var label : 			String;
	public var id : 				Number;
	public var maps : 			Array;
	
	
	public function Mode (
		id : Number, 
		name : String, 
		maps : Array
		
	) {
		this.id = id;
		this.label = name;
		this.maps = maps
		
	}		
	
	public function toString () : String {
		var s : String = "";
		s = id + "\n";
		s += label + "\n";
		s += "\tMaps:\n";
		for (var x : String in maps) {
			s += "\t\t" + maps[x] + "\n";
		}
		return s;
	}
		public static function findModeyId (modes : Array, id : Number) : Mode {
		for (var x : String in modes) {
			if (modes[x].id == id) {
				return modes[x];
			}
		}
		return null;
	}
}