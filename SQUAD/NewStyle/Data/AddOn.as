﻿import gfx.controls.SQUAD.NewStyle.Data.BaseItem;
import gfx.controls.SQUAD.NewStyle.Data.Catalogue;
import gfx.controls.SQUAD.NewStyle.Data.Gun;
import gfx.controls.SQUAD.NewStyle.XML.Localizer;

/**
 * Class lưu trữ dữ liệu về PHỤ KIỆN
 */
class gfx.controls.SQUAD.NewStyle.Data.AddOn extends BaseItem {
	private var _properties: Array;
	private var _holders: Array;		
	private static var LOCALIZATION_VALUE_PARAM : String = "**VALUE**";
	
	//----------------Danh sách các thuộc tính của add-ons	
	public static var AOP_AMMO_BASE_DAMAGE :					String					= "AmmoBaseDamage";
	public static var AOP_AMMO_RADIUS :							String					= "AmmoRadius";
	public static var AOP_AMMO_LENGTH :							String					= "AmmoLength";
	public static var AOP_AMMO_ARMOR_AFFECT :				String					= "AmmoArmorAffect";
	public static var AOP_AMMO_FORCE :								String					= "AmmoForce";
	public static var AOP_AMMO_SPEED :								String					= "AmmoSpeed";
	
	public static var AOP_CLIP_EXTRA_AMMOS :						String					= "ClipExtraAmmos";
	public static var AOP_CLIP_RELOADING_SPEED_AFFECT:		String					= "ClipReloadingSpeedAffect";
	
	public static var AOP_SCOPE_ZOOM_LEVEL :					String					= "ScopeZoomLevel";
	public static var AOP_SCOPE_DELAY :								String					= "ScopeDelay";
	public static var AOP_SCOPE_WATER_PROOF :					String					= "ScopeWaterProof";
	public static var AOP_SCOPE_FOG_PROOF :						String					= "ScopeFogProof";
	public static var AOP_SCOPE_BRIGHER_VIEW :					String					= "ScopeBrighterView";
	public static var AOP_SCOPE_MAGNIFICATION :				String					= "ScopeMagnification";//array
	public static var AOP_SCOPE_ACCURACY:							String					= "ScopeAccuracy";
	public static var AOP_SCOPE_MOVEMENT_SPEED:			String					= "ScopeMovementSpeed";
	public static var AOP_SCOPE_RATE_OF_FIRE:						String					= "ScopeRateOfFire";
	
	public static var AOP_BARREL_SOUND_REDUCE :				String					= "BarrelSoundReduction";
	public static var AOP_BARREL_DAMAGE_AFFECT :				String					= "BarrelDamage";
	public static var AOP_BARREL_RATE_OF_FIRE:					String					= "BarrelRateOfFire";
	
	public static var AOP_SIDE_FLASH_LIGHT:							String					= "SideFlashLight";
	public static var AOP_SIDE_EFFECT_RANGE:						String					= "SideEffectRange";
	public static var AOP_SIDE_LASER_RED:							String					= "SideLaserRed";
	public static var AOP_SIDE_LASER_BLUE:							String					= "SideLaserBlue";
	public static var AOP_SIDE_GRENADE_LAUNCHER:			String					= "SideGrenadeLauncher";
	public static var AOP_SIDE_GRENADE_LAUNCHER_DELAY:	String					= "SideGrenadeLauncherDelay";	
	
	/**
	 * Hàm tạo
	 *
	 * @param	baseItem BaseItem với dữ liệu đọc từ Common.xml
	 * @param	properties	Danh sách các thuộc tính của phụ kiện (mỗi phần tử có { property, value } )
	 * @param	holders	Danh sách các Catalogue hoặc vật phẩm mà phụ kiện hỗ trợ (mỗi phần tử có { id, isCatalogue, isIncluded } )
	 */
	public function AddOn(
		baseItem: BaseItem,
		properties: Array,
		holders: Array) {
		super.copyFrom(baseItem);
		_properties = properties;
		_holders = holders;		
	}
	
	/**
	 * @return Danh sách các thuộc tính của vật phẩm
	 */
	public function get properties():Array {
		return _properties;
	}
	
	/**
	 * @return Danh sách các Catalogue hoặc vật phẩm mà phụ kiện hỗ trợ
	 */
	public function get holders():Array {
		return _holders;
	}
	
	/**
	 * Kiểm tra xem phụ kiện này có thể lắp vào loại súng nào đó không.
	 * Chú ý: Phải kiểm tra 2 chiều:
	 * 	+	Phụ kiện có hỗ trợ súng không:
	 * 		+ Đầu tiên phải kiểm tra súng có nằm trong danh sách không hỗ trợ không?
	 * 			+ Nếu có thì return false
	 * 			+ Nếu không thì tiếp tục kiểm tra súng có nằm trong danh sách được hỗ trợ không
	 * 				+ Nếu không thì return false
	 * 				+ Nếu có thì tiếp tục
	 * 					+	Súng có vị trí để lắp loại phụ kiện đó không
	 * 						+ Nếu có thì return true
	 * 						+ Nếu không thì return false
	 *
	 * @param	gun				Súng
	 * @param	catalogues	Danh sách các catalogue
	 * @return		true = Có thể lắp, false = Không thể lắp
	 */
	public function canAdaptIntoGun (gun: Gun, catalogues: Array):Boolean {
		var gunId: Number = gun.id ;
		var gunCatalogue: Catalogue = catalogues [gun.catalogue];
		var thisCatalogueName : String = catalogues [_catalogue].name;
		//trace ("Check gun " + gun.name + " with add-on " + name);
		//Kiểm tra xem súng có nằm trong danh sách không được hỗ trợ không
		//trace("_holders:"+_holders.toString());
		for (var x:String in _holders) {
			var holder: Object = _holders[x];
			//trace("0000holder.isIncluded:"+holder.isIncluded);
			if (!holder.isIncluded) {
				if (holder.isCatalogue) {
					if ( gunCatalogue.isDescendantOf (holder.id) ) {
						return false;
					}
				} else {
					if ( gunId == holder.id ) {
						return false;
					}
				}
			}
		}
		//trace("Pass Khong nam trong danh sach khong duoc ho tro "+gun.name +"----"+_name);
		//trace (gun.name + " isn't in the excluding list of " + _name + "!");
		
		//Kiểm tra xem súng có nằm trong danh sách được hỗ trợ không
		for (var x:String in _holders) {
			var holder: Object = _holders[x];
			if (holder.isIncluded) {
				//trace("1111holder.isIncluded:"+holder.isIncluded);
				//Nếu có trong danh sách được hỗ trợ, kiểm tra súng có slot để lắp add-on này không
				if (holder.isCatalogue) {
					//trace("2222holder.isCatalogue:"+holder.isCatalogue);
					//trace ("\tGun catalogue = " + gunCatalogue.name + " -> is descendant of " + holder.id + " = " + gunCatalogue.isDescendantOf (holder.id));
					//trace("gunCatalogue.tags:" + gunCatalogue.tags);
					//trace("holder.id:"+holder.id);
					if ( gunCatalogue.isDescendantOf (holder.id) ) {
						//trace("33333.isCatalogue:"+thisCatalogueName);
						return gun.hasSlot (thisCatalogueName);
					}
				} else {
					//trace ("\tGun id = " + gunId + " == " + holder.id + " -> " + (gunId == holder.id));
					if ( gunId == holder.id ) {
						//trace("44444.isCatalogue:");
						return gun.hasSlot (thisCatalogueName);
					}
				}
			}
		}
		//trace (gun.name + " isn't in the including list of " + _name + "!");
		return false;
	}
	
	/**
	 * Lấy giá trị của một thuộc tính của add-on
	 * 
	 * @param	propertyName	tên thuộc tính (nên dùng các biến public static ở phía trên, dạng AOP_...)
	 * 
	 * @return 	Giá trị dạng Number của thuộc tính (nếu có) hoặc 0 (nếu không có)
	 */
	public function property (propertyName : String) : Number {
		for (var x: String in _properties) {
			if (_properties[x].property == propertyName) {
				return parseFloat(_properties[x].value);
			}
		}
		return 0;
	}
	
	/**
	 * Lấy danh sách các nấc phóng đại của add-on ống ngắm
	 */
	public function get magnificationLevels () : Array {
		var d : Array = [];
		for (var x: String in _properties) {
			if (_properties[x].property == AOP_SCOPE_MAGNIFICATION) {
				d.push(parseFloat(_properties[x].value));
			}
		}		
		return d;
	}	
	
	/**
	 * @return Mô tả phụ kiện
	 */
	public function toString():String {
		var s:String = super.toString();
		for (var x:String in _properties) {
			s += "\t" + _properties[x].property + " = " + _properties[x].value + "\n";
		}
		for (var x:String in _holders) {
			s += "\tHolder = " + _holders[x].id + " (Catalogue: " + _holders[x].isCatalogue + ", Included: " + _holders[x].isIncluded + ")\n";
		}
		return s;
	}
	
	/**
	 * Tự sinh ra thông tin mô tả về add-on
	 * 
	 * @param	localizer		Localizer dùng để "localize" các thông tin của add-on
	 * @param	catalogues	Danh sách các catalogues vật phẩm (dùng để phân loại add-on)
	 * 
	 * @return		Một chuỗi lưu mô tả của add-on
	 */
	public function extraInformation (localizer : Localizer, catalogues : Array) : String {
		var content : String = "";
		var catalogueOfAddOn = catalogues[catalogue];
		if (catalogueOfAddOn instanceof Catalogue) {
			switch (catalogues[catalogue].name) {
				case Catalogue.CTL_BULLET: {
					content = extraInformationOfBullet (localizer);
					break;
				}				
				case Catalogue.CTL_CLIP: {
					content = extraInformationOfClip (localizer);
					break;
				}				
				case Catalogue.CTL_SCOPE: {
					content = extraInformationOfScope (localizer);
					break;
				}				
				case Catalogue.CTL_BARREL: {
					content = extraInformationOfBarrel (localizer);
					break;
				}
				case Catalogue.CTL_LEFT_SIDE: 
				case Catalogue.CTL_RIGHT_SIDE: 
				case Catalogue.CTL_BOTTOM_SIDE:
				case Catalogue.CTL_MIDDLE_SIDE:
				case Catalogue.CTL_SIDE: {
					content = extraInformationOfSide (localizer);
					break;
				}
			}
		}
		if (description != undefined && description.length > 0) {
			content += localizer.localize("AOP_DESCRIPTION", [ { varName: LOCALIZATION_VALUE_PARAM, varValue: description } ]) + "\n";
		} else if (shortDescription != undefined && shortDescription.length > 0) {
			content += localizer.localize("AOP_DESCRIPTION", [{varName: LOCALIZATION_VALUE_PARAM, varValue: shortDescription}]) + "\n";
		}		
		return content;
	}
	
	/**
	 * Lấy các thông tin mô tả chuyên biệt của đạn	 	 
	 */
	private function extraInformationOfBullet (localizer : Localizer) : String {
		var content : String = "";
		content += localizer.localize ("AOP_AMMO_BASE_DAMAGE", [{varName: LOCALIZATION_VALUE_PARAM, varValue: "" + property(AOP_AMMO_BASE_DAMAGE)}]) + "\n";
		content += localizer.localize ("AOP_AMMO_ARMOR_AFFECT", [{varName: LOCALIZATION_VALUE_PARAM, varValue: "" + property(AOP_AMMO_ARMOR_AFFECT)}]) + "\n";
		return content;
	}
	
	/**
	 * Lấy các thông tin mô tả chuyên biệt của băng đạn
	 */
	private function extraInformationOfClip (localizer : Localizer) : String {
		var content : String = "";
		content += localizer.localize ("AOP_CLIP_EXTRA_AMMOS", [{varName: LOCALIZATION_VALUE_PARAM, varValue: "" + property(AOP_CLIP_EXTRA_AMMOS)}]) + "\n";
		content += localizer.localize ("AOP_CLIP_RELOADING_SPEED_AFFECT", [{varName: LOCALIZATION_VALUE_PARAM, varValue: "" + property(AOP_CLIP_RELOADING_SPEED_AFFECT)}]) + "\n";
		return content;
	}
	
	/**
	 * Lấy các thông tin mô tả chuyên biệt của ống ngắm
	 */
	private function extraInformationOfScope (localizer : Localizer) : String {
		var content : String = "";	
		var zoomLevels : Array = magnificationLevels;
		var zoomString : String = "";		
		for (var x : String in zoomLevels) {
			zoomString += "x" + Math.floor(zoomLevels[x]) + ", ";
		}
		content += localizer.localize ("AOP_SCOPE_MAGNIFICATION", [ { varName: LOCALIZATION_VALUE_PARAM, varValue: zoomString } ]) + "\n";
		content += localizer.localize ("AOP_SCOPE_ACCURACY", [ { varName: LOCALIZATION_VALUE_PARAM, varValue: "" + property(AOP_SCOPE_ACCURACY) } ]) + "\n";
		content += localizer.localize ("AOP_SCOPE_MOVEMENT_SPEED", [ { varName: LOCALIZATION_VALUE_PARAM, varValue: "" + property(AOP_SCOPE_MOVEMENT_SPEED) } ]) + "\n";
		content += localizer.localize ("AOP_SCOPE_RATE_OF_FIRE", [ { varName: LOCALIZATION_VALUE_PARAM, varValue: ""+property(AOP_SCOPE_RATE_OF_FIRE) } ]) + "\n";
		content += localizer.localize ("AOP_SCOPE_DELAY", [{varName: LOCALIZATION_VALUE_PARAM, varValue: "" + property(AOP_SCOPE_DELAY)}]) + "\n";
		if (property(AOP_SCOPE_FOG_PROOF) > 0) {
			content += localizer.localize ("AOP_SCOPE_FOG_PROOF") + "\n";
		}
		if (property(AOP_SCOPE_WATER_PROOF) > 0) {
			content += localizer.localize ("AOP_SCOPE_WATER_PROOF") + "\n";
		}
		if (property(AOP_SCOPE_BRIGHER_VIEW) > 0) {
			content += localizer.localize ("AOP_SCOPE_BRIGHTER_VIEW") + "\n";
		}
		return content;
	}
	
	/**
	 * Lấy các thông tin mô tả chuyên biệt của nòng
	 */
	private function extraInformationOfBarrel (localizer : Localizer) : String {
		var content : String = "";
		content += localizer.localize ("AOP_BARREL_SOUND_REDUCE", [{varName: LOCALIZATION_VALUE_PARAM, varValue: "" + property(AOP_BARREL_SOUND_REDUCE)}]) + "\n";
		content += localizer.localize ("AOP_BARREL_DAMAGE_AFFECT", [ { varName: LOCALIZATION_VALUE_PARAM, varValue: "" + property(AOP_BARREL_DAMAGE_AFFECT) } ]) + "\n";
		content += localizer.localize ("AOP_BARREL_RATE_OF_FIRE", [{varName: LOCALIZATION_VALUE_PARAM, varValue: "" + property(AOP_BARREL_RATE_OF_FIRE)}]) + "\n";
		return content;
	}
	
	/**
	 * Lấy các thông tin mô tả chuyên biệt của các addon lắp ở thân (trái, phải, dưới)
	 */
	private function extraInformationOfSide (localizer : Localizer) : String {
		var content : String = "";
		if (property(AOP_SIDE_FLASH_LIGHT) > 0) {
			content += localizer.localize ("AOP_SIDE_FLASH_LIGHT") + "\n";
		} else if (property(AOP_SIDE_LASER_RED) > 0) {
			content += localizer.localize ("AOP_SIDE_LASER_RED") + "\n";
		} else if (property(AOP_SIDE_LASER_BLUE) > 0) {
			content += localizer.localize ("AOP_SIDE_LASER_BLUE") + "\n";
		} else if (property(AOP_SIDE_GRENADE_LAUNCHER) > 0) {
			content += localizer.localize ("AOP_SIDE_GRENADE_LAUNCHER") + "\n";
			content += localizer.localize ("AOP_SIDE_GRENADE_LAUNCHER_DELAY", [{varName: LOCALIZATION_VALUE_PARAM, varValue: "" + property(AOP_SIDE_GRENADE_LAUNCHER_DELAY)}]) + "\n";
		}
		content += localizer.localize ("AOP_SIDE_EFFECT_RANGE", [{varName: LOCALIZATION_VALUE_PARAM, varValue: "" + property(AOP_SIDE_EFFECT_RANGE)}]) + "\n";
		return content;
	}
}