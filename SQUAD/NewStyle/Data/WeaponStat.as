import gfx.controls.SQUAD.MathUtility;
import gfx.controls.SQUAD.NewStyle.Data.AdaptedAddOns;
import gfx.controls.SQUAD.NewStyle.Data.AddOn;
import gfx.controls.SQUAD.NewStyle.Data.Catalogue;
import gfx.controls.SQUAD.NewStyle.Data.Grenade;
import gfx.controls.SQUAD.NewStyle.Data.Gun;
import gfx.controls.SQUAD.NewStyle.Data.GunStats;
import gfx.controls.SQUAD.NewStyle.Data.InventoryItem;
import gfx.controls.SQUAD.NewStyle.Data.Knife;

/**
 * Class hỗ trợ việc xác định thông số về súng, dao, lựu đạn
 */
class gfx.controls.SQUAD.NewStyle.Data.WeaponStat {
	public static var MAX_GUN_POWER 			: Number = -1;
	public static var MAX_GUN_ACCURACY 			: Number = -1;
	public static var MAX_GUN_RELOADING_SPEED 	: Number = -1;
	public static var MAX_GUN_SPEED 			: Number = -1;
	public static var MAX_GUN_RECOIL 			: Number = -1;
	public static var MAX_GUN_WEIGHT 			: Number = -1;
	
	public static var MAX_KNIFE_DAMAGE 			: Number = -1;
	public static var MAX_KNIFE_SPEED 			: Number = -1;
	public static var MAX_KNIFE_LENGTH 			: Number = -1;
	public static var MAX_KNIFE_WEIGHT 			: Number = -1;
	
	public static var MAX_GRENADE_DAMAGE 		: Number = -1;
	public static var MAX_GRENADE_RADIUS 		: Number = -1;
	public static var MAX_GRENADE_WEIGHT 		: Number = -1;
	public static var MAX_GRENADE_LIFE_TIME 	: Number = -1;
	public static var MAX_GRENADE_DISTANCE 		: Number = -1;
	
	public static var MAX_ADDON_WEIGHT 			: Number = -1;
	//-----------------------------------------------------------------------------
	/**
	 * Những súng có speed = 0, bên core chuyển thành 1000 trong DB, khi gặp phải reset nó về 0
	 */
	public static var GUN_SPEED_ZERO 			: Number = 1000;
	//-----------------------------------------------------------------------------
	/**
	 * Hàm khởi tạo. Phải được gọi trước khi sử dụng!
	 * 
	 * @param	itemList		danh sách tất cả các vật phẩm
	 */
	public static function init (itemList : Array) : Void {
		for (var x : String in itemList) {
			var y = itemList[x];
			if (y instanceof Gun) {			
				if (MAX_GUN_ACCURACY < y.accuracy ) {
					MAX_GUN_ACCURACY = y.accuracy;
				}
				if (MAX_GUN_RELOADING_SPEED < y.reloadTime(0) ) {
					MAX_GUN_RELOADING_SPEED = y.reloadTime(0) ;				
				}
				if (MAX_GUN_SPEED < y.speed && y.speed < GUN_SPEED_ZERO) {
					MAX_GUN_SPEED = y.speed ;
				}
				if (MAX_GUN_RECOIL < y.recoil ) {
					MAX_GUN_RECOIL = y.recoil ;
				}			
				if (MAX_GUN_WEIGHT < y.weight) {
					MAX_GUN_WEIGHT = y.weight;				
				}
			} else if (y instanceof Knife) {			
				if (MAX_KNIFE_SPEED < y.speed) {
					MAX_KNIFE_SPEED = y.speed;
				}
				if (MAX_KNIFE_LENGTH < y.length) {
					MAX_KNIFE_LENGTH = y.length;
				}
				if (MAX_KNIFE_WEIGHT < y.weight) {
					MAX_KNIFE_WEIGHT = y.weight;
				}
			} else if (y instanceof Grenade) {
				if (MAX_GRENADE_RADIUS < y.effectRadius) {
					MAX_GRENADE_RADIUS = y.effectRadius;
				}
				if (MAX_GRENADE_WEIGHT < y.weight) {
					MAX_GRENADE_WEIGHT = y.weight;
				}
				if (MAX_GRENADE_LIFE_TIME < y.lifeTime) {
					MAX_GRENADE_LIFE_TIME  = y.lifeTime;
				}
				if (MAX_GRENADE_DISTANCE < y.distance) {
					MAX_GRENADE_DISTANCE  = y.distance;	
				}
			} else if (y instanceof AddOn) {
				if (MAX_ADDON_WEIGHT < y.weight) {
					MAX_ADDON_WEIGHT = y.weight;
				}
			}
		}
		
		++MAX_GUN_ACCURACY;
		++MAX_GUN_RELOADING_SPEED;
		++MAX_GUN_SPEED;
		++MAX_GUN_RECOIL;
		++MAX_GUN_WEIGHT;
		++MAX_GRENADE_DISTANCE
		++MAX_GRENADE_LIFE_TIME;
		MAX_GUN_WEIGHT += (MAX_ADDON_WEIGHT * 9);
		++MAX_KNIFE_SPEED;
		++MAX_KNIFE_LENGTH;		
		++MAX_KNIFE_WEIGHT;
		++MAX_GRENADE_RADIUS;
		++MAX_GRENADE_WEIGHT;
		
		MAX_GUN_POWER = 
		MAX_KNIFE_DAMAGE = 
		MAX_GRENADE_DAMAGE = 150;
	}
	//-----------------------------------------------------------------------------
	/**
	 * Tìm BaseItemID của tất cả các add-on đang gắn vào súng
	 * 
	 * @param	gunID 			InventoryID của súng
	 * @param	addOnList		Danh sách các add-on mà người chơi sở hữu
	 * 
	 * @return 	AdaptedAddOns 	BaseItemID của các add-on đang gắn vào súng
	 */
	public static function getAdaptedAddOns (gunID : Number, addOnList : Array) : AdaptedAddOns {
		var bullet 	: Number = 0;
		var clip 	: Number = 0; 
		var scope 	: Number = 0; 
		var barrel 	: Number = 0; 
		var left 	: Number = 0; 
		var right 	: Number = 0; 
		var bottom 	: Number = 0; 
		var color 	: Number = 0; 
		var decal 	: Number = 0;
		var addOn : InventoryItem;
		for (var x : String in addOnList) {
			addOn = addOnList[x];
			if (addOn.useFor == gunID) {
				switch (addOn.number) {
					case InventoryItem.IIN_AMMO: {
						bullet = addOn.baseItemId;
						break;
					}
					case InventoryItem.IIN_CLIP: {
						clip = addOn.baseItemId;
						break;
					}
					case InventoryItem.IIN_SCOPE: {
						scope = addOn.baseItemId;
						break;
					}
					case InventoryItem.IIN_BARREL: {
						barrel = addOn.baseItemId;
						break;
					}
					case InventoryItem.IIN_LEFT_SIDE: {
						left = addOn.baseItemId;
						break;
					}
					case InventoryItem.IIN_RIGHT_SIDE: {
						right = addOn.baseItemId;
						break;
					}
					case InventoryItem.IIN_BOTTOM_SIDE: {
						bottom = addOn.baseItemId;
						break;
					}
					case InventoryItem.IIN_COLOR: {
						color = addOn.baseItemId;
						break;
					}
					case InventoryItem.IIN_DECAL: {
						decal = addOn.baseItemId;
						break;
					}
				}
			}
		}
		return new AdaptedAddOns (bullet, clip, scope, barrel, left, right, bottom, color, decal);
	}
	//-----------------------------------------------------------------------------
	/**
	 * Tìm add-on đang gắn vào súng
	 * 
	 * @param	gunID			InventoryID của súng
	 * @param	slot			Vị trí lắp add-on (sử dụng InventoryItem.IIN_....)
	 * @param	addOnList		Danh sách các add-on mà người chơi sở hữu
	 * 
	 * @return	InventoryItem	add-on đang gắn vào vị trí 'slot' của súng undefined nếu không tìm thấy
	 */
	public static function getAdaptedAddOn (gunID : Number, slot : Number, addOnList : Array) : InventoryItem {
		var addOn : InventoryItem;
		for (var x : String in addOnList) {
			addOn = addOnList[x];
			if (addOn.useFor == gunID && addOn.number == slot) {
				return addOn;
			}
		}
		return undefined;
	}
	//-----------------------------------------------------------------------------
	/**
	 * Tìm add-on  theo ID
	 * 
	 * @param	itemId      	InventoryID của add-on
	 * @param	addOnList		danh sách các add-on mà người chơi sở hữu
	 * 
	 * @return	InventoryItem	add-on ứng với InventoryID
	 */
	public static function getAddOnByID (itemId : Number, addOnList : Array) : InventoryItem {
		var addOn : InventoryItem;
		for (var x : String in addOnList) {
			addOn = addOnList[x];
			if (addOn.id == itemId) {
				return addOn;
			}
		}
		return undefined;
	}
	//-----------------------------------------------------------------------------
	/**
	 * Tìm Base Item ID của add-on đang gắn vào súng
	 * 
	 * @param	gunID			InventoryID của súng
	 * @param	slot			Vị trí lắp add-on (sử dụng InventoryItem.IIN_....)
	 * @param	addOnList		Danh sách các add-on mà người chơi sở hữu
	 * 
	 * @return	Number			Base Item ID của add-on đang gắn vào vị trí 'slot' của súng hoặc undefined nếu không tìm thấy
	 */
	public static function getAdaptedAddOnBaseID (gunID : Number, slot : Number, addOnList : Array) : Number {
		var addOn : InventoryItem = WeaponStat.getAdaptedAddOn(gunID, slot, addOnList);
		if (addOn != undefined) {
			return addOn.baseItemId;
		}
		return undefined;
	}
	//-----------------------------------------------------------------------------
	/**
	 * Tính các thông số của súng
	 * 
	 * @param	gun 			Súng muốn tính thông số
	 * @param	bullet 			Add-on "Đạn" đang lắp vào súng
	 * @param	clip 			Add-on "Băng đạn" đang lắp vào súng
	 * @param	scope 			Add-on "Ống ngắm" đang lắp vào súng
	 * @param	barrel 			Add-on "Nòng" đang lắp vào súng
	 * @param	left 			Add-on "Đèn pin" đang lắp vào súng
	 * @param	right 			Add-on "Laser" đang lắp vào súng
	 * @param	bottom 			Add-on "Ống phóng lựu" đang lắp vào súng
	 * @param	color 			Add-on "Màu sắc" đang lắp vào súng
	 * @param	decal 			Add-on "Đề can" đang lắp vào súng
	 * 
	 * @return 	GunStats 		Các thông số của súng
	 */
	public static function getGunStats (
		gun 	: Gun, 
		bullet 	: AddOn, 
		clip 	: AddOn, 
		scope 	: AddOn, 
		barrel 	: AddOn, 
		left 	: AddOn, 
		right 	: AddOn, 
		bottom 	: AddOn, 
		color 	: AddOn, 
		decal 	: AddOn
	) : GunStats {
		return new GunStats (
			WeaponStat.gunPower(gun, bullet, clip, scope, barrel, left, right, bottom, color, decal), 
			WeaponStat.gunSpeed(gun, bullet, clip, scope, barrel, left, right, bottom, color, decal), 
			WeaponStat.gunRecoil(gun, bullet, clip, scope, barrel, left, right, bottom, color, decal), 
			WeaponStat.gunAccuracy(gun, bullet, clip, scope, barrel, left, right, bottom, color, decal), 
			WeaponStat.gunReload(gun, bullet, clip, scope, barrel, left, right, bottom, color, decal), 
			WeaponStat.gunAmmos(gun, bullet, clip, scope, barrel, left, right, bottom, color, decal), 
			WeaponStat.gunLauncher(bottom), 
			WeaponStat.gunWeight(gun, bullet, clip, scope, barrel, left, right, bottom, color, decal)
		);
	}
	//-----------------------------------------------------------------------------
	/**
	 * Tính sát thương của súng
	 * 
	 * @param	gun 			Súng muốn tính sát thương
	 * @param	bullet 			Add-on "Đạn" đang lắp vào súng
	 * @param	clip 			Add-on "Băng đạn" đang lắp vào súng
	 * @param	scope 			Add-on "Ống ngắm" đang lắp vào súng
	 * @param	barrel 			Add-on "Nòng" đang lắp vào súng
	 * @param	left 			Add-on "Đèn pin" đang lắp vào súng
	 * @param	right 			Add-on "Laser" đang lắp vào súng
	 * @param	bottom 			Add-on "Ống phóng lựu" đang lắp vào súng
	 * @param	color 			Add-on "Màu sắc" đang lắp vào súng
	 * @param	decal 			Add-on "Đề can" đang lắp vào súng
	 * 
	 * @return 	Number 			Sát thương của súng
	 */
	public static function gunPower (
		gun 	: Gun, 
		bullet 	: AddOn, 
		clip 	: AddOn, 
		scope 	: AddOn, 
		barrel 	: AddOn, 
		left 	: AddOn, 
		right 	: AddOn, 
		bottom 	: AddOn, 
		color 	: AddOn, 
		decal 	: AddOn
	) : Number {
		if (!(gun instanceof Gun) || !(bullet instanceof AddOn)) {
			return 0;
		}
		var result : Number = 0;		
		result = gun.power (bullet.property(AddOn.AOP_AMMO_BASE_DAMAGE));
		if (barrel != undefined) {
			result *= (100 + barrel.property (AddOn.AOP_BARREL_DAMAGE_AFFECT)) / 100;
		}		
		return Math.abs(Number (result));
	}
	//-----------------------------------------------------------------------------
	/**
	 * Tính độ chính xác của súng
	 * 
	 * @param	gun 			Súng muốn tính độ chính xác
	 * @param	bullet 			Add-on "Đạn" đang lắp vào súng
	 * @param	clip 			Add-on "Băng đạn" đang lắp vào súng
	 * @param	scope 			Add-on "Ống ngắm" đang lắp vào súng
	 * @param	barrel 			Add-on "Nòng" đang lắp vào súng
	 * @param	left 			Add-on "Đèn pin" đang lắp vào súng
	 * @param	right 			Add-on "Laser" đang lắp vào súng
	 * @param	bottom 			Add-on "Ống phóng lựu" đang lắp vào súng
	 * @param	color 			Add-on "Màu sắc" đang lắp vào súng
	 * @param	decal 			Add-on "Đề can" đang lắp vào súng
	 * 
	 * @return 	Number 			Độ chính xác của súng
	 */
	public static function gunAccuracy (
		gun 	: Gun, 
		bullet 	: AddOn, 
		clip 	: AddOn, 
		scope 	: AddOn, 
		barrel 	: AddOn, 
		left 	: AddOn, 
		right 	: AddOn, 
		bottom 	: AddOn, 
		color 	: AddOn, 
		decal 	: AddOn	
	) : Number {
		var result : Number = 0;
		if (gun instanceof Gun) {
			result = gun.accuracy;
			if (scope instanceof AddOn) {
				result *= (100.0 + scope.property(AddOn.AOP_SCOPE_ACCURACY)) / 100.0;
			}
		}
		return Math.abs(Number (result));
	}
	//-----------------------------------------------------------------------------
	/**
	 * Tính tốc độ bắn của súng
	 * 
	 * @param	gun 			Súng muốn tính tốc độ bắn
	 * @param	bullet 			Add-on "Đạn" đang lắp vào súng
	 * @param	clip 			Add-on "Băng đạn" đang lắp vào súng
	 * @param	scope 			Add-on "Ống ngắm" đang lắp vào súng
	 * @param	barrel 			Add-on "Nòng" đang lắp vào súng
	 * @param	left 			Add-on "Đèn pin" đang lắp vào súng
	 * @param	right 			Add-on "Laser" đang lắp vào súng
	 * @param	bottom 			Add-on "Ống phóng lựu" đang lắp vào súng
	 * @param	color 			Add-on "Màu sắc" đang lắp vào súng
	 * @param	decal 			Add-on "Đề can" đang lắp vào súng
	 * 
	 * @return 	Number 			Tốc độ bắn của súng
	 */
	public static function gunSpeed (
		gun 	: Gun, 
		bullet 	: AddOn, 
		clip 	: AddOn, 
		scope 	: AddOn, 
		barrel 	: AddOn, 
		left 	: AddOn, 
		right 	: AddOn, 
		bottom 	: AddOn, 
		color 	: AddOn, 
		decal 	: AddOn		
	) : Number {
		var result : Number = 0;		
		if (gun instanceof Gun) {			
			result = gun.speed;
			if (result == GUN_SPEED_ZERO) {
				return 0;
			}
			var affect : Number = 0;
			if (barrel instanceof AddOn) {
				affect += barrel.property (AddOn.AOP_BARREL_RATE_OF_FIRE);
			}
			if (scope instanceof AddOn) {
				affect += scope.property (AddOn.AOP_SCOPE_RATE_OF_FIRE);
			}
		}
		return Math.abs(Number (result * Number(100.0 + affect) / 100.0));
	}
	//-----------------------------------------------------------------------------
	/**
	 * Tính độ giật của súng
	 * 
	 * @param	gun 			Súng muốn tính độ giật
	 * @param	bullet 			Add-on "Đạn" đang lắp vào súng
	 * @param	clip 			Add-on "Băng đạn" đang lắp vào súng
	 * @param	scope 			Add-on "Ống ngắm" đang lắp vào súng
	 * @param	barrel 			Add-on "Nòng" đang lắp vào súng
	 * @param	left 			Add-on "Đèn pin" đang lắp vào súng
	 * @param	right 			Add-on "Laser" đang lắp vào súng
	 * @param	bottom 			Add-on "Ống phóng lựu" đang lắp vào súng
	 * @param	color 			Add-on "Màu sắc" đang lắp vào súng
	 * @param	decal 			Add-on "Đề can" đang lắp vào súng
	 * 
	 * @return 	Number 			Độ giật của súng
	 */
	public static function gunRecoil (
		gun 	: Gun, 
		bullet 	: AddOn, 
		clip 	: AddOn, 
		scope 	: AddOn, 
		barrel 	: AddOn, 
		left 	: AddOn, 
		right 	: AddOn, 
		bottom 	: AddOn, 
		color 	: AddOn, 
		decal 	: AddOn		
	) : Number {
		var result : Number = 0;
		if (gun instanceof Gun) {
			result = gun.recoil;			
		}
		return Math.abs(Number (result));
	}
	//-----------------------------------------------------------------------------
	/**
	 * Tính lượng đạn của súng
	 * 
	 * @param	gun 			Súng muốn tính lượng đạn
	 * @param	bullet 			Add-on "Đạn" đang lắp vào súng
	 * @param	clip 			Add-on "Băng đạn" đang lắp vào súng
	 * @param	scope 			Add-on "Ống ngắm" đang lắp vào súng
	 * @param	barrel 			Add-on "Nòng" đang lắp vào súng
	 * @param	left 			Add-on "Đèn pin" đang lắp vào súng
	 * @param	right 			Add-on "Laser" đang lắp vào súng
	 * @param	bottom 			Add-on "Ống phóng lựu" đang lắp vào súng
	 * @param	color 			Add-on "Màu sắc" đang lắp vào súng
	 * @param	decal 			Add-on "Đề can" đang lắp vào súng
	 * 
	 * @return 	Number 			Lượng đạn của súng
	 */
	public static function gunAmmos (
		gun 	: Gun, 
		bullet 	: AddOn, 
		clip 	: AddOn, 
		scope 	: AddOn, 
		barrel 	: AddOn, 
		left 	: AddOn, 
		right 	: AddOn, 
		bottom 	: AddOn, 
		color 	: AddOn, 
		decal 	: AddOn			
	) : Number {
		var result : Number = 0;
		if (!(gun instanceof Gun)) {
			return 0;
		}
		result = gun.ammos;
		if (clip instanceof AddOn) {
			result += Number(clip.property(AddOn.AOP_CLIP_EXTRA_AMMOS));
		}
		return Math.abs(Number (result));
	}
	//-----------------------------------------------------------------------------
	/**
	 * Tính tốc độ thay đạn của súng
	 * 
	 * @param	gun 			Súng muốn tính tốc độ thay đạn
	 * @param	bullet 			Add-on "Đạn" đang lắp vào súng
	 * @param	clip 			Add-on "Băng đạn" đang lắp vào súng
	 * @param	scope 			Add-on "Ống ngắm" đang lắp vào súng
	 * @param	barrel 			Add-on "Nòng" đang lắp vào súng
	 * @param	left 			Add-on "Đèn pin" đang lắp vào súng
	 * @param	right 			Add-on "Laser" đang lắp vào súng
	 * @param	bottom 			Add-on "Ống phóng lựu" đang lắp vào súng
	 * @param	color 			Add-on "Màu sắc" đang lắp vào súng
	 * @param	decal 			Add-on "Đề can" đang lắp vào súng
	 * 
	 * @return 	Number 			Tốc độ thay đạn của súng
	 */
	public static function gunReload (
		gun 	: Gun, 
		bullet 	: AddOn, 
		clip 	: AddOn, 
		scope 	: AddOn, 
		barrel 	: AddOn, 
		left 	: AddOn, 
		right 	: AddOn, 
		bottom 	: AddOn, 
		color 	: AddOn, 
		decal 	: AddOn		
	) : Number {
		var result : Number = 0;
		if (gun instanceof Gun && clip instanceof AddOn) {			
			result = gun.reloadTime (clip.property(AddOn.AOP_CLIP_RELOADING_SPEED_AFFECT));
		}
		return Math.abs(Number (result));
	}
	//-----------------------------------------------------------------------------
	/**
	 * Tính trọng lượng của súng
	 * 
	 * @param	gun 			Súng muốn tính trọng lượng
	 * @param	bullet 			Add-on "Đạn" đang lắp vào súng
	 * @param	clip 			Add-on "Băng đạn" đang lắp vào súng
	 * @param	scope 			Add-on "Ống ngắm" đang lắp vào súng
	 * @param	barrel 			Add-on "Nòng" đang lắp vào súng
	 * @param	left 			Add-on "Đèn pin" đang lắp vào súng
	 * @param	right 			Add-on "Laser" đang lắp vào súng
	 * @param	bottom 			Add-on "Ống phóng lựu" đang lắp vào súng
	 * @param	color 			Add-on "Màu sắc" đang lắp vào súng
	 * @param	decal 			Add-on "Đề can" đang lắp vào súng
	 * 
	 * @return 	Number 			Trọng lượng của súng
	 */
	public static function gunWeight (
		gun 	: Gun, 
		bullet 	: AddOn, 
		clip 	: AddOn, 
		scope 	: AddOn, 
		barrel 	: AddOn, 
		left 	: AddOn, 
		right 	: AddOn, 
		bottom 	: AddOn, 
		color 	: AddOn, 
		decal 	: AddOn		
	) : Number {
		var result : Number = gun.weight;
		if (scope != undefined) {
			result += Number(scope.weight);
		}
		if (barrel != undefined) {
			result += Number(barrel.weight);
		}
		if (left != undefined) {
			result += Number(left .weight);
		}
		if (right != undefined) {
			result += Number(right.weight);
		}
		if (bottom != undefined) {
			result += Number(bottom.weight);
		}		
		return Math.abs(Number (result));
	}
	//-----------------------------------------------------------------------------
	/**
	 * Kiểm tra xem súng có ống phóng lựu không
	 * 
	 * @param	bottom 			Add-on "Ống phóng lựu" đang lắp vào súng
	 * 
	 * @return 	Boolean 		Có ống phóng lựu không
	 */
	public static function gunLauncher (bottom : AddOn	) : Boolean {
		if (bottom != undefined) {
			return true;
		}
		return false;
	}
}