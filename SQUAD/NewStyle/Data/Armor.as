﻿import gfx.controls.SQUAD.NewStyle.Data.BaseItem;

/**
 * Class lưu trữ dữ liệu về GIÁP
 */
class gfx.controls.SQUAD.NewStyle.Data.Armor extends BaseItem {
	private var _damageReduction: Number;
	
	/**
	 * Hàm tạo
	 *
	 * @param	baseItem	BaseItem với dữ liệu đọc từ Common.xml
	 * @param	damageReduction	Mức độ giảm sát thương
	 */
	public function Armor(
		baseItem: BaseItem,
		damageReduction: Number) {
		copyFrom(baseItem);
		_damageReduction = damageReduction;
	}
	
	/**
	 * @return Mức độ giảm sát thương
	 */
	public function get damageReduction(): Number {
		return _damageReduction;
	}
	
	/**
	 * @return Mô tả giáp
	 */
	public function toString():String {
		var s:String = super.toString();
		s += "\tDamage reduction = " + _damageReduction + "\n";
		return s;
	}
}