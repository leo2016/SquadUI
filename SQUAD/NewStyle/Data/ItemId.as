﻿/**
 * ...
 * @author ...
 */

class gfx.controls.SQUAD.NewStyle.Data.ItemId 
{
	var _itemIds:Number;
	var _image:String;
	var _isSpecial:Boolean;
	public function ItemId(itemId:Number,image:String,isSpecial:Boolean) {
		_itemIds = itemId;
		_image = image;
		_isSpecial = isSpecial;
	}
	public function get itemIDs():Number {
		return _itemIds;
	}
	public function get images():String {
		return _image;
	}
		public function get isSpecial():Boolean {
		return _isSpecial;
	}
	public function toString () : String {
		var s : String = "";
		s += "_itemIds = " + _itemIds + "\n";
		s += "_image = " + _image + "\n";
		s += "_image = " + isSpecial + "\n";
		return s;
	}
}