﻿class gfx.controls.SQUAD.NewStyle.Data.ClanPosition  {
	private var _id:		Number;
	private var _positionName:		String;
	private var _level:		Number;	
	private var _type:		Number;	
	
	public function ClanPosition(id: Number,positionName:String,level: Number){
		_id = id;
		_positionName = positionName;
		_level = level;
		_type = type;
	}		
	
	public function get id () : Number {
		return _id;
	}
	
	public function get positionName () : String {
		return _positionName;
	}
	
	public function get level () : Number {
		return _level;
	}
	public function get type () : Number {
		return _type;
	}
	
	public function toString () : String {
		var s : String = "";
		s += "ID = " + _id + "\n";
		s += "Name = " + _positionName + "\n";
		s += "Level = " + _level + "\n";
		s += "type = " + _type + "\n";
		return s;
	}
}