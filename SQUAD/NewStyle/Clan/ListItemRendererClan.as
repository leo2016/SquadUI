﻿import gfx.controls.ListItemRenderer;
import gfx.controls.Label;
import gfx.controls.SQUAD.MixedListItemRenderer;
import gfx.controls.UILoader;
import gfx.events.EventDispatcher;
import gfx.controls.Button;
import gfx.controls.SQUAD.ResolutionHelper;
import gfx.controls.SQUAD.ImagePathManager;


class gfx.controls.SQUAD.NewStyle.Clan.ListItemRendererClan extends ListItemRenderer {	
	private var UIRank:UILoader;
	private var labName:Label;
	private var labID:Label;
	private var labRank:Label;
	private var labVictories:Label;
	private var labStatus:Label;
	private var labPosition:Label;
	public function ListItemRendererClan() {
		super();
	}	
	
	private function configUI () : Void {
		super.configUI();
	}
	public function setData(data:Object):Void {
		if(data != undefined) {
			this.data = data;
			updateAfterStateChange();	
			this.visible = true;
			this._disabled = false;	
		}else {
			this.visible = false;
			this._disabled = true;
		}
	}
		
	public function updateAfterStateChange():Void {
		if (data != undefined) {
			if(lblName instanceof Label){
				lblName.text = data.name;
			}
			if (UIClanAvatar instanceof UILoader) {
				ResolutionHelper.updateUILoader(UIClanAvatar, ImagePathManager.CLAN_AVATAR+data.image);
			}
		}
	}
}
