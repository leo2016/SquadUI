﻿import gfx.controls.SQUAD.CPPSimulator;
import gfx.controls.SQUAD.NewStyle.MiniMap.MiniMap;
import gfx.controls.SQUAD.NewStyle.MiniMap.MiniMapItemDOTA;
import gfx.controls.SQUAD.NewStyle.Radar.RadarItem;

class gfx.controls.SQUAD.NewStyle.MiniMap.SQUADMiniMap extends MiniMap {
	public function SQUADMiniMap () {
		super ();
	}
	
	private function rotateItem (item : RadarItem) : Void {
		if (sItemRotateType == IRT_USER_DEFINED) {			
			if (item.type == RadarItem.TYPE_ME) {				
				item._rotation = item.rotation;//real rotation
			} else {
				item._rotation = 0;//keep original
			}
		} else {
			super.rotateItem (item);
		}
	}
	
	public function updateItemState (id : Number, action : Number/*status - if it's not LevelAIcon*/, stage : Number, resourcePoint : Boolean) : String {
		if (!bReady) {
			return;
		}		
		var radarItem /*: RadarItem*/= getItemById (id);
		if (radarItem instanceof RadarItem) {
			if (radarItem.id == id) {
				switch (radarItem.type) {
					case RadarItem.TYPE_DOTA_BUNKER: {
						//CPPSimulator.trace ("Item '"+radarItem.toString()+"' change state to " + action + " in SQUADMiniMap", 3);
						radarItem.setStatus (action, stage, resourcePoint);
						return radarItem.statusFrame;
					}
					default: {
						if (radarItem.visible) {
							CPPSimulator.trace ("Item '"+radarItem.toString()+"' set state = " + action + " in SQUADMiniMap", 3);
							radarItem.setStatus (action);
							return radarItem.statusFrame;
						} else {
							return "";
						}
					}
				}				
			} 
		}
		return "";
	}
	
	private function invalidateItemState (item : RadarItem) : Void {
		if (item instanceof RadarItem) {
			if (item instanceof MiniMapItemDOTA) {
				var icon : MiniMapItemDOTA = MiniMapItemDOTA(item);				
				this.updateItemState (icon.id, icon.status, icon.m_nStage, icon.m_bResourcePointEnabled);
			} else {
				super.updateItemState (item.id, item.status);
			}
		}
	}
	
	public function setNameForLevelAIcon (id : Number, name : Number) : Void {
		var item/* : RadarItem*/ = getItemById (id);
		if (item instanceof MiniMapItemDOTA) {
			item.setName (name);
		}
	}
}