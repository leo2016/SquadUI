﻿import gfx.controls.Label;
import gfx.controls.SQUAD.CPPSimulator;
import gfx.controls.SQUAD.MathUtility;
import gfx.controls.SQUAD.NewStyle.Radar.RadarItem;
import gfx.controls.UILoader;
import gfx.core.UIComponent;
import gfx.utils.Delegate;

class gfx.controls.SQUAD.NewStyle.MiniMap.MiniMap extends UIComponent {
	//Inspectable	
	
	//Item Rotate Type	
	[Inspectable (name = "ItemRotateType", type = "String", enumeration = "RealRotation,KeepOriginal,UserDefined", defaultValue = "RealRotation")]
	private var sItemRotateType : 				String;	
	private static var IRT_REAL_ROTATION :	String = "RealRotation";
	private static var IRT_KEEP_ORIGINAL :		String = "KeepOriginal";	
	private static var IRT_USER_DEFINED  : 	String = "UserDefined";
		
	//UI	
	private var labMapName : 						Label;
	private var uiMap : 								UILoader;	
	
	//Data
	private var aItemTypes : 						Array;
	private var nX2D0 : 								Number;
	private var nY2D0 : 								Number;
	private var nX3D1 : 								Number;
	private var nY3D1 : 								Number;
	private var nX2D1 : 								Number;
	private var nY2D1 : 								Number;
	private var aMapItems : 						Array;
	private var nScale : 								Number;		
	private var bIsRefreshing : 						Boolean;
	private var nMapWidth : 						Number;
	private var nMapHeight : 						Number;
	private var bReady : 								Boolean;
	private var nIntervalId : 							Boolean;
	
	public function MiniMap () {
		super ();
		init ();		
	}
	
	private function configUI () : Void {
		super.configUI ();
		uiMap.addEventListener ("complete", this, "strechMap");
		dispatchEvent ({type:"ready", target: this});
	}
	
	public function init () : Void {
		aMapItems 			= [];
		nScale 					= -1;
		nMapWidth			= 0;
		nMapHeight 			= 0;
		nX2D0					= 0;
		nY2D0					= 0;
		nX3D1 					= 0;
		nY3D1 					= 0;
		nX2D1 					= 0;
		nY2D1 					= 0;		
		bIsRefreshing 		= true;
		bReady					= false;
		
		aItemTypes 			= [];
		aItemTypes[RadarItem.TEAM_OTHER] 	= [];
		aItemTypes[RadarItem.TEAM_RED] 		= [];
		aItemTypes[RadarItem.TEAM_BLUE] 	= [];
	}
	
	public function setMap (mapName : String, mapImage : String) : Void {
		bReady = false;
		if (uiMap.source != mapImage) {			
			labMapName.text = mapName;
			nScale = -1;
			nMapWidth = nMapHeight = 0;
			uiMap.source = mapImage;
		} else {
			handleMapLoaded ();
		}
	}
	
	public function addItem (team : Number, type : Number, id : Number, visible : Boolean) : MovieClip {
		var newItem : MovieClip = undefined;// RadarItem
		var identifier : String = aItemTypes[team][type];
		if (identifier != "" && identifier != undefined && identifier != null) {
			newItem = uiMap.attachMovie (identifier, "item" + id, uiMap.getNextHighestDepth(), {_visible: visible});
			if (newItem instanceof RadarItem) {
				newItem.init (id, team, type, identifier, visible);
				newItem.addEventListener("changeState", this, "itemChangeStatus");
				newItem.addEventListener("backToDefaultStatus", this, "itemBackToDefaultStatus");
				aMapItems.push (newItem);
				CPPSimulator.trace ("Add item '" + id + "', type = '" + identifier + "', _visible = " + newItem._visible + ", v = " + newItem.visible + " to minimap", 2);
			}
		}
		return newItem;
	}
	
	public function updateItemPosition (id : Number, x3D : Number, y3D : Number, rotation : Number) : String {
		if (!bReady) {
			return;
		}
		//CPPSimulator.trace ("Update item position of " + id + " at ("+x3D+","+y3D+") & r = " + rotation + " in mini map", 4);
		var mapItem : RadarItem = getItemById (id);
		if (mapItem instanceof RadarItem) {
			if (mapItem.id == id) {				
				mapItem.xBlock = x3D;
				mapItem.yBlock = y3D;
				mapItem.rotation = rotation;
				mapItem.xMap = x2D(x3D);
				mapItem.yMap = y2D(y3D);				
				if (mapItem.traceEnabled) {
					//Position
					mapItem._x = uiMap.content._x + mapItem.xMap;
					mapItem._y = uiMap.content._y + mapItem.yMap;
					
					//Rotation
					rotateItem (mapItem);
					
					/*
					if (isInMinimap(mapItem)) {
						mapItem.visible = true;
					} else {
						mapItem.visible = false;
					}
					*/
				}
				return mapItem.statusFrame;				
			} 
		}
		return "";
	}
	
	public function updateItemState (id : Number, status : Number) : String {
		if (!bReady) {
			return;
		}		
		var mapItem : RadarItem = getItemById (id);
		if (mapItem instanceof RadarItem) {
			if (mapItem.id == id) {
				if (mapItem.visible) {
					CPPSimulator.trace ("Item '"+mapItem.toString()+"' set state = " + status + " in mini map", 3);
					mapItem.setStatus (status);
					return mapItem.statusFrame;
				} 
			} 
		}
		return "";
	}
	
	private function isInMinimap (item : RadarItem) : Boolean {
		if (item._x < uiMap.content._x || item._x > uiMap.content._x + nMapWidth) {
			return false;
		} else if (item._y < uiMap.content._y || item > uiMap.content._y + nMapHeight) {
			return false;
		}
		return true;
	}
	
	public function removeItem (id : Number) : Void {
		CPPSimulator.trace ("Remove item " + id + " from mini map", 2);		
		var n : Number = aMapItems.length;
		var temp : RadarItem;
		for (var i : Number = 0; i < n ; ++i ) {
			temp = aMapItems[i];
			if (temp instanceof RadarItem) {
				if (temp.id == id) {
					temp.clearTimeout ();
					aMapItems.splice (i, 1);
					temp.removeMovieClip ();
					delete temp;
					return;
				}
			}
		}
	}
	
	public function clearItems () : Void {
		CPPSimulator.trace ("Clear Items from mini map", 2);
		var temp;// : RadarItem;
		while (aMapItems.length > 0) {
			temp = aMapItems.pop ();
			if (temp instanceof RadarItem) {
				temp.clearTimeout ();
			}
			temp.removeMovieClip ();
			delete temp;
		}
	}
	
	public function x2D (x3D) : Number {
		return ((x3D * nScale) + (nX2D0 * nMapWidth));
	}
	
	public function y2D (y3D) : Number {
		return ((-y3D * nScale) + (nY2D0 * nMapHeight));
	}
	
	public function x3D (x2D) : Number {
		if (nScale == 0) {
			return x2D - (nX2D0 * nMapWidth);
		}
		return (x2D - (nX2D0 * nMapWidth)) / nScale;
	}
	
	public function y3D (y2D) : Number {
		if (nScale == 0) {
			return (nY2D0 * nMapHeight) - y2D;
		}
		return ((nY2D0 * nMapHeight) - y2D) / nScale;
	}
	
	private function handleMapLoaded () : Void {		
		nMapWidth = uiMap.content._width;
		nMapHeight = uiMap.content._height;		
		calculateOriginalPosition ();		
	}
	
	public function invalidateItems () : Void {
		var mapItem : RadarItem;
		for (var x:String in aMapItems) {
			mapItem = aMapItems[x];
			updateItemPosition (mapItem.id, mapItem.xBlock, mapItem.yBlock, mapItem.rotation);
		}
	}
	
	public function addTeam (team : Number) : Void {
		aItemTypes[team] = [];
	}
	
	public function addItemType (team : Number, type : Number, identifier : String) : Void {
		aItemTypes[team][type] = identifier;
	}
	
	public function getItemById (id : Number) : RadarItem {
		for (var x : String in aMapItems) {
			if (aMapItems[x].id == id) {
				return aMapItems[x];
			}
		}
		return undefined;
	}
	
	public function setRoot (x2D0 : Number, y2D0 : Number) : Void {
		nX2D0 = x2D0;
		nY2D0 = y2D0;
	}
	
	public function setAnchorPoint (x3D1 : Number, y3D1 : Number, x2D1 : Number, y2D1 : Number) : Void {
		nX3D1 = x3D1;
		nY3D1 = y3D1;
		nX2D1 = x2D1;
		nY2D1 = y2D1;
	}
	
	private function calculateOriginalPosition () : Void {
		var x1 : Number = nX2D0 * nMapWidth;
		var y1 : Number = nY2D0 * nMapHeight;
		var x2 : Number = nX2D1 * nMapWidth;
		var y2 : Number = nY2D1 * nMapHeight
		nScale = MathUtility.distance (x1, y1, x2, y2) / MathUtility.distance (0, 0, nX3D1, nY3D1);
		
		CPPSimulator.trace ("Mini map scale = " + nScale, 1);
		CPPSimulator.trace ("X0 = " + nX2D0 + ", Y0 = " + nY2D0 + " ~ ("+x1+", "+y1+") in Mini Map ("+nMapWidth+", "+nMapHeight+")", 1);
	}
	
	public function get scale () : Number {
		return nScale;
	}
	
	public function get x0 () : Number {
		return nX2D0;
	}
	
	public function get y0 () : Number {
		return nY2D0;
	}
	
	public function setVisible (visible : Boolean) : Void {
		this.visible = visible;
		/*
		for (var x:String in aMapItems) {
			aMapItems[x].visible = visible && aMapItems[x].traceEnabled;
		}
		*/
	}
	
	private function rotateItem (item : RadarItem) : Void {
		switch (sItemRotateType) {
			case IRT_KEEP_ORIGINAL : {
				item._rotation = 0;
				break;
			}
			case IRT_REAL_ROTATION : {
				item._rotation = item.rotation;
				break;
			}
		}
	}
	
	private function itemBackToDefaultStatus (evt : Object) : Void {
		CPPSimulator.trace (evt.target + " back to default status in mini map", 4);
		rotateItem(evt.target);
	}
	
	private function itemChangeStatus (evt : Object) : Void {
		CPPSimulator.trace (evt.target + " change status to " + evt.newState + " in mini map", 4);
	}
	
	public function setItemVisible (itemId : Number, visible : Boolean) : Void {
		CPPSimulator.trace ("Set visible for item "+itemId + " = " + visible + " in mini map", 2);
		var item : RadarItem = getItemById (itemId);
		item.visible = item.traceEnabled = visible;
		if (visible) {
			updateItemPosition (itemId, item.xBlock, item.yBlock, item.rotation);
			invalidateItemState (item);
		}
	}
	
	public function showItemTemporarily (itemId : Number, maxTime : Number) : Void {
		CPPSimulator.trace ("Show visible temporarily for item "+itemId + ", max time = " + maxTime + " in mini map", 2);
		var item : RadarItem = getItemById (itemId);
		if (item instanceof RadarItem) {
			item.clearTimeout ();
			item.visible = item.traceEnabled = true;			
			updateItemPosition (itemId, item.xBlock, item.yBlock, item.rotation);
			invalidateItemState (item);
			item.timeOutId = _global.setTimeout (Delegate.create(this, setItemVisible), maxTime, itemId, false);
		}
	}
	
	private function strechMap () : Void {
		nIntervalId = _global.setInterval (Delegate.create(this, checkMapProgress), 10);
	}
	
	private function checkMapProgress () : Void {
		//for some unknown reasons, when UILoader has completed loading, 
		//the contentHolder of UILoader still doesn't appear or it's not completed streching yet
		//so, we need to check several times to know when the contentHolder is visible to strech the map
		handleMapLoaded ();
		//trace ("w1=" + nMapWidth + ",h1=" + nMapHeight + ",w2=" + uiMap.width + ",h2=" + uiMap.height);
		var isStreched : Boolean = false;
		if (nMapWidth <= uiMap.width && Math.abs(nMapHeight - uiMap.height) < 1) {
			isStreched = true;
		} else if (Math.abs(nMapWidth - uiMap.width) < 1 && nMapHeight <= uiMap.height) {
			isStreched = true;
		}
		if (nScale > 0 && isStreched) {			
			_global.clearTimeout (nIntervalId);
			bReady = true;
			dispatchEvent ( { type: "loadedMap" } );
		}		
	}
	
	private function invalidateItemState (item : RadarItem) : Void {
		if (item instanceof RadarItem) {
			updateItemState (item.id, item.status);
		}
	}
}