﻿import gfx.controls.SQUAD.NewStyle.MiniMap.MiniMapItem;

class gfx.controls.SQUAD.NewStyle.MiniMap.MiniMapItemMe extends MiniMapItem {
	public function MiniMapItemMe () {
		super();
		defaultStatus = STATUS_MOVING;
		maxTimeInEachState = [];
		maxTimeInEachState[STATUS_MOVING] = 0;
		frameAtEachState = [];
		frameAtEachState[STATUS_MOVING] = "Moving";		
		heightStatus = "";
	}
	
	public function getRandomStatus () : Number {
		return STATUS_MOVING;
	}
}