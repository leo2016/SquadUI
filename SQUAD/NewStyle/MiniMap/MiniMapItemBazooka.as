import gfx.controls.SQUAD.NewStyle.MiniMap.MiniMapItem;

class gfx.controls.SQUAD.NewStyle.MiniMap.MiniMapItemBazooka extends MiniMapItem {
	public function MiniMapItemBazooka () {
		super();
		status = defaultStatus = STATUS_NOTIFYING;
		maxTimeInEachState = [];
		maxTimeInEachState[STATUS_DEAD] 		= 0;
		maxTimeInEachState[STATUS_NOTIFYING] 	= 0;
		frameAtEachState = [];
		frameAtEachState[STATUS_DEAD] 			= "Disabled";
		frameAtEachState[STATUS_NOTIFYING] 		= "Normal";
	}
	
	//Override
	public function get statusFrame () : String {		
		return frameAtEachState[status];
	}
}