import gfx.controls.SQUAD.NewStyle.MiniMap.MiniMapItem;

class gfx.controls.SQUAD.NewStyle.MiniMap.MiniMapItemTank extends MiniMapItem {
	public function MiniMapItemTank () {
		super();
		status = defaultStatus = ACTION_NO_ACTION;
		maxTimeInEachState = [];
		maxTimeInEachState[ACTION_NO_ACTION] 		= 0;
		maxTimeInEachState[ACTION_REPAIRNG] 		= 2000;
		maxTimeInEachState[ACTION_DESTROYING] 		= 2000;
		maxTimeInEachState[STATUS_DEAD] 			= 0;
		frameAtEachState = [];
		frameAtEachState[ACTION_NO_ACTION] 			= "Normal";
		frameAtEachState[ACTION_REPAIRNG] 			= "Repairing";
		frameAtEachState[ACTION_DESTROYING] 		= "Destroying";
		frameAtEachState[STATUS_DEAD] 				= "Broken";
	}
	
	//Override
	public function get statusFrame () : String {		
		return frameAtEachState[status];
	}
}