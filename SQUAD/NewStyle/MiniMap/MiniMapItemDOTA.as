import gfx.controls.SQUAD.NewStyle.MiniMap.MiniMapItem;

class gfx.controls.SQUAD.NewStyle.MiniMap.MiniMapItemDOTA extends MiniMapItem {
	public static var STAGE_INVALID : 					Number  = -1;
	public static var STAGE_FULLY_DESTROYED : 	Number = 0;
	public static var STAGE_HP_LOW : 					Number = 1;
	public static var STAGE_HP_HIGH : 					Number = 2;
	public static var STAGE_FULLY_REPAIRED : 		Number = 3;
	
	public static var m_aStageLabels : 					Array;//String
	public static var m_aActionLabels : 					Array;//String
	
	public var mcResourcePoint : 							MovieClip;
	public var mcStages : 									MovieClip;
	public var mcAction : 										MovieClip;
	private var lbIconName : 								TextField; 
	
	public var m_bResourcePointEnabled : 			Boolean;
	public var m_nStage  : 									Number;
	
	public function MiniMapItemDOTA () {
		super();
		defaultStatus = ACTION_NO_ACTION; 
		m_bResourcePointEnabled = true;
		m_aStageLabels = [];
		m_aStageLabels[STAGE_FULLY_DESTROYED] 		= "FullyDestroyed";
		m_aStageLabels[STAGE_HP_LOW] 						= "HP-Low";
		m_aStageLabels[STAGE_HP_HIGH]  						= "HP-High";
		m_aStageLabels[STAGE_FULLY_REPAIRED] 			= "FullyRepaired";
		
		m_aActionLabels = [];		
		m_aActionLabels[ACTION_NO_ACTION]  				= "NoAction";   // 0 ->  29%
		m_aActionLabels[ACTION_REPAIRNG]   				= "Repair";    // 30 ->  79%
		m_aActionLabels[ACTION_DESTROYING] 				= "Destroy";   // 80 -> 100%
		m_bResourcePointEnabled = true;
		
		//Force GUI to update the 1st time
		m_nStage = STAGE_INVALID;
		status = ACTION_NOT_INIT;
	}
	
	public function setStatus (action:Number, stage:Number, resourcePoint:Boolean) : Void {
		if (action == undefined || stage == undefined || resourcePoint == undefined) {
			return;
		}
		
		if (status != action) {
			status = action;
			mcAction.gotoAndPlay(m_aActionLabels[status]);
			dispatchEvent ( { type : "changeState", target : this, newState : status} );
		}
		
		if (m_bResourcePointEnabled != resourcePoint) {
			m_bResourcePointEnabled = mcResourcePoint._visible = resourcePoint;
		}
		
		if (m_nStage != stage) {
			m_nStage  = stage;
			mcStages.gotoAndPlay(m_aStageLabels[m_nStage]);	
		}
	}
	
	public function setName (name : Number) : Void {
		lbIconName.text = "" + name;
	}
}