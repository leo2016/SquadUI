﻿import gfx.controls.SQUAD.Data.BaseItem;
import gfx.controls.SQUAD.DragDropComponent;
import gfx.controls.SQUAD.ImagePathManager;

import gfx.controls.Label;
import gfx.controls.ListItemRenderer;
import gfx.controls.UILoader;
import gfx.events.EventDispatcher;

class gfx.controls.SQUAD.Shop.LIRShopAddOn extends ListItemRenderer {
	private var uiItemImage : 			UILoader;
	private var labItemName : 			Label;	
	private var labPrice :					Label;
	private var mcBorder : 				MovieClip;
	private var mcMask : 					MovieClip;
	private var ddc : 						DragDropComponent;
	
	public function LIRShopAddOn () {
		super ();
		
		EventDispatcher.initialize (this);		
		addEventListener ("handleItemDrag", _parent._parent);
		//ddc._visible = false;
	}
	
	private function configUI () : Void {
		super.configUI ();
		
		ddc.addEventListener("beginDrag", this, "beginDrag");
		ddc.addEventListener("drag", this, "drag1");		
	}
	
	private function beginDrag(evt:Object):Void {		
		if (data == undefined || data == null || !allowDrag()) {
			ddc.cancelDrag();
		}
	}		
	
	private function drag1(evt:Object):Void {
		if (!disabled) {			
			dispatchEvent( { type: "handleItemDrag", target: this, data: data.item, from: _parent._parent } );
		}
	}
	
	public function setData(data:Object):Void {
		/**
		 * data
		 * 		item : BaseItem		 
		 * 		suitable : Boolean
		 */
		if(data != undefined) {
			this.data = data;
			updateAfterStateChange();							
			
			this.visible = true;			
		} else {
			this.visible = false;
			this._disabled = true;
		}		
	}
		
	public function updateAfterStateChange():Void {
		if (data.item != null && data.item != undefined && (labItemName instanceof Label)) {
			labItemName.text = data.item.name ();			
			if (data.suitable) {
				mcMask._visible = false;
				this._disabled = false;
			} else {
				mcMask._visible = true;
				this._disabled = true;
			}			
			if (data.item.image() == undefined) {
				if (uiItemImage.source != ImagePathManager.DEFAULT_ITEM_IMAGE) {
					uiItemImage.source = ImagePathManager.DEFAULT_ITEM_IMAGE;
				}
			} else if (uiItemImage.source != ImagePathManager.INVENTORY_ITEM+data.item.image()) {
				uiItemImage.source = ImagePathManager.INVENTORY_ITEM+data.item.image();
			}
			var s : String = "";			
			if (data.item.allowBuy()) {
				var minDay : Number = data.item.getMinRentingDays();			
				if (data.item.getGPPriceForRentingDays(minDay) != undefined && data.item.getGPPriceForRentingDays(minDay) != -1) {
					s = data.item.getGPPriceForRentingDays(minDay) + " GP (";				
				} else {
					s = data.item.getVCoinPriceForRentingDays(minDay) + " VCoin (";				
				}
				if (minDay == -1) {
					s += "Mua)";
				} else {
					s += "Dùng trong " + minDay + " ngày)";
				}
			} else {
				s = "Chưa bán";
			}
			labPrice.text = s;
		}		
	}
	
	private function allowDrag() : Boolean {
		return data.suitable;
	}
}