﻿import gfx.controls.Button;
import gfx.controls.SQUAD.Data.BaseItem;
import gfx.controls.SQUAD.StringUtility;
import gfx.controls.UILoader;
import gfx.managers.DragManager;

import gfx.controls.SQUAD.DragDropComponent;
import gfx.controls.SQUAD.ImagePathManager;

class gfx.controls.SQUAD.Shop.DataSlot extends Button {
	public var mcMask	:	MovieClip;
	public var mcBorder :	MovieClip;
	public var uiImage: 	UILoader;
	public var ddc: 			DragDropComponent;
	public var allowDrag:	Boolean;
	
	private var item: BaseItem;	
	public var suitableCatalogueId : Number;
	public var suitableCatalogueName : String;	
		
	public function DataSlot() {
		super();
		item = undefined;	
		hideHint();
		mcMask._visible = false;
		allowDrag = false;
	}
	
	private function configUI():Void {
		super.configUI();
		
		ddc.addEventListener("beginDrag", this, "beginDrag");
		ddc.addEventListener("drag", this, "drag");		
		DragManager.instance.addEventListener ("dragEnd", this, "hideHint");		
		
		label = "";
	}
	
	private function beginDrag(evt:Object):Void {
		if (item == undefined || item == null || !allowDrag) {
			ddc.cancelDrag();
		}
	}
	
	private function drag(evt:Object):Void {
		if (!_disabled && item != null && item != undefined) {
			var draggingItem:MovieClip = DragManager.instance.startDrag (this, "DraggingItem", { target:this, data:item, from:this }, this, this);
			draggingItem.setData (item);
			draggingItem._xscale = 100;
			draggingItem._yscale = 100;
		}
	}
	
	public function setData (item : BaseItem) : Void {
		this.item = item;
		_label = item.name(); 
		trace ("Set name for DataSlot = " + _label);
		updateAfterStateChange ();
	}
	
	public function unsetData () : Void {
		this.item = undefined;
		_label = StringUtility.convert(suitableCatalogueName, StringUtility.MODE_IN_UTF8, StringUtility.MODE_OUT_UPPER_UTF8);
		updateAfterStateChange ();
	}
	
	private function updateAfterStateChange():Void {		
		// Redraw should only happen AFTER the initialization.
		if (!initialized) { return; }
		validateNow();// Ensure that the width/height is up to date.
		
		textField.text = _label;
		if (item == undefined) {
			uiImage._visible = false;
		} else {		
			uiImage._visible = true;
			if (item.image() == undefined) {
				if (uiImage.source != ImagePathManager.DEFAULT_ITEM_IMAGE) {
					uiImage.source = ImagePathManager.DEFAULT_ITEM_IMAGE;
				}
			} else if (uiImage.source != ImagePathManager.INVENTORY_ITEM_SIDE_SLOTS + item.image()) {				
				uiImage.source = ImagePathManager.INVENTORY_ITEM_SIDE_SLOTS + item.image();
			}
		}
		mcMask._visible = _disabled;
		if (constraints != null) { 
			constraints.update(width, height);
		}
		dispatchEvent({type:"stateChange", state:state});
	}
	
	public function showHint () : Void {
		mcBorder._visible = true;
	}
	
	public function hideHint () : Void {
		mcBorder._visible = false;
	}
}