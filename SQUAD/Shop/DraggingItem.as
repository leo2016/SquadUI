﻿import gfx.controls.SQUAD.Data.BaseItem;
import gfx.controls.SQUAD.ImagePathManager;

import gfx.controls.Label;
import gfx.controls.UILoader;

class gfx.controls.SQUAD.Shop.DraggingItem extends MovieClip{
	public var mcContainerName : 		MovieClip;	
	public var mcContainerImage :		MovieClip;	
	public var mcContainerPrice :			MovieClip;
	
	private var labItemName : 				MovieClip;
	private var uiItemImage : 				MovieClip;	
	private var labItemPrice :				MovieClip;	
		
	public function DraggingItem () {
		super ();
		
		labItemName 		= mcContainerName.attachMovie ("labItemName", "labItemName", getNextHighestDepth ());		
		labItemPrice 			= mcContainerPrice.attachMovie ("labItemExpiration", "labItemExpiration", getNextHighestDepth ());		
		var w: Number 		= mcContainerImage._width;
		var h : Number 		= mcContainerImage._height;
		uiItemImage			= mcContainerImage.attachMovie ("uiItemImage", "uiItemImage", getNextHighestDepth ());			
		uiItemImage._width = 166;
		uiItemImage._height = 53;
	}
	
	public function setData (data : BaseItem) : Void {		
		if (data != undefined && data != null) {			
			labItemName.text = data.name ();
			var s : String = "";
			if (data.allowBuy()) {
				var minDay : Number = data.getMinRentingDays();				
				if (data.getGPPriceForRentingDays(minDay) != undefined && data.getGPPriceForRentingDays(minDay) != -1) {
					s = data.getGPPriceForRentingDays(minDay) + " GP (";				
				} else {
					s = data.getVCoinPriceForRentingDays(minDay) + " VCoin (";				
				}
				if (minDay == -1) {
					s += "Mua)";
				} else {
					s += "Dùng trong " + minDay + " ngày)";
				}
			}
			labItemPrice.text = s;
			if (data.image() == undefined) {
				if (uiItemImage.source != ImagePathManager.DEFAULT_ITEM_IMAGE) {
					uiItemImage.source = ImagePathManager.DEFAULT_ITEM_IMAGE;
				}
			} else if (uiItemImage.source != ImagePathManager.INVENTORY_ITEM+data.image()) {
				uiItemImage.source = ImagePathManager.INVENTORY_ITEM+data.image();
			}			
		}
	}
}