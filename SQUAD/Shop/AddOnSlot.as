﻿import gfx.controls.Button;
import gfx.controls.SQUAD.ImagePathManager;
import gfx.controls.UILoader;
import gfx.managers.DragManager;

import gfx.controls.SQUAD.Data.BaseItem;
import gfx.controls.SQUAD.DragDropComponent;

class gfx.controls.SQUAD.Shop.AddOnSlot extends Button {
	public var mcMask	:	MovieClip;
	public var mcBorder :	MovieClip;
	public var uiImage: 	UILoader;
	public var ddc: 			DragDropComponent;
	public var allowDrag:	Boolean;
	
	private var item: BaseItem;	
	public var suitableCatalogueId : Number;
	public var suitableCatalogueName : String;	
		
	public function AddOnSlot() {
		super();
		item = undefined;	
		hideHint();
		mcMask._visible = false;
		allowDrag = true;
	}
	
	private function configUI():Void {
		super.configUI();
		
		ddc.addEventListener("beginDrag", this, "beginDrag");
		ddc.addEventListener("drag", this, "drag");
		ddc.addEventListener("drop", this, "drop");
		DragManager.instance.addEventListener ("dragEnd", this, "hideHint");
	}
	
	private function beginDrag(evt:Object):Void {
		if (item == undefined || item == null || !allowDrag) {
			ddc.cancelDrag();
		}
	}
	
	private function drag(evt:Object):Void {
		if (!_disabled && item != null && item != undefined) {
			var draggingItem:MovieClip = DragManager.instance.startDrag (this, "DraggingAddOn", { target:this, data:item, from:this }, this, this);
			draggingItem.setData (item);
			draggingItem._xscale = 100;
			draggingItem._yscale = 100;
		}
	}
	
	private function drop(evt:Object):Void {		
		if (!_disabled) {
			dispatchEvent( { type: "dropIntoAddOnSlot", target: this, from: evt.data.from, data: evt.data.data } );
		}
	}
	
	public function setAddOn (item : BaseItem) : Void {
		this.item = item;
		_label = item.name(); 
		updateAfterStateChange ();
	}
	
	public function unsetAddOn () : Void {
		this.item = undefined;
		_label = suitableCatalogueName;
		updateAfterStateChange ();
	}
	
	public function getAddOn () : BaseItem{
		return item;
	}
	
	private function updateAfterStateChange():Void {		
		// Redraw should only happen AFTER the initialization.
		if (!initialized) { return; }
		validateNow();// Ensure that the width/height is up to date.
		
		textField.text = _label;	
		if (item == undefined) {
			uiImage._visible = false;
		} else {			
			uiImage._visible = true;
			if (item.image() == undefined) {				
				if (uiImage.source != ImagePathManager.DEFAULT_ITEM_IMAGE) {
					uiImage.source = ImagePathManager.DEFAULT_ITEM_IMAGE;
				}
			} else if (uiImage.source != ImagePathManager.INVENTORY_ITEM+item.image()) {
				uiImage.source = ImagePathManager.INVENTORY_ITEM+item.image();
			}			
		}
		mcMask._visible = _disabled;		
		if (constraints != null) { 
			constraints.update(width, height);
		}
		dispatchEvent({type:"stateChange", state:state});	
	}
	
	public function showHint () : Void {
		mcBorder._visible = true;
	}
	
	public function hideHint () : Void {				
		mcBorder._visible = false;
	}
}