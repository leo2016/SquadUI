﻿import gfx.controls.SQUAD.Data.AddOn;
import gfx.controls.SQUAD.Data.BaseItem;
import gfx.controls.SQUAD.DragDropComponent;
import gfx.controls.SQUAD.ImagePathManager;

import gfx.controls.Label;
import gfx.controls.ListItemRenderer;
import gfx.controls.ProgressBar;
import gfx.controls.UILoader;
import gfx.events.EventDispatcher;

class gfx.controls.SQUAD.Shop.LIRItem extends ListItemRenderer {
	private var uiItemImage : 			UILoader;
	private var labItemName : 			Label;
	private var labPrice :					Label;
	private var uiUnlocked:				UILoader;
	private var ddc : 						DragDropComponent;
	
	public static var POP_UP_BUY : 			String = "MUA";
	public static var POP_UP_TRY : 			String = "THỬ";
	public static var POP_UP_BASKET : 		String = "CHO VÀO GIỎ";
	public static var POP_UP_GIVE : 			String = "TẶNG";
	public static var POP_UP_COMPARE : 	String = "SO SÁNH";
	
	public static var GP_FIRST :					Boolean = true;
	
	public function LIRItem () {
		super ();
		
		EventDispatcher.initialize (this);
		addEventListener ("handleItemDrag", _parent._parent);
		//ddc._visible = false;
	}
	
	private function configUI () : Void {
		super.configUI ();
		
		ddc.addEventListener("beginDrag", this, "beginDrag");
		ddc.addEventListener("drag", this, "drag");
	}
	
	private function beginDrag(evt:Object):Void {		
		if (data == undefined || data == null || !allowDrag()) {
			ddc.cancelDrag();
		}		
	}
	
	private function drag(evt:Object):Void {
		if (!disabled) {
			dispatchEvent( { type: "handleItemDrag", target: this, data: data, from: _parent._parent } );
		}
	}
	
	public function setData(data:Object):Void {
		if(data != undefined) {
			this.data = data;
			updateAfterStateChange();
						
			this.visible = true;
			this._disabled = false;
		} else {
			this.visible = false;
			this._disabled = true;
		}
	}

	public function updateAfterStateChange():Void {
		if (data instanceof BaseItem && (labItemName instanceof Label)) {
			labItemName.text = data.name ();
			var s : String = "";
			if (data.allowBuy()) {
				var minDay : Number = data.getMinRentingDays();
				if (GP_FIRST) {
					if (data.getGPPriceForRentingDays(minDay) != undefined && data.getGPPriceForRentingDays(minDay) != -1) {
						s = data.getGPPriceForRentingDays(minDay) + " GP (";				
					} else {
						s = data.getVCoinPriceForRentingDays(minDay) + " VCoin (";				
					}				
				} else {
					if (data.getVCoinPriceForRentingDays(minDay) != undefined && data.getVCoinPriceForRentingDays(minDay) != -1) {
						s = data.getVCoinPriceForRentingDays(minDay) + " VCoin (";				
					} else {
						s = data.getGPPriceForRentingDays(minDay) + " GP (";
					}				
				}
				if (minDay == -1) {
					s += "Mua)";
				} else {
					s += "Dùng trong " + minDay + " ngày)";
				}
			} else {
				s = "Chưa bán";
			}
			labPrice.text = s;
			if (data.image() == undefined) {
				if (uiItemImage.source != ImagePathManager.DEFAULT_ITEM_IMAGE) {
					uiItemImage.source = ImagePathManager.DEFAULT_ITEM_IMAGE;
				}
			} else if (uiItemImage.source != ImagePathManager.INVENTORY_ITEM+data.image()) {
				uiItemImage.source = ImagePathManager.INVENTORY_ITEM+data.image();
			}
			uiUnlocked._visible = !data.allowBuy();
		}
	}	
	
	private function allowDrag () : Boolean {
		if (data instanceof AddOn || !data.allowBuy()) {
			return false;
		}
		return true;
	}	
}