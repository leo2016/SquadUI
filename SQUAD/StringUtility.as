﻿/**
 * Class dùng để chuyển đổi qua lại các ký tự tiếng Việt có dấu (mã UTF8) sang tiếng Việt không dấu (mã ASCII) hoặc
 * có thêm các ký tự mã ASCII thể hiện dấu tiếng Việt
 * 
 * Chẳng hạn:
 * 		UTF8		"Phụ kiện khác"
 * 		ASCII		"Phu kien khac"
 * 		FAKE		"Phu. kie^.n kha'c"
 * 		VNI		"Phuï kieän khaùc"
 */
class gfx.controls.SQUAD.StringUtility {
	/**
	 * Mảng chứa các ký tự cần chuyển đổi qua lại giữa các "bảng mã"
	 * Lập trình viên không cần và không được can thiệp tới mảng này!
	 */
	private static var m_aExtendMap : Array = [
		{
			lowerUTF8	:	"á", 
			upperUTF8:	"Á", 
			lowerASCII:	"a", 
			upperASCII:	"A", 
			lowerFake:		"a'", 
			upperFake:	"A'", 
			lowerVNI:		"aù", 
			upperVNI:		"AÙ"
		}, 
		{
			lowerUTF8	:	"à", 
			upperUTF8:	"À", 
			lowerASCII:	"a", 
			upperASCII:	"A", 
			lowerFake:		"a`", 
			upperFake:	"A`", 
			lowerVNI:		"aø", 
			upperVNI:		"AØ"
		}, 
		{
			lowerUTF8	:	"ả", 
			upperUTF8:	"Ả", 
			lowerASCII:	"a", 
			upperASCII:	"A", 
			lowerFake:		"a?", 
			upperFake:	"A?", 
			lowerVNI:		"aû", 
			upperVNI:		"AÛ"
		}, 
		{
			lowerUTF8	:	"ã", 
			upperUTF8:	"Ã", 
			lowerASCII:	"a", 
			upperASCII:	"A", 
			lowerFake:		"a~", 
			upperFake:	"A~", 
			lowerVNI:		"aõ", 
			upperVNI:		"AÕ"
		}, 
		{
			lowerUTF8	:	"ạ", 
			upperUTF8:	"Ạ", 
			lowerASCII:	"a", 
			upperASCII:	"A", 
			lowerFake:		"a.", 
			upperFake:	"A.", 
			lowerVNI:		"aï", 
			upperVNI:		"AÏ"
		}, 		
		{
			lowerUTF8	:	"ắ", 
			upperUTF8:	"Ắ", 
			lowerASCII:	"a", 
			upperASCII:	"A", 
			lowerFake:		"ä'", 
			upperFake:	"ä'", 
			lowerVNI:		"aé", 
			upperVNI:		"AÉ"
		}, 
		{
			lowerUTF8	:	"ằ", 
			upperUTF8:	"Ằ", 
			lowerASCII:	"a", 
			upperASCII:	"A", 
			lowerFake:		"ä`", 
			upperFake:	"ä`", 
			lowerVNI:		"aè", 
			upperVNI:		"AÈ"
		}, 
		{
			lowerUTF8	:	"ẳ", 
			upperUTF8:	"Ẳ", 
			lowerASCII:	"a", 
			upperASCII:	"A",
			lowerFake:		"ä?", 
			upperFake:	"ä?", 
			lowerVNI:		"aú", 
			upperVNI:		"AÚ"
		}, 		
		{
			lowerUTF8	:	"ẵ", 
			upperUTF8:	"Ẵ", 
			lowerASCII:	"a", 
			upperASCII:	"A", 
			lowerFake:		"ä~", 
			upperFake:	"ä~", 
			lowerVNI:		"aü", 
			upperVNI:		"AÜ"
		}, 
		{
			lowerUTF8	:	"ặ", 
			upperUTF8:	"Ặ", 
			lowerASCII:	"a", 
			upperASCII:	"A", 
			lowerFake:		"ä.", 
			upperFake:	"ä.", 
			lowerVNI:		"aë", 
			upperVNI:		"AË"
		}, 
		{
			lowerUTF8	:	"ă", 
			upperUTF8:	"Ă", 
			lowerASCII:	"a", 
			upperASCII:	"A", 
			lowerFake:		"ä", 
			upperFake:	"ä", 
			lowerVNI:		"aê", 
			upperVNI:		"AÊ"
		}, 		
		{
			lowerUTF8	:	"ấ", 
			upperUTF8:	"Ấ", 
			lowerASCII:	"a", 
			upperASCII:	"A", 
			lowerFake:		"a^'", 
			upperFake:	"A^'", 
			lowerVNI:		"aá", 
			upperVNI:		"AÁ"
		}, 
		{
			lowerUTF8	:	"ầ", 
			upperUTF8:	"Ầ", 
			lowerASCII:	"a", 
			upperASCII:	"A", 
			lowerFake:		"a^`", 
			upperFake:	"A^`", 
			lowerVNI:		"aà", 
			upperVNI:		"AÀ"
		}, 
		{
			lowerUTF8	:	"ẩ", 
			upperUTF8:	"Ẩ", 
			lowerASCII:	"a", 
			upperASCII:	"A", 
			lowerFake:		"a^?", 
			upperFake:	"A^?", 
			lowerVNI:		"aå", 
			upperVNI:		"AÅ"
		}, 		
		{
			lowerUTF8	:	"ẫ", 
			upperUTF8:	"Ẫ", 
			lowerASCII:	"a", 
			upperASCII:	"A", 
			lowerFake:		"a^~", 
			upperFake:	"A^~", 
			lowerVNI:		"aã", 
			upperVNI:		"AÃ"
		}, 
		{
			lowerUTF8	:	"ậ", 
			upperUTF8:	"Ậ", 
			lowerASCII:	"a", 
			upperASCII:	"A", 
			lowerFake:		"a^.", 
			upperFake:	"A^.", 
			lowerVNI:		"aä", 
			upperVNI:		"AÄ"
		},
		{
			lowerUTF8	:	"â", 
			upperUTF8:	"Â", 
			lowerASCII:	"a", 
			upperASCII:	"A",
			lowerFake:		"a^", 
			upperFake:	"A^", 
			lowerVNI:		"aâ", 
			upperVNI:		"AÂ"
		}, 
		{
			lowerUTF8	:	"é", 
			upperUTF8:	"É", 
			lowerASCII:	"e", 
			upperASCII:	"E", 
			lowerFake:		"e'", 
			upperFake:	"E'", 
			lowerVNI:		"eù", 
			upperVNI:		"EÙ"
		},
		{
			lowerUTF8	:	"è", 
			upperUTF8:	"È", 
			lowerASCII:	"e", 
			upperASCII:	"E", 
			lowerFake:		"e`", 
			upperFake:	"E`", 
			lowerVNI:		"eø", 
			upperVNI:		"EØ"
		},
		{
			lowerUTF8	:	"ẻ", 
			upperUTF8:	"Ẻ", 
			lowerASCII:	"e", 
			upperASCII:	"E", 
			lowerFake:		"e?", 
			upperFake:	"E?", 
			lowerVNI:		"eû", 
			upperVNI:		"EÛ"
		},
		{
			lowerUTF8	:	"ẽ", 
			upperUTF8:	"Ẽ", 
			lowerASCII:	"e", 
			upperASCII:	"E",
			lowerFake:		"e~", 
			upperFake:	"E~", 
			lowerVNI:		"eõ", 
			upperVNI:		"EÕ"
		},
		{
			lowerUTF8	:	"ẹ", 
			upperUTF8:	"Ẹ", 
			lowerASCII:	"e", 
			upperASCII:	"E", 
			lowerFake:		"e.", 
			upperFake:	"E.", 
			lowerVNI:		"eï", 
			upperVNI:		"EÏ"
		},		
		{
			lowerUTF8	:	"ế", 
			upperUTF8:	"Ế", 
			lowerASCII:	"e", 
			upperASCII:	"E", 
			lowerFake:		"e^'", 
			upperFake:	"E^'", 
			lowerVNI:		"eá", 
			upperVNI:		"EÁ"
		},
		{
			lowerUTF8	:	"ề", 
			upperUTF8:	"Ề", 
			lowerASCII:	"e", 
			upperASCII:	"E", 
			lowerFake:		"e^`", 
			upperFake:	"E^`", 
			lowerVNI:		"eà", 
			upperVNI:		"EÀ"
		},
		{
			lowerUTF8	:	"ể", 
			upperUTF8:	"Ể", 
			lowerASCII:	"e", 
			upperASCII:	"E", 
			lowerFake:		"e^?", 
			upperFake:	"E^?", 
			lowerVNI:		"eå", 
			upperVNI:		"EÅ"
		},
		{
			lowerUTF8	:	"ễ", 
			upperUTF8:	"Ễ", 
			lowerASCII:	"e", 
			upperASCII:	"E", 
			lowerFake:		"e^~", 
			upperFake:	"E^~", 
			lowerVNI:		"eã", 
			upperVNI:		"EÃ"
		},
		{
			lowerUTF8	:	"ệ", 
			upperUTF8:	"Ệ", 
			lowerASCII:	"e", 
			upperASCII:	"E", 
			lowerFake:		"e^.", 
			upperFake:	"E^.", 
			lowerVNI:		"eä", 
			upperVNI:		"EÄ"
		},
		{
			lowerUTF8	:	"ê", 
			upperUTF8:	"Ê", 
			lowerASCII:	"e", 
			upperASCII:	"E", 
			lowerFake:		"e^", 
			upperFake:	"E^", 
			lowerVNI:		"eâ", 
			upperVNI:		"EÂ"
		},
		{
			lowerUTF8	:	"ý", 
			upperUTF8:	"Ý", 
			lowerASCII:	"y", 
			upperASCII:	"Y", 
			lowerFake:		"y'", 
			upperFake:	"Y'", 
			lowerVNI:		"yù", 
			upperVNI:		"YÙ"
		},
		{
			lowerUTF8	:	"ỳ", 
			upperUTF8:	"Ỳ", 
			lowerASCII:	"y", 
			upperASCII:	"Y", 
			lowerFake:		"y`", 
			upperFake:	"Y`", 
			lowerVNI:		"yø", 
			upperVNI:		"YØ"
		},
		{
			lowerUTF8	:	"ỷ", 
			upperUTF8:	"Ỷ", 
			lowerASCII:	"y", 
			upperASCII:	"Y", 
			lowerFake:		"y?", 
			upperFake:	"Y?", 
			lowerVNI:		"yû", 
			upperVNI:		"YÛ"
		},
		{
			lowerUTF8	:	"ỹ", 
			upperUTF8:	"Ỹ", 
			lowerASCII:	"y", 
			upperASCII:	"Y", 
			lowerFake:		"y~", 
			upperFake:	"Y~", 
			lowerVNI:		"yõ", 
			upperVNI:		"YÕ"
		},
		{
			lowerUTF8	:	"ỵ", 
			upperUTF8:	"Ỵ", 
			lowerASCII:	"y", 
			upperASCII:	"Y", 
			lowerFake:		"y.", 
			upperFake:	"Y.", 
			lowerVNI:		"î", 
			upperVNI:		"Î"
		},
		{
			lowerUTF8	:	"ù", 
			upperUTF8:	"Ù", 
			lowerASCII:	"u", 
			upperASCII:	"U", 
			lowerFake:		"u`", 
			upperFake:	"U`", 
			lowerVNI:		"uø", 
			upperVNI:		"UØ"
		},
		{
			lowerUTF8	:	"ú", 
			upperUTF8:	"Ú", 
			lowerASCII:	"u", 
			upperASCII:	"U", 
			lowerFake:		"u'", 
			upperFake:	"U'", 
			lowerVNI:		"uù", 
			upperVNI:		"UÙ"
		},
		{
			lowerUTF8	:	"ủ", 
			upperUTF8:	"Ủ", 
			lowerASCII:	"u", 
			upperASCII:	"U", 
			lowerFake:		"u?", 
			upperFake:	"U?", 
			lowerVNI:		"uû", 
			upperVNI:		"UÛ"
		},
		{
			lowerUTF8	:	"ũ", 
			upperUTF8:	"Ũ", 
			lowerASCII:	"u", 
			upperASCII:	"U", 
			lowerFake:		"u~", 
			upperFake:	"U~", 
			lowerVNI:		"uõ", 
			upperVNI:		"UÕ"
		},
		{
			lowerUTF8	:	"ụ", 
			upperUTF8:	"Ụ", 
			lowerASCII:	"u", 
			upperASCII:	"U", 
			lowerFake:		"u.", 
			upperFake:	"U.", 
			lowerVNI:		"uï", 
			upperVNI:		"UÏ"
		},		
		{
			lowerUTF8	:	"ừ", 
			upperUTF8:	"Ừ", 
			lowerASCII:	"u", 
			upperASCII:	"U", 
			lowerFake:		"u*`", 
			upperFake:	"U*`", 
			lowerVNI:		"öø", 
			upperVNI:		"ÖØ"
		},
		{
			lowerUTF8	:	"ứ", 
			upperUTF8:	"Ứ", 
			lowerASCII:	"u", 
			upperASCII:	"U", 
			lowerFake:		"u*'", 
			upperFake:	"U*'", 
			lowerVNI:		"öù", 
			upperVNI:		"ÖÙ"
		},
		{
			lowerUTF8	:	"ử", 
			upperUTF8:	"Ử", 
			lowerASCII:	"u", 
			upperASCII:	"U", 
			lowerFake:		"u*?", 
			upperFake:	"U*?", 
			lowerVNI:		"öû", 
			upperVNI:		"ÖÛ"
		},
		{
			lowerUTF8	:	"ữ", 
			upperUTF8:	"Ữ", 
			lowerASCII:	"u", 
			upperASCII:	"U", 
			lowerFake:		"u*~", 
			upperFake:	"U*~", 
			lowerVNI:		"öõ", 
			upperVNI:		"ÖÕ"
		},
		{
			lowerUTF8	:	"ự", 
			upperUTF8:	"Ự", 
			lowerASCII:	"u", 
			upperASCII:	"U", 
			lowerFake:		"u*.", 
			upperFake:	"U*.", 
			lowerVNI:		"öï", 
			upperVNI:		"ÖÏ"
		},
		{
			lowerUTF8	:	"ư", 
			upperUTF8:	"Ư", 
			lowerASCII:	"u", 
			upperASCII:	"U", 
			lowerFake:		"u*", 
			upperFake:	"U*", 
			lowerVNI:		"ö", 
			upperVNI:		"Ö"
		},
		{
			lowerUTF8	:	"ò", 
			upperUTF8:	"Ò", 
			lowerASCII:	"o", 
			upperASCII:	"O", 
			lowerFake:		"o`", 
			upperFake:	"O`", 
			lowerVNI:		"oø", 
			upperVNI:		"OØ"
		},
		{
			lowerUTF8	:	"ó", 
			upperUTF8:	"Ó", 
			lowerASCII:	"o", 
			upperASCII:	"O", 
			lowerFake:		"o'", 
			upperFake:	"O'", 
			lowerVNI:		"où", 
			upperVNI:		"OÙ"
		},
		{
			lowerUTF8	:	"ỏ", 
			upperUTF8:	"Ỏ", 
			lowerASCII:	"o", 
			upperASCII:	"O", 
			lowerFake:		"o?", 
			upperFake:	"O?", 
			lowerVNI:		"oû", 
			upperVNI:		"OÛ"
		},
		{
			lowerUTF8	:	"õ", 
			upperUTF8:	"Õ", 
			lowerASCII:	"o", 
			upperASCII:	"O", 
			lowerFake:		"o~", 
			upperFake:	"O~", 
			lowerVNI:		"oõ", 
			upperVNI:		"OÕ"
		},
		{
			lowerUTF8	:	"ọ", 
			upperUTF8:	"Ọ", 
			lowerASCII:	"o", 
			upperASCII:	"O", 
			lowerFake:		"o.", 
			upperFake:	"O.", 
			lowerVNI:		"oï", 
			upperVNI:		"OÏ"
		},
		{
			lowerUTF8	:	"ồ", 
			upperUTF8:	"Ồ", 
			lowerASCII:	"o", 
			upperASCII:	"O", 
			lowerFake:		"o^`", 
			upperFake:	"O^`", 
			lowerVNI:		"oà", 
			upperVNI:		"OÀ"
		},
		{
			lowerUTF8	:	"ố", 
			upperUTF8:	"Ố", 
			lowerASCII:	"o", 
			upperASCII:	"O", 
			lowerFake:		"o^'", 
			upperFake:	"O^'", 
			lowerVNI:		"oá", 
			upperVNI:		"OÁ"
		},
		{
			lowerUTF8	:	"ổ", 
			upperUTF8:	"Ổ", 
			lowerASCII:	"o", 
			upperASCII:	"O", 
			lowerFake:		"o^?", 
			upperFake:	"O^?", 
			lowerVNI:		"oå", 
			upperVNI:		"OÅ"
		},
		{
			lowerUTF8	:	"ỗ", 
			upperUTF8:	"Ỗ", 
			lowerASCII:	"o", 
			upperASCII:	"O", 
			lowerFake:		"o^~", 
			upperFake:	"O^~", 
			lowerVNI:		"oã", 
			upperVNI:		"OÃ"
		},
		{
			lowerUTF8	:	"ộ", 
			upperUTF8:	"Ộ", 
			lowerASCII:	"o", 
			upperASCII:	"O", 
			lowerFake:		"o^.", 
			upperFake:	"O^.", 
			lowerVNI:		"oä", 
			upperVNI:		"OÄ"
		},
		{
			lowerUTF8	:	"ô", 
			upperUTF8:	"Ô", 
			lowerASCII:	"o", 
			upperASCII:	"O", 
			lowerFake:		"o^", 
			upperFake:	"O^", 
			lowerVNI:		"oâ", 
			upperVNI:		"OÂ"
		},
		{
			lowerUTF8	:	"ờ", 
			upperUTF8:	"Ờ", 
			lowerASCII:	"o", 
			upperASCII:	"O", 
			lowerFake:		"o*`", 
			upperFake:	"O*`", 
			lowerVNI:		"ôø", 
			upperVNI:		"ÔØ"
		},
		{
			lowerUTF8	:	"ớ", 
			upperUTF8:	"Ớ", 
			lowerASCII:	"o", 
			upperASCII:	"O", 
			lowerFake:		"o*'", 
			upperFake:	"O*'", 
			lowerVNI:		"ôù", 
			upperVNI:		"ÔÙ"
		},
		{
			lowerUTF8	:	"ở", 
			upperUTF8:	"Ở", 
			lowerASCII:	"o", 
			upperASCII:	"O", 
			lowerFake:		"o*?", 
			upperFake:	"O*?", 
			lowerVNI:		"ôû", 
			upperVNI:		"ÔÛ"
		},
		{
			lowerUTF8	:	"ỡ", 
			upperUTF8:	"Ỡ", 
			lowerASCII:	"o", 
			upperASCII:	"O", 
			lowerFake:		"o*~", 
			upperFake:	"O*~", 
			lowerVNI:		"ôõ", 
			upperVNI:		"ÔÕ"
		},
		{
			lowerUTF8	:	"ợ", 
			upperUTF8:	"Ợ", 
			lowerASCII:	"o", 
			upperASCII:	"O", 
			lowerFake:		"o*.", 
			upperFake:	"O*.", 
			lowerVNI:		"ôï", 
			upperVNI:		"ÔÏ"
		},
		{
			lowerUTF8	:	"ơ", 
			upperUTF8:	"Ơ", 
			lowerASCII:	"o", 
			upperASCII:	"O", 
			lowerFake:		"o*", 
			upperFake:	"O*", 
			lowerVNI:		"ô", 
			upperVNI:		"Ô"
		}, 
		{
			lowerUTF8	:	"ì", 
			upperUTF8:	"Ì", 
			lowerASCII:	"i", 
			upperASCII:	"I", 
			lowerFake:		"i`", 
			upperFake:	"I`", 
			lowerVNI:		"ì", 
			upperVNI:		"Ì"
		},
		{
			lowerUTF8	:	"í", 
			upperUTF8:	"Í", 
			lowerASCII:	"i", 
			upperASCII:	"I", 
			lowerFake:		"i'", 
			upperFake:	"I'", 
			lowerVNI:		"í", 
			upperVNI:		"Í"
		},
		{
			lowerUTF8	:	"ỉ", 
			upperUTF8:	"Ỉ", 
			lowerASCII:	"i", 
			upperASCII:	"I", 
			lowerFake:		"i?", 
			upperFake:	"I?", 
			lowerVNI:		"æ", 
			upperVNI:		"Æ"
		},
		{
			lowerUTF8	:	"ĩ", 
			upperUTF8:	"Ĩ", 
			lowerASCII:	"i", 
			upperASCII:	"I", 
			lowerFake:		"i~", 
			upperFake:	"I~", 
			lowerVNI:		"ó", 
			upperVNI:		"Ó"
		},
		{
			lowerUTF8	:	"ị", 
			upperUTF8:	"Ị", 
			lowerASCII:	"i", 
			upperASCII:	"I", 
			lowerFake:		"i.", 
			upperFake:	"I.", 
			lowerVNI:		"ò", 
			upperVNI:		"Ò"
		}, 
		{
			lowerUTF8	:	"đ", 
			upperUTF8:	"Đ", 
			lowerASCII:	"d", 
			upperASCII:	"D", 
			lowerFake:		"d¯", 
			upperFake:	"Ð", 
			lowerVNI:		"ñ", 
			upperVNI:		"Ñ"
		}
	];
	
	//------------------------------------------------------------------------------------------------------------	
	public static var MODE_IN_UTF8 : Number						= 0;		//Tiếng Việt có dấu
	public static var MODE_IN_ASCII : Number						= 1;		//Tiếng Việt không dấu
	public static var MODE_IN_FAKE : Number						= 2;		//Tiếng Việt giả dấu
	public static var MODE_IN_VNI : Number							= 3;		//Tiếng Việt viết bằng bảng mã VNI
	
	//------------------------------------------------------------------------------------------------------------
	public static var MODE_OUT_LOWER_UTF8 : 	Number 	= 0;		//Tiếng Việt có dấu, chữ thường
	public static var MODE_OUT_NORMAL_UTF8 : 	Number 	= 1;		//Tiếng Việt có dấu
	public static var MODE_OUT_UPPER_UTF8 : 		Number 	= 2;		//Tiếng Việt có dấu, chữ hoa
	public static var MODE_OUT_LOWER_ASCII : 	Number 	= 3;		//Tiếng Việt không dấu, chữ thường
	public static var MODE_OUT_NORMAL_ASCII : 	Number 	= 4;		//Tiếng Việt không dấu
	public static var MODE_OUT_UPPER_ASCII : 		Number 	= 5;		//Tiếng Việt không dấu, chữ hoa
	public static var MODE_OUT_LOWER_FAKE : 	Number 	= 6;		//Tiếng Việt giả dấu, chữ thường
	public static var MODE_OUT_NORMAL_FAKE : 	Number 	= 7;		//Tiếng Việt giả dấu
	public static var MODE_OUT_UPPER_FAKE : 		Number 	= 8;		//Tiếng Việt giả dấu, chữ hoa
	public static var MODE_OUT_LOWER_VNI : 	Number 		= 9;		//Tiếng Việt VNI, chữ thường
	public static var MODE_OUT_NORMAL_VNI : 	Number 	= 10;	//Tiếng Việt VNI
	public static var MODE_OUT_UPPER_VNI : 		Number 	= 11;	//Tiếng Việt VNI, chữ hoa
	
	/**
	 * Hàm chuyển đổi "bảng mã" cho các xâu ký tự
	 * 
	 * @param	input				Chuỗi đầu vào
	 * @param	modeIn			Bảng mã của chuỗi đầu vào	(MODE_IN_*)
	 * @param	modeOut		Bảng mã của chuỗi đầu ra		(MODE_OUT_*)
	 * 
	 * @return		Chuỗi ký tự đã được chuyển đổi "bảng mã"
	 */
	public static function convert (input : String, modeIn : Number, modeOut : Number) : String {
		var output : String = "";
		switch (modeIn) {
			case MODE_IN_ASCII:
			case MODE_IN_UTF8:
			{
				var n : Number = input.length;
				for (var i : Number = 0; i < n; ++i ) {					
					output += map (input.charAt(i), modeIn, modeOut);
				}
				break;
			}
			case MODE_IN_FAKE:
			case MODE_IN_VNI:
			{
				var append : String;
				var n : Number = input.length;
				for (var i : Number = 0; i < n; ++i ) {										
					//test substring with length = 3
					append = map (input.substr(i, 3), modeIn, modeOut);
					if (append != "") {						
						output += append;
						i += 2;	//move i to the end of the substring						
					} else {
						//test substring with length = 2
						append = map (input.substr(i, 2), modeIn, modeOut);						
						if (append != "") {
							output += append;
							++i;	//move i to the end of the substring							
						} else {												
							output += map (input.charAt(i), MODE_IN_ASCII, modeOut);
						}
					}					
				}
				break;
			}
			default:
				break;
		}
		return output;
	}
	
	/**
	 * Hàm chuyển đổi "bảng mã" cho 1 ký tự UTF8 hoặc 1 ký tự ASCII hoặc "1 ký tự" FAKE.
	 * Hàm convert sẽ thực hiện việc chuyển đổi bằng cách áp dụng hàm map này với từng "ký tự"
	 * (Riêng FAKE thì "1 ký tự" định nghĩa bằng tối đa 3 ký tự ASCII)	 
	 * 
	 * @param	input			Ký tự đầu vào
	 * @param	modeIn		Bảng mã của ký tự đầu vào	(MODE_IN_*)
	 * @param	modeOut	Bảng mã của ký tự đầu ra		(MODE_OUT_*)
	 * 
	 * @return		Ký tự đã được chuyển đổi "bảng mã"
	 */
	private static function map (input : String, modeIn : Number, modeOut : Number) : String {
		switch (modeIn) {			
			//-----------------------------------------------------------------------------------
			case MODE_IN_ASCII:
			{
				switch (modeOut) {
					case MODE_OUT_LOWER_ASCII:
					case MODE_OUT_LOWER_FAKE:
					case MODE_OUT_LOWER_UTF8:
					{
						return input.toLowerCase();
					}
					case MODE_OUT_UPPER_ASCII:
					case MODE_OUT_UPPER_FAKE:
					case MODE_OUT_UPPER_UTF8:
					{
						return input.toUpperCase ();
					}
					default:
					{
						return input;
					}
				}
			}
			//-----------------------------------------------------------------------------------
			case MODE_IN_UTF8:
			{
				switch (modeOut) {
					case MODE_OUT_LOWER_ASCII:
					{
						for (var x : String in m_aExtendMap) {
							if (m_aExtendMap[x].lowerUTF8 == input || m_aExtendMap[x].upperUTF8 == input) {
								return m_aExtendMap[x].lowerASCII;
							}
						}
						return input.toLowerCase();
					}
					case MODE_OUT_NORMAL_ASCII:
					{
						for (var x : String in m_aExtendMap) {
							if (m_aExtendMap[x].lowerUTF8 == input) {
								return m_aExtendMap[x].lowerASCII;
							} else if (m_aExtendMap[x].upperUTF8 == input) {
								return m_aExtendMap[x].upperASCII;
							}
						}
						return input;
					}
					case MODE_OUT_UPPER_ASCII:
					{
						for (var x : String in m_aExtendMap) {
							if (m_aExtendMap[x].lowerUTF8 == input || m_aExtendMap[x].upperUTF8 == input) {
								return m_aExtendMap[x].upperASCII;
							}
						}
						return input.toUpperCase();
					}
					case MODE_OUT_LOWER_FAKE:
					{
						for (var x : String in m_aExtendMap) {
							if (m_aExtendMap[x].lowerUTF8 == input || m_aExtendMap[x].upperUTF8 == input) {
								return m_aExtendMap[x].lowerFake;
							}
						}
						return input.toLowerCase();
					}
					case MODE_OUT_NORMAL_FAKE:
					{
						for (var x : String in m_aExtendMap) {
							if (m_aExtendMap[x].lowerUTF8 == input) {
								return m_aExtendMap[x].lowerFake;
							} else if (m_aExtendMap[x].upperUTF8 == input) {
								return m_aExtendMap[x].upperFake;								
							}
						}
						return input;
					}
					case MODE_OUT_UPPER_FAKE:
					{
						for (var x : String in m_aExtendMap) {
							if (m_aExtendMap[x].lowerUTF8 == input || m_aExtendMap[x].upperUTF8 == input) {
								return m_aExtendMap[x].upperFake;
							}
						}
						return input.toUpperCase();
					}
					case MODE_OUT_LOWER_UTF8:
					{
						for (var x : String in m_aExtendMap) {
							if (m_aExtendMap[x].lowerUTF8 == input || m_aExtendMap[x].upperUTF8 == input) {
								return m_aExtendMap[x].lowerUTF8;
							}
						}
						return input.toLowerCase ();
					}
					case MODE_OUT_UPPER_UTF8:
					{
						for (var x : String in m_aExtendMap) {
							if (m_aExtendMap[x].lowerUTF8 == input || m_aExtendMap[x].upperUTF8 == input) {
								return m_aExtendMap[x].upperUTF8;
							}
						}
						return input.toUpperCase ();
					}					
					case MODE_OUT_LOWER_VNI:
					{
						for (var x : String in m_aExtendMap) {
							if (m_aExtendMap[x].lowerUTF8 == input || m_aExtendMap[x].upperUTF8 == input) {
								return m_aExtendMap[x].lowerVNI;
							}
						}
						return input.toLowerCase();
					}
					case MODE_OUT_NORMAL_VNI:
					{
						for (var x : String in m_aExtendMap) {
							if (m_aExtendMap[x].lowerUTF8 == input) {
								return m_aExtendMap[x].lowerVNI;
							} else if (m_aExtendMap[x].upperUTF8 == input) {
								return m_aExtendMap[x].upperVNI;
							}
						}
						return input;
					}
					case MODE_OUT_UPPER_VNI:
					{
						for (var x : String in m_aExtendMap) {
							if (m_aExtendMap[x].lowerUTF8 == input || m_aExtendMap[x].upperUTF8 == input) {
								return m_aExtendMap[x].upperVNI;
							}
						}
						return input.toUpperCase();
					}
					case MODE_OUT_NORMAL_UTF8:
					default:
					{
						return input;
					}
				}
			}
			//-----------------------------------------------------------------------------------
			case MODE_IN_FAKE:
			{
				switch (modeOut) {
					case MODE_OUT_LOWER_ASCII:
					{
						for (var x : String in m_aExtendMap) {
							if (m_aExtendMap[x].lowerFake == input || m_aExtendMap[x].upperFake == input) {
								return m_aExtendMap[x].lowerASCII;
							}
						}
						return "";
					}
					case MODE_OUT_NORMAL_ASCII:
					{
						for (var x : String in m_aExtendMap) {
							if (m_aExtendMap[x].lowerFake == input) {
								return m_aExtendMap[x].lowerASCII;
							} else if (m_aExtendMap[x].upperFake == input) {
								return m_aExtendMap[x].upperASCII;
							}
						}
						return "";
					}
					case MODE_OUT_UPPER_ASCII:
					{
						for (var x : String in m_aExtendMap) {
							if (m_aExtendMap[x].lowerFake == input || m_aExtendMap[x].upperFake == input) {
								return m_aExtendMap[x].upperASCII;
							}
						}
						return "";
					}
					case MODE_OUT_LOWER_UTF8:
					{
						for (var x : String in m_aExtendMap) {
							if (m_aExtendMap[x].lowerFake == input || m_aExtendMap[x].upperFake == input) {
								return m_aExtendMap[x].lowerUTF8;
							}
						}
						return "";
					}
					case MODE_OUT_NORMAL_UTF8:
					{
						for (var x : String in m_aExtendMap) {
							if (m_aExtendMap[x].lowerFake == input) {
								return m_aExtendMap[x].lowerUTF8;
							} else if (m_aExtendMap[x].upperFake == input) {
								return m_aExtendMap[x].upperUTF8;
							}
						}
						return "";
					}
					case MODE_OUT_UPPER_UTF8:
					{
						for (var x : String in m_aExtendMap) {
							if (m_aExtendMap[x].lowerFake == input || m_aExtendMap[x].upperFake == input) {
								return m_aExtendMap[x].upperUTF8;
							}
						}
						return "";
					}
					case MODE_OUT_LOWER_FAKE:
					{
						for (var x : String in m_aExtendMap) {
							if (m_aExtendMap[x].lowerFake == input || m_aExtendMap[x].upperFake == input) {
								return m_aExtendMap[x].lowerFake;
							}
						}
						return "";
					}
					case MODE_OUT_UPPER_FAKE:
					{
						for (var x : String in m_aExtendMap) {
							if (m_aExtendMap[x].lowerFake == input || m_aExtendMap[x].upperFake == input) {
								return m_aExtendMap[x].upperFake
							}
						}
						return "";
					}					
					case MODE_OUT_LOWER_VNI:
					{
						for (var x : String in m_aExtendMap) {
							if (m_aExtendMap[x].lowerFake == input || m_aExtendMap[x].upperFake == input) {
								return m_aExtendMap[x].lowerVNI;
							}
						}
						return input.toLowerCase();
					}
					case MODE_OUT_NORMAL_VNI:
					{
						for (var x : String in m_aExtendMap) {
							if (m_aExtendMap[x].lowerFake == input) {
								return m_aExtendMap[x].lowerVNI;
							} else if (m_aExtendMap[x].upperFake == input) {
								return m_aExtendMap[x].upperVNI;
							}
						}
						return input;
					}
					case MODE_OUT_UPPER_VNI:
					{
						for (var x : String in m_aExtendMap) {
							if (m_aExtendMap[x].lowerFake == input || m_aExtendMap[x].upperFake == input) {
								return m_aExtendMap[x].upperVNI;
							}
						}
						return input.toUpperCase();
					}
					case MODE_OUT_NORMAL_FAKE:
					default:
					{
						return "";
					}
				}				
			}
			case MODE_IN_VNI:
			{
				switch (modeOut) {
					case MODE_OUT_LOWER_ASCII:
					{
						for (var x : String in m_aExtendMap) {
							if (m_aExtendMap[x].lowerVNI == input || m_aExtendMap[x].upperVNI == input) {
								return m_aExtendMap[x].lowerASCII;
							}
						}
						return input.toLowerCase();
					}
					case MODE_OUT_NORMAL_ASCII:
					{
						for (var x : String in m_aExtendMap) {
							if (m_aExtendMap[x].lowerVNI == input) {
								return m_aExtendMap[x].lowerASCII;
							} else if (m_aExtendMap[x].upperVNI == input) {
								return m_aExtendMap[x].upperASCII;
							}
						}
						return input;
					}
					case MODE_OUT_UPPER_ASCII:
					{
						for (var x : String in m_aExtendMap) {
							if (m_aExtendMap[x].lowerVNI == input || m_aExtendMap[x].upperVNI == input) {
								return m_aExtendMap[x].upperASCII;
							}
						}
						return input.toUpperCase();
					}
					case MODE_OUT_LOWER_FAKE:
					{
						for (var x : String in m_aExtendMap) {
							if (m_aExtendMap[x].lowerVNI == input || m_aExtendMap[x].upperVNI == input) {
								return m_aExtendMap[x].lowerFake;
							}
						}
						return input.toLowerCase();
					}
					case MODE_OUT_NORMAL_FAKE:
					{
						for (var x : String in m_aExtendMap) {
							if (m_aExtendMap[x].lowerVNI == input) {
								return m_aExtendMap[x].lowerFake;
							} else if (m_aExtendMap[x].upperVNI == input) {
								return m_aExtendMap[x].upperFake;								
							}
						}
						return input;
					}
					case MODE_OUT_UPPER_FAKE:
					{
						for (var x : String in m_aExtendMap) {
							if (m_aExtendMap[x].lowerVNI == input || m_aExtendMap[x].upperVNI == input) {
								return m_aExtendMap[x].upperFake;
							}
						}
						return input.toUpperCase();
					}
					case MODE_OUT_LOWER_UTF8:
					{
						for (var x : String in m_aExtendMap) {
							if (m_aExtendMap[x].lowerVNI == input || m_aExtendMap[x].upperVNI == input) {
								return m_aExtendMap[x].lowerUTF8;
							}
						}
						return input.toLowerCase ();
					}
					case MODE_OUT_NORMAL_UTF8:
					{
						for (var x : String in m_aExtendMap) {
							if (m_aExtendMap[x].lowerVNI == input) {
								return m_aExtendMap[x].lowerUTF8;
							} else if (m_aExtendMap[x].upperVNI == input) {
								return m_aExtendMap[x].upperUTF8;								
							}
						}
						return input;
					}
					case MODE_OUT_UPPER_UTF8:
					{
						for (var x : String in m_aExtendMap) {
							if (m_aExtendMap[x].lowerVNI == input || m_aExtendMap[x].upperVNI == input) {
								return m_aExtendMap[x].upperUTF8;
							}
						}
						return input.toUpperCase ();
					}										
					case MODE_OUT_LOWER_VNI:
					{
						for (var x : String in m_aExtendMap) {
							if (m_aExtendMap[x].lowerVNI == input || m_aExtendMap[x].upperVNI == input) {
								return m_aExtendMap[x].lowerVNI;
							}
						}
						return input.toLowerCase();
					}									
					case MODE_OUT_UPPER_VNI:
					{
						for (var x : String in m_aExtendMap) {
							if (m_aExtendMap[x].lowerVNI == input || m_aExtendMap[x].upperVNI == input) {
								return m_aExtendMap[x].upperVNI;
							}
						}
						return input.toUpperCase();
					}
					case MODE_OUT_NORMAL_VNI:
					default:
					{
						return input;
					}
				}
			}
		}
	}
	
	/**
	 * Hàm thay thế tất cả các chuỗi findString bằng replaceString trong chuỗi longString
	 * 
	 * @param	longString			Chuỗi mà hàm sẽ thực hiện tìm kiếm và thay thế
	 * @param	findString			Chuỗi tìm kiếm
	 * @param	replaceString		Chuỗi thay thế
	 * 
	 * @return		Chuỗi longString sau khi đã thay thế
	 */
	public static function replaceAll (longString : String, findString : String, replaceString : String) : String {		
		 var elements : Array = longString.split (findString);
		var output : String = "";
		
		var length : Number = elements.length-1;
		for (var i : Number = 0; i<length; ++i) {
			output += elements[i] + replaceString;
		}		
		
		return output + elements[length]; 
		/*
		var output : String = longString;
		var pos : Number = 0;
		var pre : String = "";
		var post : String = "";
		var length : Number = findString.length;
		pos = output.indexOf (findString, pos);
		while (pos != -1) {
			pre = output.substring (0, pos);
			post = output.substring (pos + length);			
			output = pre + replaceString + post;
			pos = output.indexOf (findString, pos);
		}
		return output;
		*/
	}
	
	
	/**
	 * Hàm so sánh 2 chuỗi UTF8
	 * 
	 * @param	string1	Chuỗi 1
	 * @param	string2	Chuỗi 2
	 * 
	 * @return		true nếu string1 giống hệt string 2, false nếu ngược lại
	 */
	public static function compareStrings (string1:String, string2:String) : Boolean {
		string1 = new String(string1);
		string2 = new String(string2);
		return (string1.valueOf() == string2.valueOf());
	}
	
	/**
	 * Lấy chuỗi HTML text để hiển thị với 1 màu tùy chỉnh
	 * 
	 * @param	text				xâu ký tự sẽ hiển thị
	 * @param	colorCode		mã màu (Hexa)
	 * 
	 * @return		HTML text
	 */
	public static function colorText (text : String, colorCode : String) : String {
		return "<font color='" + colorCode + "'>" + text + "</font>";
	}
	
	public static function formatMoney (moneyInString : String) : String {
		var isNegative : Boolean = false;
		var output : String = moneyInString;
		if (output.charAt(0) == "-") {
			isNegative = true;
			output = output.substr (1);
		}
		var temp : String = "";		
		var index : Number = output.indexOf (".");
		if (index == -1) {
			index = output.length;
		}
		while (index > 3) {			
			temp = output.substr (index - 3);			
			output = output.substr (0, index - 3) + "," + temp;
			index -= 3;			
		}
		if (isNegative) {
			output = "-" + output;
		}
		return output;
	}
	
	public static function unformatMoney (formattedMoneyInString : String) : Number {
		return parseInt(replaceAll (formattedMoneyInString, ",", ""));
	}
	
	public function toString () : String {
		return "[CCK - StringUtility "+_name+"]";
	}
}