﻿import gfx.core.UIComponent;
import gfx.managers.DragManager;

class gfx.controls.SQUAD.DragDropComponent extends UIComponent {
	public var allowDrop:Function;
	
	public function DragDropComponent() {
		super();
		allowDrop = alwaysAllow;
	}
	
	public function toString () : String {
		return "[CCK - DragDropComponent "+_name+"]";
	}
	
	private function configUI():Void {
		super.configUI();
		
		onMouseDown = handleMouseDown;
		onMouseUp = handleMouseUp;
				
		DragManager.instance.addEventListener("dragEnd", this, "handleDragEnd");
	}
	
	//=================DRAG=================
	
	private function handleMouseDown(evt:Object):Void {
		if(!_disabled && this.hitTest(_root._xmouse, _root._ymouse, true)) {
			onMouseMove = beginDrag;
			dispatchEvent( { type: "beginDrag", target: this } );			
		}
	}
	
	private function beginDrag():Void {		
		delete onMouseMove;
		dispatchEvent( { type: "drag", target: this } );		
	}
	
	private function handleMouseUp(evt:Object):Void {
		delete onMouseMove;
	}
	
	public function cancelDrag():Void {
		if (DragManager.instance.inDrag == true) {
			DragManager.instance.cancelDrag();
		} else {
			delete onMouseMove;
		}
	}
	
	//=================DROP=================
	
	private function handleDragEnd(event:Object):Void {				
		delete onMouseMove;
		if(!_disabled && allowDrop(event.data)) {
			if (this.hitTest(_root._xmouse, _root._ymouse, true) && !event.cancelled) {
				dispatchEvent( { type:"drop", target:this, data:event.data} );
			}
		}
	}
	
	private function alwaysAllow(data: Object):Boolean {
		return true;
	}	
}