﻿import gfx.controls.TextInput;
import gfx.ui.InputDetails;
import gfx.ui.NavigationCode;

class gfx.controls.SQUAD.TextInputWithClick extends TextInput {	
	public function handleInput(details:InputDetails, pathToFocus:Array):Boolean {

		if (Selection.getFocus() != textField) { return false; }
		switch(details.navEquivalent) {
			case NavigationCode.TAB:
				if (details.type != "key") { 
					break;
				}
			case NavigationCode.DOWN:
				if (details.type != "key") { 
					break;
				}
			case NavigationCode.UP:
				if (details.type != "key") { 
					break;
				}		
				return true;
			case NavigationCode.LEFT:
				if (details.type != "key") { 
					break;
				}
			case NavigationCode.RIGHT:
				if (details.type != "key") { 
					break;
				}		
				return true;
		}
		return false;
	}
}