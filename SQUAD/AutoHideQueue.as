import gfx.core.UIComponent;
import gfx.utils.Delegate;

/**
 * Ví dụ sử dụng class này: Hiển thị ai hạ được ai trong các game FPS. 
 * 		Đặc điểm là: 
 * 			Sau khi hiển thị thì tự biến mất sau một khoảng thời gian (Auto-hide)
 * 			Luôn cập nhật dữ liệu mới nhất = cách: Để dữ liệu mới nhất ở trên cùng, 
 * 				các dữ liệu cũ hơn bị đẩy xuống (Push direction = E_PD_UP)
 * 			Chỉ lưu trữ 1 số lượng dữ liệu nhất định (Queue Size) (thường = 3)
 * 
 * Ví dụ khác sử dụng class này: Hiển thị thông báo sự kiện trong game GunBound
 * 		Đặc điểm là:
 * 			Sau khi hiển thị thì tự biến mất sau một khoảng thời gian
 * 			Luôn cập nhật dữ liệu mới nhất = cách: Để dữ liệu mới nhất ở dưới cùng, 
 * 				các dữ liệu cũ hơn bị đẩy lên (Push direction = E_PD_DOWN)
 * 			Chỉ lưu trữ 1 số lượng dữ liệu nhất định (Queue Size) (thường = 6)
 * 
 * -> Class dùng để hiển thị 1 tập (số lượng = Queue Size) các component tự biến mất (Auto-hide) 
 * 			sau một khoảng thời gian và lập trình viên tự chọn hướng hiển thị dữ liệu mới nhất
 */
class gfx.controls.SQUAD.AutoHideQueue extends UIComponent {
	
	/**
	 * Library Identifier của component sẽ được dùng trong AutoHideQueue
	 * Thay đổi bằng getter và setter
	 * Chú ý: Thay đổi m_fRepresent trước khi thay đổi m_sItemIdentifier
	 * 
	 * Nếu không sử dụng Label như mặc định thì nên thiết lập các thuộc tính khác trước rồi cuối cùng mới thiết lập
	 * m_fRepresent và m_sItemIdentifier (xem ví dụ dưới cùng) để tối ưu!
	 */
	private var m_sItemIdentifier : String;
	
	/**
	 * Kích thước của Queue = Số bản ghi được lưu giữ trong AutoHideQueue
	 * Thay đổi bằng getter và setter
	 */
	private var m_iQueueSize :		Number;
	
	/**
	 * Khoảng cách giữa 2 component liên tiếp
	 * Thay đổi bằng getter và setter
	 */
	private var m_iMargin:			Number;
	
	/**
	 * MovieClip sẽ chứa các component được sinh ra
	 */
	private var m_mcContainer:	MovieClip;
	
	/**
	 * Hướng hiển thị dữ liệu mới nhất (chỉ được sử dụng các giá trị E_PD_*)
	 * Thay đổi bằng getter và setter
	 */
	private var m_ePushDirection:			String;
	public static var E_PD_UP:				String = "Up";		//Dữ liệu mới nhất ở trên cùng, đẩy các dữ liệu cũ hơn xuống dưới
	public static var E_PD_DOWN:			String = "Down";	//Dữ liệu mới nhất ở dưới cùng, đẩy các dữ liệu cũ hơn lên trên
	public static var E_PD_LEFT:				String = "Left";		//Dữ liệu mới nhất ở bên trái, đẩy các dữ liệu cũ hơn sang phải
	public static var E_PD_RIGHT:			String = "Right";	//Dữ liệu mới nhất ở bên phải, đẩy các dữ liệu cũ hơn sang trái
	
	/**
	 * Nếu thiết lập = true thì sẽ các dữ liệu tự động biến mất sau một khoảng thời gian
	 * Nếu thiết lập = false thì các dữ liệu sẽ không biến mất mà sẽ chỉ bị thay thế bởi dữ liệu mới hơn
	 */
	public var m_bAutoHide :		Boolean;
	
	/**
	 * Khoảng thời gian 1 dữ liệu sẽ hiển thị trước khi biến mất (tính bằng miligiây)
	 * Chú ý: Nếu m_bAutoHide = false thì m_iHideAfter không có tác dụng
	 */
	public var m_iHideAfter:			Number;//miliseconds
		
	/**
	 * Tập dữ liệu của AutoHideQueue.
	 * Lập trình viên lần lượt đưa dữ liệu vào bằng hàm enqueue
	 */
	private var m_aData:				Array;
	
	/**
	 * Tập  đánh dấu lượt biến mất nào của dữ liệu nào
	 */
	private var m_aHideCmds:		Array;
	
	/**
	 * Biến đếm lượt đánh dấu (phục vụ cho m_aHideCmds)
	 */
	private var m_iTurn:				Number;
	
	/**
	 * Hàm biến đổi dữ liệu đưa vào m_aData (bằng hàm enqueue) thành dạng trực quan
	 * Chẳng hạn đưa vào tên người thắng, phương tiện thắng, người thua vào làm dữ liệu.
	 * 		Hàm này sẽ 
	 * 			Điền tên người thắng vào Label bên trái
	 * 			Load ảnh phương tiện thắng vào UILoader ở giữa
	 * 			Điền tên người thua vào Label bên phải
	 * 
	 * Hàm này nhận 2 tham số:
	 * 		item			Component
	 * 		itemData	dữ liệu để thể hiện trong Component đó
	 * 
	 * Hàm không trả về (Void)
	 * 
	 * Có thể xem hàm mặc định defaultRepresent để hiểu rõ hơn
	 * 
	 * Chú ý: Phải xử lý cả 2 trường hợp có dữ liệu và không có dữ liệu
	 */
	public var m_fRepresent:		Function;
	
	/**
	 * Hàm tạo
	 * Thiết lập mặc định các thông số của AutoHide (có thể điều chỉnh lại sau)
	 * 		Component									Label
	 * 		Kích thước queue							3
	 * 		Tự động biến mất							Có
	 * 		Biến mất sau									2 giây
	 * 		Hướng cập nhật							E_PD_UP (trên cùng là dữ liệu mới nhất)
	 * 		Khoảng cách giữa các component: 	5px
	 * 		Hàm thể hiện dữ liệu						defaultRepresent
	 */
	public function AutoHideQueue () {
		super ();
		
		m_sItemIdentifier 	= "";
		m_iQueueSize 		= 3;
		m_bAutoHide 		= true;
		m_iHideAfter			= 2000;	//2 seconds
		m_ePushDirection 	= E_PD_UP;
		m_iMargin				= 5;
		m_fRepresent 		= defaultRepresent;
		
		m_aData 				= [];
		m_aHideCmds		= [];
		m_iTurn 				= 0;
		m_mcContainer		= createEmptyMovieClip ("container", getNextHighestDepth());		
		createItems ();
	}
	
	//---------------Item Identifier getter & setter
	public function get ItemIdentifier () : String {
		return m_sItemIdentifier;
	}
	public function set ItemIdentifier (ii : String ) : Void {
		if (m_sItemIdentifier != ii) {
			m_sItemIdentifier = ii;
			createItems ();
		}
	}
	
	//---------------Queue size getter & setter
	public function get QueueSize () : Number {
		return m_iQueueSize;
	}
	public function set QueueSize (qs : Number) : Void {
		if (m_iQueueSize != qs) {
			m_iQueueSize = qs;
			createItems ();
		}		
	}
	
	//---------------Margin getter & setter
	public function get Margin () : Number {
		return m_iMargin;
	}
	public function set Margin (m : Number ) : Void {
		if (m_iMargin != m) {
			m_iMargin = m;
			createItems ();
		}
	}
	
	//---------------Push direction getter & setter
	public function get PushDirection () : String {
		return m_ePushDirection;
	}
	public function set PushDirection (pd: String) : Void {
		if (pd == m_ePushDirection) {
			return;
		}
		switch (pd) {
			case E_PD_UP:
			case E_PD_DOWN:
			case E_PD_LEFT:
			case E_PD_RIGHT:
				m_ePushDirection = pd;
				createItems ();				
				break;
		}				
	}
	
	/**
	 * Nguyên lý hoạt động của AutoHideQueue (chưa có phần Auto-hide) là:
	 * 		Tạo ra 1 loạt các component ngay từ đầu
	 * 		Mỗi khi dữ liệu thay đổi thì cập nhật lại bằng hàm invalidate
	 * 		Dữ liệu đưa vào m_aData có 1 quy tắc là nếu chỉ số tính từ 0 trở lên thì dữ liệu 
	 * 			ứng với chỉ số lớn hơn là dữ liệu mới hơn
	 * 		Đối với bất kỳ một PushDirection nào thì mảng m_aData cũng không thay đổi 
	 * 			-> chỉ cần thay đổi thứ tự đặt các component ngay từ ban đầu là có thể thiết lập được hướng cập nhật dữ liệu
	 * 
	 * Hàm này dùng để tạo ngay 1 loạt các component ngay từ ban đầu
	 * Hàm sẽ được gọi lại khi các tham số làm thay đổi tính chất của component được thay đổi như:
	 * 		+ Component nào được dùng (ItemIdentifier)
	 * 		+ Khoảng cách giữa các component (Margin)
	 * 		+ Hướng cập nhật dữ liệu (PushDirection)
	 * 		+ Số lượng component tối đa (QueueSize)
	 */
	private function createItems () : Void {
		if (m_sItemIdentifier == "") {
			return;
		}
		for (var x : String in m_mcContainer) {			
			m_mcContainer[x].removeMovieClip ();			
		}
		switch (m_ePushDirection) {
			case E_PD_UP: {				
				for (var i : Number = 0 ; i < m_iQueueSize; ++i ) {
					var tempMC : MovieClip = m_mcContainer.attachMovie (
						m_sItemIdentifier, 
						nameOfItem (i), 
						m_mcContainer.getNextHighestDepth ()
					);
					tempMC._y = (tempMC._height + m_iMargin) * i;//top->bottom
				}
				break;
			}
			case E_PD_DOWN: {				
				for (var i : Number = 0 ; i < m_iQueueSize; ++i ) {
					var tempMC : MovieClip = m_mcContainer.attachMovie (
						m_sItemIdentifier, 
						nameOfItem (i), 
						m_mcContainer.getNextHighestDepth ()
					);
					tempMC._y = - tempMC._height - (tempMC._height + m_iMargin) * i;//bottom->top
				}
				break;
			}
			case E_PD_LEFT: {
				for (var i : Number = 0 ; i < m_iQueueSize; ++i ) {
					var tempMC : MovieClip = m_mcContainer.attachMovie (
						m_sItemIdentifier, 
						nameOfItem (i), 
						m_mcContainer.getNextHighestDepth ()
					);
					tempMC._x = (tempMC._width + m_iMargin) * i;//left->right
				}
				break;
			}
			case E_PD_RIGHT: {
				for (var i : Number = 0 ; i < m_iQueueSize; ++i ) {
					var tempMC : MovieClip = m_mcContainer.attachMovie (
						m_sItemIdentifier, 
						nameOfItem (i), 
						m_mcContainer.getNextHighestDepth ()
					);
					tempMC._x = -tempMC._width - (tempMC._width + m_iMargin) * i;//right->left
				}
				break;
			}
		}
		invalidate ();
	}
	
	/**
	 * Hàm đưa thêm dữ liệu vào AutoHideQueue
	 * Nếu còn chỗ trống (do Queue bị giới hạn bởi số lượng bản ghi đưa vào) thì 
	 * 		dữ liệu sẽ được đưa vào cuối m_aData (mới nhất)
	 * Nếu không còn chỗ trống thì dữ liệu cũ nhất sẽ bị đẩy ra khỏi Queue
	 * 
	 * @param	item	dữ liệu đưa vào
	 */
	public function enqueue (item : Object) : Void {		
		//trace ("Enqueue " + m_iTurn);
		//Push data into array
		m_aData[m_aData.length] = item;
		if (m_bAutoHide) {
			//Set auto-hide command		
			_global.setTimeout (Delegate.create(this, dequeue), m_iHideAfter);
			//Ensure that there's a handler for this item even if it doesn't exist anymore
			m_aHideCmds[m_aHideCmds.length] = {
				turn:		m_iTurn,	//use to identify packet when dequeue is called
				hide:		true			//default: true = auto hide after a period of time, but when this item is popped out of stack -> no need to hide it anymore -> set to false
			};		
		}
		if (m_aData.length == m_iQueueSize + 1) {
			if (m_bAutoHide) {
				var i : Number = 0;
				while (i < m_aHideCmds.length && m_aHideCmds[i].hide == false) {
					++i;
				}			
				if (i < m_aHideCmds.length) {
					m_aHideCmds[i].hide = false;
				}
			}
			m_aData.splice (0, 1);
		}
		
		//trace (m_aHideCmds);		
		++m_iTurn;
		invalidate ();
	}
	
	/**
	 * Hàm cập nhật lại dữ liệu
	 * Đặt modifier là public nên Lập trình viên có thể gọi khi dữ liệu đã đưa vào trong m_aData lại thay đổi
	 * 
	 * Thực chất các component đã được dựng sẵn, hàm invalidate này chỉ gọi hàm m_fRepresent để đưa dữ liệu vào component
	 */
	public function invalidate () : Void {
		var n : Number = m_aData.length;
		for (var i : Number = 0; i < m_iQueueSize; ++i ) {
			var tempMC : MovieClip = m_mcContainer[nameOfItem(i)];
			if (i < n) {								
				m_fRepresent (tempMC, m_aData[n-1-i]);
			} else {				
				m_fRepresent (tempMC, undefined);
			}
		}		
	}		
	
	/**
	 * Hàm đặt tên cho các component được sinh ra
	 * Sở dĩ phải có một hàm đặt tên vì hàm createItems khi sinh ra các component sẽ phải đặt tên để phân biệt
	 * 		Nhưng hàm invalidate lại cũng phải sử dụng tên của các component để điền dữ liệu vào đúng component
	 * 		-> Cần nhất quán cách đặt tên component -> tách ra thành 1 hàm nameOfItem mà tất cả cùng dùng chung
	 * 
	 * @param	index	Chỉ số thứ tự của component
	 * 
	 * @return		Tên của component với chỉ số tương ứng
	 */
	private function nameOfItem (index : Number) : String {
		return _name + "_" + m_sItemIdentifier + "_" + index;
	}
	
	/**
	 * Hàm biểu diễn dữ liệu mặc định
	 * 
	 * @param	item			Component sẽ thể hiện dữ liệu
	 * @param	itemData	Dữ liệu được đưa vào component
	 * 
	 * Chú ý: Cần xử lý cả trường hợp có và không có dữ liệu ( = undefined)
	 */
	private function defaultRepresent (item: MovieClip, itemData: Object) : Void {
		if (itemData != undefined) {						
			item.text = itemData;						
		} else {
			item.text = "";			
		}
	}
	
	/**
	 * Hàm đẩy dữ liệu ra khỏi AutoHideQueue (do đã tới thời điểm component cần bị ẩn đi hoặc có thêm dữ liệu mới
	 * 		mà Queue không còn chỗ trống nữa)
	 * 
	 * Tuy đặt modifier là public nhưng lập trình viên không được gọi hàm này!!! 
	 * Đặt public vì Delegate sẽ gọi hàm dequeue từ bên ngoài class (do hàm setTimeout chỉ được dùng với _global)
	 * -> Class tự chịu trách nhiệm gọi hàm này -> lập trình viên không được dùng hàm này!
	 */
	public function dequeue () : Void {					
		var hideCmd : Object = m_aHideCmds[0];
		//trace ("Dequeue " + hideCmd.turn);
		if (hideCmd.hide == true) {			
			m_aData.splice (0, 1);
			
			invalidate ();			
		}
		m_aHideCmds.splice(0, 1);
		if (m_aHideCmds.length == 0) {
			m_iTurn = 0;
		}
	}
	
	public function toString () : String {
		return "[CCK - AutoHideQueue "+_name+"]";
	}
}

/*
//Ví dụ đơn giản sử dụng AutoHideQueue
//Đặt AutoHideQueue vào Stage và đặt tên là "ahqu"
//Đặt Button vào Stage và đặt tên là "bu"
//Đặt đoạn code vào frame và chạy
//Click vào nút bấm để thêm dữ liệu vào AutoHideQueue (tự biến mất sau 2 giây, sử dụng Label làm component, tối đa 3 bản ghi dữ liệu)

import gfx.controls.SQUAD.AutoHideQueue;
var indexU : Number = 1;
bu.addEventListener ("click", this, "addU");
function addU () : Void {
	ahqu.enqueue ("item "+indexU);
	indexU++;
}
ahq.ItemIdentifier = "Label";
*/

/*
//Ví dụ phức tạp hơn sử dụng AutoHideQueue
//Chuẩn bị 1 component đặt tên là User bên trong có Label tên "labUser" và UILoader tên "uiAvatar"
//Chuẩn bị sẵn 1 loạt các ảnh đặt tên là 1.png, 2.png, 3.png
//Đặt AutoHideQueue vào Stage và đặt tên là "ahq"
//Đặt Button vào Stage và đặt tên là "b"
//Đặt đoạn code vào frame và chạy
//Click vào nút bấm để thêm dữ liệu vào AutoHideQueue (tự biến mất sau 2 giây, sử dụng Label làm component, tối đa 6 bản ghi dữ liệu)

import gfx.controls.SQUAD.AutoHideQueue;
import gfx.controls.Label;
import gfx.controls.UILoader;
var index : Number = 1;
b.addEventListener ("click", this, "add");
function add () : Void {
	ahq.enqueue ({
		name:		"User #" + index, 
		avatar:		(random(3)+1) + ".png"
	});
	index++;
}ahq.Margin			= 3;
ahq.PushDirection	= AutoHideQueue.E_PD_DOWN;
ahq.QueueSize		= 6;
ahq.m_fRepresent	= function (item: MovieClip, itemData: Object) : Void {
	if (itemData == undefined) {
		item._visible = false;
	} else {
		if (item.labUser instanceof Label) {
			item.labUser.text = itemData.name;
		}
		if (item.uiAvatar instanceof UILoader) {
			item.uiAvatar.source = itemData.avatar;
		}
		item._visible = true;
	}
}
ahq.ItemIdentifier = "User";
*/