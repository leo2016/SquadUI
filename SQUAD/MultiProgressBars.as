﻿import gfx.controls.SQUAD.CPPSimulator;
import gfx.controls.SQUAD.MoviePlayer;
import gfx.core.UIComponent;

/**
 * Class hiển thị quá trình tăng giảm của thanh progress bar.
 * 
 * Ví dụ sử dụng:
 * 		- Khi lắp thêm phụ kiện vào súng, một vài chỉ số thay đổi, ta cần biết những chỉ số nào thay đổi và thay đổi như thế nào (tăng/giảm) (game FPS)
 * 		- Khi hạ được một đối thủ, thanh điểm kinh nghiệm đầy dần lên, ta cần biết là thanh này sẽ chạy tới điểm nào thì dừng (đã được bao nhiêu kinh nghiệm) (game RPG)
 * 		- Khi bị một đối thủ đánh, ta cần biết là đòn đánh của đối thủ đã làm ta mất bao nhiêu điểm HP (game Fighting)
 * 
 * Thể hiện:
 * 		Khi thể hiện sự tăng lên, thanh màu "xanh" thể hiện mức độ hiện tại, thanh màu "vàng" tăng dần lên tới mức mới
 * 			-> phần màu "xanh" che mất màu "vàng" và người dùng chỉ nhìn thấy phần màu "vàng" còn lại tăng dần lên
 * 			-> phần màu "vàng" thể hiện mức độ tăng lên
 * 		Khi thể hiện sự giảm xuống, thanh màu "đỏ" thể hiện mức độ hiện tại, thanh màu xanh giảm đi tới mức mới
 * 			-> phần màu "xanh" che mất màu "đỏ" và người dùng chỉ nhìn thấy phần "đỏ" được mở rộng dần dần do màu "xanh" hẹp dần
 * 			-> phần màu "đỏ" thể hiện mức độ giảm đi
 * 		Gọi là "xanh", "đỏ", "vàng" nhưng không nhất thiết các thanh này phải có màu như vậy. 
 * 		Chỉ cần đọc kỹ phần "Thể hiện" và hiểu ý nghĩa của mỗi thanh thì có thể tự tùy chỉnh skin cho các thanh progress bar.
 */
class gfx.controls.SQUAD.MultiProgressBars extends UIComponent {
	[Inspectable (name="RedPgbIdentifier", type="String", defaultValue="PgbRed")]
	public var m_sIdentifierRed :	String;
	[Inspectable (name="GreenPgbIdentifier", type="String", defaultValue="PgbGreen")]
	public var m_sIdentifierGreen :	String;
	[Inspectable (name="YellowPgbIdentifier", type="String", defaultValue="PgbYellow")]
	public var m_sIdentifierYellow :	String;
	
	private var m_kMP_Green :		MoviePlayer;		//Movie Player of green progress bar
	private var m_kMP_Red :		MoviePlayer;		//Movie Player of red progress bar
	private var m_kMP_Yellow :		MoviePlayer;		//Movie Player of yellow progress bar
	
	private var m_pgbGreen : 		MovieClip;			//green progress bar (top)
	private var m_pgbRed : 			MovieClip;			//red progress bar (middle)
	private var m_pgbYellow : 		MovieClip;			//yellow progress bar (bottom)
	
	public var m_iCurrent :			Number;			//current 
	public var m_iMax :				Number;			//max
	public var m_iPercentage :		Number;			//current percentage
	public var m_iPreviewPercent:	Number;			//current preview percentage
	
	[Inspectable (name="RepeatPreview", type="Boolean", defaultValue=false)]
	public var m_bRepeatPreview:	Boolean;	
	
	/**
	 * Hàm tạo	 
	 */
	public function MultiProgressBars () {
		super();
		
		m_iCurrent = 0;
		m_iMax = 0;
		m_iPercentage = 0;
		m_iPreviewPercent = 0;		
		m_bRepeatPreview = false;
	}
	
	/*
	 * Hàm khởi tạo (phải được gọi trước khi sử dụng set, animate....)	 
	 * Đưa vào 3 identifier của 3 thanh ProgressBar.
	 * Chú ý: 
	 * 		Component này (MultiProgressBars) nên có background và 3 thanh progress bar nên không có background
	 * 		hoặc
	 * 		chỉ có thanh màu vàng mới có background
	 * Vì 3 thanh progress bar này sẽ nằm chồng lên nhau -> nếu thanh ở trên có background sẽ che mất thanh ở dưới
	 * 
	 * Khi hàm này thực hiện hết các lệnh khởi tạo, sẽ có sự kiện "ready".
	 */
	private function configUI () : Void {
		super.configUI ();
		
		//Must follow this order
		m_pgbYellow = this.attachMovie (m_sIdentifierYellow, "pgbYellow", this.getNextHighestDepth());		//yellow is in the bottom layer		
		m_pgbRed = this.attachMovie (m_sIdentifierRed, "pgbRed", this.getNextHighestDepth());				//red is in the middle layer
		m_pgbGreen = this.attachMovie (m_sIdentifierGreen, "pgbGreen", this.getNextHighestDepth());		//green is in the top layer
		
		m_kMP_Green = new MoviePlayer (m_pgbGreen);
		m_kMP_Red = new MoviePlayer (m_pgbRed);
		m_kMP_Yellow = new MoviePlayer (m_pgbYellow);
		
		m_kMP_Green.stopReceiveEvent ();
		m_kMP_Yellow.stopReceiveEvent ();
		m_kMP_Red.stopReceiveEvent ();		
		
		m_kMP_Yellow.addEventListener ("onFinish", this, "continueAnimatingDown");
		m_kMP_Green.addEventListener ("onFinish", this, "continueAnimatingUp");		
		
		//m_kMP_Green.addEventListener ("onFinish", this, "invalidate");
		
		dispatchEvent ( {type:	"ready", target: this});
	}
	
	/**
	 * Hàm chuyển đổi tỉ lệ phần trăm ra số thứ tự của frame tương ứng. (Chỉ dùng trong class)
	 * 
	 * Ví dụ sử dụng:
	 * 		ProgressBar "pgb" có 500 frame, muốn tìm frame thể hiện tỉ lệ 70%
	 * 		gọi hàm
	 * 		percentageToFrame (pgb, 70)
	 * 		= 350 -> frame 350 là frame cần tìm
	 * 
	 * @param	mc				MovieClip chứa các frame
	 * @param	percentage	Tỉ lệ
	 * 
	 * @return		Số thứ tự của frame tương ứng với tỉ lệ
	 */
	private function percentageToFrame (mc : MovieClip, percentage : Number) : Number {		
		return Math.floor(mc._totalframes * percentage / 100);
	}
	
	/**
	 * Hàm thiết lập ngay thể hiện "preview" của các thanh progress bar mà không có animation
	 * 
	 * @param	next	Mức xem trước
	 * @param	max	Mức tối đa	 	
	 */
	public function setPreview (next : Number, max : Number ) : Void {
		stopAll ();
		if (max <= 0) {
			return;
		}
		if (next < 0) {
			next = 0;
		} else if (next > max) {
			next = max;
		}
		var percentage : Number = Math.floor (next / max * 100);
		m_iPreviewPercent = percentage;
		if (percentage < m_iPercentage) {
			setPercentage (m_pgbGreen, percentage);
			setPercentage (m_pgbRed, m_iPercentage);
		} else if (percentage > m_iPercentage) {
			setPercentage (m_pgbGreen, m_iPercentage);
			setPercentage (m_pgbYellow, percentage);
		}
	}
	
	/**
	 * Hàm thể hiện "preview" của các thanh progress bar bằng animation
	 * 
	 * @param	next				Mức xem trước
	 * @param	max				Mức tối đa	 
	 * @param 	fromCurrent	Có preview từ mức preview hiện tại hay không (nếu không, preview từ mức được chọn hiện tại)
	 */
	public function animatePreview (next : Number, max : Number, fromCurrent : Boolean ) : Void {
		if (!fromCurrent) {
			stopAll ();
		}
		if (max <= 0) {
			return;
		}
		if (next < 0) {
			next = 0;
		} else if (next > max) {
			next = max;
		}
		var percentage : Number = Math.floor (next / max * 100);
		if (m_iPreviewPercent == percentage) {
			return;
		}
		if (fromCurrent) {
			CPPSimulator.trace ("animate " + this +" from current = " + m_iPreviewPercent + " -> " + percentage + ", while exact current percentage = " + m_iPercentage, 10);
			if (percentage < m_iPreviewPercent) {			
				animatePreviewDownFromCurrent (percentage);				
			} else if (percentage > m_iPreviewPercent) {			
				animatePreviewUpFromCurrent (percentage);
			}
		} else {
			if (percentage < m_iPercentage) {			
				animatePreviewDown (m_iPercentage, percentage);			
			} else if (percentage > m_iPercentage) {			
				animatePreviewUp (m_iPercentage, percentage);
			}
		}
		m_iPreviewPercent = percentage;
	}
	
	/**
	 * Hàm thể hiện sự giảm đi khi "preview" (Chỉ dùng trong class)
	 * 
	 * @param	fromPercentage	Mức hiện tại
	 * @param	toPercentage	 	Mức sau khi giảm
	 */
	private function animatePreviewDown (fromPercentage : Number, toPercentage : Number) : Void {
		hideYellow ();
		setPercentage (m_pgbRed, fromPercentage);
		setPercentage (m_pgbGreen, fromPercentage);
		m_kMP_Green.stopReceiveEvent ();
		m_kMP_Green.play (
			percentageToFrame (m_pgbGreen, fromPercentage), 
			percentageToFrame (m_pgbGreen, toPercentage), 
			m_bRepeatPreview
		);
	}
	
	/**
	 * Hàm thể hiện sự tăng lên khi "preview" (Chỉ dùng trong class)
	 * 
	 * @param	fromPercentage	Mức hiện tại
	 * @param	toPercentage	 	Mức sau khi tăng
	 */
	private function animatePreviewUp (fromPercentage : Number, toPercentage : Number) : Void {		
		hideRed ();
		setPercentage (m_pgbYellow, fromPercentage);
		setPercentage (m_pgbGreen, fromPercentage);		
		m_kMP_Yellow.play (
			percentageToFrame (m_pgbYellow, fromPercentage), 
			percentageToFrame (m_pgbYellow, toPercentage), 
			m_bRepeatPreview
		);
	}
	
	/**
	 * Hàm thể hiện sự giảm đi khi "preview" kể từ preview hiện tại (Chỉ dùng trong class)
	 * 	 
	 * @param	toPercentage	 	Mức sau khi giảm
	 */
	private function animatePreviewDownFromCurrent (toPercentage : Number) : Void {		
		if (m_iPreviewPercent <= m_iPercentage) {//red
			CPPSimulator.trace ("red = " + m_iPercentage + ", green = " + m_iPreviewPercent + "->" + toPercentage, 10);
			//red -> red
			hideYellow ();
			setPercentage (m_pgbRed, m_iPercentage);
			setPercentage (m_pgbGreen, m_iPreviewPercent);
			m_kMP_Green.play (
				percentageToFrame (m_pgbGreen, m_iPreviewPercent), 
				percentageToFrame (m_pgbGreen, toPercentage), 
				m_bRepeatPreview
			);
		} else {//yellow			
			hideRed ();
			setPercentage (m_pgbGreen, m_iPercentage);
			setPercentage (m_pgbYellow, m_iPreviewPercent);
			if (toPercentage < m_iPercentage) {//yellow -> red				
				CPPSimulator.trace ("green = " + m_iPercentage + ", yellow = " + m_iPreviewPercent + "->" + m_iPercentage + ", waiting....", 10);
				m_kMP_Yellow.startReceiveEvent ();
				m_kMP_Yellow.play (
					percentageToFrame (m_pgbYellow, m_iPreviewPercent), 
					percentageToFrame (m_pgbYellow, m_iPercentage), 
					m_bRepeatPreview
				);
			} else {//yellow -> yellow				
				CPPSimulator.trace ("green = " + m_iPercentage + ", yellow = " + m_iPreviewPercent + "->" + toPercentage, 10);
				m_kMP_Yellow.play (
					percentageToFrame (m_pgbYellow, m_iPreviewPercent), 
					percentageToFrame (m_pgbYellow, toPercentage), 
					m_bRepeatPreview
				);			
			}
		}		
	}
	
	/**
	 * Hàm thể hiện sự tăng lên khi "preview" tính từ preview hiện tại (Chỉ dùng trong class)
	 * 	 
	 * @param	toPercentage	 	Mức sau khi tăng
	 */
	private function animatePreviewUpFromCurrent (toPercentage : Number) : Void {		
		if (m_iPreviewPercent >= m_iPercentage) {//yellow
			CPPSimulator.trace ("green = " + m_iPercentage + ", yellow = " + m_iPreviewPercent + "->" + toPercentage, 10);
			//yellow -> yellow
			hideRed ();
			setPercentage (m_pgbGreen, m_iPercentage);
			setPercentage (m_pgbYellow, m_iPreviewPercent);
			m_kMP_Yellow.play (
				percentageToFrame (m_pgbYellow, m_iPreviewPercent), 
				percentageToFrame (m_pgbYellow, toPercentage), 
				m_bRepeatPreview
			);			
		} else {//red
			hideYellow ();	
			setPercentage (m_pgbRed, m_iPercentage);
			setPercentage (m_pgbGreen, m_iPreviewPercent);
			if (toPercentage > m_iPercentage) {//red -> yellow
				CPPSimulator.trace ("red = " + m_iPercentage + ", green = " + m_iPreviewPercent + "->" + m_iPercentage + ", waiting....", 10);
				setPercentage (m_pgbGreen, m_iPreviewPercent);
				m_kMP_Green.startReceiveEvent ();
				m_kMP_Green.play (
					percentageToFrame (m_pgbGreen, m_iPreviewPercent), 
					percentageToFrame (m_pgbGreen, m_iPercentage), 
					m_bRepeatPreview
				);
			} else {//red -> red		
				CPPSimulator.trace ("red = " + m_iPercentage + ", green = " + m_iPreviewPercent + "->" + toPercentage, 10);				
				m_kMP_Green.play (
					percentageToFrame (m_pgbGreen, m_iPreviewPercent), 
					percentageToFrame (m_pgbGreen, toPercentage), 
					m_bRepeatPreview
				);		
			}
		}
	}
	
	/**
	 * Hàm thiết lập ngay thể hiện của các thanh progress bar mà không có animation
	 * 
	 * @param	current	Mức hiện tại
	 * @param	max		Mức tối đa	 
	 */
	public function setProgress (current : Number, max : Number) : Void {
		stopAll ();
		if (max <= 0) {
			return;
		}
		if (current < 0) {
			current = 0;
		} else if (current > max) {
			current = max;
		}
		
		m_iCurrent = current;
		m_iMax = max;
		m_iPercentage = Math.floor (current / max * 100);
		m_iPreviewPercent = m_iPercentage;
				
		hideYellow();
		hideRed();
		setPercentage (m_pgbGreen, m_iPercentage);
	}
	
	/**
	 * Hàm thể hiện các thanh progress bar bằng animation
	 * 
	 * @param	current	Mức xem trước
	 * @param	max	Mức tối đa	 
	 */
	public function animateProgress (current : Number, max : Number ) : Void {		
		stopAll ();
		if (max <= 0) {
			return;
		}
		if (current < 0) {
			current = 0;
		} else if (current > max) {
			current = max;
		}
		var percentage : Number = Math.floor (current / max * 100);		
		if (percentage < m_iPercentage) {			
			animateDown (m_iPercentage, percentage);			
		} else if (percentage > m_iPercentage) {			
			animateUp (m_iPercentage, percentage);
		}
		
		m_iCurrent = current;
		m_iMax = max;
		m_iPercentage = percentage;
		m_iPreviewPercent = m_iPercentage;
	}
	
	/**
	 * Hàm thể hiện sự giảm đi (Chỉ dùng trong class)
	 * 
	 * @param	fromPercentage	Mức hiện tại
	 * @param	toPercentage	 	Mức sau khi giảm
	 */
	private function animateDown (fromPercentage : Number, toPercentage : Number) : Void {
		hideYellow();		
		setPercentage (m_pgbRed, fromPercentage);
		setPercentage (m_pgbGreen, fromPercentage);
		m_kMP_Green.startReceiveEvent ();
		m_kMP_Green.play (
			percentageToFrame (m_pgbGreen, fromPercentage), 
			percentageToFrame (m_pgbGreen, toPercentage)			
		);
	}
	
	/**
	 * Hàm thể hiện sự tăng lên (Chỉ dùng trong class)
	 * 
	 * @param	fromPercentage	Mức hiện tại
	 * @param	toPercentage	 	Mức sau khi tăng
	 */
	private function animateUp (fromPercentage : Number, toPercentage : Number) : Void {		
		hideRed();
		setPercentage (m_pgbYellow, toPercentage);	
		setPercentage (m_pgbGreen, fromPercentage);
		m_kMP_Green.startReceiveEvent ();
		m_kMP_Green.play (
			percentageToFrame (m_pgbGreen, fromPercentage), 
			percentageToFrame (m_pgbGreen, toPercentage)
		);		
	}
	
	/**
	 * Hàm thể hiện các thanh progress bar theo dữ liệu lưu bên trong class này
	 */
	public function invalidate () : Void {
		if (m_kMP_Green instanceof MoviePlayer) {
			if (m_kMP_Red instanceof MoviePlayer) {
				if (m_kMP_Yellow instanceof MoviePlayer) {
					setProgress (m_iCurrent, m_iMax);
				}
			}
		}		
	}
		
	/**
	 * Giấu thanh màu đỏ đi
	 */
	private function hideRed () : Void {
		setPercentage (m_pgbRed, 0);
	}
	
	/**
	 * Giấu thanh màu xanh đi
	 */
	private function hideGreen () : Void {
		setPercentage (m_pgbGreen, 0);
	}
	
	/**
	 * Giấu thanh màu vàng đi
	 */
	private function hideYellow () : Void {
		setPercentage (m_pgbYellow, 0);
	}
	
	private function setPercentage (pgb : MovieClip, percentage : Number) : Void {		
		pgb.setProgress (percentageToFrame(pgb, percentage), pgb._totalframes);
		pgb.invalidate ();
	}
	
	/**
	 * Dừng tất cả các quá trình "play" lại (dùng khi có task mới chen vào khi task cũ chưa hoàn thành)
	 */
	private function stopAll () : Void {
		var a : Array = [m_kMP_Green, m_kMP_Red, m_kMP_Yellow];
		for (var x : String in a) {
			a[x].stopReceiveEvent ();
			a[x].finish ();
		}
	}
	
	private function continueAnimatingDown () : Void {		
		m_kMP_Yellow.stopReceiveEvent ();
		hideYellow ();
		setPercentage (m_pgbRed, m_iPercentage);
		setPercentage (m_pgbGreen, m_iPercentage);
		m_kMP_Green.play (
			percentageToFrame (m_pgbGreen, m_iPercentage), 
			percentageToFrame (m_pgbGreen, m_iPreviewPercent), 
			m_bRepeatPreview
		);
	}
	
	private function continueAnimatingUp () : Void {
		m_kMP_Green.stopReceiveEvent ();
		hideRed ();
		setPercentage (m_pgbGreen, m_iPercentage);
		setPercentage (m_pgbYellow, m_iPercentage);
		m_kMP_Yellow.play (
			percentageToFrame (m_pgbYellow, m_iPercentage), 
			percentageToFrame (m_pgbYellow, m_iPreviewPercent), 
			m_bRepeatPreview
		);
	}
	
	public function get pgbRed () : MovieClip {
		return m_pgbRed;
	}
	
	public function get pgbGreen () : MovieClip {
		return m_pgbGreen;
	}
	
	public function get pgbYellow () : MovieClip {
		return m_pgbYellow;
	}
	
	public function toString () : String {
		return "[CCK - MultiProgressBars " + _name + "]";
	}
}